#include "qi.h"
#include "mqi_log.h"
#include <string.h>
#include <cmm/mempool/mempool.h>

/* Option management and syntax inspired from GNU Make
 * http://www.gnu.org/software/make/
 *
 * IMPORTANT:
 * If you add, remove or modify an option, you must alter:
 * 	- The "usage" string
 * 	- The "switches" structure declaration (*not* command_switch)
 * Additionally, you may add, remove or modify the appropriate
 * variables holding the value of the argument.
 * You may also add, modify or remove the default values.
 * To finish, you may need to free extra memory
 * in the "die" function.
 *
 */

/* Default values for the optional arguments.
 * To avoid confusions, use the same names as the
 * long option names but in capital letters and
 * with an underscore instead of a hyphen.
 */
#define DEFAULT_MAXFACTOR		( 10000	)
#define DEFAULT_OPTIMIZE		( 0	)
#define DEFAULT_PROJECTIVE		( 0	)
#define DEFAULT_OMIT_IMAGINARY		( 0	)
#define DEFAULT_INPUT_EUCLIDEAN		( 0	)
#define DEFAULT_VERBOSITY		( 3	)
#define DEFAULT_BACKEND			( "plain" )

/* Variables holding the value of the optional arguments,
 * assigned with their default values.
 * As for the default values, use the same name
 * as the long options.
 */
static	long	opt_maxfactor		=	DEFAULT_MAXFACTOR;
static	int	opt_optimize		=	DEFAULT_OPTIMIZE;
static	int	opt_projective		=	DEFAULT_PROJECTIVE;
static	int	opt_omit_imaginary	=	DEFAULT_OMIT_IMAGINARY;
static	int	opt_input_euclidean	=	DEFAULT_INPUT_EUCLIDEAN;
static	int	opt_verbosity		=	DEFAULT_VERBOSITY;
static  char	*opt_backend		=	DEFAULT_BACKEND;

/* Variables holding the input quadrics.
 * Note that the maximum size is only limited by the maximum size of
 * a command line argument.
 */
static	char	*param_q1		=	(char *)0;
static	char	*param_q2		=	(char *)0;


static const char * const	splash =
"\n"
"%s (c) 2008 By Julien CLEMENT : clementj2005@yahoo.fr\n"
"Part of the Quadric Intersection project\n"
"http://www.loria.fr/equipes/vegas/qi/index.html\n"
"This program usage is ruled by the license specified in the LICENSE\n"
"file, at the root level of the QI package.\n"
"\n"
;

static const char * const	usage =

"Parameters:\n"
"\n"

"Input parameters (mandatory)\n"
"\n"
"Notes:\n"
"1. The input quadrics can be expressed in one of the following forms:\n"
"\033[10G- Affine equation\033[40Gexample: \"x^2-y^2+5\"\n"
"\033[10G- Projective equation\033[40Gexample: \"x^2-y^2+5*w^2\"\n"
"\033[10G- Vector of coefficients\033[40Gexample: \"[1 0 0 0 -1 0 0 0 0 1]\"\n"
"\033[10G  The coefficients have the following layout:\n"
"\033[10G  [x^2 x*y x*z x*w y^2 y*z y*w z^2 z*w w^2]\n"
"2. You must wrap the equation strings by quotes (single or double), and it can\n"
"   contain spaces, example: \"x ^  2 - y ^ 2 + 5\".\n"
"   The vectorial form must contain *at least* a *single* space between each\n"
"   coefficient.\n"
"\n"

"--q1=EQUATION_STRING\033[30GFirst quadric to intersect.\n"
"--q2=EQUATION_STRING\033[30GFirst quadric to intersect.\n"
"\n"

"Kernel parameters (optional)\n"
"-m [N], --maxfactor=N\033[30GUpper bound for the search of prime factors (coded as a C signed long).\n"
"-O, --optimize\033[30GAttempts to reduce the square roots in the output parametrizations.\n"
"\n"

"Output parameters (optional)\n"
"-p, --projective\033[30GOutput all the equations in the projective space,\n"
"\033[30Gincluding the input equations.\n"
"-i, --omit-imaginary\033[30GDon't output the intersection components not\n"
"\033[30Gbelonging in the real affine space (R^3)\n"
"-e, --input-euclidean\033[30GOutput the euclidean type of the input quadrics\n"
"-V [N], --verbosity=N\033[30GChange the verbosity level of the output information:\n"
	"\033[35G0 : print only the parametrization of the intersection\n"
	"\033[35G1 : 0 plus print the type of the intersection\n"
	"\033[35G2 : 1 plus add some description labels, exemple: \"Type in complex projective space\"\n"
	"\033[35G3 : 2 plus output the input quadrics and a detailed explanation of the result format.\n"
"\n"
"-o [backend], --output=backend\033[35GSelect the backend to use, amongst: \"plain\", \"html\", \"latex\"\n"
"\n"

"Misc parameters\n"
"-h, --help\033[30GOutput this help message and quit.\n"
"-v, --version\033[30GOutput the current version of Quadric Intersection (QI)\n"
"\n"

"Default parameters\n"
"Only the input quadrics are mandatory. Any other parameter is optional and if not specified\n"
"take the following default values:\n"
;


/* A simplified version from GNU Make */
struct command_switch
 {
	int c;

	enum
	{
		positive_int,	/* A positive integer */
		positive_long,	/* A positive long integer */
		cstring		/* One string per switch.
				   Note that we replaced "string" by "cstring"
				   to avoid any possible collision with the
				   C++ "string" class.
				*/
	} type;

	char *value_flag; /* Pointer to the flag variable */
	char *value_opt;  /* Pointer to the option variable */
	char **value_optstr;  /* Double pointer to a string option variable */
	char *long_name;  /* Long option name. */
 };

/* This structure is not const because we are likely to modify some memory areas
 * pointed to by the "value_flag" and "value_opt" fields.
 */
static struct command_switch	switches[] =
 {
	{ 'm', positive_long, (char *)0,   (char *) &opt_maxfactor,     (char **)0, "maxfactor"       },
	{ 'O', positive_int,  (char *) &opt_optimize,        (char *)0,	(char **)0, "optimize"        },
	{ 'p', positive_int,  (char *) &opt_projective,      (char *)0,	(char **)0, "projective"      },
	{ 'i', positive_int,  (char *) &opt_omit_imaginary,  (char *)0,	(char **)0, "omit-imaginary"  },
	{ 'e', positive_int,  (char *) &opt_input_euclidean, (char *)0,	(char **)0, "input-euclidean" },
	{ 'V', positive_int,  (char *)0,   (char *) &opt_verbosity,	(char **)0, "verbosity"       },
	{ 'o', cstring,  (char *)0, 	             (char *)0,	(char **) &opt_backend, "output"  },
	{ 'h', positive_int,  (char *)0,                     (char *)0,	(char **)0, "help"            },
	{ 'v', positive_int,  (char *)0,                     (char *)0,	(char **)0, "version"         },
	/* Note that the parameters for specifying the input quadrics doesn't have
	 * a short version so the character switch is set to the string termination
	 * character. Memory is allocated dynamically for "param_q1" and "param_q2" that's why we
	 * use the special field dedicated to character strings (value_optstr).
	 */
	{ '\0',cstring,       (char *)0,   		     (char *)0, (char **) &param_q1, "q1"              },
	{ '\0',cstring,       (char *)0,   		     (char *)0, (char **) &param_q2, "q2"              }
 };

/** Called atexit */
static void die (void)
{
	if ( param_q1 )
	{
		free ( param_q1 );
		param_q1 = (char *)0;
	}
	if ( param_q2 )
	{
		free ( param_q2 );
		param_q2 = (char *)0;
	}
}

static char progname[255];

static void help_and_quit (void)
{
	printf (
		"Usage: %s [options]\n"
		"%s",
		progname,
		usage
	       );
	exit (0);
}

static void splash_and_quit (void)
{
	printf (splash, progname);
	exit (0);
}

/* Parse the next argument (optional or not).
 * Returns -1 on error, 0 on success.
 * "argc" is modified only when we have a short argument
 * taking a value. In that case argc is decremented.
 */
static int parse_opt (int * const argc, char *** argsptr)
{

	/* Current command switch */	
	struct command_switch * cswitch;

	/* C way to get the size of an array of elements */
	const int nswitch = sizeof(switches) / sizeof(switches[0]);

	int k;
	int long_option; /* keep in memory if it was a long option */
	char * arg, * val;
	char ** args;
	char * option; /* Keep trace of the original parameter (short option or long option) */
	int free_option; /* Set to 1 if "option" should be freed */
	int free_val;	 /* Same for "val" */

/* Handy memory cleaner */
#define FREEMEM					\
	do{					\
	if ( free_option ) free (option);	\
	if ( free_val	 ) free (val);		\
	}while(0)

	free_option = 0;
	free_val    = 0;

	args = *argsptr;
	arg  =  *args;

	/* We must start with a hyphen anyway */
	if ( *arg != '-' )
	{
		fprintf (stderr,
		"Syntax error in the argument list. A hyphen (-) is expected, but a '%c' was provided.\n",
		*arg);

		FREEMEM;
		return -1;
	}

	/* Switch between a short option and a long option, and
	 * find the command_switch.
	 * If none is found, then a bad argument error is reported.
	 */


	/* Skip the hyphen */
	arg++;
	cswitch = NULL;
	option = arg;

	/* A second hyphen has been found: long option */
	if ( *arg == '-' )
	{
		/* Long option */
		long_option = 1;
		
		/* Skip the hyphen */
		arg ++;
		option = arg;

		/* Check if there is a value */

		/* Initialize a new pointer "option".
		 * By default it is the same as "arg".
		 * If a value is passed, then "option" is reduced
		 * to the substring *before* the equal sign.
		 * This is the reason for declaring another pointer.
		 */
		val = index (arg, '=');

		if ( val )
		{
			/* Store the long option name as the
			 * string *before* the equal sign.
			 * Using "arg" directly would make the
			 * option beeing not recognized so we use
			 * "option" instead.
			 */
			option = (char *)calloc(val - arg, sizeof(char));
			free_option = 1;

			strncpy (option, arg, val - arg);

			/* Skip the equal sign */
			val ++;
		}

		for ( k = 0; k < nswitch; k++ )
		{
			/* Again, note that we use "option" and not "arg"
			 * for the reason previously exposed.
			 */
			if ( ! strncmp(option, switches[k].long_name,
					strlen(switches[k].long_name)) )
			{
				/* Found a matching switch, the option
				 * has been recognized successfully
				 */
				cswitch = &switches[k];
				break;
			}
		}
		
	}
	else
	{
		/* Short option */
		long_option = 0;

		/* Note that in the case of a short option,
		 * the management of a possible value is not performed right now.
		 * The reason is that we don't know yet if an option is required.
		 * To collection the value of a short option it is need to
		 * modify argc, and report an error if no option was provided.
		 * So, it is done *after* we are sure that a value is expected.
		 */
		for ( k = 0; k < nswitch; k++ )
		{
			if ( switches[k].c == *arg )
			{
				cswitch = &switches[k];
				break;
			}
		}
	}
	
	/* If no switch has been found, a bad argument error is reported */
	if ( cswitch == NULL )
	{
		fprintf (stderr, "Invalid parameter: \"%s%s\"\n", (long_option) ? ("--") : ("-"), option);
		fprintf (stderr, "Try \"-h\" or \"--help\"\n");

		FREEMEM;
		return -1;
	}

	/* The option has been identified.
	 * First, handle miscellaneous options (with *no* value)
	 */
	switch ( cswitch->c )
	{

		case 'h':
			help_and_quit();
			/* Never reached */
			FREEMEM;
			return 0;

		case 'v':
			splash_and_quit();
			/* Never reached */
			FREEMEM;
			return 0;
	}

	/* Handle flag arguments (with *no* value) */
	if ( (cswitch->value_opt == NULL) && (cswitch->value_optstr == NULL) )
	{
		/* Set the flag to one */
		*(cswitch->value_flag) = 1;

		/* Return no error */

		FREEMEM;
		return 0;
	}
	

	/* Handle arguments with value: we don't care the option
	 * anymore, we only interpret the argument and load
	 * it in the specified variable.
	 */

	/* Check that we provided a value */
	if ( long_option )
	{
		if (val == NULL)
		{
			fprintf (stderr,
			"Value expected for parameter %s\n", *args);

			FREEMEM;
			return -1;
		}
	}
	else
	{
		/* Short option: Now we're sure that a value is expected so
		 * we decrement argc and attempt to find one.
		 * If either argc is zero or we find another
		 * option (string beginning by a hyphen), then
		 * the value is missing.
		 */
		(*argc) --;

		if ( *argc == 0 )
		{
			fprintf (stderr,
			"Value expected for parameter %s\n", option);
			fprintf (stderr, "try \"-h\" or \"--help\"\n");

			FREEMEM;
			return -1;
		}

		/* Advance args and copy the next argument string into val */
		(*argsptr) ++;
		args = *argsptr;
		val = strdup (*args);
		free_val = 1;

		/* A hyphen has been found, which is not correct for a value. */
		if ( val[0] == '-' )
		{
			fprintf (stderr,
			"Value expected for parameter %s\n", option);
			fprintf (stderr, "try \"-h\" or \"--help\"\n");

			FREEMEM;
			return -1;
		}
	}

	/* All is ok, we can interpret the string stored in the field
	 * val according to the type of the switch and finally load
	 * the data into the "value_opt" field.
	 * Note that it is necessary to reinterpret the generic pointer
	 * in the command_switch structure to the specific data types
	 * it points to !
	 */

	/* Detect incoherent options */
	if ( strlen(val) == 0 )
	{
		fprintf (stderr,
		"Error: Zero length option provided for argument %s\n", 
		option);

		FREEMEM;
		return -1;
	}

	switch ( cswitch->type )
	{
		case positive_int:
			*(int *)cswitch->value_opt = atoi ( val );
			break;

		case positive_long:
			*(long *)cswitch->value_opt = atol ( val );
			break;

		case cstring:
			/* Warning, cswitch->value_opt must point to
			 * a *valid* memory area, i.e. which has been
			 * previously allocated somewhere !
			 * We know already that cswitch->value_opt is *not*
			 * NULL. Otherwise the current option would have
			 * already been interpreted as a simple flag.
			 */
			*cswitch->value_optstr = strdup(val);
			break;

		default:
			fprintf (stderr,
			"Unexpected error: unsupported option type %d\n", cswitch->type);
			
			FREEMEM;
			return -1;
	}
	
	/* Free local memory */
	FREEMEM;

#undef FREEMEM

}

/* Do not alter those functions, unless you find a bug.
 * getopt_long could have been used, but it would have
 * led to additional changes to provide when
 * adding/modifying/removing the arguments.
 * Hence, here is a homemade function for parsing arguments.
 */
static void parse_args (int argc, char ** argv)
{

	/* Current argument */
	char ** arg;

	/* Return value of "parse_opt" */
	int parse_ret;

	arg = &argv[0];
	strncpy(progname, *arg, strlen(*arg));

	for ( ; ; )
	{
		/* No more argument to parse */		
		argc--;
		if ( argc == 0 ) break;
		
		/* Advance to the next argument */
		arg++;

		/* Treat it and modify argc accordingly. */
		parse_ret = parse_opt (&argc, &arg);

		if ( parse_ret == -1 )
		{
			/* "-1" is returned either when an error occured
			 * or when a premature end is required, like
			 * when passing the "-h/--help" argument.
			 * If an error occurs, it is reported in "parse_opt".
			 * Anyway, die and exit.
			 */
			exit(parse_ret);
		}
	}

	/* Parsing is finished, no error occured, no premature end
	 * occured
	 */

	/* Detect inconsistent argument list.
	 * (one of the required arguments is missing).
	 */
	if ( (param_q1 == (char *)0 ) || (param_q2 == (char *)0) )
	{
		fprintf (stderr, "An input quadric is missing, try --help.\n");
		exit (-1);
	}

	/* All was ok */
	return;

}

/* Print a summary of the specified options */
static void debug_options (void)
{

#define _FLAG(integer)			(integer) ? ('Y') : ('N')
#define _MODIF(val,default_val)		(val == default_val) ? ('-') : ('*')

	printf (
	"--- Option summary ('*' means \"modified\" and '-' means \"default\")\n"
	"maxfactor:\033[30G%ld\033[40G(%c)\n"
	"optimize:\033[30G%c\033[40G(%c)\n"
	"projective:\033[30G%c\033[40G(%c)\n"
	"omit imaginary:\033[30G%c\033[40G(%c)\n"
	"input euclidean:\033[30G%c\033[40G(%c)\n"
	"verbosity:\033[30G%d\033[40G(%c)\n"
	"output:\033[30G%s\033[40G(%c)\n",	
		opt_maxfactor, 			_MODIF(opt_maxfactor, 		DEFAULT_MAXFACTOR),
		_FLAG(opt_optimize),		_MODIF(opt_optimize,  		DEFAULT_OPTIMIZE),
		_FLAG(opt_projective),		_MODIF(opt_projective,		DEFAULT_PROJECTIVE),
		_FLAG(opt_omit_imaginary),	_MODIF(opt_omit_imaginary,	DEFAULT_OMIT_IMAGINARY),
		_FLAG(opt_input_euclidean),	_MODIF(opt_input_euclidean,	DEFAULT_INPUT_EUCLIDEAN),
		opt_verbosity,			_MODIF(opt_verbosity,		DEFAULT_VERBOSITY),
		opt_backend,			(!strcmp(opt_backend,DEFAULT_BACKEND)) ? ('-') : ('*')
	);

	if ( param_q1 )
	{
		printf (
		"\n--- Input quadrics\n"
		"q1: %s\n"
		"q2: %s\n",
		param_q1, param_q2
		);
	}

#undef _FLAG
#undef _MODIF

	return;
}

static void assign_settings (void)
{
	qi_settings.maxfactor	= opt_maxfactor;
	qi_settings.optimize	= opt_optimize;
	qi_settings.output_projective
				= opt_projective;
	qi_settings.output_omit_imaginary_components
				= opt_omit_imaginary;
	qi_settings.output_show_input_euclidean_type
				= opt_input_euclidean;
	qi_settings.output_verbosity
				= opt_verbosity;
	
	if ( !strcmp(opt_backend,"latex") )
		qi_settings.backend = QI_BACKEND_LATEX;
	else
	if ( !strcmp(opt_backend,"html") )
		qi_settings.backend = QI_BACKEND_HTML;
	else
	if ( !strcmp(opt_backend,"plain") )
		qi_settings.backend = QI_BACKEND_PLAIN;
	else
	{
		fprintf (stderr,
		"Bad output option: %s.\n", opt_backend);
		exit (-1);
	}

}

int main (int argc, char ** argv)
{

	qi_inter_t quad_inter;
	mqi_mat_t  q1;
	mqi_mat_t  q2;
	mqi_vect_t vect;

	mempool_t pool;
	mempool_init (pool);
	atexit(die);


	parse_args (argc, argv);

	/* Keep this function call commented in case
	 * something goes weird and you'd need
	 * to uncomment it.
	 */
	debug_options();

	assign_settings ();

	qi_parse (vect, param_q1);
	qi_vect_to_quad(q1, vect);
	
	mqi_vect_clear(vect);
	qi_parse (vect, param_q2);

	qi_vect_to_quad(q2, vect);

	printf ("Input quadrics:\n");
	mqi_mat_print(q1);
	mqi_log_printf ("\n");
	mqi_mat_print(q2);
	mqi_log_printf ("\n");
	qi_intersect (quad_inter, q1, q2, opt_optimize);

	/* Printout the result */
	qi_inter_print(quad_inter);

	/** Free local memory */
	qi_inter_clear (quad_inter);
	mqi_mat_list_clear(q1, q2, NULL);
	mqi_vect_clear(vect);
	mempool_clear();
	/** Return to shell */
	exit (0);
}

