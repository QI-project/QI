#include "qi.h"

void help (void) {

	printf ("\n");
	printf ("Syntax is: intersector <quadric1> <quadric2>\n");
	printf ("Each quadric can be described in the following formats:\n\n");
	printf (" 1) Equation: x^2-y^2+w^2\n");
	printf (" 2) Vector:   [1 0 0 0 1 0 0 1 0 0]\n");
	printf ("\n");
	exit (0);

}

int main (int argc, char **argv) {

	mqi_mempool_t	mempool;
	mqi_vect_t	Q1_vect;
	mqi_vect_t	Q2_vect;
	mqi_mat_t	Q1_mat;
	mqi_mat_t	Q2_mat;
	mqi_inter_t	inter;

#ifndef MQI_MEMPOOLS
#error "Mempools are recquired for libqi to work"
#endif

	if ( argc < 2 ) help();


	mqi_mempool_init (mempool);

	qi_parse (Q1_vect, argv[1]);
	qi_parse (Q2_vect, argv[2]);

	mqi_log_printf ("Parsing result:\n");
	mqi_vect_print (Q1_vect);
	mqi_log_printf ("\n");
	mqi_vect_print (Q2_vect);
	mqi_log_printf ("\n");

	/** Convert into symetric matrices */
	qi_vect_to_matrix (Q1_mat, Q1_vect);
	qi_vect_to_matrix (Q2_mat, Q2_vect);

	/** Launches the intersection */
	qi_intersect (inter, Q1_mat, Q2_mat, 0);

	/** Prints the result */
	mqi_inter_print_information (inter);

/*	mqi_mempool_clear ();*/

	return (EXIT_SUCCESS);

}

