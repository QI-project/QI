#include "qi_inter.h"



/** ****************************************************************************************** **/
/** Input : initial quadrics q1, q2								*/
/**          determinental equation of the pencil : det_p					*/
/**          a quadric 22 : q									*/
/**          case_flag  = 0 if the intersection is a smooth quartic, 2 real affinely finite	*/
/**                     = 1           "                              1 real affinely finite	*/
/**                     = 2           "                              2 real affinely infinite	*/
/**                     = 3 if intersection is a cubic and a line secant			*/
/**                     = 4           "                           non-secant			*/
/**                     = 5 if intersection is two real skew lines				*/
/**  Output : Compute the Parameterization s1 + sqrt(xi) s2 of a quadric 22 in the pencil 	*/
/**            and the Associated biquadratic equation poly1 + sqrt(xi) poly2 = 0		*/
/**     Call smooth_quartic(...) or cubic_line(...) for computing the intersection curve	*/
/** ******************************************************************************************* */
void qi_inter_with_22 (qi_inter_t __INIT rop,  mqi_mat_t __IN q1, mqi_mat_t __IN q2,
		       mqi_hpoly_t __IN det_p, mqi_mat_t __IN q_cp,
		       int case_flag)
{
	
	mqi_vect_t		point_on_q, det_q;
	mqi_coef_t		det_R;
	mqi_mat_t		q;
	mqi_surface_param_t	s1, s2;
	mqi_hhpoly_t		poly1, poly2, hhtmp;

	/* Compute the new quadric q  of inertia 2,2; 
	 * point_on_q is a point on it; det_q its determinant in the form [a,b] with det
	 *  = a^2. b */
	/** $$ Beware of the order of arguments, different from the original version ! */
	qi_inter_find_quadric_22_and_point (q,point_on_q,det_R,det_q,q1,q2,det_p,q_cp);

#ifdef DEBUG
	mqi_log_printf ("Quadric (2,2) found:\n");
	mqi_mat_print  (q);
	mqi_log_printf ("\n");
	mqi_log_printf ("Decomposition of its determinant (a b) : "); mqi_vect_print(det_q); mqi_log_printf("\n");
	mqi_log_printf ("A point on the quadric : "); mqi_vect_print(point_on_q); mqi_log_printf("\n");
#endif

	mqi_surface_param_list_init_dynamic(4, mqi_vect_typeof(det_q), s1, s2, NULL);
	qi_inter_find_parametrization_quadric_22 (s1,s2,q,point_on_q,det_q,det_R);

	/* Parameterization of the quadric is  s1 + sqrt(det_q[1]) s2
	* If det_q[1]=1 then  s2=[0,0,0,0]

	* Compute the bi-quadratic homogeneous polynomial poly = poly1 + sqrt(xi) poly2
	* poly = (s1 + sqrt(det_q[1]) s2)^t. q1. (s1 + sqrt(det_q[1]) s2)
	* (or with q2 if poly = 0)
	* poly = s1.q1.s1 + det_q[1]. s2.q1.s2 + 2. sqrt(det_q[1]). s1.q1.s2
	* We set poly1 = s1.q1.s1 + det_q[1]. s2.q1.s2 
	* and poly2 = 2. s1.q1.s2 */
	qi_param_plug_sp_in_quadric (poly1, s1, q1, s1);
	qi_param_plug_sp_in_quadric (hhtmp, s2, q1, s2);

	mqi_hhpoly_mul_coef (hhtmp, hhtmp, VECT(det_q,1));

	mqi_hhpoly_add (poly1, poly1, hhtmp);

	qi_param_plug_sp_in_quadric (poly2, s1, q1, s2);

	mqi_hhpoly_mul_int (poly2, poly2, 2);

	if ( mqi_hhpoly_is_zero(poly1) && mqi_hhpoly_is_zero(poly2) ) {

		/** We recompute poly1 and poly2 after replacing q1 by q2 */
		/* discard old poly1, poly2 and hhtmp */
		mqi_hhpoly_list_clear(poly1, poly2, hhtmp, NULL);

		qi_param_plug_sp_in_quadric (poly1, s1, q2, s1);
		qi_param_plug_sp_in_quadric (hhtmp, s2, q2, s2);

		mqi_hhpoly_mul_coef (hhtmp, hhtmp, VECT(det_q,1));

		mqi_hhpoly_add (poly1, poly1, hhtmp);

		qi_param_plug_sp_in_quadric (poly2, s1, q2, s2);

		mqi_hhpoly_mul_int (poly2, poly2, 2);

	}

	qi_hhpoly_optimize_gcd (poly1, poly2); /* remove the gcd of all (integer) coefficients */

	/* End of the computation of the bi-quadratic homogeneous polynomial poly */

	/* If xi = 1 then s2 and poly2 are 0 */
	if ( case_flag >= 0 && case_flag <= 2 ) /** Smooth quartic */
		__qi_inter_with_22_smooth_quartic(rop,q1,q2,s1,s2,poly1,poly2,VECT(det_q,1),case_flag);
	else if ( (case_flag == 3) || (case_flag == 4) ) /** Intersection is a cubic and a line */
	{
	/* For debug purpose */
	/*	if ( !mqi_hhpoly_is_zero(poly2) || !mqi_hhpoly_is_zero(SP(s2,0)) || !mqi_hhpoly_is_zero(SP(s2,1)) ||
		     !mqi_hhpoly_is_zero(SP(s2,2)) || !mqi_hhpoly_is_zero(SP(s2,3)) ) */
		/** Cubic line */
		__qi_inter_with_22_cubic_line (rop,q1,q2,s1,poly1,case_flag);
	}else /** case_flag = 5 : Two real skew lines */
		__qi_inter_with_22_two_real_skew_lines (rop,q1,q2,s1,s2,poly1,poly2,VECT(det_q,1));


	/** Free local memory */
	mqi_vect_list_clear (point_on_q, det_q, NULL);
	mqi_coef_clear (det_R);
	mqi_mat_clear (q);
	mqi_surface_param_list_clear(s1, s2, NULL);
	mqi_hhpoly_list_clear(poly1, poly2, hhtmp, NULL);

}

/** Returns the max coefficient of the discriminant of the polynomial "p+sqrt(d)*q", after simplification */
void __qi_inter_with_22_h_of_discr4 (mqi_coef_t rop, mqi_hpoly_t __IN p, mqi_hpoly_t __IN q, mqi_coef_t __IN d) {

	mqi_coef_t		p0, p1, p2, p3, p4;
	mqi_coef_t		q0, q1, q2, q3, q4;
	mqi_coef_t		ir, id, jr, jd;/* Dd, pr, pd, rr;*/

	mqi_coef_list_init_dynamic (
		mqi_coef_typeof(d),
		rop, p0,p1,p2,p3,p4, q0,q1,q2,q3,q4, ir,id,jr,jd, NULL
	);

	mqi_coef_cpy (p0, POLY(p,0));
	mqi_coef_cpy (p1, POLY(p,1));
	mqi_coef_cpy (p2, POLY(p,2));
	mqi_coef_cpy (p3, POLY(p,3));
	mqi_coef_cpy (p4, POLY(p,4));

	if ( mqi_poly_is_zero(q) )
	{
		mqi_coef_set_schar    (q0, 0);
		mqi_coef_set_schar    (q1, 0);
		mqi_coef_set_schar    (q2, 0);
		mqi_coef_set_schar    (q3, 0);
		mqi_coef_set_schar    (q4, 0);
	}
	else
	{
		mqi_coef_cpy (q0, POLY(q,0));
		mqi_coef_cpy (q1, POLY(q,1));
		mqi_coef_cpy (q2, POLY(q,2));
		mqi_coef_cpy (q3, POLY(q,3));
		mqi_coef_cpy (q4, POLY(q,4));
	}

	mqi_coef_linexpr3 (ir, 6,	12,p0,p4,NULL,	12,d,q0,q4,	-3,p1,p3,NULL,		-3,d,q1,q3,	1,p2,p2,NULL,  1,d,q2,q2);
	mqi_coef_linexpr3 (id, 5,	12,q0,p4,NULL,	12,p0,q4,NULL,	-3,p1,q3,NULL,		-3,q1,p3,NULL,	2,p2,q2,NULL);	

	mqi_coef_linexpr4 (jr, 16,	72,p0,p2,p4,NULL,	9,p1,p2,p3,NULL,	72,d,p0,q2,q4,		-54,d,p1,q1,q4,
					9,d,p1,p2,q3,		72,d,q0,p2,q4,		-54,d,q0,p3,q3,		72,d,q0,q2,p4,
					9,d,p2,q1,q3,		9,d,q1,p3,q2,		-2,p2,p2,p2,NULL	-27,p0,p3,p3,NULL,
					-27,p1,p1,p4,NULL,	-27,d,p0,q3,q3		-6,d,p2,q2,q2,		-27,d,q1,q1,p4);
	mqi_coef_linexpr4 (jd, 16,	72,p0,p2,q4,NULL,	-54,p0,p3,q3,NULL,	72,p0,q2,p4,NULL,	9,p1,p2,q3,NULL,
					-54,p1,q1,p4,NULL,	9,p1,p3,q2,NULL,	72,q0,p2,p4,NULL,	9,p2,q1,p3,NULL,
					72,d,q0,q2,q4,		9,d,q1,q2,q3,		-2,d,q2,q2,q2,		-27,q0,p3,p3,NULL,
					-6,p2,p2,q2,NULL,	-27,p1,p1,q4,NULL,	-27,d,q0,q3,q3,		-27,d,q1,q1,q4);

	mqi_coef_linexpr4 (rop, 4,	4,ir,ir,ir,NULL,	12,d,ir,id,id,		-1,jr,jr,NULL,NULL,	-1,d,jd,jd,NULL);

	/** $$ Original code contains some debug information I think */

	/** Free local memory */
	mqi_coef_list_clear(p0,p1,p2,p3,p4, q0,q1,q2,q3,q4, ir,id,jr,jd, NULL);


}

/** No precision specified */
int __qi_inter_with_22_disc_co (mqi_hhpoly_t __IN c1, mqi_hhpoly_t __IN c2, mqi_coef_t __IN xi, const unsigned short i) {

	mqi_coef_t		c10,c11,c12,c20,c21,c22,p1,p2;
	int			sg;

	mqi_coef_list_init_dynamic(
		mqi_coef_typeof(xi),
		c10,c11,c12,c20,c21,c22,p1,p2, NULL
	);

	if ( !mqi_hhpoly_is_zero(c1) && !mqi_poly_is_zero(HHPOLY(c1,i)) )
	{
		mqi_coef_cpy (c10, HHCOEF(c1,i,0));
		mqi_coef_cpy (c11, HHCOEF(c1,i,1));
		mqi_coef_cpy (c12, HHCOEF(c1,i,2));
	}
	if ( !mqi_hhpoly_is_zero(c2) && !mqi_poly_is_zero(HHPOLY(c2,i)) )
	{
		mqi_coef_cpy (c20, HHCOEF(c2,i,0));
		mqi_coef_cpy (c21, HHCOEF(c2,i,1));
		mqi_coef_cpy (c22, HHCOEF(c2,i,2));
	}

	mqi_coef_linexpr3 (p1,4,	1,c11,c11,NULL,		1,xi,c21,c21,		-4,c12,c10,NULL,	-4,xi,c22,c10);
	mqi_coef_linexpr2 (p2,3,	2,c11,c21,		-4,c12,c20,		-4,c22,c10);

	/** Disc is p1+sqrt(xi)*p2 */
	sg = qi_coef_sign(p1,p2,xi);

	/** Free local memory */
	mqi_coef_list_clear(
		c10,c11,c12,c20,c21,c22,p1,p2, NULL
	);

	if ( sg ) return (sg);
	else	  return (3);	/** Double root. */

}

/** Smooth quartic.
Input : initial quadrics q1, q2 (only for checking)
         Parameterization s1 + sqrt(xi) s2 of a quadric 22 in the pencil
         Associated biquadratic equation poly1 + sqrt(xi) poly2 = 0
           (if xi = 1 then s2 and poly2 are 0)
Output : compute of the parameterization of the intersection curve
     c1 + sqrt(xi). c2 + eps. sqrt(Delta). (c3 + sqrt(xi). c4)
 where Delta = Delta1 + sqrt(xi). Delta2
 (if xi = 1 then c2, c4, and Delta2 are 0) */
void __qi_inter_with_22_smooth_quartic (qi_inter_t __INIT rop, mqi_mat_t __IN q1, mqi_mat_t __IN q2,
					mqi_surface_param_t __IN s1, mqi_surface_param_t __IN s2,
				        mqi_hhpoly_t __IN poly1, mqi_hhpoly_t __IN poly2, mqi_coef_t __IN xi, int case_flag)
{

	mqi_hhpoly_t		poly1sw, poly2sw;
	mqi_hpoly_t		Delta1, Delta2;
	qi_inter_component_t	comp;

	qi_inter_init(rop);

	if ( case_flag == 0 )
		qi_inter_set_type (rop, 1,2);
	else if ( case_flag == 1 )
		qi_inter_set_type (rop, 1,3);
	else
		qi_inter_set_type (rop, 1,4);

	/** Store the (2,2) quadric surface param */
	qi_inter_set_22 (rop, s1, s2);

	mqi_hhpoly_init_dynamic (poly1sw, mqi_hhpoly_typeof(poly1));
	mqi_hhpoly_init_dynamic (poly2sw, mqi_hhpoly_typeof(poly2));

	mqi_hhpoly_exchange_xwst (poly1sw, poly1);
	mqi_hhpoly_exchange_xwst (poly2sw, poly2);

	/* Compute the discriminant Delta of the bi-quadratic  equation poly
	* seen as a degree 2 equation in one of the (projective) variables
	* poly = poly1 + sqrt(xi) poly2
	* =  (poly1[2] + sqrt(xi). poly2[2]). u^2 + (poly1[1] + sqrt(xi). poly2[1]). u.v
	*                                     + (poly1[0] + sqrt(xi). poly2[0]). v^2
	* Delta = Delta1 + sqrt(xi). Delta2 where 
	* Delta1 = poly1[1]^2 + d.poly2[1]^2 - 4.poly1[0].poly1[2] - 4.d.poly2[0].poly2[2]
	* Delta2 = 2.poly1[1].poly2[1] - 4.poly1[2].poly2[0] - 4.poly1[0].poly2[2] */
	mqi_hpoly_list_init_dynamic (mqi_hhpoly_typeof(poly1), Delta1, Delta2, NULL);

	if ( !mqi_hhpoly_is_zero(poly1) ) {

		mqi_hpoly_t htmp;

		mqi_poly_cpy 	   (Delta1, HHPOLY(poly1,1));
		mqi_poly_mul	   (Delta1, Delta1, HHPOLY(poly1,1));
		mqi_poly_init_cpy  (htmp, HHPOLY(poly1,0));
		mqi_poly_mul	   (htmp, htmp, HHPOLY(poly1,2));
		mqi_poly_mul_int   (htmp, htmp, 4);
		mqi_poly_neg	   (htmp, htmp);
		mqi_poly_add	   (Delta1, Delta1, htmp);

		/** Free local memory */
		mqi_poly_clear(htmp);

	}

	if ( !mqi_hhpoly_is_zero(poly2) ) {

		mqi_hpoly_t htmp;

		mqi_poly_init_cpy  (htmp, HHPOLY(poly2,1));
		mqi_poly_mul	   (htmp, htmp, HHPOLY(poly2,1));
		mqi_poly_mul_coef  (htmp, htmp, xi);
		mqi_poly_add	   (Delta1, Delta1, htmp);
		mqi_poly_mul	   (htmp, HHPOLY(poly2,0), HHPOLY(poly2,2));
		mqi_poly_mul_coef  (htmp, htmp, xi);
		mqi_poly_mul_int   (htmp, htmp, 4);
		mqi_poly_neg	   (htmp, htmp);
		mqi_poly_add	   (Delta1, Delta1, htmp);

		/** Free local memory */
		mqi_poly_clear(htmp);

	}

	if ( !mqi_hhpoly_is_zero(poly1) && !mqi_hhpoly_is_zero(poly2) ) {

		mqi_hpoly_t htmp;

		/* Delta2 = 2.poly1[1].poly2[1] - 4.poly1[2].poly2[0] - 4.poly1[0].poly2[2] */
		mqi_poly_cpy  (Delta2, HHPOLY(poly1,0));
		mqi_poly_mul	   (Delta2, Delta2, HHPOLY(poly2,2));
		mqi_poly_init_cpy  (htmp, HHPOLY(poly1,2));
		mqi_poly_mul	   (htmp, htmp, HHPOLY(poly2,0));
		mqi_poly_add	   (Delta2, Delta2, htmp);
		mqi_poly_neg	   (Delta2, Delta2);
		mqi_poly_mul_int   (Delta2, Delta2, 2);
		mqi_poly_mul	   (htmp, HHPOLY(poly1,1), HHPOLY(poly2,1));
		mqi_poly_add	   (Delta2, Delta2, htmp);
		mqi_poly_mul_int   (Delta2, Delta2, 2);

		/** Free local memory */
		mqi_poly_clear(htmp);

	}

	/* End of the computation of the discriminant Delta of the bi-quadratic equation poly */

	/* Recall the parameterization of the quadric 22 : s = s1 + sqrt(xi) s2    
	* and the bi-quadratic homogeneous polynomial : poly = poly1 + sqrt(xi) poly2
	* =  (poly1[2] + sqrt(xi). poly2[2]). u^2 + (poly1[1] + sqrt(xi). poly2[1]). u.v
	*                                       + (poly1[0] + sqrt(xi). poly2[0]). v^2
	* Note that neither the coefficient in u^2 nor the coeff in v^2 is zero because 
	* poly is irreducible since we are in the smooth quartic case. 

	* Compute the output  parameterized curve : 
	*                     c = c1 + sqrt(xi). c2 + eps. sqrt(Delta). (c3 + sqrt(xi). c4)
	* c1 = s1(u = -poly1[1], v = 2.poly1[2]) + xi. s2(u = -poly2[1], v = 2.poly2[2])
	* c2 = s1(u = -poly2[1], v = 2.poly2[2]) + s2(u = -poly1[1], v = 2.poly1[2])
	* c3 = s1(u = 1, v = 0) 
	* c4 = s2(u = 1, v = 0)	 */
	
	mqi_curve_param_t c1, c2, c3, c4, c_tmp;
	mqi_curve_param_list_init_dynamic(4, mqi_hhpoly_typeof(poly1),
						c1, c2, c3, c4, c_tmp, NULL);

	mqi_hpoly_t htmp1, htmp2;	
	mqi_hpoly_init_dynamic	     (htmp1, mqi_hhpoly_typeof(poly1));
	mqi_hpoly_init_dynamic	     (htmp2, mqi_hhpoly_typeof(poly2));

	mqi_poly_set_zero 	(htmp1);
	mqi_poly_set_at_int  	(htmp2,0, 1);

	/** $$ Here, it seems that the original code is a little bit unprecise: a cast to bigint
	       has been set instead of a cast to "hom_polynomial" */
	mqi_surface_param_eval_poly (c3, s1, htmp2, htmp1);
	mqi_surface_param_eval_poly (c4, s2, htmp2, htmp1);

	if ( !mqi_hhpoly_is_zero(poly1) ) {
	
		/* discard old htmp1 and htmp2 */
		mqi_poly_list_clear(htmp1, htmp2, NULL);

		mqi_poly_init_cpy  (htmp1, HHPOLY(poly1,1));
		mqi_poly_neg	   (htmp1, htmp1);
		mqi_poly_init_cpy  (htmp2, HHPOLY(poly1,2));
		mqi_poly_mul_int   (htmp2, htmp2, 2);
		mqi_surface_param_eval_poly (c1, s1, htmp1, htmp2);
		mqi_surface_param_eval_poly (c2, s2, htmp1, htmp2);
	
	}

	if ( !mqi_hhpoly_is_zero(poly2) ) {

		/* discard old htmp1 and htmp2 */
		mqi_poly_list_clear(htmp1, htmp2, NULL);

		mqi_poly_init_cpy (htmp1, HHPOLY(poly2,1));
		mqi_poly_neg	  (htmp1, htmp1);
		mqi_poly_init_cpy (htmp2, HHPOLY(poly2,2));
		mqi_poly_mul_int  (htmp2, htmp2, 2);
		mqi_surface_param_eval_poly  (c_tmp, s2, htmp1, htmp2);
		mqi_curve_param_mul_coef (c_tmp, c_tmp, xi);
		mqi_curve_param_add	 (c1, c1, c_tmp);
		mqi_surface_param_eval_poly  (c_tmp, s1, htmp1, htmp2);
		mqi_curve_param_add (c2, c2, c_tmp);

	}
	
	/** Square factors are extracted from Delta */
	qi_curve_param_optimize_smooth_quartic (c1, c2, c3, c4, Delta1, Delta2);
		
	 /* Output parameterized curve : c = c1 + sqrt(xi). c2 + eps. sqrt(Delta). (c3 +
  	  sqrt(xi). c4) 
	  if xi=1 c2=[0,0,0,0] and c4 =[0,0,0,0] */
	if ( mqi_coef_cmp_schar(xi,1) == 0 ) {
		
		qi_inter_component_init (comp, QI_INTER_COMPONENT_4, c1, c3, Delta1);
		qi_inter_component_set_type (comp, QI_INTER_COMPONENT_SMOOTH_QUARTIC_BRANCH_1);
		qi_inter_add_component (rop, comp);
		/* discard comp */
		qi_inter_component_clear(comp);

		mqi_curve_param_neg (c3, c3);

		qi_inter_component_init (comp, QI_INTER_COMPONENT_4, c1, c3, Delta1);
		qi_inter_component_set_type (comp, QI_INTER_COMPONENT_SMOOTH_QUARTIC_BRANCH_2);
		qi_inter_add_component (rop, comp);
		/* discard comp */
		qi_inter_component_clear(comp);

		qi_inter_set_optimal (rop);

	}else{

		qi_inter_component_init (comp, QI_INTER_COMPONENT_7, c1, c2, xi, c3, c4, Delta1, Delta2);
		qi_inter_component_set_type (comp, QI_INTER_COMPONENT_SMOOTH_QUARTIC_BRANCH_1);
		qi_inter_add_component (rop, comp);
		/* discard comp */
		qi_inter_component_clear(comp);

		mqi_curve_param_neg (c3, c3);
		mqi_curve_param_neg (c4, c4);
	
		qi_inter_component_init (comp, QI_INTER_COMPONENT_7, c1, c2, xi, c3, c4, Delta1, Delta2);
		qi_inter_component_set_type (comp, QI_INTER_COMPONENT_SMOOTH_QUARTIC_BRANCH_2);
		qi_inter_add_component (rop, comp);
		/* discard comp */
		qi_inter_component_clear(comp);

		/** No global optimality is set: default is "none" */

	}
	
	/** Free local memory */
	mqi_hhpoly_list_clear(poly1sw, poly2sw, NULL);
	mqi_poly_list_clear(Delta1, Delta2, htmp1, htmp2, NULL);
	mqi_curve_param_list_clear(c1, c2, c3, c4, c_tmp, NULL);	

}

/** Two real skew lines */
/* Input : initial quadrics q1, q2 (only for checking)
         Parameterization s1_cp + sqrt(xi) s2_cp of a quadric 22 in the pencil
         Associated biquadratic equation poly1 + sqrt(xi) poly2 = 0
          (if xi = 1 then s2_cp and poly2 are 0)
   Output : print of the parameterization of the intersection curves 
 (one curve_param for each of the two lines)
 c1 + sqrt(xi). c2 + eps. sqrt(Delta). (c3 + sqrt(xi). c4)
 where the coeff of the ci are linear and Delta = (Delta1 + sqrt(xi). Delta2) in R
 (if xi = 1 then c2, c4, and Delta2 are 0) */
void __qi_inter_with_22_two_real_skew_lines (qi_inter_t __INIT rop, mqi_mat_t __IN q1, mqi_mat_t __IN q2, mqi_surface_param_t __IN s1_cp,
					     mqi_surface_param_t __IN s2_cp, mqi_hhpoly_t __IN poly1, mqi_hhpoly_t __IN poly2,
					     mqi_coef_t __IN xi)
{

	qi_inter_component_t	comp;
	unsigned short 		i;

	/* The bi-variate homogeneous degree (2,2) polynomial poly can be factored into 
	* two univariate polynomials of degree 2. One of these has two real roots and 
	* can be factored into two linear terms (the two real lines). The other has complex 
	* roots and correspond to two complex lines. 

	* The 2 factors are (up to a complex factor) 
	*          coeff(poly, u^2)   and coeff(poly, s^2).
	* Indeed, poly = F1.F2 = (a1.u^2 + b1.uv + c1.v^2) . (a2.s^2 + b2.st + c2.t^2) 
	* coeff(poly, u^2) = a1.F2 
	* coeff(poly, s^2) = a2.F1 
	* coeff(poly, u^2) * coeff(poly, s^2) = a1.a2.F1.F2 = coeff(poly, u^2s^2). poly
	* More precesely the 2 factors are, up to a complex factor, 
	* (1) any of the coeff of poly of u^2, uv or v^2 that is non-zero and
	* (2) any of the coeff of poly of s^2, st or t^2 that is non-zero

	* Let content = coeff(poly, u^2 or uv or v^2) and primpart = coeff(poly, s^2 or
	* st or t^2) such that they are non-zero and the sum_of_squares of the coeff is
	* minimized 
	* content = content1 + sqrt(xi) content2
	* content_i = s^2.content_i[2] + st.content_i[1] + t^2.content_i[0]
	* where the content_i[*] are of degree 0 in (s,t)
	* Primpart = primpart1 + sqrt(xi) primpart2
	* primpart_i = u^2.primpart_i[2] + uv.primpart_i[1] + v^2.primpart_i[0]  */
	
	
	/*mqi_inter_init (rop, 2);*/ /** 2 components */
	qi_inter_init(rop);
	qi_inter_set_type (rop, 14,4);

	mqi_surface_param_t s1, s2;

	mqi_surface_param_init_cpy(s1, s1_cp);
	mqi_surface_param_init_cpy(s2, s2_cp);

	mqi_hpoly_t content1, content2, primpart1, primpart2;
	mqi_coef_t  norm, norm_htmp;

	/* Choose content1 = poly1[i] and content2 = poly2[i] such that there are not both 0 
	 * and sum_of_squares of the coeff is minimized */
	mqi_coef_init_dynamic (norm, mqi_mat_typeof(q1));
	mqi_coef_set_schar (norm, 0);

	mqi_coef_t coef;
	mqi_coef_list_init_dynamic (mqi_mat_typeof(q1), norm_htmp, coef, NULL);

	mqi_hpoly_t htmp1, htmp2;

	for ( i = 0; i < 3; i++ ) {

		mqi_hpoly_init_dynamic (htmp1, mqi_hhpoly_typeof(poly1));
		mqi_hpoly_init_dynamic (htmp2, mqi_hhpoly_typeof(poly2));
		mqi_coef_set_schar (norm_htmp, 0);

		if ( !mqi_hhpoly_is_zero(poly1) ) {
			mqi_poly_cpy (htmp1, HHPOLY(poly1,i));
			if ( !mqi_poly_is_zero(htmp1))
				mqi_coef_linexpr2(norm_htmp,3,	
						  1,POLY(htmp1,0),POLY(htmp1,0),
						  1,POLY(htmp1,1),POLY(htmp1,1),
						  1,POLY(htmp1,2),POLY(htmp1,2));
		}
		
		if ( !mqi_hhpoly_is_zero(poly2) ) {
			mqi_poly_cpy (htmp2, HHPOLY(poly2,i));
			if ( !mqi_poly_is_zero(htmp2))
				mqi_coef_cpy (coef, norm_htmp);
				mqi_coef_linexpr2(norm_htmp,3,
						  1,POLY(htmp2,0),POLY(htmp2,0),
						  1,POLY(htmp2,1),POLY(htmp2,1),
						  1,POLY(htmp2,2),POLY(htmp2,2));
				mqi_coef_add (norm_htmp, norm_htmp, coef);
		}

		/** $$ In what follows, there's redundancy of the same portion of code ... */	
		if ( ! mqi_coef_sign(norm) ) { /** Content = 0 */
			
			mqi_poly_init_cpy (content1, htmp1);
			mqi_poly_init_cpy (content2, htmp2);
			mqi_coef_init_cpy  (norm, norm_htmp);

		}
		else if ( mqi_coef_sign(norm_htmp) ) { /** content != 0 and htmp != 0 */

			if ( (mqi_poly_is_zero(htmp1) || mqi_poly_is_zero(htmp2)) &&
			     ( (!mqi_poly_is_zero(content1) && !mqi_poly_is_zero(content2)) || (mqi_coef_cmp (norm_htmp,norm) < 0)
				)) {
				
				/* if htmpi =0 and 
				 * ((content1 !=0 and content2 !=0) or ||htmp||<||content||) */
				mqi_poly_init_cpy (content1, htmp1);
				mqi_poly_init_cpy (content2, htmp2);
				mqi_coef_init_cpy  (norm, norm_htmp);

			}else if ( !mqi_poly_is_zero(content1) && !mqi_poly_is_zero(content2) && (mqi_coef_cmp(norm_htmp,norm) < 0))
			{

				mqi_poly_init_cpy (content1, htmp1);
				mqi_poly_init_cpy (content2, htmp2);
				mqi_coef_init_cpy  (norm, norm_htmp);

			}

		}

	}

	/* poly1 and poly2 are const */
	mqi_hhpoly_t poly1_cp, poly2_cp;
	mqi_hhpoly_init_cpy (poly1_cp, poly1);
	mqi_hhpoly_init_cpy (poly2_cp, poly2);

	/* We now do exactly the same thing for primpart */
	mqi_hhpoly_exchange_xwst (poly1_cp, poly1);
	mqi_hhpoly_exchange_xwst (poly2_cp, poly2);

	mqi_coef_set_schar (norm, 0);

	for ( i = 0; i < 3; i++ ) {

		mqi_hpoly_init_dynamic (htmp1, mqi_hhpoly_typeof(poly1));
		mqi_hpoly_init_dynamic (htmp2, mqi_hhpoly_typeof(poly2));
		mqi_coef_set_schar (norm_htmp, 0);

		if ( !mqi_hhpoly_is_zero(poly1) ) {
			mqi_poly_cpy (htmp1, HHPOLY(poly1,i));
			if ( !mqi_poly_is_zero(htmp1))
				mqi_coef_linexpr2(norm_htmp,3,	
						  1,POLY(htmp1,0),POLY(htmp1,0),
						  1,POLY(htmp1,1),POLY(htmp1,1),
						  1,POLY(htmp1,2),POLY(htmp1,2));
		}
		
		if ( !mqi_hhpoly_is_zero(poly2) ) {
			mqi_poly_cpy (htmp2, HHPOLY(poly2,i));
			if ( !mqi_poly_is_zero(htmp2))
				mqi_coef_cpy (coef, norm_htmp);
				mqi_coef_linexpr2(norm_htmp,3,
						  1,POLY(htmp2,0),POLY(htmp2,0),
						  1,POLY(htmp2,1),POLY(htmp2,1),
						  1,POLY(htmp2,2),POLY(htmp2,2));
				mqi_coef_add (norm_htmp, norm_htmp, coef);
		}

		/** $$ In what follows, there's redundancy of the same portion of code ... */	
		if ( ! mqi_coef_sign(norm) ) { /** Content = 0 */
			
			mqi_poly_init_cpy (primpart1, htmp1);
			mqi_poly_init_cpy (primpart2, htmp2);
			mqi_coef_init_cpy  (norm, norm_htmp);

		}
		else if ( mqi_coef_sign(norm_htmp) ) { /** primpart != 0 and htmp != 0 */

			if ( (mqi_poly_is_zero(htmp1) || mqi_poly_is_zero(htmp2)) &&
			     ( (!mqi_poly_is_zero(primpart1) && !mqi_poly_is_zero(primpart2)) || (mqi_coef_cmp (norm_htmp,norm) < 0)
				)) {
				
				/* if htmpi =0 and 
				 * ((primpart1 !=0 and primpart2 !=0) or ||htmp||<||primpart||) */
				mqi_poly_init_cpy (primpart1, htmp1);
				mqi_poly_init_cpy (primpart2, htmp2);
				mqi_coef_init_cpy  (norm, norm_htmp);

			}else if ( !mqi_poly_is_zero(primpart1) && !mqi_poly_is_zero(primpart2) && (mqi_coef_cmp(norm_htmp,norm) < 0))
			{

				mqi_poly_init_cpy (primpart1, htmp1);
				mqi_poly_init_cpy (primpart2, htmp2);
				mqi_coef_init_cpy  (norm, norm_htmp);

			}

		}

	}


	/** ************* */
	/** OpTiMiZaTiOnS */
	/** ************* */
	qi_hpoly_optimize_gcd (content1, content2);
	qi_hpoly_optimize_gcd (primpart1, primpart2);

	/* Delta = b^2 -4ac, discriminant  of the polynomial content
	 *  = (b1 + sqrt(xi) b2)^2 - 4 (a1 + sqrt(xi) a2) (c1 + sqrt(xi) c2) 
	 *  = b1^2 + xi b2^2 - 4 a1 c1 - 4 xi a2 c2 + sqrt(xi) [2 b1 b2 - 4 a1 c2 -4 a2 c1]
	 * Delta = Delta1 + sqrt(xi) Delta2 */
	mqi_coef_t Delta1, Delta2;
	mqi_coef_list_init_dynamic (mqi_poly_typeof(content1), Delta1, Delta2, NULL);

	if ( !mqi_poly_is_zero(content1) )
		mqi_coef_linexpr2 (Delta1,2,	1,POLY(content1,1),POLY(content1,1),	-4,POLY(content1,2),POLY(content1,0));
	if ( !mqi_poly_is_zero(content2) ) {
		mqi_coef_cpy (coef, Delta1);
		mqi_coef_linexpr3(Delta1,2,	1,xi,POLY(content2,1),POLY(content2,1),	  -4,xi,POLY(content2,2),POLY(content2,0));
		mqi_coef_add (Delta1, Delta1, coef);
	}
	if ( !mqi_poly_is_zero(content1) && !mqi_poly_is_zero(content2) )
		mqi_coef_linexpr2 (Delta2,3,	2,POLY(content1,1),POLY(content2,1),	-4,POLY(content1,2),POLY(content2,0),
					-4,POLY(content2,2),POLY(content1,0));
	
	if ( qi_coef_sign(Delta1, Delta2, xi) < 0 ) {	 
		/** If Delta < 0
		  * The content correspond to the two complex lines
		  * we consider instead the primitive part of poly (ie poly =
		  * coeff*prim_part*content) which we recall content for simplicity */
		mqi_poly_cpy (content1, primpart1);
		mqi_poly_cpy (content2, primpart2);

		/** We redefine Delta as before */
		mqi_coef_set_schar (Delta1, 0);
		mqi_coef_set_schar (Delta2, 0);

		if ( !mqi_poly_is_zero(content1) )
			mqi_coef_linexpr2 (Delta1,2,	1,POLY(content1,1),POLY(content1,1),	-4,POLY(content1,2),POLY(content1,0));
		if ( !mqi_poly_is_zero(content2) ) {
			mqi_coef_cpy (coef, Delta1);
			mqi_coef_linexpr3(Delta1,2,	1,xi,POLY(content2,1),POLY(content2,1),	  -4,xi,POLY(content2,2),POLY(content2,0));
			mqi_coef_add (Delta1, Delta1, coef);
		}
		if ( !mqi_poly_is_zero(content1) && !mqi_poly_is_zero(content2) )
			mqi_coef_linexpr2 (Delta2,3,	2,POLY(content1,1),POLY(content2,1),	-4,POLY(content1,2),POLY(content2,0),
					-4,POLY(content2,2),POLY(content1,0));
		
	}else{

		/* If (u,v) and (s,t) haven't been exchanged in poly (ie. content and
		 primpart haven't been exchanged) then the solutions of content are in (s,t)
		 and we have to exchange (u,v) and (s,t) in the parameterization of the
		 quadric (2,2)	 */
		mqi_surface_param_t stmp;

		mqi_surface_param_init_cpy (stmp, s1);
		mqi_surface_param_exchange_xwst (s1, stmp);
		
		mqi_surface_param_clear(stmp);
		mqi_surface_param_init_cpy (stmp, s2);
		mqi_surface_param_exchange_xwst (s2, stmp);

		/** Free local memory */
		mqi_surface_param_clear(stmp);

	}

	/* Take out the square factor from Delta1+sqrt(xi). Delta2 */
	mqi_coef_t cofactor, delta;

	mqi_coef_cpy (coef, Delta1);
	mqi_coef_gcd (coef, coef, Delta2);

	mqi_coef_init_dynamic (delta, mqi_coef_typeof(Delta1));
	qi_coef_extract_square_factors (delta, cofactor, coef); /* "delta" contains the square factor */

	if ( mqi_coef_cmp_schar(delta,1) > 0 ) {

		mqi_coef_cpy (coef, delta);
		mqi_coef_mul (coef, coef, coef);

		mqi_coef_divexact (Delta1, Delta1, coef);
		mqi_coef_divexact (Delta2, Delta2, coef);

	}

	mqi_hpoly_t Delta1h, Delta2h;
	mqi_hpoly_init_dynamic (Delta1h, mqi_coef_typeof(Delta1));
	mqi_hpoly_init_dynamic (Delta2h, mqi_coef_typeof(Delta2));

	mqi_poly_set_at (Delta1h,0,  Delta1);
	mqi_poly_set_at (Delta2h,0,  Delta2);

	/* Recall the parameterization of the quadric 22 : s = s1 + sqrt(xi) s2    
	* and the quadratic homogeneous polynomial : content  = content1 + sqrt(xi) content2
	* =  (content1[2] + sqrt(xi). content2[2]). u^2 + (content1[1] +
	* sqrt(xi). content2[1]). u.v + (content1[0] + sqrt(xi). content2[0]). v^2

	* Compute the output  parameterized curve : 
	*                     c = c1 + sqrt(xi). c2 + eps. sqrt(Delta). (c3 + sqrt(xi). c4)
	* If the coeff of u^2 is not zero then : 
	* c1 = s1(u = -content1[1], v = 2.content1[2]) + xi. s2(u = -content2[1], v =
	*     2.content2[2]) 
	* c2 = s1(u = -content2[1], v = 2.content2[2]) + s2(u = -content1[1], v =
	*     2.content1[2]) 
	* c3 = s1(u = 1, v = 0) 
	* c4 = s2(u = 1, v = 0) 
	* If the coeff of v^2 is not zero then : 
	* c1 = s1(u = 2.content1[0], v = -content1[1]) + xi. s2(u = 2.content2[0], v =
	*     -content2[1]) 
	* c2 = s1(u = 2.content2[0], v = -content2[1]) +      s2(u = 2.content1[0], v =
	*     -content1[1]) 
	* c3 = s1(u = 0, v = 1) 
	* c4 = s2(u = 0, v = 1) 
	* else is the coeffs of u^2 and v^2 are 0 then the two lines are 
	*       c= s1(u = 0, v = 1) + sqrt(xi) s2(u = 0, v = 1)
	* and c= s1(u = 1, v = 0) + sqrt(xi) s2(u = 1, v = 0)	 */
	mqi_curve_param_t c1, c2, c3, c4, c_tmp;

	if ( (!mqi_poly_is_zero(content1) && (mqi_coef_sign(POLY(content1,2)) != 0))
		||
	     (!mqi_poly_is_zero(content2) && (mqi_coef_sign(POLY(content2,2)) != 0)))
		{
		

			/* if coeff of u^2 is not 0 */
			/* solutions of content are (u,v) = (-b+-sqrt(Delta) , 2a) */
			if ( !mqi_poly_is_zero(content1) ) {

				mqi_hpoly_t htmp1, htmp2;

				/** $$ This comment is very obscure. Obscure terms are set between quotes */
				/** Otherwise "crash" when is zero and "call" content1[i] */
				mqi_curve_param_init_dynamic (c1, mqi_surface_param_n_equations(s1), mqi_surface_param_typeof(s1));
				mqi_curve_param_init_dynamic (c2, mqi_surface_param_n_equations(s2), mqi_surface_param_typeof(s2));
				
				mqi_coef_neg (POLY(content1,1), POLY(content1,1));
				mqi_coef_mul_schar (POLY(content1,2), POLY(content1,2), 2);

				/** $$ Surface param evaluation in two points not yet implemented */
				/** $$ I use constant polynomials instead ! */
				mqi_hpoly_list_init_dynamic(mqi_surface_param_typeof(s1), htmp1, htmp2, NULL);
				mqi_poly_set_at (htmp1,0, POLY(content1,1));
				mqi_poly_set_at (htmp2,0, POLY(content1,2));

				mqi_surface_param_eval_poly (c1, s1, htmp1, htmp2);
				mqi_surface_param_eval_poly (c2, s2, htmp1, htmp2);

				/** Free local memory */
				mqi_poly_list_clear(htmp1, htmp2, NULL);

			}
			
			if ( !mqi_poly_is_zero(content2) ) {

				mqi_hpoly_t htmp1, htmp2;

				mqi_curve_param_init_dynamic (c_tmp, mqi_surface_param_n_equations(s2), mqi_surface_param_typeof(s1));
					
				mqi_coef_neg (POLY(content2,1), POLY(content2,1));
				mqi_coef_mul_schar (POLY(content2,2), POLY(content2,2), 2);

				/** $$ Surface param evaluation in two points not yet implemented */
				/** $$ Same remark: I use constant polynomials */
				mqi_hpoly_list_init_dynamic(mqi_surface_param_typeof(s1), htmp1, htmp2, NULL);
				mqi_poly_set_at (htmp1,0, POLY(content2,1));
				mqi_poly_set_at (htmp2,0, POLY(content2,2));
				
				mqi_surface_param_eval_poly (c_tmp, s2, htmp1, htmp2);				

				mqi_curve_param_mul_coef (c_tmp, c_tmp, xi);
				mqi_curve_param_add (c1, c1, c_tmp);

				mqi_surface_param_eval_poly (c_tmp, s1, htmp1, htmp2);

				mqi_curve_param_add (c2, c2, c_tmp);

				/** Free local memory */
				mqi_poly_list_clear(htmp1, htmp2, NULL);

			}

			mqi_coef_set_schar (coef, 0);
	
			mqi_curve_param_init_dynamic (c3, mqi_surface_param_n_equations(s1), mqi_surface_param_typeof(s1));
			mqi_curve_param_init_dynamic (c4, mqi_surface_param_n_equations(s2), mqi_surface_param_typeof(s2));
			
			/** $$ Surface param evaluation in two points not yet implemented */
			mqi_hpoly_t htmp1, htmp2;
			mqi_hpoly_list_init_dynamic(mqi_surface_param_typeof(s1), htmp1, htmp2, NULL);

			mqi_poly_set_at (htmp1,0, delta);
			mqi_poly_set_at (htmp2,0, coef);
			mqi_surface_param_eval_poly (c3, s1, htmp1, htmp2);
			mqi_surface_param_eval_poly (c4, s2, htmp1, htmp2);

			/** Free local memory */
			mqi_poly_list_clear(htmp1, htmp2, NULL);

	}
	else if ( (!mqi_poly_is_zero(content1) && (mqi_coef_sign(POLY(content1,0)) != 0))
		||
	     (!mqi_poly_is_zero(content2) && (mqi_coef_sign(POLY(content2,0)) != 0)))
		{

			/* if coeff of v^2 is not 0 */
			/* solutions of content are (u,v) = (2c, -b+-sqrt(Delta)) */
			if ( !mqi_poly_is_zero(content1) ) {

				mqi_hpoly_t htmp1, htmp2;

				/** $$ This comment is very obscure. Obscure terms are set between quotes */
				/** Otherwise "crash" when is zero and "call" content1[i] */
				mqi_curve_param_init_dynamic (c1, mqi_surface_param_n_equations(s1), mqi_surface_param_typeof(s1));
				mqi_curve_param_init_dynamic (c2, mqi_surface_param_n_equations(s2), mqi_surface_param_typeof(s2));
				
				mqi_coef_neg (POLY(content1,1), POLY(content1,1));
				mqi_coef_mul_schar (POLY(content1,0), POLY(content1,0), 2);

				/** $$ Surface param evaluation in two points not yet implemented */
				/** $$ I use constant polynomials instead ! */
				mqi_hpoly_list_init_dynamic(mqi_surface_param_typeof(s1), htmp1, htmp2, NULL);
				mqi_poly_set_at (htmp1,0, POLY(content1,1));
				mqi_poly_set_at (htmp2,0, POLY(content1,0));

				mqi_surface_param_eval_poly (c1, s1, htmp2, htmp1);
				mqi_surface_param_eval_poly (c2, s2, htmp2, htmp1);

				/** Free local memory */
				mqi_poly_list_clear(htmp1, htmp2, NULL);

			}
			
			if ( !mqi_poly_is_zero(content2) ) {

				mqi_hpoly_t htmp1, htmp2;

				mqi_curve_param_init_dynamic (c_tmp, mqi_surface_param_n_equations(s2), mqi_surface_param_typeof(s1));
					
				mqi_coef_neg (POLY(content2,1), POLY(content2,1));
				mqi_coef_mul_schar (POLY(content2,0), POLY(content2,0), 2);

				/** $$ Surface param evaluation in two points not yet implemented */
				/** $$ Same remark: I use constant polynomials */
				mqi_hpoly_list_init_dynamic(mqi_surface_param_typeof(s1), htmp1, htmp2, NULL);
				mqi_poly_set_at (htmp1,0, POLY(content2,1));
				mqi_poly_set_at (htmp2,0, POLY(content2,0));
				
				mqi_surface_param_eval_poly (c_tmp, s2, htmp2, htmp1);				

				mqi_curve_param_mul_coef (c_tmp, c_tmp, xi);
				mqi_curve_param_add (c1, c1, c_tmp);

				mqi_surface_param_eval_poly (c_tmp, s1, htmp2, htmp1);

				mqi_curve_param_add (c2, c2, c_tmp);

				/** Free local memory */
				mqi_poly_list_clear(htmp1, htmp2, NULL);

			}

			mqi_coef_set_schar (coef, 0);
	
			mqi_curve_param_init_dynamic (c3, mqi_surface_param_n_equations(s1), mqi_surface_param_typeof(s1));
			mqi_curve_param_init_dynamic (c4, mqi_surface_param_n_equations(s2), mqi_surface_param_typeof(s2));
			
			/** $$ Surface param evaluation in two points not yet implemented */
			mqi_hpoly_t htmp1, htmp2;
			mqi_hpoly_list_init_dynamic(mqi_surface_param_typeof(s1), htmp1, htmp2, NULL);

			mqi_poly_set_at (htmp1,0, delta);
			mqi_poly_set_at (htmp2,0, coef);
			mqi_surface_param_eval_poly (c3, s1, htmp1, htmp2);
			mqi_surface_param_eval_poly (c4, s2, htmp1, htmp2);

			/** Free local memory */
			mqi_poly_list_clear(htmp1, htmp2, NULL);


	}else {

		/* if coeff of u^2 and v^2 are  0
		* solutions are (u,v) = (0, 1) and (1, 0)
		* The two lines are c= s1(u = 0, v = 1) + sqrt(xi) s2(u = 0, v = 1)
		* and c= s1(u = 1, v = 0) + sqrt(xi) s2(u = 1, v = 0)
		* That is  c1 + sqrt(xi) c2 
		* and       c1b + sqrt(xi) c2b */
		mqi_curve_param_t c1b, c2b;
		mqi_coef_t coef2;

		mqi_coef_set_schar (coef, 0);
		mqi_coef_init_cpy (coef2, coef);
		mqi_coef_set_schar (coef2, 1);

		mqi_curve_param_init_dynamic (c1,  mqi_surface_param_n_equations(s1), mqi_surface_param_typeof(s1));
		mqi_curve_param_init_dynamic (c2,  mqi_surface_param_n_equations(s2), mqi_surface_param_typeof(s2));
		mqi_curve_param_init_dynamic (c1b, mqi_surface_param_n_equations(s1), mqi_surface_param_typeof(s1));
		mqi_curve_param_init_dynamic (c2b, mqi_surface_param_n_equations(s2), mqi_surface_param_typeof(s2));

		mqi_hpoly_t htmp1, htmp2;

		mqi_hpoly_list_init_dynamic(mqi_surface_param_typeof(s1), htmp1, htmp2, NULL);
		mqi_poly_set_at (htmp1,0, coef);
		mqi_poly_set_at (htmp2,0, coef2);
		mqi_surface_param_eval_poly (c1, s1, htmp1, htmp2);
		mqi_surface_param_eval_poly (c2, s2, htmp1, htmp2);
		mqi_surface_param_eval_poly (c1b, s1, htmp2, htmp1);
		mqi_surface_param_eval_poly (c2b, s2, htmp2, htmp1);

		qi_curve_param_optimize_gcd (c1, c2);
		qi_curve_param_optimize_gcd (c1b, c2b);
		
		qi_inter_component_init (comp, QI_INTER_COMPONENT_3, c1, c2, xi);
		qi_inter_component_set_type (comp, QI_INTER_COMPONENT_LINE);

		qi_inter_add_component (rop, comp);
		/* discard comp */
		qi_inter_component_clear(comp);

		qi_inter_component_init (comp, QI_INTER_COMPONENT_3, c1b, c2b, xi);
		qi_inter_component_set_type (comp, QI_INTER_COMPONENT_LINE);

		qi_inter_add_component (rop, comp);
		/* discard comp */
		qi_inter_component_clear(comp);

		qi_inter_set_optimal (rop);
	
		/** Free local memory */
		mqi_curve_param_list_clear(c1b, c2b, NULL);
		mqi_coef_clear(coef2);
		mqi_poly_list_clear(htmp1, htmp2, NULL);

	}

	/** $$ Careful with this "is_one" stuff. I simulate it using a little trick */
	if ( (mqi_poly_degree(Delta1h) == 0) && (mqi_coef_cmp_schar(POLY(Delta1h,0),1) == 0) &&
	     (mqi_poly_highest_x_degree(Delta1h) == 0) && mqi_poly_is_zero(Delta2h) )
	{

		mqi_curve_param_t ca, cb;
		mqi_curve_param_init_cpy (ca, c1);
		mqi_curve_param_init_cpy (cb, c1);

		mqi_curve_param_add (ca, ca, c3);
		mqi_curve_param_sub (cb, cb, c3);

		qi_curve_param_optimize (ca);
		qi_curve_param_optimize (cb);

		qi_inter_component_init (comp, QI_INTER_COMPONENT_2, ca);
		qi_inter_component_set_type (comp, QI_INTER_COMPONENT_LINE);
		qi_inter_add_component (rop, comp);
		/* discard comp */
		qi_inter_component_clear(comp);

		qi_inter_component_init (comp, QI_INTER_COMPONENT_2, cb);
		qi_inter_component_set_type (comp, QI_INTER_COMPONENT_LINE);
		qi_inter_add_component (rop, comp);
		/* discard comp */
		qi_inter_component_clear(comp);

		/** Free local memory */
		mqi_curve_param_list_clear(ca, cb, NULL);


	}else if ( mqi_coef_cmp_schar(xi,1) == 0 ) {

		/** the 2 lines are c1 + sqrt(Delta1h[0]) c3 and c1 - sqrt(Delta1h[0]) c3 */
		qi_curve_param_optimize_gcd (c1, c3);

		/** $$ Return value ignored in function returning non void ! Why ? */
		/**qi_param_line_improved1 (c1, c3, POLY(Delta1h,0)); */

		qi_inter_create_components (rop, 2, QI_INTER_COMPONENT_LINE,
					    QI_INTER_COMPONENT_3, c1, c3, POLY(Delta1h,0));

	}else {

		/* Output parameterized curve : c = c1 + sqrt(xi). c2 +
		* eps. sqrt(Delta). (c3 + sqrt(xi). c4) 
		* Delta = Delta1 + sqrt(xi). Delta2 */
		qi_param_line_improved3 (c1, c2, c3, c4, xi, POLY(Delta1h,0), POLY(Delta2h,0));

		qi_inter_create_components (rop, 2, QI_INTER_COMPONENT_LINE,
					    QI_INTER_COMPONENT_6, c1, c2, xi, c3, c4, POLY(Delta1h,0), POLY(Delta2h,0));

	}

	qi_inter_set_optimal (rop);

	/** Free local memory */
	mqi_coef_list_clear(cofactor, delta, Delta1, Delta2, norm, norm_htmp, NULL);
	mqi_hhpoly_list_clear(poly1_cp, poly2_cp, NULL);
	mqi_surface_param_list_clear(s1, s2, NULL);
	mqi_curve_param_list_clear(c1, c2, c3, c4, c_tmp, NULL);
	mqi_poly_list_clear(Delta1h, Delta2h, content1, content2, primpart1, primpart2, NULL);


}

/** Cubic and line */
/* Compute the parameterization of a cubic and a line
 Input : initial quadrics q1, q2 (only for checking)
         Parameterization par of a quadric 22 in the pencil
         Associated biquadratic equation poly = 0
 Output : print of the parameterization of the intersection curves 
 (one curve_param for the cubic and one for the line) */
void __qi_inter_with_22_cubic_line (qi_inter_t rop, mqi_mat_t __IN q1, mqi_mat_t __IN q2,  mqi_surface_param_t __IN par_cp,
				    mqi_hhpoly_t __IN poly_cp, int case_flag)
{

	mqi_hhpoly_t		poly, prim_part, hhtmp;
	mqi_surface_param_t	par;
	mqi_hpoly_t		content, uu, vv;
	mqi_curve_param_t	line, cubic;
	qi_inter_component_t	comp;
	qi_inter_cut_t		cut;
	unsigned char		exchange_flag = 0;

	/** Two components */
	/*mqi_inter_init (rop, 2);*/
	qi_inter_init(rop);

	if ( case_flag == 3 )
		qi_inter_set_type (rop,	12,1);
	else
		qi_inter_set_type (rop,	12,2);

	mqi_hhpoly_init_cpy (poly, poly_cp);
	mqi_surface_param_init_cpy (par, par_cp);

	/** A linear term "content" can be factored in poly: */
	mqi_hhpoly_content_poly (content, poly);

	if ( mqi_poly_degree(content) < 1 ) {

		exchange_flag = 1;
		/** exchange_xyzw does *not* support passing result = parameter, i.e.
		    the parameter pair "(poly, poly)" */
		mqi_hhpoly_init_cpy (hhtmp, poly);
		mqi_hhpoly_exchange_xwst (poly, hhtmp);
		mqi_hhpoly_content_poly (content, poly);

	}
	
	/* The primitive part of poly is (ie poly = prim_part*content) : */
	/* discard old hhtmp */
	mqi_hhpoly_clear(hhtmp);
	qi_hhpoly_pp_hpoly (hhtmp, poly, content);

	 /* prim_part = u^2.prim_part[2](s,t) + uv.prim_part[1](s,t) + v^2.prim_part[0](s,t) 
	  where the prim_part[i](s,t) are of degree 1
	  We exchange (u,v) and (s,t) in  prim_part : */
	mqi_hhpoly_exchange_xwst (prim_part, hhtmp); /** Don't pass the two same arguments ! */

	/* prim_part = u.prim_part[1](s,t) + v.prim_part[0](s,t) 
	 where the prim_part[i](s,t) are of degree 2


	 We also  exchange (u,v) and (s,t) in  the surface param par if 
	 the exhange of (u,v) and (s,t) has been done only once in poly/prim_part */
	if ( !exchange_flag ) {

		mqi_surface_param_t partmp;

		mqi_surface_param_init_cpy (partmp, par);
		mqi_surface_param_exchange_xwst (par, partmp);

		/** Free local memory */
		mqi_surface_param_clear(partmp);

	}

	/* We now replace u=prim_part[0] and v=-prim_part[1] in the surface parameterization */
	mqi_poly_init_cpy (uu, HHPOLY(prim_part,0));
	mqi_poly_init_cpy (vv, HHPOLY(prim_part,1));
	
	mqi_poly_neg (vv, vv);

	mqi_curve_param_init_dynamic (cubic, mqi_surface_param_n_equations(par), mqi_surface_param_typeof(par));
	mqi_surface_param_eval_poly  (cubic, par, uu, vv);

	/* The content part of poly is content = s.content[1] + t.content[0]
	 * We replace s=content[0] and t=-content[1] in the surface parameterization
	 * where (u,v) and (s,t) have to be exchanged if content is the content of poly 
	 * (vs. of the content of exchange_uv_st(poly)); this has already been done at 
	 * if (!exchange_flag)  par = exchange_uv_st(par); */
	mqi_curve_param_init_dynamic (line, mqi_surface_param_n_equations(par), mqi_surface_param_typeof(par));
	mqi_coef_neg (POLY(content,1), POLY(content,1));

	/** $$ !! */
	/** $$ Is it the constant polynomial built from content[0] and the one built from -content[1] ? */
	/** $$ We use the following trick: evaluate in 2 constant polynomials is equivalent to evaluate in 2 points */
/*	mqi_surface_param_eval_hpoly	     (line, par, POLY(content,0), POLY(content,1));*/
	mqi_poly_set_at (uu,0, POLY(content,0));
	mqi_poly_set_at (vv,0, POLY(content,1));
	mqi_surface_param_eval_poly (line, par, uu, vv);

	qi_curve_param_optimize (cubic);
	qi_curve_param_optimize (line);

	/** $$TODO: Cut parameters of the lines ? */

	qi_inter_component_init		(comp, QI_INTER_COMPONENT_2, cubic);
	qi_inter_component_set_type	(comp, QI_INTER_COMPONENT_CUBIC);
	qi_inter_add_component		(rop, comp);
	/* discard comp */
	qi_inter_component_clear(comp);

	qi_inter_component_init		(comp, QI_INTER_COMPONENT_2, line);
	qi_inter_component_set_type	(comp, QI_INTER_COMPONENT_LINE);
	qi_inter_add_component		(rop, comp);
	/* discard comp */
	qi_inter_component_clear(comp);

	qi_inter_set_optimal (rop);

	if ( case_flag == 3 ) {
	
		mqi_coef_t coef;

		mqi_coef_init_dynamic (coef, mqi_poly_typeof(uu));
		mqi_coef_set_schar    (coef, 0);
		
		qi_inter_cut_init (cut, QI_INTER_CUT_0, coef, coef);
		
		qi_inter_component_add_cut_parameter (rop->components[0], cut);
		qi_inter_cut_set_owner(cut, rop->components[0]);

		/* Free local memory */
		mqi_coef_clear(coef);

	}


	/** Free local memory */
	mqi_hhpoly_list_clear(poly, prim_part, hhtmp, NULL);
	mqi_surface_param_clear(par);
	mqi_poly_list_clear(content, uu, vv, NULL);
	mqi_curve_param_list_clear(line, cubic, NULL);

}


/** Input : initial quadrics q1, q2
 * determinental equation of the pencil : det_p
 * a quadric 22  q
 * Output : a quadric 22  quadric_sol and a point on it point_sol
 * the determinant a^2.b of the quadric 22 in the form [a,b]
 *
 * This function is rather special, as it uses MPFR directly to adjust
 * the floatting point precision.
 * $$ This is the reason why MPFR support is needed to compile libqi. */
/** $$ Parameters should be initialized automatically ? */
void qi_inter_find_quadric_22_and_point (mqi_mat_t __INIT quadric_sol, mqi_vect_t __INIT point_sol, mqi_coef_t __INIT det_R, mqi_vect_t __INIT det_sol,
					 mqi_mat_t __IN q1, mqi_mat_t __IN q2,
					 mqi_hpoly_t __IN det_p,
					 mqi_mat_t __IN q_cp)
{
	mqi_mat_t		q, tm_float, axis, tm;
	mqi_vect_t		coeffs_sol, m_sols, n_sols, real_point, mn_sol, approximated_point, tmp3;
	mqi_coef_t		sol_a2k, a, b, c, Delta, real_point_max_abs, coef, coefz;
	mqi_coef_t		det_new_R, tmp_bi, cx, cy, gcd_c, multiply_factor, multiply_factor_current_precision, tmp_fr;
	unsigned int		increment = 0;
	unsigned int		k = 0, k_current_precision;
	unsigned char		not_found = 1;
	mp_prec_t		bigfloat_precision;
	unsigned short		n_digits = 5;
	unsigned short		i, j, n, sol_index, floor_ceil;
	int 			clear_tm;

	/* axis is a 4x2 matrix such that axis.(m,n) is a parametrization of one of the
  	 * 6 axis in P^3, with (m,n) in P^1. axis is initialized to 0 */

	/** $$ The original code specifies explicitly to initialize axis to a zero matrix.
	       Is it useful ? */
	mqi_mat_init_cpy(q, q_cp);

	mqi_mat_init_mpfr_t (axis, 4, 2);

	mqi_mat_init_mpfr_t  (tm_float, 4, 4);
	mqi_coef_list_init_mpfr_t (a, b, c, Delta, coef, real_point_max_abs, tmp_fr, NULL);
	mqi_coef_list_init_mpz_t  (sol_a2k, det_new_R, tmp_bi, cx, cy, gcd_c, coefz, multiply_factor,
					multiply_factor_current_precision, NULL);
	mqi_vect_list_init_mpfr_t (2, m_sols, n_sols, mn_sol, NULL);
	mqi_vect_init_mpfr_t 	  (real_point, 4);
	mqi_vect_list_init_mpz_t  (4, approximated_point, tmp3, NULL);

	mqi_vect_init_mpz_t (coeffs_sol, 2);

	/** multiply_factor = 10^k
	 *  k stores the actual exponent of multiply_factor to avoid
	 *  calculating 10^k at each loop or doing a logarithmic
	 *  operation to retreive the current exponent. */
	k = 0;

	mqi_coef_set_schar (multiply_factor, 1);

	/* Initialize memory for the output */
	mqi_coef_init_dynamic(det_R, mqi_mat_typeof(q1));
	mqi_mat_init_dynamic(quadric_sol, mqi_mat_nrows(q1), mqi_mat_ncols(q1),
				mqi_mat_typeof(q1));
	mqi_vect_init_dynamic(point_sol, mqi_vect_size(approximated_point),
				mqi_vect_typeof(approximated_point));
	mqi_vect_init_dynamic(det_sol, 2, mqi_mat_typeof(q1));	

	/** The LiDIA's default precision is 5 decimal digits for the mantissa.
	 *  As we're using mpfr, we have to convert this into a number of bits.
	 *  According the the LiDIA's documentation, the equivalent precision is:
	 *
	 *  p = ceil ( 5 * ln(10) / ln(2) ) + 3 = 20
	 *
	 * */
	bigfloat_precision = 20;

#ifdef DEBUG
	mqi_log_printf ("Initial q:\n");
	mqi_mat_print  (q);
	mqi_log_printf ("\n");
#endif

	while ( not_found ) {

#ifdef DEBUG
	mqi_log_printf ("Approximated point: \033[30G"); mqi_vect_print(approximated_point);
	mqi_log_printf ("\nReal point: \033[30G"); mqi_vect_print(real_point);
	mqi_log_printf ("\n");
#endif

		mpfr_set_default_prec (bigfloat_precision);
	
		k_current_precision = k;
		mqi_coef_cpy (multiply_factor_current_precision, multiply_factor);

		clear_tm = 0;

		if ( increment == 1 ) {

#ifdef TRACEFUNC
			tracefunc("increment = 1");
#endif

			mqi_mat_t tmpmat;

			/** Replace q by its canonical form */
			mqi_mat_init_cpy(tmpmat,q);
			/* discard old q */
			mqi_mat_clear(q);
			qi_mat_gauss (q, tmpmat, tm);
			clear_tm = 1;

			k_current_precision = 0;
			mqi_coef_set_schar (multiply_factor_current_precision, 1);
	
			/** Free local memory */
			mqi_mat_clear(tmpmat);

		}

		if ( increment ) {
			mqi_mat_dynamic_cast (tm_float, tm);
		}

		if ( clear_tm ) mqi_mat_clear(tm);
#ifdef DEBUG
		mqi_log_printf ("Gaussian form of q:\n");
		mqi_mat_print(q);
		mqi_log_printf("\n");
#endif

		for ( i = 0; i < 4; i++ ) {
			mqi_mat_set_at_int (axis, i,0, 1);
			for ( j = i + 1; j < 4; j++ ) {
				mqi_mat_set_at_int (axis,j,1, 1);
				/* axis.(m,n) = (m,n,0,0), (m,0,n,0), (m,0,0,n), (0,m,n,0),
				*               * (0,m,0,n) or (0,0,m,n) 
				*
				*                             * (axis.(m,n))^t . q . axis.(m,n) = a.m^2 + 2.b.m.n +c.n^2 */
				mqi_coef_dynamic_cast (a, MAT(q,i,i));
				mqi_coef_dynamic_cast (b, MAT(q,i,j));
				mqi_coef_dynamic_cast (c, MAT(q,j,j));
				mqi_coef_linexpr2 (Delta,2,	1,b,b,	-1,a,c);

#ifdef DEBUG
				mqi_log_printf ("(delta, a, b, c) = (");
				mqi_coef_print(Delta); mqi_log_printf(", ");
				mqi_coef_print(a); mqi_log_printf(", ");
				mqi_coef_print(b); mqi_log_printf(", ");
				mqi_coef_print(c);
				mqi_log_printf(")\n");
#endif

				if ( (mqi_coef_sign(Delta) >= 0) && ( mqi_coef_sign(a) || mqi_coef_sign(b) || mqi_coef_sign(c) ) )
				{

					/** Compute the solutions (m,n) of the 2nd degree equation */
					if ( mqi_coef_sign(a) ) {
						mqi_coef_sqrt (coef, Delta);
						mqi_coef_sub  (VECT(m_sols,0), coef, b);
						mqi_coef_add  (VECT(m_sols,1), coef, b);
						mqi_coef_neg  (VECT(m_sols,1), VECT(m_sols,1));
						mqi_vect_set_at  (n_sols,0, a);
						mqi_vect_set_at  (n_sols,1, a);
					}else{
						mqi_vect_set_at_int (m_sols, 0, 1);
						mqi_coef_neg  (VECT(m_sols,1), c);
						mqi_vect_set_at_int (n_sols, 0, 0);
						mqi_coef_mul_int (VECT(n_sols,1), b, 2);
					}
			
#ifdef DEBUG
			mqi_log_printf ("m_sols: "); mqi_vect_print(m_sols); mqi_log_printf("\n");
			mqi_log_printf ("n_sols: "); mqi_vect_print(n_sols); mqi_log_printf("\n");
#endif
					for ( sol_index = 0; sol_index < 2; sol_index++ )
					{

						/* real_point =
						* (axis.(m_sols[sol_index],n_sols[sol_index])) +
						* normalization ou  tm_float * axis * (m_sols[sol_index],
						* n_sols[sol_index]) + normalization */
						mqi_vect_set_at (mn_sol, 0, VECT(m_sols,sol_index));
						mqi_vect_set_at (mn_sol, 1, VECT(n_sols,sol_index));
#ifdef DEBUG
						mqi_log_printf ("mn_sol: "); mqi_vect_print(mn_sol); mqi_log_printf("\n");
						mqi_log_printf ("axis:   "); mqi_mat_print(axis);
						mqi_log_printf ("\n");
#endif
						mqi_vect_print(real_point);
						mqi_mat_mul  ((mqi_mat_ptr)real_point, axis, (mqi_mat_ptr)mn_sol);

						if ( increment ) /** if increment > 0 */
						{
							mqi_mat_mul ((mqi_mat_ptr)real_point, tm_float, (mqi_mat_ptr)real_point);
						}

						mqi_coef_abs (real_point_max_abs, VECT(real_point,0));

						mqi_coef_abs (coef, VECT(real_point,1));
						if ( mqi_coef_cmp (real_point_max_abs, coef) < 0 )
							mqi_coef_cpy (real_point_max_abs, coef);
						mqi_coef_abs (coef, VECT(real_point,2));
						if ( mqi_coef_cmp (real_point_max_abs, coef) < 0 )
							mqi_coef_cpy (real_point_max_abs, coef);
						mqi_coef_abs (coef, VECT(real_point,3));
						if ( mqi_coef_cmp (real_point_max_abs, coef) < 0 )
							mqi_coef_cpy (real_point_max_abs, coef);
						
						/** $$ Floatting point division */
						/** As libmqi doesn't support it yet, we need to dive in the sublayer of libmqi,
						    which means to use MPFR's routines and libmqi's data structures directly. */
						
						/* Avoid a division by zero */
						if ( mqi_coef_sign(real_point_max_abs) )
						{
						  for ( n = 0; n < 4; n++ )
						  {
							/** The compiler does the cast to "mpfr_ptr" automatically, and it is
							 *  correct as we know that each variable is typed mpfr_t */
							mpfr_div (mqi_coef_get_mpfr_t(VECT(real_point,n)),
								  mqi_coef_get_mpfr_t(VECT(real_point,n)), 
								  mqi_coef_get_mpfr_t(real_point_max_abs), MQI_MPFR_RNDMODE);
						  }
						}

						/** Approximation of real_point */
						for ( floor_ceil = 0; floor_ceil < 2; floor_ceil ++ )
						{
							
							k = k_current_precision;
							mqi_coef_cpy (multiply_factor, multiply_factor_current_precision);

							mqi_coef_set_schar (det_new_R, 0);
							
							/** $$ Beware of how the precision is managed */
							/*while ( (mqi_coef_sign(det_new_R) <= 0) && (k <= bigfloat_precision) ) 
							 */
							while ( (mqi_coef_sign(det_new_R) <= 0) && (k <= n_digits) )
							{

								/** We need to have the multiply_factor as a mpfr_t, because
								 *  libmqi 0.2.0 doesn't support crosstypes arithmetic yet. */
								mqi_coef_dynamic_cast (tmp_fr, multiply_factor);
								
								if ( !floor_ceil ) {

									mqi_coef_mul 	(VECT(real_point,0), VECT(real_point,0), tmp_fr);
									mqi_coef_floor	(VECT(real_point,0), VECT(real_point,0));
									mqi_coef_dynamic_cast (VECT(approximated_point,0), VECT(real_point,0));

									mqi_coef_mul 	(VECT(real_point,1), VECT(real_point,1), tmp_fr);
									mqi_coef_floor	(VECT(real_point,1), VECT(real_point,1));
									mqi_coef_dynamic_cast (VECT(approximated_point,1), VECT(real_point,1));
									
									mqi_coef_mul 	(VECT(real_point,2), VECT(real_point,2), tmp_fr);
									mqi_coef_floor	(VECT(real_point,2), VECT(real_point,2));
									mqi_coef_dynamic_cast (VECT(approximated_point,2), VECT(real_point,2));
									
									mqi_coef_mul 	(VECT(real_point,3), VECT(real_point,3), tmp_fr);
									mqi_coef_floor	(VECT(real_point,3), VECT(real_point,3));
									mqi_coef_dynamic_cast (VECT(approximated_point,3), VECT(real_point,3));

								}else{

									mqi_coef_mul 	(VECT(real_point,0), VECT(real_point,0), tmp_fr);
									mqi_coef_ceil	(VECT(real_point,0), VECT(real_point,0));
									mqi_coef_dynamic_cast (VECT(approximated_point,0), VECT(real_point,0));

									mqi_coef_mul 	(VECT(real_point,1), VECT(real_point,1), tmp_fr);
									mqi_coef_ceil	(VECT(real_point,1), VECT(real_point,1));
									mqi_coef_dynamic_cast (VECT(approximated_point,1), VECT(real_point,1));
									
									mqi_coef_mul 	(VECT(real_point,2), VECT(real_point,2), tmp_fr);
									mqi_coef_ceil	(VECT(real_point,2), VECT(real_point,2));
									mqi_coef_dynamic_cast (VECT(approximated_point,2), VECT(real_point,2));
									
									mqi_coef_mul 	(VECT(real_point,3), VECT(real_point,3), tmp_fr);
									mqi_coef_ceil	(VECT(real_point,3), VECT(real_point,3));
									mqi_coef_dynamic_cast (VECT(approximated_point,3), VECT(real_point,3));

								}
			
#ifdef DEBUG
								mqi_log_printf ("Real point: "); mqi_vect_print(real_point);
								mqi_log_printf ("\n");
#endif
								/* The approximated point is  computed
								* We now compute cx, cy such that 
								* (approx_point)^t. (x.q1 + y.q2). approx_point =
								* cx.x +cy.y */

								/** $$ Here, I suppose that q1 is a matrix of big integers (mpz_t) */
								mqi_mat_mul ((mqi_mat_ptr)tmp3, q1, (mqi_mat_ptr)approximated_point);
								mqi_vect_scalar_product (cx, approximated_point, tmp3);
								
								/** $$ Same supposition */
								mqi_mat_mul ((mqi_mat_ptr)tmp3, q2, (mqi_mat_ptr)approximated_point);
								mqi_vect_scalar_product (cy, approximated_point, tmp3);

								if ( mqi_coef_sign(cx) && mqi_coef_sign(cy) ) {
									mqi_coef_gcd (gcd_c, cx, cy);
									mqi_coef_divexact (cx, cx, gcd_c);
									mqi_coef_divexact (cy, cy, gcd_c);
								}

#ifdef DEBUG
								mqi_log_printf ("cx: "); mqi_coef_print(cx);
								mqi_log_printf("\ncy: "); mqi_coef_print(cy);
								mqi_log_printf("\n");
#endif
								
								/* The quadric through approx_point is -cy.q1 + cx.q2
								* unless cx=cy=0 in which case q1 and q2 contain
								* approx_point; 
								* Determinant of the quadric -cy.q1 + cx.q2
								* through approx_point is: */
								if ( mqi_coef_sign(cx) || mqi_coef_sign(cy) ) {
									mqi_coef_neg (coefz, cy);
									mqi_poly_eval_xw (det_new_R, det_p, coefz, cx);
								}else{ /** cx = cy = 0 */
									mqi_coef_set_schar (cx, 1);
									mqi_coef_neg (coefz, cy);
									mqi_poly_eval_xw (det_new_R, det_p, coefz, cx);
									
									if ( mqi_coef_sign(det_new_R) <= 0 ) {
										mqi_coef_set_schar (cx, 0);
										mqi_coef_set_schar (cy, 1);
										mqi_coef_neg (coefz, cy);
										mqi_poly_eval_xw (det_new_R, det_p, coefz, cx);
									}
									
								}

#ifdef DEBUG
								mqi_log_printf("det_new_R:\n");
								mqi_coef_print(det_new_R);
								mqi_log_printf("\n");
#endif

								if ( mqi_coef_sign(det_new_R) > 0 )
								{

									mqi_coef_t square, cofactor;

#ifdef TRACEFUNC
									tracefunc("det_new_R > 0");
#endif

									qi_coef_extract_square_factors (square, cofactor, det_new_R);
									mqi_coef_mul (tmp_bi, square, multiply_factor);

									if ( not_found == 1 )
									{
										not_found = 0;
										mqi_coef_cpy (VECT(det_sol,0), square);
										mqi_coef_cpy (VECT(det_sol,1), cofactor);
										mqi_coef_cpy (det_R, det_new_R);
										mqi_coef_cpy (sol_a2k, tmp_bi);
										mqi_coef_neg (VECT(coeffs_sol,0), cy);
										mqi_coef_cpy (VECT(coeffs_sol,1), cx);
										mqi_vect_cpy (point_sol, approximated_point);

									}
									else
									{
										if ( (mqi_coef_cmp(cofactor,VECT(det_sol,1)) < 0) ||
										     ((mqi_coef_cmp(cofactor,VECT(det_sol,1)) == 0) &&
										      mqi_coef_cmp(tmp_bi, sol_a2k) < 0) ) {
										
											mqi_coef_cpy (VECT(det_sol,0), square);
											mqi_coef_cpy (VECT(det_sol,1), cofactor);
											mqi_coef_cpy (det_R, det_new_R);
											mqi_coef_cpy (sol_a2k, tmp_bi);
											mqi_coef_neg (VECT(coeffs_sol,0), cy);
											mqi_coef_cpy (VECT(coeffs_sol,1), cx);
											mqi_vect_cpy (point_sol, approximated_point);

										}

									}

									/** Free local memory */
									mqi_coef_list_clear(square, cofactor, NULL);

								}
								k ++; 
								mqi_coef_mul_schar(multiply_factor, multiply_factor, 10);

							} /** end while */
							
						} /** End for (floor_ceil ... */

					} /** End for (sol_index ... */
	
				} /** End if (delta ... */
				
				mqi_mat_set_at_int (axis, j,1, 0);				

			} /** End for (j = i+1 ... */
		
			mqi_mat_set_at_int (axis, i,0, 0);

		} /** End for (i ... */

		if ( not_found ) /** No quadric22 had been found */
		{

#ifdef TRACEFUNC
			tracefunc("No quadric 22 found, raising bigfloat precision.\n");
#endif
			increment ++; 
			
			if ( increment > 1 ) {
				bigfloat_precision += 37;  /** Obtained applying the same formula as for the init value */
				n_digits += 10;
			}

		}
		else
		{

			mqi_mat_t q1tmp, q2tmp;

#ifdef TRACEFUNC
			tracefunc("Quadric 22 found.\n");
#endif
			/** The matrix associated to p */
			mqi_mat_init_cpy (q1tmp, q1);
			mqi_mat_mul_coef (q1tmp, q1tmp, VECT(coeffs_sol,0));
			mqi_mat_init_cpy (q2tmp, q2);
			mqi_mat_mul_coef (q2tmp, q2tmp, VECT(coeffs_sol,1));
			mqi_mat_add      (quadric_sol, q1tmp, q2tmp);

#ifdef DEBUG
			qi_vect_optimize(coeffs_sol);
			mqi_log_printf ("Ending up using lambda ");
			mqi_vect_print(coeffs_sol);
			mqi_log_printf ("\n");
#endif

			/** Free local memory */
			mqi_mat_list_clear(q1tmp, q2tmp, NULL);

		}


	} /** End while (not_found) */

	qi_vect_optimize (point_sol);
	
	/** Free local memory */
	mqi_mat_list_clear(q, tm_float, axis, NULL);
	mqi_vect_list_clear(coeffs_sol, m_sols, n_sols, real_point, mn_sol, approximated_point, tmp3, NULL);
	mqi_coef_list_clear(sol_a2k, a, b, c, Delta, real_point_max_abs, coef, coefz,
			det_new_R, tmp_bi, cx, cy, gcd_c, multiply_factor, multiply_factor_current_precision, tmp_fr, NULL);

	/* This function seems memory safe:
	 *
	 * Most of the initializations are performed before the main loop and memory is cleared after this loop
	 * Local initializations are performed in local blocks and immediatly cleared at the end of each block.
	 */
}

/** 
 * Input:  a matrix q of inertia 2,2 
 * a point  on it and the determinant of q in the form [a,b] such that det(q)=a^2. b
 * Output : Parameterization of the quadric :   s1 + sqrt(det_q[1]) s2
 * If det_q[1]=1 then  s2=[0,0,0,0] */
void qi_inter_find_parametrization_quadric_22 (mqi_surface_param_t s1, mqi_surface_param_t s2,
					       mqi_mat_ptr __IN q_cp,
					       mqi_vect_ptr __IN point_on_q,
					       mqi_vect_ptr __IN det_q_cp,
					       mqi_coef_ptr __IN det_R)
{
							  
							    
	mqi_mat_t		q, P, Rp, m1, m2;
	mqi_vect_t		det_q, p0, Rp0, v;
	mqi_coef_t		m0, a1, a2, a3, a4, a5, a6, a7, a8, a9, A, B, C;
	mqi_coef_t		l1, l2, l3, cm2;
	mqi_coef_t		alpha, beta, theta;
	mqi_surface_param_t	sp;

	/* TMP DEBUG ONLY */
	mqi_log_printf ("q_cp:"); mqi_mat_print(q_cp); mqi_log_printf("\n");
	mqi_log_printf ("point_on_q: "); mqi_vect_print(point_on_q); mqi_log_printf("\n");
	mqi_log_printf ("det_q_cp: "); mqi_vect_print(det_q_cp); mqi_log_printf("\n");
	mqi_log_printf ("det_R: "); mqi_coef_print(det_R); mqi_log_printf("\n");
	/******************/


	mqi_mat_init_cpy  (q, q_cp);
	mqi_vect_init_cpy (det_q, det_q_cp);
	mqi_vect_init_cpy (p0, point_on_q);

	/* The transformation matrix
	 * First find a point of the canonical basis that is not on the tangent plane to
	 * q at p0 */
	mqi_mat_mul_init ((mqi_mat_ptr)Rp0, q, (mqi_mat_ptr)p0);
	
	/* Try (1,0,0,0), (0,1,0,0), (0,0,1,0) and (0,0,0,1) in that order -- At least
  	 * one of the four points is good */
	mqi_vect_init_dynamic (v, 4, mqi_mat_typeof(q));

	mqi_vect_set_at_int (v,0, 1);
	
	/** $$ I guess we talk about the scalar product here */
	mqi_coef_init_dynamic   (m0, mqi_vect_typeof(Rp0));
	mqi_vect_scalar_product (m0, Rp0, v);

	/* TMP DEBUG ONLY */
	mqi_log_printf ("m0: "); mqi_coef_print(m0); mqi_log_printf("\n");

	if ( !mqi_coef_sign(m0) ) { /** (1,0,0,0) isn't good */

		mqi_vect_set_at_int (v,0, 0);
		mqi_vect_set_at_int (v,1, 1);

		mqi_vect_scalar_product (m0, Rp0, v);
		
		if ( !mqi_coef_sign(m0) ) { /** (0,1,0,0) isn't good */

			mqi_vect_set_at_int (v,1, 0);
			mqi_vect_set_at_int (v,2, 1);

			mqi_vect_scalar_product (m0, Rp0, v);
		
			if ( !mqi_coef_sign(m0) ) { /** (0,0,1,0) isn't good */

				mqi_vect_set_at_int (v,2, 0);
				mqi_vect_set_at_int (v,3, 1);

				mqi_vect_scalar_product (m0, Rp0, v); /** (0,0,0,1) is obviously good then */
			}
		}
	}

	/* TMP DEBUG ONLY */
	mqi_log_printf ("m0 (after retries): "); mqi_coef_print(m0); mqi_log_printf("\n");

	qi_mat_send_to_zw (P, v, p0);

	/* TMP DEBUG ONLY */
	mqi_log_printf ("P: "); mqi_mat_print(P); mqi_log_printf("\n");

	/** The determinant of the transformation matrix */
	mqi_coef_init_dynamic (theta, mqi_mat_typeof(P));
	mqi_mat_det (theta, P);

	/** The transformed matrix */
	mqi_mat_transformation (Rp, q, P, MQI_MAT_NOTRANSCO);

	/* TMP DEBUG ONLY */
	mqi_log_printf ("Rp: "); mqi_mat_print(Rp); mqi_log_printf("\n");

	/** Entries of Rp */
	mqi_coef_init_cpy (a1, MAT(Rp,0,0));
	mqi_coef_init_cpy (a2, MAT(Rp,0,1));
	mqi_coef_init_cpy (a3, MAT(Rp,1,1));
	mqi_coef_init_cpy (a4, MAT(Rp,0,2));
	mqi_coef_init_cpy (a5, MAT(Rp,1,2));
	mqi_coef_init_cpy (a6, MAT(Rp,2,2));
	mqi_coef_init_cpy (a7, MAT(Rp,0,3));
	mqi_coef_init_cpy (a8, MAT(Rp,1,3));
	mqi_coef_init_cpy (a9, MAT(Rp,2,3));

	/** Minors */
	mqi_coef_init_dynamic (l1, mqi_coef_typeof(a1));
	mqi_coef_init_dynamic (l2, mqi_coef_typeof(a6));
	mqi_coef_init_dynamic (l3, mqi_coef_typeof(a2));

	mqi_coef_linexpr2 (l1,2,		1,a1,a9,	-1,a4,a7);
	mqi_coef_linexpr2 (l2,2,		1,a6,a7,	-1,a4,a9);
	mqi_coef_linexpr2 (l3,2,		1,a2,a9,	-1,a5,a7);

	/** The coefficients alpha and beta */
	mqi_coef_init_dynamic (alpha, mqi_coef_typeof(a9));
	mqi_coef_init_dynamic (beta, mqi_coef_typeof(a9));

	mqi_coef_linexpr2 (alpha,2,	1,a9,l1,	1,a7,l2);
	mqi_coef_linexpr2 (beta,2,	1,a9,l3,	1,a8,l2);

	/** The matrices of the result */
	mqi_mat_init_dynamic (m1, 4, 4, mqi_coef_typeof(alpha));
	mqi_mat_init_dynamic (m2, 4, 4, mqi_mat_typeof(m1));

	/* TMP DEBUG ONLY */
	mqi_log_printf ("alpha: "); mqi_coef_print(alpha); mqi_log_printf("\n");
	mqi_log_printf ("beta:  "); mqi_coef_print(beta);  mqi_log_printf("\n");
	mqi_log_printf ("a1: "); mqi_coef_print(a1); mqi_log_printf("\n");
	mqi_log_printf ("a2: "); mqi_coef_print(a2); mqi_log_printf("\n");
	mqi_log_printf ("a3: "); mqi_coef_print(a3); mqi_log_printf("\n");
	mqi_log_printf ("a4: "); mqi_coef_print(a4); mqi_log_printf("\n");
	mqi_log_printf ("a5: "); mqi_coef_print(a5); mqi_log_printf("\n");
	mqi_log_printf ("a6: "); mqi_coef_print(a6); mqi_log_printf("\n");
	mqi_log_printf ("a7: "); mqi_coef_print(a7); mqi_log_printf("\n");
	mqi_log_printf ("a8: "); mqi_coef_print(a8); mqi_log_printf("\n");
	mqi_log_printf ("a9: "); mqi_coef_print(a9); mqi_log_printf("\n");


	/** We have two cases according to whether alpha is 0 or not */
	if ( !mqi_coef_sign(alpha) ) {

		mqi_coef_t tp, sig1, sig2, gamma;

		mqi_coef_init_cpy (tp, theta);
		mqi_coef_abs	  (tp, tp);
		mqi_coef_mul (tp, tp, VECT(det_q,0));
		mqi_coef_mul_schar(tp, tp, mqi_coef_sign(beta));
		mqi_coef_mul_schar(tp, tp, mqi_coef_sign(a9));
		
		mqi_coef_init_dynamic (gamma, mqi_coef_typeof(tp));
		mqi_coef_linexpr3 (gamma,3,	1,a3,a9,a9,	1,a6,a8,a8,	-2,a5,a8,a9);
		
		mqi_coef_init_dynamic (sig1, mqi_coef_typeof(beta));
		mqi_coef_linexpr2 (sig1,2,	2,beta,a8,	-1,gamma,a7);
		mqi_coef_init_dynamic (sig2, mqi_coef_typeof(sig1));
		mqi_coef_linexpr2 (sig2,2,	1,a5,a9,	1,a6,a8);
		mqi_coef_linexpr2 (sig2,2,	2,beta,sig2,	1,l2,gamma);
		mqi_coef_divexact(sig2,sig2,a9);

		mqi_coef_mul (MAT(m1,0,2), gamma, a9);
		mqi_coef_mul (MAT(m1,0,3), a9, beta);
		mqi_coef_mul_schar (MAT(m1,1,2), MAT(m1,0,3), -2);
		mqi_coef_neg (MAT(m1,2,1), MAT(m1,1,2));
		mqi_coef_cpy (MAT(m1,2,2), sig1);
		mqi_coef_linexpr2 (MAT(m1,2,3),1,  -1,a7,beta);
		mqi_coef_linexpr3(MAT(m1,3,0),1,  1,theta,theta,det_R);
		mqi_coef_linexpr2 (MAT(m1,3,1),1,  -1,beta,a6);
		mqi_coef_cpy (MAT(m1,3,2), sig2);
		mqi_coef_mul (MAT(m1,3,3), l2, tp);

		/** Free local memory */
		mqi_coef_list_clear(tp, sig1, sig2, gamma, NULL);

	}else {

		mqi_coef_t tau1, tau2;
		/** Coefficients tau1 and tau2 */
		mqi_coef_list_init_dynamic (mqi_coef_typeof(a8), tau1, tau2, NULL);
		
		mqi_coef_linexpr2 (tau1,2,	1,a8,l1,	-1,a7,l3);
		mqi_coef_linexpr2 (tau2,2,	1,a4,a4,	-1,a1,a6);
		mqi_coef_linexpr2 (tau2,3,	1,a8,tau2,	1,a2,l2,	1,a5,l1);


		/** Param matrices */
		mqi_coef_mul (MAT(m1,0,2), a9, beta);
		mqi_coef_cpy (MAT(m1,0,3), MAT(m1,0,2));
		mqi_coef_linexpr2 (MAT(m1,1,2),1,  -1,a9,alpha);
		mqi_coef_cpy (MAT(m1,1,3), MAT(m1,1,2));
		mqi_coef_mul_schar (MAT(m1,2,1), MAT(m1,1,2), -2);
		mqi_coef_mul (MAT(m1,2,2), a9, tau1);
		mqi_coef_cpy (MAT(m1,2,3), MAT(m1,2,2));
		mqi_coef_linexpr3 (MAT(m1,3,0),1,  1,theta,theta,det_R);
		mqi_coef_linexpr2 (MAT(m1,3,1),1,  -1,a6,alpha);
		mqi_coef_mul (MAT(m1,3,2), a9, tau2);
		mqi_coef_cpy (MAT(m1,3,3), MAT(m1,3,2));

		mqi_coef_linexpr2 (MAT(m2,0,2),1,  -1,a9,a9);
		mqi_coef_neg (MAT(m2,0,3), MAT(m2,0,2));
		mqi_coef_mul (MAT(m2,2,2), a7, a9);
		mqi_coef_neg (MAT(m2,2,3), MAT(m2,2,2));
		mqi_coef_neg (MAT(m2,3,2), l2);
		mqi_coef_cpy (MAT(m2,3,3), l2);

		/** Free local memory */
		mqi_coef_list_clear(tau1, tau2, NULL);

	}

	/** Put back in global frame */
	mqi_mat_t tmpmat;

	mqi_mat_init_cpy(tmpmat, m1);
	mqi_mat_clear(m1);
	mqi_mat_mul_init (m1, P, tmpmat);
	mqi_mat_clear(tmpmat);

	mqi_mat_init_cpy(tmpmat, m2);
	mqi_mat_clear(m2);
	mqi_mat_mul_init (m2, P, tmpmat);

	/** TMP DEBUG ONLY */
	mqi_log_printf ("m1 (after transfo):"); mqi_mat_print(m1); mqi_log_printf("\n");
	mqi_log_printf ("m2 (after transfo):"); mqi_mat_print(m2); mqi_log_printf("\n");


	/** m2 should be multiplied by this */
	mqi_coef_init_cpy (cm2, theta);
	mqi_coef_mul (cm2, cm2, VECT(det_q,0));

	/** sp = [ut, vs, us, vt] */
	/** $$ It seems that "qi_param_surface_22" doesn't init the result */
	mqi_surface_param_init_dynamic (sp, 4, mqi_vect_typeof(det_q));
	qi_param_surface_22 (sp);

	/* TMP DEBUG ONLY */
	mqi_log_printf ("sp:"); mqi_surface_param_print(sp); mqi_log_printf("\n");

	mqi_coef_init_dynamic (A, mqi_vect_typeof(v));
	mqi_vect_content (A, (mqi_vect_ptr)COL(m1,0));

	mqi_coef_init_dynamic (B, mqi_vect_typeof(v));
	mqi_vect_content (B, (mqi_vect_ptr)COL(m1,1));

	/** Simplification */
	if ( mqi_coef_cmp_schar (VECT(det_q,1), 1) == 0 ) {


		mqi_coef_t C, D, a, b, c, d, gcdAC, gcdBD;


		mqi_mat_mul_coef (m2, m2, cm2);
		mqi_mat_add	 (m1, m1, m2);

		/* Find the factors a, b, c, d of the 4 columns of M such that a*b = c*d   */

		/* First, the gcds of the columns  */
		mqi_coef_init_dynamic (C, mqi_vect_typeof(v));
		mqi_vect_content (C, (mqi_vect_ptr)COL(m1,2));

		mqi_coef_init_dynamic (D, mqi_vect_typeof(v));
		mqi_vect_content (D, (mqi_vect_ptr)COL(m1,3));
		
		mqi_coef_init_cpy (a,A);
		mqi_coef_gcd (a, a, D);

		mqi_coef_init_cpy (d, a);

		mqi_coef_init_cpy (b, B);
		mqi_coef_gcd (b, b, C);

		mqi_coef_init_cpy (c, b);

		mqi_coef_divexact (A, A, a);
		mqi_coef_divexact (B, B, b);
		mqi_coef_divexact (C, C, c);
		mqi_coef_divexact (D, D, d);

		mqi_coef_init_cpy (gcdBD, B);
		mqi_coef_gcd (gcdBD, gcdBD, D);

		mqi_coef_init_cpy (gcdAC, A);
		mqi_coef_gcd (gcdAC, gcdAC, C);

		mqi_coef_mul (b, b, gcdBD);
		mqi_coef_mul (d, d, gcdBD);
		mqi_coef_mul (a, a, gcdAC);
		mqi_coef_mul (c, c, gcdAC);

		/* TMP DEBUG ONLY */
		mqi_log_printf ("a: "); mqi_coef_print(a); mqi_log_printf("\n");
		mqi_log_printf ("b: "); mqi_coef_print(b); mqi_log_printf("\n");
		mqi_log_printf ("c: "); mqi_coef_print(c); mqi_log_printf("\n");
		mqi_log_printf ("d: "); mqi_coef_print(d); mqi_log_printf("\n");

		mqi_vect_divexact_coef ((mqi_vect_ptr)COL(m1,0), (mqi_vect_ptr)COL(m1,0), a);
		mqi_vect_divexact_coef ((mqi_vect_ptr)COL(m1,1), (mqi_vect_ptr)COL(m1,1), b);
		mqi_vect_divexact_coef ((mqi_vect_ptr)COL(m1,2), (mqi_vect_ptr)COL(m1,2), c);
		mqi_vect_divexact_coef ((mqi_vect_ptr)COL(m1,3), (mqi_vect_ptr)COL(m1,3), d);

		qi_mat_load_balancing (m1, 1);

		mqi_surface_param_mul_mat (s1, m1, sp);

		/** Free local memory */
		mqi_coef_list_clear(C, D, a, b, c, d, gcdAC, gcdBD, NULL);

	}else{

		mqi_coef_t gcdAB, alpha, beta, gamma, a, b, c, a0, b0, cofactor, coef2;

		/* Find the factors a, b, c, d of the 4 columns of M such that a*b = c*d
		 * This time with two matrices!	 */
		mqi_vect_content(C, (mqi_vect_ptr)COL(m1,2));
		mqi_coef_init_dynamic(coef2, mqi_mat_typeof(m2));
		mqi_vect_content(coef2, (mqi_vect_ptr)COL(m2,2));
		mqi_coef_mul(coef2, coef2, cm2);

		mqi_coef_gcd (C, C, coef2);

		mqi_coef_init_cpy (gcdAB, A);
		mqi_coef_gcd (gcdAB, gcdAB, B);

		mqi_coef_divexact (A, A, gcdAB);
		mqi_coef_divexact (B, B, gcdAB);

		qi_coef_extract_square_factors (a0, cofactor, A);
		mqi_coef_clear(cofactor);
		qi_coef_extract_square_factors (b0, cofactor, B);

		/* A=a0^2.a1.gcdAB and B=b0^2.b1.gcdAB */
		mqi_coef_init_cpy (gamma, gcdAB);
		mqi_coef_gcd (gamma, gamma, C);
		mqi_coef_divexact (C, C, gamma);
		mqi_coef_init_cpy (alpha, a0);
		mqi_coef_gcd (alpha, alpha, C);
		mqi_coef_init_cpy (beta, b0);
		mqi_coef_gcd (beta, beta, C);
		
		mqi_coef_init_cpy (a, alpha);
		mqi_coef_mul (a, a, a);
		mqi_coef_mul (a, a, gamma);

		mqi_coef_init_cpy (b, beta);
		mqi_coef_mul (b, b, b);
		mqi_coef_mul (b, b, gamma);

		mqi_coef_init_cpy (c, alpha);
		mqi_coef_mul (c, c, beta);
		mqi_coef_mul (c, c, gamma);
			
		/* TMP DEBUG ONLY */
		mqi_log_printf ("a: "); mqi_coef_print(a); mqi_log_printf("\n");
		mqi_log_printf ("b: "); mqi_coef_print(b); mqi_log_printf("\n");
		mqi_log_printf ("c: "); mqi_coef_print(c); mqi_log_printf("\n");

		/* a|A, b|B, sqrt(ab)|C because gamma | A, B and C,  alpha^2 | A, 
		 * alpha | C/gamma, beta^2 | B, beta | C/gamma, and alpha and beta
		 * are relatively prime (since a0 and b0 are) */
		mqi_vect_divexact_coef ((mqi_vect_ptr)COL(m1,0), (mqi_vect_ptr)COL(m1,0), a);
		mqi_vect_divexact_coef ((mqi_vect_ptr)COL(m1,1), (mqi_vect_ptr)COL(m1,1), b);
		mqi_vect_divexact_coef ((mqi_vect_ptr)COL(m1,2), (mqi_vect_ptr)COL(m1,2), c);
		mqi_vect_divexact_coef ((mqi_vect_ptr)COL(m1,3), (mqi_vect_ptr)COL(m1,3), c);

		mqi_mat_t m2tmp;

		mqi_mat_mul_init(m2tmp, (mqi_mat_ptr)cm2, m2);

		mqi_vect_divexact_coef ((mqi_vect_ptr)COL(m2tmp,2), (mqi_vect_ptr)COL(m2tmp,2), c);
		mqi_vect_divexact_coef ((mqi_vect_ptr)COL(m2tmp,3), (mqi_vect_ptr)COL(m2tmp,3), c);


		/* We can still do load_balancing on the first two columns of m1 */
		qi_mat_load_balancing (m1, 0);

		mqi_surface_param_mul_mat (s1, m1, sp);
		mqi_surface_param_mul_mat (s2, m2, sp);

		/** Free local memory */
		mqi_coef_list_clear(gcdAB, alpha, beta, gamma, a, b, c, a0, b0, cofactor, coef2, NULL);
		mqi_mat_clear(m2tmp);

	}

	/* TMP DEBUG ONLY */
	mqi_log_printf ("m1 (after simplification): "); mqi_mat_print(m1); mqi_log_printf("\n");
	mqi_log_printf ("m2 (after simplification): "); mqi_mat_print(m2); mqi_log_printf("\n");
	mqi_log_printf ("s1: "); mqi_surface_param_print_xwst (s1, 's', 't', 'u', 'v'); mqi_log_printf("\n");
	mqi_log_printf ("s2: "); mqi_surface_param_print_xwst (s2, 's', 't', 'u', 'v'); mqi_log_printf("\n");
	/*****************/

#ifdef DEBUG
	mqi_log_printf ("Parametrization of quadric (2,2): ");
	mqi_surface_param_print_xwst (s1, 'u', 'v', 's', 't');
	
	if ( mqi_coef_cmp_schar(VECT(det_q,1),1) )
	{
		mqi_log_printf (" + sqrt("); mqi_coef_print(VECT(det_q,1)); mqi_log_printf (")*");
		mqi_surface_param_print_xwst (s2, 'u', 'v', 's', 't');
		mqi_log_printf ("\n");
	}
#endif

	/** Free local memory */
	mqi_mat_list_clear (q, P, Rp, m1, m2, tmpmat, NULL);
	mqi_vect_list_clear (det_q, v, Rp0, p0, NULL);
	mqi_coef_list_clear (m0, cm2, A, B, a1, a2, a3, a4, a5, a6, a7, a8, a9,
				l1, l2, l3, alpha, beta, theta, NULL);
	mqi_surface_param_clear (sp);


}

