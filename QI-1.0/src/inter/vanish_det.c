#include "qi_inter.h"

static void __qi_inter_vanish_det_vanishes (qi_inter_t __INIT rop, mqi_mat_t q1, mqi_mat_t q2, mqi_hpoly_t det_p,
					    mqi_mat_t sing, mqi_mat_t sing1, mqi_mat_t sing2, size_t d,
					    mqi_poly_t det_p3, mqi_vect_t sing_p0)
{

	qi_inter_component_t	comp;

#ifdef TRACEFUNC
	tracefunc("");
#endif

	if ( d == 0 )
	{

#ifdef TRACEFUNC
	tracefunc("d == 0");
#endif
		qi_inter_init (rop);
		qi_inter_set_type (rop, 22,1);

		mqi_coef_t D;
		mqi_mat_t  m1, m2;

		/* All the quadrics of the pencil are pairs of planes.
		 * Parameterize the pair of planes q1 */
		mqi_mat_init_dynamic (m1, 4, 3, mqi_mat_typeof(q1));
		mqi_mat_init_dynamic (m2, 4, 1, mqi_mat_typeof(q1));
		qi_param_plane_pair (q1, sing1, D, m1, m2);

		/* The two planes are rational so we can directly replace the first column
		 * of m1 by m1(0) +/- m2(0) */
		mqi_mat_t m1p, m1m;
		mqi_mat_ptr col;

		mqi_mat_init_cpy  (m1p, m1);
		mqi_mat_init_cpy  (m1m, m1);


		/* Store m1(0) + m2(0) in the first column of m1p */
		col = COL(m1p,0);
		mqi_mat_add(col, COL(m1,0), COL(m2,0));

		col = COL(m1m,0);
		mqi_mat_sub(col, COL(m1,0), COL(m2,0));
		
		/* Optimize */
		qi_mat_optimize (m1p);
		qi_mat_optimize (m1m);

		mqi_mat_t qop, qom;

		/* Apply the transformation to q2 */
		mqi_mat_transformation (qop, q2, m1p, MQI_MAT_NOTRANSCO);
		mqi_mat_transformation (qom, q2, m1m, MQI_MAT_NOTRANSCO);

		/* One matrix has rank 0, the other has rank 2 */
		if ( mqi_mat_rank(qop) != 0 )
		{
			mqi_mat_swap(qop, qom);
			mqi_mat_swap(m1p, m1m);
		}

		/* The plane */
		qi_inter_component_init (comp, QI_INTER_COMPONENT_1, m1p);
		qi_inter_component_set_type (comp, QI_INTER_COMPONENT_PLANE);
		qi_inter_add_component	(rop, comp);
		/* discard comp */
		qi_inter_component_clear(comp);

		mqi_mat_t m1d, m2d;
		mqi_mat_t singular;

		mqi_mat_init_dynamic(m1d, 3, 2, mqi_mat_typeof(q1));
		mqi_mat_init_dynamic(m2d, 3, 1, mqi_mat_typeof(q1));

		qi_mat_singular(singular, qom);

		qi_param_line_pair (qom, (mqi_vect_ptr)COL(singular,0), D, m1d, m2d);

		mqi_mat_clear(singular);

		mqi_mat_t l1p, l1m;

		/* The two lines are necessarily rational */
		mqi_mat_init_cpy (l1p, m1d);
		mqi_mat_init_cpy (l1m, m1d);

	col = COL(l1p, 0);
		mqi_mat_add (col, COL(m1d,0), COL(m2d,0));

		col = COL(l1m, 0);
		mqi_mat_sub (col, COL(m1d,0), COL(m2d,0));

		/* $$ DOUBT HERE: is there a resizing ? */

		/* The lines in 3D */
		mqi_mat_mul (l1p, m1m, l1p);
		mqi_mat_mul (l1m, m1m, l1m);

		/* The lines we want is the one that is not on the plane */
		mqi_mat_ptr l1;
		mqi_mat_t trans, kernel, tmpmat;

		mqi_mat_transpose_init (trans, m1p);
		mqi_mat_kernel	       (kernel, trans);
		mqi_mat_clear(trans);
		mqi_mat_transpose_init (trans, kernel);
		mqi_mat_mul_init (tmpmat, trans, l1m);

		mqi_mat_list_clear(trans, kernel, NULL);
		
		if ( mqi_mat_rank(tmpmat) == 0 )
			l1 = l1p;
		else
			l1 = l1m;

		qi_mat_optimize (l1);

		mqi_curve_param_t lin;

		mqi_curve_param_init_dynamic(lin, 2, mqi_mat_typeof(l1));
		mqi_curve_param_line_through_two_points
			(lin, (mqi_vect_ptr)COL(l1,0), (mqi_vect_ptr)COL(l1,1));

		/* $$ CUT PARAMETERS : TODO, see original C++ code from version 0.9 */
		qi_inter_component_init(comp, QI_INTER_COMPONENT_2, lin);
		qi_inter_component_set_type (comp, QI_INTER_COMPONENT_LINE);
		qi_inter_add_component (rop, comp);
		/* discard comp */
		qi_inter_component_clear (comp);

		/* Set the optimal flag on all the components */
		qi_inter_set_optimal(rop);

		/* Free local memory */
		mqi_coef_clear(D);
		mqi_mat_list_clear(m1, m2, m1p, m1m, qop, qom, m1d, m2d, l1p, l1m, tmpmat, NULL);
		mqi_curve_param_clear(lin);

	}
	else
	{

#ifdef TRACEFUNC
	tracefunc("d != 0");
#endif

		/* More than one singular point in common: send the singular line to
		 * z = w = 0 and "rotate" */

		/* The second common singular point */
		mqi_vect_t sing_p1;

		mqi_vect_init_cpy (sing_p1, (mqi_vect_ptr)COL(sing,1));
		qi_vect_optimize(sing_p1);

		/* Compute a 4x4 projective transformation sending the two common
		 * singular points to the line z = w = 0 */
		mqi_mat_t proj_mat;
		qi_mat_send_to_zw (proj_mat, sing_p0, sing_p1);

		/* Note we could have started from q1_r and have rotated them... it
		 * would have made aggregation of transformation matrices harder
		 * "Rotate" q1 and q2, and remove the two last lines and columns */
		mqi_mat_t q1_r, q2_r;
		mqi_mat_t tmpmat;

		mqi_mat_transformation (q1_r, q1, proj_mat, MQI_MAT_NOTRANSCO);
		mqi_mat_transformation (q2_r, q2, proj_mat, MQI_MAT_NOTRANSCO);

		mqi_mat_minor (tmpmat, q1_r, 3, 3);
		mqi_mat_clear (q1_r);
		mqi_mat_minor (q1_r, tmpmat, 2, 2);
		mqi_mat_clear (tmpmat);

		mqi_mat_minor (tmpmat, q2_r, 3, 3);
		mqi_mat_clear (q2_r);
		mqi_mat_minor (q2_r, tmpmat, 2, 2);

		mqi_poly_t det_p2;
		qi_pencil_det2x2 (det_p2, q1_r, q2_r);

		/* The reduced 2x2 determinental equation vanishes */
		if ( mqi_poly_is_zero(det_p2) )
		{

#ifdef TRACEFUNC
	tracefunc("det_p2 == 0");
#endif
			mqi_mat_t q, q_r, __sing; /* The prefix avoids a collision with 
						     the parameter "sing" */

			/* If q1_r is zero, consider q2_r */
			if ( (mqi_coef_sign(MAT(q1_r,0,0)) == 0) &&
			     (mqi_coef_sign(MAT(q1_r,1,1)) == 0) )
			{
				mqi_mat_init_cpy(q_r, q2_r);
				mqi_mat_init_cpy(q, q2);
				mqi_mat_init_cpy(__sing, sing2);
			}
			else
			{
				mqi_mat_init_cpy(q_r, q1_r);
				mqi_mat_init_cpy(q, q1);
				mqi_mat_init_cpy(__sing, sing1);
			}

			qi_inter_init(rop);
			
			/* All the quadrics of the pencil are zero */
			if ( (mqi_coef_sign(MAT(q_r,0,0)) == 0) &&
			     (mqi_coef_sign(MAT(q_r,1,1)) == 0) )
			{
				qi_inter_set_type(rop, 27,1);
				qi_inter_component_init (comp, QI_INTER_COMPONENT_0); /* Universe */
				qi_inter_add_component (rop, comp);
				/* discard comp */
				qi_inter_component_clear(comp);
			}
			else
			{
				mqi_mat_t m1;

				/* One component in the intersection */
				qi_inter_set_type(rop, 26,1);
				
				mqi_mat_init_dynamic(m1, 4, 3, mqi_mat_typeof(q1));
				qi_param_plane_double(q, __sing, m1);
				qi_mat_optimize(m1);
				
				qi_inter_component_init (comp, QI_INTER_COMPONENT_1, m1);
				qi_inter_component_set_type (comp, QI_INTER_COMPONENT_PLANE);
				/* discard comp */
				qi_inter_component_clear(comp);

				/* Free local memory */
				mqi_mat_clear (m1);
			}

			/* Free local memory */
			mqi_mat_list_clear(q, q_r, __sing, NULL);		

		}
		else
		{

#ifdef TRACEFUNC
	tracefunc("det_p2 != 0");
#endif

		/* The reduced 2x2 determinental equation does not vanish */
			qi_hpoly_optimize(det_p2);

			mqi_coef_t delta_p2;

			mqi_coef_init_dynamic(delta_p2, mqi_poly_typeof(det_p2));
			qi_hpoly_pseudo_discriminant (delta_p2, det_p2);

			int sign_delta = mqi_coef_sign(delta_p2);

			/* The discriminant of the 2x2 equation is not zero */
			if ( sign_delta != 0 )
			{

#ifdef TRACEFUNC
	tracefunc("sign_delta != 0");
#endif
				/* One component in the intersection */
				qi_inter_init (rop);

				if ( sign_delta > 0 )
					qi_inter_set_type(rop, 23,1);
				else
					qi_inter_set_type(rop, 23,2);

				mqi_curve_param_t lin;

				mqi_curve_param_init_dynamic (lin, mqi_vect_size(sing_p0), mqi_coef_typeof(delta_p2));
				mqi_curve_param_line_through_two_points (lin, sing_p0, sing_p1);

				/* $$ CUT PARAMETERS : TODO - See the original C++ code from QI 0.9 */
				qi_inter_component_init (comp, QI_INTER_COMPONENT_2, lin);
				qi_inter_component_set_type(comp, QI_INTER_COMPONENT_LINE);
				qi_inter_component_set_multiplicity(comp, 4);
				qi_inter_add_component (rop, comp);
				/* discard comp */
				qi_inter_component_clear(comp);
	
				/* Set the optimality on each component of the intersection */
				qi_inter_set_optimal(rop);
					
			}
			else
			{

#ifdef TRACEFUNC
	tracefunc("sign_delta == 0");
#endif
			/* The discriminant of the 2x2 equation vanishes */				

				mqi_vect_t root;

				mqi_vect_init_dynamic(root, 2, mqi_poly_typeof(det_p2));

				if ( mqi_coef_sign(POLY(det_p2,0)) == 0 )
				{
					mqi_coef_cpy(VECT(root,0), POLY(det_p2,1));
					mqi_coef_mul_schar(VECT(root,1), POLY(det_p2,2), -2);	
				}
				else
				{
					mqi_coef_mul_schar(VECT(root,0), POLY(det_p2,0), -2);
					mqi_coef_cpy(VECT(root,1), POLY(det_p2,1));
				}

				qi_vect_optimize(root);

				mqi_mat_t q;

				mqi_mat_init_dynamic(q, mqi_mat_nrows(q1_r), mqi_mat_ncols(q1_r), mqi_mat_typeof(q1_r));
				mqi_mat_linexpr(q,1,	VECT(root,0),q1_r,	VECT(root,1),q2_r);

				mqi_vect_t in_q;

				qi_mat_inertia (in_q, q);

				/* The singular quadric of the 2x2 pencil corresponds to a plane */
				if ( mqi_coef_cmp_schar(VECT(in_q,0), 1) == 0 )
				{

#ifdef TRACEFUNC
	tracefunc("VECT(in_q,0) == 1");
#endif
					mqi_mat_t m1;

					/* One component in the intersection */
					qi_inter_init(rop);

					qi_inter_set_type(rop, 24,1);

					/* The two quadrics share a common (rational) plane
					 * Let's parameterize q */

					mqi_mat_init_dynamic(tmpmat, 4, 3, mqi_mat_typeof(q));
					if ( mqi_coef_sign(MAT(q,0,0)) != 0 )
					{
						mqi_mat_set_at (tmpmat, 0,0, MAT(q,0,1));
						mqi_coef_neg(MAT(tmpmat,0,0), MAT(tmpmat,0,0));
						mqi_mat_set_at (tmpmat, 1,0, MAT(q,0,0));
					}
					else
					{
						mqi_mat_set_at (tmpmat, 0,0, MAT(q,1,1));
						mqi_mat_set_at (tmpmat, 1,0, MAT(q,0,1));
						mqi_coef_neg   (MAT(tmpmat,1,0), MAT(tmpmat,1,0));
					}
					
					mqi_mat_set_at_int(tmpmat, 2,1, 1);
					mqi_mat_set_at_int(tmpmat, 3,2, 1);

					mqi_mat_mul_init (m1, proj_mat, tmpmat);
					/* discard tmpmat */
					mqi_mat_clear (tmpmat);

					qi_mat_optimize(m1);

					qi_inter_component_init (comp, QI_INTER_COMPONENT_1, m1);
					qi_inter_component_set_type (comp, QI_INTER_COMPONENT_PLANE);
					qi_inter_add_component (rop, comp);
					/* discard comp */
					qi_inter_component_clear(comp);
	
					/* Free local memory */
					mqi_mat_clear(m1);

				}
				else
				{

#ifdef TRACEFUNC
	tracefunc("VECT(in_q,0) != 1");
#endif
					/* To avoid picking the zero matrix */
					mqi_mat_t __sing; /* The prefix avoids a collision with
							     with the parameter "sing" */
				
					/* discard old q */
					mqi_mat_clear(q);

					if ( mqi_coef_sign(VECT(root,0)) == 0 )
					{
						mqi_mat_init_cpy(q, q1);
						mqi_mat_init_cpy(__sing, sing1);
					}
					else
					{
						mqi_mat_init_cpy(q, q2);
						mqi_mat_init_cpy(__sing, sing2);		
					}

					/* Imaginary pair of planes */
					mqi_vect_t inertia;

					qi_mat_inertia_known_rank(inertia, q, 2);

					if ( mqi_coef_cmp_schar(VECT(inertia,0), 2) == 0 )
					{

#ifdef TRACEFUNC
	tracefunc("VECT(inertia,0)==2");
#endif
						/* One component in the intersection */
						qi_inter_init(rop);
						qi_inter_set_type(rop, 25,1);
						mqi_curve_param_t lin;

						mqi_curve_param_init_dynamic(lin, mqi_vect_size(sing_p0), mqi_vect_typeof(sing_p0));
						mqi_curve_param_line_through_two_points(lin, sing_p0, sing_p1);

						/* $$ CUT PARAMETERS : TODO - See the original C++ code from QI 0.9 */
						qi_inter_component_init (comp, QI_INTER_COMPONENT_2, lin);
						qi_inter_component_set_type (comp, QI_INTER_COMPONENT_LINE);

						qi_inter_add_component (rop, comp);
						/* discard comp */
						qi_inter_component_clear(comp);

						/* Set the optimality flag to all the components of the intersection */
						qi_inter_set_optimal (rop);

						/* Free local memory */
						mqi_curve_param_clear(lin);

					}
					else
					{
#ifdef TRACEFUNC
	tracefunc("VECT(inertia,0)!=2");
#endif
						/* We separate the planes if the discrim is a square
						 * Parameterize the pair of planes q */
						mqi_coef_t D;
						mqi_mat_t  m1, m2;

						mqi_mat_init_dynamic (m1, 4, 3, mqi_mat_typeof(q));
						mqi_mat_init_dynamic (m2, 4, 1, mqi_mat_typeof(q));

						qi_param_plane_pair (q, __sing, D, m1, m2);

						if ( mqi_coef_sign(D) == 0 )
						{
							/* The planes are rational */
							mqi_mat_t m1p, m1m;
							mqi_mat_ptr col;

							col = COL(m1p,0);
							mqi_mat_add (col, COL(m1,0), COL(m2,0));
				
							col = COL(m1m,0);
							mqi_mat_sub (col, COL(m1,0), COL(m2,0));

							qi_mat_optimize(m1p);
							qi_mat_optimize(m1m);

							qi_inter_init(rop);
							qi_inter_set_type(rop, 25,2);

							qi_inter_component_init(comp, QI_INTER_COMPONENT_1, 1, m1p);
							qi_inter_component_set_type (comp, QI_INTER_COMPONENT_PLANE);
							qi_inter_add_component (rop, comp);
							/* discard comp */
							qi_inter_component_clear(comp);
						
							qi_inter_component_init(comp, QI_INTER_COMPONENT_1, m1m);
							qi_inter_component_set_type (comp, QI_INTER_COMPONENT_PLANE);
							qi_inter_add_component (rop, comp);
							/* discard comp */
							qi_inter_component_clear(comp);

							/* Free local memory */
							mqi_mat_list_clear (m1p, m1m, NULL);
						}
						else
						{
							/* D is not a square: do not separate */

							/* One component in the intersection */
							qi_inter_init (rop);
							qi_inter_set_type (rop, 25,2);

							qi_inter_component_init(comp, QI_INTER_COMPONENT_1, q);
							qi_inter_component_set_type(comp, QI_INTER_COMPONENT_PAIR_OF_PLANES);
							qi_inter_add_component(rop, comp);
							/* discard comp */
							qi_inter_component_clear(comp);
						}

						/* Free local memory */
						mqi_coef_clear(D);
						mqi_mat_list_clear(m1, m2, NULL);

					}

					/* Free local memory */
					mqi_mat_clear(__sing);
					mqi_vect_clear(inertia);

				}

				/* Free local memory */
				mqi_mat_clear(q);
				mqi_vect_list_clear(in_q, root, NULL);

			}
		
			/* Free local memory */
			mqi_coef_clear (delta_p2);

		}

		/* Free local memory */
		mqi_vect_clear(sing_p1);
		mqi_mat_list_clear (proj_mat, q1_r, q2_r, tmpmat, NULL);
		mqi_poly_clear(det_p2);

	}

	/* Nothing to clear */
	return;

}

static void __qi_inter_vanish_det_no_vanish (qi_inter_t __INIT rop, mqi_mat_t q1, mqi_mat_t q2, mqi_hpoly_t det_p,
					     mqi_mat_t sing, mqi_mat_t sing1, mqi_mat_t sing2,
					     mqi_poly_t det_p3, mqi_mat_t q1_r, mqi_mat_t q2_r, mqi_mat_t proj_mat,
					     mqi_vect_t sing_p0)
{

	size_t d;
	mqi_hpoly_t	derx, derw;
	mqi_hpoly_t	gcd_p3;
	qi_inter_component_t comp;

	qi_hpoly_optimize(det_p3);

	mqi_hpoly_list_init_dynamic (mqi_poly_typeof(det_p3), derx, derw, gcd_p3, NULL);

	mqi_poly_derivate_xw (derx, det_p3, 'x');
	mqi_poly_derivate_xw (derw, det_p3, 'w');

	/* The gcd of the derivatives of the 3 x 3 det equation */
	mqi_poly_gcd (gcd_p3, derx, derw);

	/* Its degree */
	d = mqi_poly_degree (gcd_p3);

	/* Always optimize, otherwise may run into trouble in division */
	qi_hpoly_optimize (gcd_p3);


	/* The reduced 3 x 3 det equation has no multiple root */
	if ( d == 0 )
		__qi_inter_vanish_four_concurrent_lines (rop, det_p3, q1, q2, q1_r, q2_r, sing_p0, proj_mat);
	else
	{
		/* The reduced 3 x 3 det equation has a multiple root */
		/* Double or triple real root */

		/* The (lambda,mu) of the multiple root */
		mqi_vect_t root;

		mqi_vect_init_dynamic(root, 2, mqi_mat_typeof(q1));

		/* Double real root */
		if ( d == 1 )
		{
			mqi_coef_neg(VECT(root,0), POLY(gcd_p3,0));
			mqi_coef_cpy(VECT(root,1), POLY(gcd_p3,1));
		}
		else
		{
			/* Triple root (d = 2) */	
			if ( mqi_coef_sign(POLY(gcd_p3,0)) != 0 )
			{
				mqi_coef_mul_schar(VECT(root,0),POLY(gcd_p3,0),-2);
				mqi_coef_cpy(VECT(root,1), POLY(gcd_p3,1));
			}
			else
			{
				mqi_coef_neg(VECT(root,0),POLY(gcd_p3,1));
				mqi_coef_mul_schar(VECT(root,1),POLY(gcd_p3,2),2);
			}
		}

		qi_vect_optimize(root);

		/* The associated matrix */
		mqi_mat_t q;

		mqi_mat_init_dynamic(q, mqi_mat_nrows(q1_r), mqi_mat_ncols(q1_r), mqi_mat_typeof(q1_r));
		mqi_mat_linexpr (q,2,	VECT(root,0),q1_r,	VECT(root,1),q2_r);

		/* Its inertia */
		mqi_vect_t in_q;
		mqi_coef_t rank_q;

		qi_mat_inertia(in_q, q);

		/* Its rank */
		mqi_coef_init_cpy(rank_q, VECT(in_q,0));
		mqi_coef_add(rank_q, rank_q, VECT(in_q,1));

		/* The singular locus of q (not needed in all cases, but eases
	       	   readability of code) */
		mqi_mat_t q_sing;

		qi_mat_singular(q_sing, q);

		/* The reduced 3 x 3 det equation has a double real root: the rank
	      	 * of the associated quadric is either 2 or 1 */
		if ( d == 1 )
		{
			/* Divide det_p3 by the gcd squared */
			mqi_hpoly_t lin3, det_e3;

			mqi_hpoly_init_dynamic(lin3, mqi_poly_typeof(gcd_p3));
			mqi_poly_mul (lin3, gcd_p3, gcd_p3);
			mqi_hpoly_init_dynamic(det_e3, mqi_poly_typeof(det_p3));
			mqi_poly_mul (det_e3, det_p3, lin3);

			/* Compute the other simple root of the pencil */
			mqi_vect_t root2;

			mqi_vect_init_dynamic(root2, 2, mqi_poly_typeof(det_e3));

			mqi_coef_neg(VECT(root2,0), POLY(det_e3,0));
			mqi_coef_cpy(VECT(root2,1), POLY(det_e3,1));

			qi_vect_optimize(root2);

			mqi_mat_t q_other;

			mqi_mat_init_dynamic(q_other, mqi_mat_nrows(q1_r), mqi_mat_ncols(q1_r), mqi_mat_typeof(q1_r));
			mqi_mat_linexpr(q_other,2,	VECT(root2,0),q1_r,	VECT(root2,1),q2_r);

			/* Rank of singular quadric is 2 */
			if ( mqi_coef_cmp_schar(rank_q,2) == 0 )
				__qi_inter_vanish_two_conc_lines(rop,q1,q2,q,q_other,q_sing,proj_mat,sing_p0,VECT(in_q,0));
			/* Rank of singular quadric is 1 */
			else
				__qi_inter_vanish_two_double_lines(rop,q1,q2,q,q_other,q_sing,proj_mat,sing_p0);

			/* Free local memory */
			mqi_vect_clear(root2);
			mqi_poly_list_clear(lin3, det_e3, NULL);
			mqi_mat_clear(q_other);

		}
		else
		{
			/* The reduced 3 x 3 det equation has a triple real root: the rank
			   of the associated quadric is either 2, 1 or 0 */
			mqi_mat_ptr q_other;

			if ( mqi_coef_sign(VECT(root,0)) == 0 )
				q_other = q1_r;
			else
				q_other = q2_r;

			if ( mqi_coef_cmp_schar(rank_q,2) == 0 )
				__qi_inter_vanish_triple_and_line(rop,q1,q2,q,q_other,q_sing,proj_mat,sing_p0);
			else if ( mqi_coef_cmp_schar(rank_q,1) == 0 )
				__qi_inter_vanish_quadruple_line(rop,q1,q2,q,q_other,q_sing,proj_mat,sing_p0);
			else
			{
			/* Rank of singular quadric is zero */
				/* To avoid picking the zero matrix */
				/* Wrong: root is for 3x3 det eq, not 4x4 */
				if ( mqi_coef_sign(VECT(root,0)) == 0 )
				{
					/* discard old q */
					mqi_mat_clear(q);
					mqi_mat_init_cpy(q, q1);
				}
				else
				{
					/* discard old q */
					mqi_mat_init_cpy(q, q2);
				}
	
				qi_inter_init(rop);

				/* The pencil is only made of imaginary cones */
				mqi_vect_t inertia;

				qi_mat_inertia_known_rank(inertia, q, 3);

				if ( mqi_coef_cmp_schar(VECT(inertia,0),3) == 0 )
				{
					qi_inter_set_type (rop, 21,1);

					mqi_curve_param_t cone_p;

					mqi_curve_param_init_dynamic (cone_p, mqi_vect_size(sing_p0), mqi_vect_typeof(sing_p0));
					mqi_curve_param_set_vect (cone_p, sing_p0);
					
					qi_inter_component_init (comp, QI_INTER_COMPONENT_2, cone_p);
					qi_inter_component_set_type (comp, QI_INTER_COMPONENT_POINT);

					qi_inter_add_component(rop, comp);
					/* discard comp */
					qi_inter_component_clear(comp);

					/* Set the optimality flag on all the components of the intersection */
					qi_inter_set_optimal(rop);

					/* Free local memory */
					mqi_curve_param_clear(cone_p);

				}
				else
				{
					/* The pencil is only made of real cones */

					/* One component in the intersection */
					qi_inter_set_type(rop, 21,2);

					qi_inter_component_init(comp, QI_INTER_COMPONENT_1, q);
					qi_inter_component_set_type(comp, QI_INTER_COMPONENT_CONE);
					qi_inter_add_component(rop, comp);
				}

				/* Free local memory */
				mqi_vect_clear(inertia);

			}

			/* Free local memory */
			/* Nothing to free */
		}

		/* Free local memory */
		mqi_vect_list_clear(root, in_q, NULL);
		mqi_mat_list_clear(q_sing, q, NULL);
		mqi_coef_clear(rank_q);

	}

	/* Free memory */
	mqi_poly_list_clear(derx, derw, gcd_p3, NULL);

}


/** ****************************************************** */
/** Intersection when the determinential equation vanishes */
/** ****************************************************** */
void qi_inter_vanish_det (qi_inter_t __INIT rop, mqi_mat_t q1, mqi_mat_t q2,
			  mqi_hpoly_t det_p)
{

	mqi_mat_t	sing, sing1, sing2;
	size_t		d;

#ifdef TRACEFUNC
	tracefunc("");
#endif

	/* Singular loci of the input quadrics */
	qi_mat_singular (sing1, q1);
	qi_mat_singular (sing2, q2);

	/* The common singular locus of the two quadrics */
	qi_mat_linear_intersect (sing, sing1, sing2);

	/* Dimension of this singular locus */
	d = mqi_mat_ncols(sing) - 1;

	/* *********** */
	/* Main switch */
	/* *********** */
	if ( d == -1 )
	{
#ifdef TRACEFUNC
	tracefunc("d == -1");
#endif
		__qi_inter_vanish_conic_double_line (rop, q1, q2, sing1, sing2);
	}
	else
	{
#ifdef TRACEFUNC
	tracefunc("d != -1");
#endif
		/* There is at least one common singular point */

		/* The first common singular point */
		mqi_vect_t sing_p0;
		mqi_vect_init_cpy (sing_p0, (mqi_vect_ptr)COL(sing,0));

		qi_vect_optimize(sing_p0);

		/* Compute a 4x4 projective transformation sending infinity to the
		 * first point of sing */
		mqi_mat_t proj_mat;
		qi_mat_send_to_infinity (proj_mat, sing_p0);

		/* "Rotate" q1 and q2, and remove last line and column
		 * After that: we enumerate the possible intersections of conics */
		mqi_mat_t tmpmat, q1_r, q2_r;

		mqi_mat_transformation (tmpmat, q1, proj_mat, MQI_MAT_NOTRANSCO);
		mqi_mat_minor (q1_r, tmpmat, 3, 3);
		mqi_mat_clear (tmpmat);
		mqi_mat_transformation (tmpmat, q2, proj_mat, MQI_MAT_NOTRANSCO);
		mqi_mat_minor (q2_r, tmpmat, 3, 3);

		mqi_poly_t det_p3;

		qi_pencil_det3x3 (det_p3, q1_r, q2_r);

		/* Separate between the case where the new determinental equation vanishes
		 * and the one where it doesn't */
		if ( mqi_poly_is_zero (det_p3) )
		{
			/* *************************************** */
			/* New 3x3 determinental equation vanishes */
			/* *************************************** */
			__qi_inter_vanish_det_vanishes (rop, q1, q2, det_p,
							sing, sing1, sing2, d,
							det_p3, sing_p0);
		}
		else
		{
			/* ********************************************************** */
			/* New 3x3 determinental equation does not identically vanish */
			/* ********************************************************** */
			__qi_inter_vanish_det_no_vanish (rop, q1, q2, det_p,
							 sing, sing1, sing2,
							 det_p3, q1_r, q2_r, proj_mat,
							 sing_p0);
		}

		/* Free local memory */
		mqi_mat_list_clear((mqi_mat_ptr)sing_p0, proj_mat, q1_r, q2_r, tmpmat, NULL);
		mqi_poly_clear (det_p3);

	}


	/* Free memory */
	mqi_mat_list_clear (sing, sing1, sing2, NULL);

}

		
/** -------------- */
/** Local routines */
/** -------------- */

/** Conic and double line */
void __qi_inter_vanish_conic_double_line (qi_inter_t rop, mqi_mat_t __IN q1, mqi_mat_t __IN q2,
					  mqi_mat_ptr __IN sing1, mqi_mat_ptr __IN sing2)
{

		qi_inter_component_t	comp;
		qi_inter_cut_t		cut;	


		qi_inter_init(rop);
		qi_inter_set_type (rop, 15, 1);

		/* There are two cases:
		 * 	1. when one of q1 or q2 is a rational pair of planes,
		 * 	2. when the two given quadrics are cones.
		 */
		if ( (mqi_mat_ncols(sing1) == 2) || (mqi_mat_ncols(sing2) == 2) ) {
		
			/** q1 or q2 is a pair of planes */
			mqi_mat_t q, q_other, sing;
			
			if ( mqi_mat_ncols(sing1) == 2) {
				mqi_mat_init_cpy (q, q1);
				mqi_mat_init_cpy (sing, sing1);
				mqi_mat_init_cpy (q_other, q2);
			} else {
				mqi_mat_init_cpy (q, q2);
				mqi_mat_init_cpy (sing, sing2);
				mqi_mat_init_cpy (q_other, q1);
			}


			/** Parametrizes the pair of planes q */
			mqi_coef_t D;
			mqi_mat_t m1, m2;

			mqi_mat_init_dynamic(m1, 4, 3, mqi_mat_typeof(q1));
			mqi_mat_init_dynamic(m2, 4, 1, mqi_mat_typeof(q1));
			qi_param_plane_pair (q, sing, D, m1, m2);

			/** The two planes are rational so we can directly replace the first column
			 * of m1 by m1(0) +/- m2(0) */
			mqi_mat_t m1p, m1m;
			mqi_mat_ptr col;

			mqi_mat_init_cpy (m1p, m1);
			mqi_mat_init_cpy (m1m, m1);

			col = COL(m1p,0);
			mqi_mat_add (col, COL(m1,0), COL(m2,0));
		
			col = COL(m1m, 0);
			mqi_mat_sub (col, COL(m1,0), COL(m2,0));

			qi_mat_optimize (m1p);
			qi_mat_optimize (m1m);

			/** Now apply the transformations to q_other */
			mqi_mat_t qop, qom;

			mqi_mat_transformation (qop, q_other, m1p, MQI_MAT_NOTRANSCO);
			mqi_mat_transformation (qom, q_other, m1m, MQI_MAT_NOTRANSCO);

			/** One of the two matrices represents a double line, the other a conic */
			mqi_coef_t coef;

			mqi_coef_init_dynamic (coef, mqi_mat_typeof(qop));
			mqi_mat_det (coef, qop);

			if ( mqi_coef_sign(coef) != 0 ) {
				mqi_mat_swap(qop, qom);
				mqi_mat_swap(m1p, m1m);
			}

			/** Parametrizes the double line */
			mqi_mat_t m1db;
			mqi_mat_t singular;

			mqi_mat_init_dynamic(m1db, 3, 2, mqi_mat_typeof(qop));

			qi_mat_singular (singular, qop);
			qi_param_line_double (qop, singular, m1db);
			/* discard singular */
			mqi_mat_clear(singular);

			mqi_mat_t tr;

			/** Stacks the transformations */
			mqi_mat_mul_init (tr, m1p, m1db);

			qi_mat_optimize (tr);

			/** The double line is this line */
			mqi_curve_param_t line;

			mqi_curve_param_init_dynamic(line, 2, mqi_mat_typeof(tr));
			mqi_curve_param_line_through_two_points
				(line, (mqi_vect_ptr)COL(tr,0), (mqi_vect_ptr)COL(tr,1));

			/** $$ CUT PARAMETERS: TODO */
			qi_inter_component_init (comp, QI_INTER_COMPONENT_2, line);
			qi_inter_component_set_type (comp, QI_INTER_COMPONENT_LINE);
			qi_inter_component_set_multiplicity(comp, 2);

			qi_inter_add_component (rop, comp);
			/* discard comp */
			qi_inter_component_clear(comp);

			/** Parameterize the conic: it goes through a rational point which is the
			 * intersection of the singular line of q with q_other */
			mqi_curve_param_t tmp, sgline4;

			mqi_curve_param_initpool_dynamic(tmp, 4, mqi_mat_typeof(sing));
			mqi_curve_param_initpool_dynamic(sgline4, mqi_mat_nrows(sing), mqi_mat_typeof(sing));
			mqi_curve_param_line_through_two_points
				(sgline4, (mqi_vect_ptr)COL(sing,0), (mqi_vect_ptr)COL(sing,1));

			mqi_hpoly_t res;

			mqi_hpoly_init_dynamic(res, mqi_curve_param_typeof(sgline4));

			mqi_curve_param_mul_mat (tmp, q_other, sgline4);
			mqi_curve_param_scalar_product (res, sgline4, tmp);

			mqi_vect_t dbpoint;
			mqi_vect_initpool_dynamic (dbpoint, 4, mqi_poly_typeof(res));


			mqi_coef_t tmpcoef, tmpcoef2;

			if ( mqi_coef_sign(POLY(res,2)) != 0 ) {
				mqi_coef_init_cpy (tmpcoef, POLY(res,1));
				mqi_coef_neg	  (tmpcoef, tmpcoef);
				mqi_coef_init_cpy (tmpcoef2, POLY(res,2));
				mqi_coef_mul_schar(tmpcoef2, tmpcoef2, 2);
				mqi_curve_param_eval (dbpoint, sgline4, tmpcoef, tmpcoef2);
			}else{
				mqi_coef_init_cpy (tmpcoef2, POLY(res,1));
				mqi_coef_neg	  (tmpcoef2, tmpcoef2);
				mqi_coef_init_cpy (tmpcoef, POLY(res,0));
				mqi_coef_mul_schar(tmpcoef, tmpcoef, 2);
				mqi_curve_param_eval (dbpoint, sgline4, tmpcoef, tmpcoef2);
			}
			
			/* discard tmp vars */
			mqi_coef_list_clear(tmpcoef, tmpcoef2, NULL);

			qi_vect_optimize (dbpoint);

			mqi_vect_t dbpoint2d;
			mqi_mat_t  transpose, transco, tmpmat;

			mqi_vect_init_dynamic (dbpoint2d, 3, mqi_mat_typeof(m1m));

			mqi_mat_transpose_init (transpose, m1m);
			mqi_mat_mul_init       (tmpmat, transpose, m1m);
			mqi_mat_transco	       (transco, tmpmat);
			mqi_mat_mul	       (transco, transco, transpose);
			mqi_mat_mul	       ((mqi_mat_ptr)dbpoint2d, transco, (mqi_mat_ptr)dbpoint);

			mqi_curve_param_t par;
			mqi_mat_t	  m1co;
			mqi_vect_t	  l;

			mqi_curve_param_init_dynamic(par, 3, mqi_mat_typeof(qom));
			mqi_mat_init_dynamic(m1co, 3, 3, mqi_mat_typeof(qom));
			mqi_vect_init_dynamic(l, 2, mqi_mat_typeof(qom));
			qi_param_conic_through_ratpoint (qom, dbpoint2d, m1co, par, l);

			/** Stack the transformations */
			/* discard old tr */
			mqi_mat_clear (tr);
			mqi_mat_mul_init (tr, m1m, m1co);


			/** Rescale so that the double line and the conic meet at the point
			 *  corresponding to the parameters (1,0) */
			mqi_vect_t m;
			mqi_vect_init_dynamic (m, 2, mqi_vect_typeof(l));

			if ( mqi_coef_sign(VECT(l,1)) == 0 ) {
				mqi_vect_set_at_int(m,0,	0);
				mqi_vect_set_at_int(m,1,	1);
			}else{
				mqi_vect_set_at_int(m,0,	1);
				mqi_vect_set_at_int(m,1,	0);
			}

			/** Now rewrite the par accordingly, i.e. replace l[0]*u+l[1]*v by v and 
			 * m[0]*u+m[1]*v by u */
			mqi_mat_t par_trans;
			mqi_mat_init_dynamic (par_trans, 3, 3, mqi_vect_typeof(l));

			mqi_mat_set_at (par_trans, 0,0, VECT(l,1));
			mqi_coef_mul   (MAT(par_trans,0,0), MAT(par_trans,0,0), MAT(par_trans,0,0));
			mqi_mat_set_at (par_trans, 0,1, VECT(m,1));
			mqi_coef_mul   (MAT(par_trans,0,1), MAT(par_trans,0,1), MAT(par_trans,0,1));
			mqi_mat_set_at (par_trans, 1,0, VECT(l,0));
			mqi_coef_mul   (MAT(par_trans,1,0), MAT(par_trans,1,0), MAT(par_trans,1,0));
			mqi_mat_set_at (par_trans, 1,1, VECT(m,0));
			mqi_coef_mul   (MAT(par_trans,1,1), MAT(par_trans,1,1), MAT(par_trans,1,1));

			mqi_mat_set_at (par_trans, 0,2, VECT(m,1));
			mqi_coef_mul   (MAT(par_trans,0,2), MAT(par_trans,0,2), VECT(l,1));
			mqi_coef_mul_schar
				       (MAT(par_trans,0,2), MAT(par_trans,0,2), -2);
			mqi_mat_set_at (par_trans, 1,2, VECT(m,0));
			mqi_coef_mul   (MAT(par_trans,1,2), MAT(par_trans,1,2), VECT(l,0));
			mqi_coef_mul_schar
				       (MAT(par_trans,1,2), MAT(par_trans,1,2), -2);

			mqi_mat_set_at (par_trans, 2,0, VECT(l,0));
			mqi_coef_mul   (MAT(par_trans,2,0), MAT(par_trans,2,0), VECT(l,1));
			mqi_coef_neg   (MAT(par_trans,2,0), MAT(par_trans,2,0));
			mqi_mat_set_at (par_trans, 2,1, VECT(m,0));
			mqi_coef_mul   (MAT(par_trans,2,1), MAT(par_trans,2,1), VECT(m,1));
			mqi_coef_neg   (MAT(par_trans,2,1), MAT(par_trans,2,1));

			
			mqi_coef_linexpr2(MAT(par_trans,2,2),2,	
						1,VECT(l,0),VECT(m,1),	1,VECT(l,1),VECT(m,0));

			/** $$ Quid of resizing ? */
			mqi_mat_t trcpy;

			mqi_mat_init_cpy(trcpy,tr);
			/* discard old tr */
			mqi_mat_clear(tr);

			mqi_mat_mul_init (tr, trcpy, par_trans);

			/** $$ Is it the correct optimization routine ? */
			qi_mat_optimize_3_update(tr,l);

			mqi_curve_param_t conic;

			mqi_curve_param_init_dynamic(conic, 4, mqi_mat_typeof(par_trans));
			mqi_curve_param_mul_mat (conic, tr, par);

			qi_inter_component_init (comp, QI_INTER_COMPONENT_2, conic);
			qi_inter_component_set_type (comp, QI_INTER_COMPONENT_CONIC);

			qi_inter_add_component (rop, comp);
			/* discard comp */
			qi_inter_component_clear(comp);

			
			/* Free local memory */
			mqi_mat_list_clear(q, q_other, sing, m1p, m1m, m1db, tr, trcpy, transpose, transco,
					   tmpmat, m1co, par_trans, qop, qom, m1, m2, NULL);
			mqi_coef_list_clear(D, coef, NULL);
			mqi_curve_param_list_clear(tmp, sgline4, par, conic, line, NULL);
			mqi_poly_clear(res);
			mqi_vect_list_clear(dbpoint, dbpoint2d, l, m, NULL);

		}else {
			/* The two initial quadrics are cones */

			/** The apex of each cone is a rational point on the other cone - We find a simple
			 *  rational point on the line linking the two vertices */

			/** The line param */
			mqi_curve_param_t line;
			mqi_vect_t	  ratpoint;

			mqi_curve_param_init_dynamic(line, mqi_mat_nrows(sing1), mqi_mat_typeof(sing1));
			mqi_curve_param_line_through_two_points
				(line, (mqi_vect_ptr)COL(sing1,0), (mqi_vect_ptr)COL(sing2,0));

			/** The rational point */
			mqi_vect_init_cpy(ratpoint, (mqi_vect_ptr)COL(sing2,0));

			/** Try to find a small height on the line by looking at the parameter
			 *  values such that one of the components is zero */

			/** $$ CUT PARAMETERS: TODO */
			mqi_vect_t vect;
			mqi_vect_initpool_dynamic (vect, mqi_curve_param_n_equations(line),
						   mqi_mat_typeof(sing2));
			mqi_curve_param_eval_int  (vect, line, 1,0);

			if ( mqi_vect_equals ((mqi_vect_ptr)COL(sing1,0), vect) == 1 )
				mqi_curve_param_eval_int (vect, line, 0,1);
			
			mqi_vect_init_cpy (ratpoint, vect);

			qi_inter_component_init (comp, QI_INTER_COMPONENT_2, line);
			qi_inter_component_set_type (comp, QI_INTER_COMPONENT_LINE);
			qi_inter_component_set_multiplicity(comp, 2);

			qi_inter_add_component (rop, comp);
			/* discard comp */
			qi_inter_component_clear(comp);

			mqi_surface_param_t par;
			mqi_mat_t	    p_trans, q_tmp;
			mqi_vect_t	    l;

			/* $$ Actually, the current implementation of qi_param_cone_through_ratpoint
			 *    initializes par and p_trans automatically. The original code
			 *    makes this initialization before the call.
			 */
			/*mqi_surface_param_init_dynamic(par, 4, mqi_mat_typeof(q2));
			mqi_mat_init_dynamic(p_trans, 4, 4, mqi_mat_typeof(q2));*/
			mqi_vect_init_dynamic(l, 2, mqi_mat_typeof(q2));


			/** Parametrize one of the cones */
			qi_param_cone_through_ratpoint(q2, (mqi_vect_ptr)COL(sing1,0), ratpoint, p_trans, par, l);

			/** $$ Is it the correct optimization routine ? */
			qi_mat_optimize_3_update(p_trans, l);

			/** The quadric is the canonical frame of the cone */
			mqi_mat_transformation (q_tmp, q2, p_trans, MQI_MAT_NOTRANSCO);

			/** Plug in the other quadric */
			mqi_hhpoly_t res;

			qi_param_plug_sp_in_quadric (res, par, q_tmp, par);

			/** The common factor of res (see below)
			  * 		  * res has the form:
			  *       ---> (l[0]*s+l[1]*t)^2*(u^2*p(s,t) + u*v)
			  *
			  * The solution (-l[1],l[0]) corresponds to the double line. The
			  * solution v = -u*p(s,t) corresponds to the conic. Let us rescale so
			  * that (1,0) corresponds to the point of intersection of the line and
			  * the conic */
			
			mqi_vect_t  m;

			mqi_vect_init_dynamic (m, 2, mqi_vect_typeof(l));

			if ( mqi_coef_sign(VECT(l,1)) == 0 ) {
				mqi_vect_set_at_int(m,0,	0);
				mqi_vect_set_at_int(m,1,	1);
			}else{
				mqi_vect_set_at_int(m,0,	1);
				mqi_vect_set_at_int(m,1,	0);
			}

			/** Now rewrite the par accordingly, i.e. replace l[0]*s+l[1]*t by t and 
			 * m[0]*s+m[1]*t by s */
			mqi_mat_t par_trans;

			mqi_mat_init_dynamic (par_trans, 4, 4, mqi_vect_typeof(l));

			mqi_mat_set_at (par_trans, 0,0, VECT(l,1));
			mqi_coef_mul   (MAT(par_trans,0,0), MAT(par_trans,0,0), MAT(par_trans,0,0));
			mqi_mat_set_at (par_trans, 0,1, VECT(m,1));
			mqi_coef_mul   (MAT(par_trans,0,1), MAT(par_trans,0,1), MAT(par_trans,0,1));
			mqi_mat_set_at (par_trans, 1,0, VECT(l,0));
			mqi_coef_mul   (MAT(par_trans,1,0), MAT(par_trans,1,0), MAT(par_trans,1,0));
			mqi_mat_set_at (par_trans, 1,1, VECT(m,0));
			mqi_coef_mul   (MAT(par_trans,1,1), MAT(par_trans,1,1), MAT(par_trans,1,1));

			mqi_mat_set_at (par_trans, 0,2, VECT(m,1));
			mqi_coef_mul   (MAT(par_trans,0,2), MAT(par_trans,0,2), VECT(l,1));
			mqi_coef_mul_schar
				       (MAT(par_trans,0,2), MAT(par_trans,0,2), -2);
			mqi_mat_set_at (par_trans, 1,2, VECT(m,0));
			mqi_coef_mul   (MAT(par_trans,1,2), MAT(par_trans,1,2), VECT(l,0));
			mqi_coef_mul_schar
				       (MAT(par_trans,1,2), MAT(par_trans,1,2), -2);

			mqi_mat_set_at (par_trans, 2,0, VECT(l,0));
			mqi_coef_mul   (MAT(par_trans,2,0), MAT(par_trans,2,0), VECT(l,1));
			mqi_coef_neg   (MAT(par_trans,2,0), MAT(par_trans,2,0));
			mqi_mat_set_at (par_trans, 2,1, VECT(m,0));
			mqi_coef_mul   (MAT(par_trans,2,1), MAT(par_trans,2,1), VECT(m,1));
			mqi_coef_neg   (MAT(par_trans,2,1), MAT(par_trans,2,1));

			mqi_coef_linexpr2(MAT(p_trans,2,2),2,	1,VECT(l,0),VECT(m,1),	1,VECT(l,1),VECT(m,0));

			mqi_mat_set_at_int(par_trans, 1, 3, 3);

			/** And do the plug again */
			mqi_mat_t tmpmat;

			mqi_mat_transformation (tmpmat, q_tmp, par_trans, MQI_MAT_NOTRANSCO);
			/* discard old res */
			mqi_hhpoly_clear(res);
			qi_param_plug_sp_in_quadric (res, par, tmpmat, par);

			mqi_mat_clear(tmpmat);
			mqi_mat_init_cpy(tmpmat, p_trans);
			/* discard old ptrans */
			mqi_mat_clear(p_trans);
			mqi_mat_mul_init (p_trans, tmpmat, par_trans);
		
			mqi_hpoly_t tmp;
			mqi_hpoly_init_dynamic (tmp, mqi_mat_typeof(p_trans));
			mqi_poly_set_wi       (tmp, 2);

			mqi_vect_set_at_int (l,0,	0);
			mqi_vect_set_at_int (l,1,	-1);

			mqi_hpoly_t p1, p2, R;
			mqi_coef_t  coef;

			mqi_poly_div_qr (p1, R, coef, HHPOLY(res,2), tmp);
			/* discard previously initialized remainder and coefficient */
			mqi_poly_clear(R);
			mqi_coef_clear(coef);
			mqi_poly_div_qr (p2, R, coef, HHPOLY(res,1), tmp);
			mqi_poly_neg  (p2, p2);

			/* For the conic, build the param [u^2 v^2 u*v] and the transformation
			 * matrix as follows */
			mqi_curve_param_t conic2d;

			mqi_curve_param_init_dynamic(conic2d, 3,  mqi_mat_typeof(p_trans));
			mqi_poly_set_xi     (CP(conic2d,0), 2);
			mqi_poly_set_wi     (CP(conic2d,1), 2);
			mqi_poly_set_xw     (CP(conic2d,2));

			mqi_mat_t tr;
			mqi_mat_init_dynamic (tr, 4, 3, mqi_mat_typeof(par_trans));

			mqi_mat_set_at (tr, 0,0,	POLY(p2,0));
			mqi_mat_set_at (tr, 1,1,	POLY(p2,0));
			mqi_mat_set_at (tr, 2,2,	POLY(p2,0));

			if ( mqi_poly_is_zero(p1) == 0 ) {
				mqi_mat_set_at (tr, 3,0,	POLY(p1,2));
				mqi_mat_set_at (tr, 3,1,	POLY(p1,0));
				mqi_mat_set_at (tr, 3,2,	POLY(p1,1));
			}

			/** Stack the transformation */
			/** $$ Careful of resizing ! */
			mqi_mat_t trcpy;
			
			mqi_mat_init_cpy(trcpy,tr);
			/* discard old tr */
			mqi_mat_clear(tr);
			mqi_mat_mul_init (tr, p_trans, trcpy);

			qi_mat_optimize_3 (tr);

			mqi_curve_param_t conic;
			mqi_curve_param_init_dynamic(conic, 4, mqi_mat_typeof(p_trans));
			mqi_curve_param_mul_mat (conic, tr, conic2d);

			qi_inter_component_init (comp, QI_INTER_COMPONENT_2, conic);
			qi_inter_component_set_type (comp, QI_INTER_COMPONENT_CONIC);

			qi_inter_add_component (rop, comp);
			/* discard comp */
			qi_inter_component_clear(comp);


			/* Free local memory */
			mqi_vect_list_clear(ratpoint, vect, l, m, NULL);
			mqi_curve_param_list_clear(line, conic2d, NULL);
			mqi_surface_param_clear(par);
			mqi_mat_list_clear(p_trans, q_tmp, par_trans, tr, trcpy, tmpmat, NULL);
			mqi_coef_clear(coef);
			mqi_poly_list_clear(tmp, p1, p2, R, NULL);
			mqi_hhpoly_clear(res);

		}	


		/** $$ CUT PARAMETERS: Temporary ? */
		mqi_coef_t coef;

		mqi_coef_init_dynamic	  (coef, mqi_mat_typeof(q1));;
		mqi_coef_set_schar	  (coef, 0);
		qi_inter_cut_init	  (cut, QI_INTER_CUT_0, coef, coef);

		qi_inter_component_add_cut_parameter (rop->components[0], cut); 
		qi_inter_component_add_cut_parameter (rop->components[1], cut);
	
		qi_inter_cut_set_owner(cut, rop->components[0]);

		qi_inter_set_optimal (rop);

		/* Free local memory */
		qi_inter_cut_clear(cut);
		mqi_coef_clear(coef);


}

/** Line and triple line */
void __qi_inter_vanish_triple_and_line (qi_inter_t rop, mqi_mat_t __IN q1, mqi_mat_t __IN q2, 
				        mqi_mat_t __IN q, mqi_mat_t __IN q_other, mqi_mat_t __IN q_sing,
				        mqi_mat_t __IN proj_mat, __IN mqi_vect_t sing_p0) {

		qi_inter_component_t	comp;
		qi_inter_cut_t		cut;

		qi_inter_init(rop);
		qi_inter_set_type (rop, 18,1);

		/** The multiple line is the singular line of the 3D pair of planes q
		  * We already now one point of this line (sing_p0). We compute a second point
		  * by taking the singular point of the pair of lines q. */
		mqi_mat_t q_sing3, tmpmat;

		mqi_mat_resize (tmpmat, q_sing, 4, 1);
		mqi_mat_mul_init(q_sing3, proj_mat, tmpmat);

		qi_mat_optimize (q_sing3);

		/** Build a transformation matrix */
		mqi_mat_t tr;
		mqi_mat_init_dynamic (tr, 4, 2, mqi_mat_typeof(q_sing));
		mqi_mat_cpy_col	     (tr, (mqi_mat_ptr)sing_p0, 0, 0);
		mqi_mat_cpy_col	     (tr, q_sing3, 0, 1);

		mqi_curve_param_t lin;
		mqi_curve_param_init_dynamic(lin, mqi_mat_nrows(tr), mqi_mat_typeof(tr));
		mqi_curve_param_line_through_two_points (lin,
				(mqi_vect_ptr)COL(tr,0), (mqi_vect_ptr)COL(tr,1));

		/** $$ CUT PARAMETERS: TODO */

		qi_inter_component_init (comp, QI_INTER_COMPONENT_2, lin);
		qi_inter_component_set_type (comp, QI_INTER_COMPONENT_LINE);
		qi_inter_component_set_multiplicity (comp, 3);

		qi_inter_add_component (rop, comp);
		/* discard comp */
		qi_inter_component_clear(comp);

		mqi_coef_t D;
		mqi_mat_t m1, m2;

		mqi_mat_init_dynamic(m1, 3, 2, mqi_mat_typeof(q1));
		mqi_mat_init_dynamic(m2, 3, 1, mqi_mat_typeof(q1));
		qi_param_line_pair (q, (mqi_vect_ptr)COL(tr,0), D, m1, m2);

		/** The two lines are necessarily rational */
		mqi_mat_t l1p, l1m;
		mqi_mat_init_cpy (l1p, m1);
		mqi_mat_init_cpy (l1m, m1);

		mqi_mat_add (COL(l1p,0), COL(m1,0), COL(m2,0));
		mqi_mat_sub (COL(l1m,0), COL(m1,0), COL(m2,0));

		/** Now, one of trans(l1p)*q_other*l1p and trans(l1m)*q_other*l1m represents
		  * a double point, and the other a pair of points */
		mqi_mat_t it_pts, trmat;

		mqi_mat_transformation (it_pts, q_other, l1p, MQI_MAT_NOTRANSCO);
		mqi_mat_init_cpy (trmat, l1p);

		if ( mqi_coef_sign (MAT(it_pts,0,1)) == 0 ) {
			/** The pair of points is on the other */
			/* discard old it_pts */
			mqi_mat_clear(it_pts);
			mqi_mat_transformation (it_pts, q_other, l1m, MQI_MAT_NOTRANSCO);
			/* discard old trmat */
			mqi_mat_clear(trmat);
			mqi_mat_init_cpy (trmat, l1m);
		}

		 /** The two solutions are (0,1) and (2*it_pts(0,1),-it_pts(0,0)): the first
		   * point is already known */
		mqi_mat_t p1d;
		mqi_mat_init_dynamic (p1d, 2, 1, mqi_mat_typeof(it_pts));

		mqi_mat_set_at 	   (p1d, 0,0,	MAT(it_pts,0,1));
		mqi_coef_mul_schar (MAT(p1d,0,0), MAT(p1d,0,0), 2);
		mqi_mat_set_at	   (p1d, 1,0,	MAT(it_pts,0,0));
		mqi_coef_neg	   (MAT(p1d,1,0), MAT(p1d,1,0));

		qi_mat_optimize(p1d);

		/** The point in 2D */
		mqi_mat_t p1dcpy;

		mqi_mat_init_cpy(p1dcpy, p1d);
		/* discard old p1d */
		mqi_mat_clear(p1d);
		mqi_mat_mul_init (p1d, trmat, p1dcpy);

		/** The point in 3D */
		/* discard old tmpmat */
		mqi_mat_clear (tmpmat);
		mqi_mat_resize (tmpmat, p1d, 4, 1);
		mqi_mat_mul_init (p1d, proj_mat, tmpmat);

		qi_mat_optimize(p1d);

		/** Build a transformation matrix */
		mqi_mat_t tr2;

		mqi_mat_init_dynamic (tr2, 4, 2, mqi_mat_typeof(p1d));

		mqi_mat_cpy_col (tr2, (mqi_mat_ptr)sing_p0, 0, 0);
		mqi_mat_cpy_col (tr2, p1d, 0, 1);

		mqi_curve_param_t lin2;

		mqi_curve_param_initpool_dynamic(lin2, mqi_mat_nrows(tr2), mqi_mat_typeof(tr2));
		mqi_curve_param_line_through_two_points (lin2,
				(mqi_vect_ptr)COL(tr2,0), (mqi_vect_ptr)COL(tr2,1));

		/** $$ CUT PARAMETERS: TODO */

		/** The simple line */
		qi_inter_component_init (comp, QI_INTER_COMPONENT_2, lin2);
		qi_inter_component_set_type (comp, QI_INTER_COMPONENT_LINE);
		qi_inter_add_component (rop, comp);


		/** $$ CUT PARAMETERS: Temporary ? */
		mqi_coef_t coef;

		mqi_coef_init_dynamic (coef, mqi_mat_typeof(tr2));
		mqi_coef_set_schar    (coef, 0);
		qi_inter_cut_init (cut, QI_INTER_CUT_0, coef, coef);

		qi_inter_component_add_cut_parameter (rop->components[0], cut);
		qi_inter_component_add_cut_parameter (rop->components[1], cut);

		qi_inter_cut_set_owner (cut, rop->components[0]);

		qi_inter_set_optimal (rop);

		
		/** Free local memory */
		mqi_mat_list_clear (q_sing3, tmpmat, tr, m1, m2, l1p, l1m, it_pts,
				    trmat, p1d, p1dcpy, tr2, NULL);
		mqi_curve_param_list_clear(lin, lin2, NULL);
		mqi_coef_list_clear(D, coef, NULL);


}

/** Quadruple line */
void __qi_inter_vanish_quadruple_line (qi_inter_t rop, mqi_mat_t __IN q1, mqi_mat_t __INq2,
				      mqi_mat_t __IN q, mqi_mat_t __IN q_other, mqi_mat_t __IN q_sing, mqi_mat_t __IN proj_mat,
				      mqi_vect_t __IN sing_p0)
{

		qi_inter_component_t	comp;

		qi_inter_init(rop);
		qi_inter_set_type (rop, 20,1);

		/** We already know one point of the multiple line: sing_p0. The other point if
		  * found by parameterizing the double plane q and computing its intersection
		  * with q_other */
		mqi_mat_t m1;

		mqi_mat_init_dynamic(m1, 3, 2, mqi_mat_typeof(q1));
		qi_param_line_double (q, q_sing, m1);

		mqi_mat_t two_two;
		mqi_mat_transformation (two_two, q_other, m1, MQI_MAT_NOTRANSCO);

		/** This 2x2 matrix is singular and represents a double point */
		mqi_mat_t point1d;
		mqi_mat_init_dynamic (point1d, 2, 1, mqi_mat_typeof(q1));

		if ( mqi_coef_sign(MAT(two_two,0,0)) != 0 ) {
			mqi_mat_set_at (point1d, 0,0,	MAT(two_two,0,1));
			mqi_coef_neg   (MAT(point1d,0,0), MAT(point1d,0,0));
			mqi_mat_set_at (point1d, 1,0,	MAT(two_two,0,0));
		}else {
			mqi_mat_set_at (point1d, 1,0,	MAT(two_two,0,1));
			mqi_coef_neg (MAT(point1d,1,0), MAT(point1d,1,0));
			mqi_mat_set_at (point1d, 0,0,	MAT(two_two,1,1));
		}

		/** Stacks the transformations */
		/** "Resizes" m1 to 4x2 */
		mqi_mat_t tmpmat, point3d;

		mqi_mat_resize(tmpmat, m1, 4, 2);
		/* discard m1 */
		mqi_mat_clear(m1);
		mqi_mat_init_cpy (m1, tmpmat);

		/* discard tmpmat */
		mqi_mat_clear(tmpmat);

		mqi_mat_mul_init (tmpmat, m1, point1d);
		mqi_mat_mul_init (point3d, proj_mat, tmpmat);

		qi_mat_optimize(point3d);

		mqi_curve_param_t lin;
		mqi_curve_param_init_dynamic(lin, mqi_vect_size(sing_p0), mqi_vect_typeof(sing_p0));
		mqi_curve_param_line_through_two_points (lin, sing_p0, (mqi_vect_ptr)COL(point3d,0));

		/** Reparametrizes the line */
		qi_inter_component_init (comp, QI_INTER_COMPONENT_2, lin);
		qi_inter_component_set_type (comp, QI_INTER_COMPONENT_LINE);
		qi_inter_component_set_multiplicity(comp, 4);
		qi_inter_add_component (rop, comp);
	
		/* discard comp */
		qi_inter_component_clear(comp);

		qi_inter_set_optimal (rop);

	
		/* Free local memory */
		mqi_mat_list_clear (m1, two_two, tmpmat, point1d, point3d, NULL);
		mqi_curve_param_clear(lin);

}

/** Two double lines */
void __qi_inter_vanish_two_double_lines (qi_inter_t rop, mqi_mat_t __IN q1, mqi_mat_t __IN q2, 
					 mqi_mat_t __IN q, mqi_mat_t __IN q_other, mqi_mat_t __IN q_sing,
					 mqi_mat_t __IN proj_mat, mqi_vect_t __IN sing_p0)
{

		qi_inter_component_t	comp;
		qi_inter_cut_t		cut;

		mqi_vect_t in_qo;

		mqi_vect_init_dynamic (in_qo, 2, mqi_mat_typeof(q_other));
		qi_mat_inertia (in_qo, q_other);

		/** Inertia of other singular quadric is [1 1] */
		qi_inter_init(rop);
		if ( mqi_coef_cmp_schar(VECT(in_qo,0), 1) == 0 ) {
			qi_inter_set_type (rop, 19,2);
		}else {
		/** Inertia of other singular quadric is [2 0] */
			qi_inter_set_type (rop, 19,1);
		}

		 /** q is a double plane, q_other a pair of planes. When q_other is imaginary, the
		   * intersection is reduced to the common singular point sing_p0 */

		if ( mqi_coef_cmp_schar (VECT(in_qo,0), 2) == 0 ) {

			mqi_curve_param_t point;
			mqi_curve_param_init_dynamic(point, mqi_vect_size(sing_p0), mqi_vect_typeof(sing_p0));
			mqi_curve_param_set_vect(point, sing_p0);
			
			qi_inter_component_init (comp, QI_INTER_COMPONENT_2, point);
			qi_inter_component_set_type (comp, QI_INTER_COMPONENT_POINT);

			qi_inter_add_component (rop, comp);
			
			/** Free local memory */
			qi_inter_component_clear (comp);
			mqi_curve_param_clear (point);

		}else {
			/** First parametrize q */
			mqi_mat_t m1;

			mqi_mat_init_dynamic (m1, 3, 2, mqi_mat_typeof(q1));
			qi_param_line_double (q, q_sing, m1);
			
			/* Now parameterize the pair of lines */
			mqi_mat_t q_other_1d, m1_1d, m2_1d;
			mqi_coef_t D;

			mqi_mat_transformation (q_other_1d, q_other, m1, MQI_MAT_NOTRANSCO);

			mqi_mat_init_dynamic (m1_1d, 2, 1, mqi_mat_typeof(q1));
			mqi_mat_init_dynamic (m2_1d, 2, 1, mqi_mat_typeof(q1));
			qi_param_2x2	     (q_other_1d, D, m1_1d, m2_1d);

			if ( mqi_coef_sign(D) == 0 ) {

				mqi_mat_t m1_1dp, m1_1dm;

				/** The two lines are rational */
				mqi_mat_init_cpy (m1_1dp, m1_1d);
				mqi_mat_init_cpy (m1_1dm, m1_1d);

				mqi_mat_add (m1_1dp, m1_1dp, m2_1d);
				mqi_mat_sub (m1_1dm, m1_1dm, m2_1d);

				mqi_mat_mul (m1_1dp, m1, m1_1dp);
				mqi_mat_mul (m1_1dm, m1, m1_1dm);

				qi_mat_optimize (m1_1dp);
				qi_mat_optimize (m1_1dm);

				/** Go to 3D: we now have two points on the pair of lines */
				mqi_mat_t tmpmat;

				mqi_mat_resize(tmpmat, m1_1dp, 4, 1);
				mqi_mat_clear(m1_1dp);
				mqi_mat_init_cpy(m1_1dp, tmpmat);

				mqi_mat_resize(tmpmat, m1_1dm, 4, 1);
				mqi_mat_clear(m1_1dm);
				mqi_mat_init_cpy(m1_1dm, tmpmat);

				mqi_mat_mul (m1_1dp, proj_mat, m1_1dp);
				mqi_mat_mul (m1_1dm, proj_mat, m1_1dm);

				qi_mat_optimize (m1_1dp);
				qi_mat_optimize (m1_1dm);

				mqi_curve_param_t linep, linem;
				mqi_curve_param_init_dynamic (linep, mqi_vect_size(sing_p0), mqi_vect_typeof(sing_p0));
				mqi_curve_param_init_dynamic (linem, mqi_vect_size(sing_p0), mqi_vect_typeof(sing_p0));	
		
				mqi_curve_param_line_through_two_points (linep,
						sing_p0, (mqi_vect_ptr)COL(m1_1dp,0));
				mqi_curve_param_line_through_two_points (linem,
						sing_p0, (mqi_vect_ptr)COL(m1_1dm,0));

				/** $$ CUT PARAMETERS: TODO */
				qi_inter_component_init (comp, QI_INTER_COMPONENT_2, linep);
				qi_inter_component_set_type (comp, QI_INTER_COMPONENT_LINE);
				qi_inter_add_component (rop, comp);
				/* discard comp */
				qi_inter_component_clear(comp);

				qi_inter_component_init (comp, QI_INTER_COMPONENT_2, linem);
				qi_inter_component_set_type (comp, QI_INTER_COMPONENT_LINE);
				qi_inter_add_component (rop, comp);
				/* discard comp */
				qi_inter_component_clear(comp);

				/** Free local memory */
				mqi_mat_list_clear (m1_1dp, m1_1dm, tmpmat, NULL);
				mqi_curve_param_list_clear(linep, linem, NULL);
			

			}else {
				/** Lines are not rational */
				
				/** Stacks the transformations */
				mqi_mat_mul (m1_1d, m1, m1_1d);
				mqi_mat_mul (m2_1d, m1, m2_1d);

				/** Go to 3D: we now have two points on the pair of lines */
				mqi_mat_t tmpmat;

				mqi_mat_resize(tmpmat, m1_1d, 4, 1);
				mqi_mat_clear(m1_1d);
				mqi_mat_init_cpy(m1_1d, tmpmat);

				mqi_mat_resize(tmpmat, m2_1d, 4, 1);
				mqi_mat_clear(m2_1d);
				mqi_mat_init_cpy(m2_1d, tmpmat);

				mqi_mat_mul (m1_1d, proj_mat, m1_1d);
				mqi_mat_mul (m2_1d, proj_mat, m2_1d);

				qi_mat_optimize_2 (m1_1d, m2_1d);

				/** The components of the lines */
				/** Null vector */
				mqi_vect_t vect;
				mqi_vect_init_dynamic (vect, 4, mqi_mat_typeof(m1_1d));

				mqi_curve_param_t line_rat, line_D;
				mqi_curve_param_init_dynamic (line_rat, mqi_vect_size(sing_p0), mqi_vect_typeof(sing_p0));
				mqi_curve_param_init_dynamic (line_D, mqi_vect_size(vect), mqi_vect_typeof(vect));

				mqi_curve_param_line_through_two_points (line_rat, sing_p0, (mqi_vect_ptr)COL(m1_1d,0));
				mqi_curve_param_line_through_two_points (line_D, vect, (mqi_vect_ptr)COL(m2_1d,0));

				/** $$ Reparametrize the lines: ?? Resulting vector not used in the original QI code... */
				mqi_vect_t ignored;

				qi_param_line_improved2 (ignored, line_rat, line_D, D);

				qi_inter_create_components (rop, 2, QI_INTER_COMPONENT_LINE,
							    QI_INTER_COMPONENT_3, line_rat, line_D, D);
		
				/** Free local memory */
				mqi_mat_clear(tmpmat);
				mqi_vect_list_clear(vect, ignored, NULL);
				mqi_curve_param_list_clear(line_rat, line_D, NULL);

			}

			qi_inter_component_set_multiplicity(rop->components[0], 2);
			qi_inter_component_set_multiplicity(rop->components[1], 2);

			/** Free local memory */
			mqi_mat_list_clear(m1, q_other_1d, m1_1d, m2_1d, NULL);
			mqi_coef_clear(D);

		}

		if ( mqi_coef_cmp_schar(VECT(in_qo,0),2) != 0 )
		{
				
			/** Create cut parameters */
			mqi_coef_t coef;

			mqi_coef_init_dynamic(coef, mqi_mat_typeof(q1));
			mqi_coef_set_schar(coef,0);

			qi_inter_cut_init (cut, QI_INTER_CUT_0, coef, coef);

			qi_inter_component_add_cut_parameter(rop->components[0], cut);
			qi_inter_component_add_cut_parameter(rop->components[1], cut);

			/* When there is a cut parameter, design exactly one of the components
			 * to be the owner of this cut parameter.
			 * It is for internal memory management. */
			qi_inter_cut_set_owner(cut, rop->components[0]);

			/** Free local memory */
			mqi_coef_clear(coef);

		}

		qi_inter_set_optimal (rop);

		/** Free local memory */
		mqi_vect_clear(in_qo);

}

/** Two concurrent lines and one double line */
void __qi_inter_vanish_two_conc_lines (qi_inter_t rop, mqi_mat_t __IN q1, mqi_mat_t __IN q2, 
                                       mqi_mat_t __IN q, mqi_mat_t __IN q_other, mqi_mat_t __IN q_sing,
				       mqi_mat_t __IN proj_mat, mqi_vect_t __IN sing_p0,
				       mqi_coef_t __IN in_q)
{

	qi_inter_component_t	comp;
	qi_inter_cut_t		cut;

	/** The double line is the singular line of the pair of planes q
	 * We already now one point of this line (sing_p0). We compute a second point
	 * by taking the singular point of the pair of lines q */
	
	qi_inter_init(rop);
	/** Inertia is [1 1]: real pair of planes */
	if ( mqi_coef_cmp_schar(in_q, 1) == 0 ) {
		qi_inter_set_type	(rop, 17,2);
	}else{	/** Inertia is [2 0]: imaginary pair of planes */
		qi_inter_set_type	(rop, 17,1);
	}

	mqi_mat_t tmpmat, q_sing3;

	mqi_mat_resize(tmpmat, q_sing, 4, 1);
	mqi_mat_mul_init(q_sing3, proj_mat, tmpmat);

	qi_mat_optimize(q_sing3);

	/** Build a transformation matrix */
	mqi_mat_t tr;

	mqi_mat_init_dynamic (tr, 4, 2, mqi_mat_typeof(q1));
	mqi_mat_cpy_col	     (tr, (mqi_mat_ptr)sing_p0, 0, 0);
	mqi_mat_cpy_col	     (tr, q_sing3, 0, 1);

	mqi_curve_param_t lin;
	mqi_curve_param_init_dynamic (lin, mqi_mat_nrows(tr), mqi_mat_typeof(tr));
	mqi_curve_param_line_through_two_points (lin,
			(mqi_vect_ptr)COL(tr,0), (mqi_vect_ptr)COL(tr,1));

	/** $$ CUT PARAMETERS of the lines (?) : TODO */
	qi_inter_component_init 	(comp, QI_INTER_COMPONENT_2, lin);
	qi_inter_component_set_type 	(comp, QI_INTER_COMPONENT_LINE);
	qi_inter_component_set_multiplicity(comp, 2);

	qi_inter_add_component (rop, comp);
	/* discard comp */
	qi_inter_component_clear(comp);

	if ( mqi_coef_cmp_schar (in_q, 1) == 0 ) {

		mqi_mat_t m1, m2, singular;
		mqi_coef_t D;

		qi_mat_singular (singular, q_other);

		mqi_mat_init_dynamic (m1, 3, 2, mqi_mat_typeof(q1));
		mqi_mat_init_dynamic (m2, 3, 1, mqi_mat_typeof(q1));
		
		/** q_other is a pair of lines: parametrize it */
		qi_param_line_pair (q_other, (mqi_vect_ptr)COL(singular,0), D, m1, m2);

		/** The two lines are necessarily rational */

		mqi_mat_t l1p, l1m;
		mqi_mat_init_cpy (l1p, m1);
		mqi_mat_init_cpy (l1m, m1);

		mqi_mat_add (COL(l1p,0), COL(m1,0), COL(m2,0));
		mqi_mat_sub (COL(l1m,0), COL(m1,0), COL(m2,0));

		/** Now, one of trans(l1p)*q_other*l1p and trans(l1m)*q_other*l1m has
		 * determinant zero: one line of q_other is in q */
		mqi_mat_t it_pts, tr_mat;
		mqi_coef_t coef;

		mqi_mat_transformation (it_pts, q, l1p, MQI_MAT_NOTRANSCO);
		mqi_mat_init_cpy (tr_mat, l1p);

		mqi_coef_init_dynamic(coef, mqi_mat_typeof(it_pts));
		mqi_mat_det (coef, it_pts);

		if ( mqi_coef_sign(coef) == 0 ) {
			/* discard old it_pts */
			mqi_mat_clear(it_pts);
			mqi_mat_transformation(it_pts, q, l1m, MQI_MAT_NOTRANSCO);
			/* discard old tr_mat */
			mqi_mat_clear(tr_mat);
			mqi_mat_init_cpy (tr_mat, l1m);
		}

		mqi_mat_t m1_1d, m2_1d;
		
		/** Parametrize it_pts */
		mqi_mat_init_dynamic (m1_1d, 2, 1, mqi_mat_typeof(q1));
		mqi_mat_init_dynamic (m2_1d, 2, 1, mqi_mat_typeof(q1));
		
		/* discard old D */
		mqi_coef_clear(D);
		qi_param_2x2 (it_pts, D, m1_1d, m2_1d);

		if ( mqi_coef_sign(D) == 0 )
		{

			mqi_mat_t m1_1dp, m1_1dm;

			/** The two lines are rational */
			mqi_mat_init_cpy (m1_1dp, m1_1d);
			mqi_mat_init_cpy (m1_1dm, m1_1d);

			mqi_mat_add (m1_1dp, m1_1dp, m2_1d);
			mqi_mat_sub (m1_1dm, m1_1dm, m2_1d);

			mqi_mat_mul (m1_1dp, tr_mat, m1_1dp);
			mqi_mat_mul (m1_1dm, tr_mat, m1_1dm);

			qi_mat_optimize (m1_1dp);
			qi_mat_optimize (m1_1dm);

			/** Go to 3D: we now have two points on the pair of lines */
			mqi_mat_t tmpmat;

			mqi_mat_resize(tmpmat, m1_1dp, 4, 1);
			mqi_mat_clear(m1_1dp);
			mqi_mat_mul_init(m1_1dp, proj_mat, tmpmat);

			mqi_mat_clear(tmpmat);
			mqi_mat_resize(tmpmat, m1_1dm, 4, 1);
			mqi_mat_clear(m1_1dm);
			mqi_mat_mul_init(m1_1dm, proj_mat, tmpmat);

			qi_mat_optimize (m1_1dp);
			qi_mat_optimize (m1_1dm);

			mqi_curve_param_t linep, linem;
			mqi_curve_param_init_dynamic (linep, mqi_vect_size(sing_p0), mqi_vect_typeof(sing_p0));
			mqi_curve_param_init_dynamic (linem, mqi_vect_size(sing_p0), mqi_vect_typeof(sing_p0));
			
			mqi_curve_param_line_through_two_points (linep, sing_p0, (mqi_vect_ptr)COL(m1_1dp,0));
			mqi_curve_param_line_through_two_points (linem, sing_p0, (mqi_vect_ptr)COL(m1_1dm,0));

			/** $$ CUT PARAMETERS: TODO */
			qi_inter_component_init (comp, QI_INTER_COMPONENT_2, linep);
			qi_inter_component_set_type(comp, QI_INTER_COMPONENT_LINE);
			qi_inter_add_component (rop, comp);
			/*discard comp */
			qi_inter_component_clear(comp);

			qi_inter_component_init (comp, QI_INTER_COMPONENT_2, linem);
			qi_inter_component_set_type(comp, QI_INTER_COMPONENT_LINE);

			qi_inter_add_component (rop, comp);
			/* discard comp */
			qi_inter_component_clear(comp);


			/** Free local memory */
			mqi_curve_param_list_clear(linep, linem, NULL);
			mqi_mat_list_clear(tmpmat, m1_1dp, m1_1dm, NULL);

		}else {
			/** Lines are not rational */
			
			/** Stacks the transformations */
			mqi_mat_mul (m1_1d, tr_mat, m1_1d);
			mqi_mat_mul (m2_1d, tr_mat, m2_1d);

			/** Go to 3D: we now have two points on the pair of lines */
			mqi_mat_t tmpmat;

			mqi_mat_resize(tmpmat, m1_1d, 4, 1);
			mqi_mat_clear(m1_1d);
			mqi_mat_mul_init(m1_1d, proj_mat, tmpmat);

			mqi_mat_clear(tmpmat);
			mqi_mat_resize(tmpmat, m2_1d, 4, 1);
			mqi_mat_clear(m2_1d);
			mqi_mat_mul_init(m2_1d, proj_mat, tmpmat);

			qi_mat_optimize_2 (m1_1d, m2_1d);

			/** The components of the lines */
			/** Null vector */
			mqi_vect_t vect, ignored;
			mqi_vect_init_dynamic (vect, 4, mqi_mat_typeof(m1_1d));

			mqi_curve_param_t line_rat, line_D;
			mqi_curve_param_init_dynamic (line_rat, mqi_vect_size(sing_p0), mqi_vect_typeof(sing_p0));
			mqi_curve_param_init_dynamic (line_D, mqi_vect_size(vect), mqi_vect_typeof(vect));

			mqi_curve_param_line_through_two_points (line_rat, sing_p0, (mqi_vect_ptr)COL(m1_1d,0));
			mqi_curve_param_line_through_two_points (line_D, vect, (mqi_vect_ptr)COL(m2_1d,0));

			qi_param_line_improved2 (ignored, line_rat, line_D, D);

			qi_inter_create_components (rop, 2, QI_INTER_COMPONENT_LINE,
						    QI_INTER_COMPONENT_2, line_rat, line_D, D);

			/** Free local memory */
			mqi_mat_clear(tmpmat);
			mqi_vect_list_clear(ignored, vect, NULL);
			mqi_curve_param_list_clear(line_rat, line_D, NULL);

		}

		/** Free local memory */
		mqi_mat_list_clear(m1, m2, singular, l1p, l1m, it_pts, tr_mat, m1_1d, m2_1d, NULL);
		mqi_coef_list_clear(coef, D);

	}
	
	/** Create cut parameters */
	mqi_coef_t coef;

	mqi_coef_set_schar(coef,0);
	qi_inter_cut_init (cut, QI_INTER_CUT_0, coef, coef);

	qi_inter_component_add_cut_parameter(rop->components[0], cut); 
	qi_inter_component_add_cut_parameter(rop->components[1], cut); 
	qi_inter_component_add_cut_parameter(rop->components[2], cut);

	qi_inter_cut_set_owner(cut, rop->components[0]);

	qi_inter_set_optimal (rop);

	/** Free local memory */
	mqi_coef_clear(coef);

}


/** Four concurrent lines (over the complexes) */
void __qi_inter_vanish_four_concurrent_lines (qi_inter_t rop, mqi_hpoly_t __IN det_p3, mqi_mat_t __IN q1, mqi_mat_t __IN q2,
					      mqi_mat_t __IN q1_r, mqi_mat_t __IN q2_r, mqi_vect_t __IN sing_p0,
					      mqi_mat_t __IN proj_mat)
{

	qi_inter_component_t	comp;
	qi_inter_cut_t		cut;

	/** If q1 is a pair of planes */
	if ( mqi_coef_sign(POLY(det_p3,3)) == 0 ) {
		__qi_inter_vanish_four_concurrent_lines_ratplane (rop, q1, q2, q1_r, q2_r, sing_p0, proj_mat);
		qi_inter_set_optimal(rop);
		return;
	/** If q2 is a pair of planes */
	}else if ( mqi_coef_sign(POLY(det_p3,0)) ==0 ){
		__qi_inter_vanish_four_concurrent_lines_ratplane (rop, q1, q2, q2_r, q1_r, sing_p0, proj_mat);
		qi_inter_set_optimal(rop);
		return;
	}	

	int rat_found = 0;

	/* Try to find rational roots */
	if ( qi_settings.optimize )
	{

		mqi_prime_factorization_t	f, g;
		unsigned long int		maxdiv = 100;
		mqi_coef_t			coef;
		int				desc;
		mqi_vect_t			div0, div3;

		mqi_prime_factorization_init (f);
		mqi_prime_factorization_init (g);

		mqi_coef_init_cpy (coef, POLY(det_p3,0));
		mqi_coef_abs	  (coef, coef);

		mqi_primefactor (f, coef, maxdiv);
		
		mqi_coef_abs	  (coef, POLY(det_p3,3));
		
		mqi_primefactor	  (g, coef, maxdiv);

		desc = qi_poly_descartes(det_p3);

		switch (desc)
		{
			case 0:
				__qi_inter_vanish_all_positive_divisors(div0, f, maxdiv);
				__qi_inter_vanish_all_negative_divisors(div3, g, maxdiv);
				break;

			case 3:
				__qi_inter_vanish_all_positive_divisors(div0, f, maxdiv);
				__qi_inter_vanish_all_positive_divisors(div3, g, maxdiv);
				break;

			default:
				__qi_inter_vanish_all_positive_divisors(div0, f, maxdiv);
				__qi_inter_vanish_all_divisors(div3, g, maxdiv);
				break;
		}
		
		int i, j;
		
		rat_found = 0;

		for ( i = 0; (i < mqi_vect_size(div0)) && !rat_found; i++ )
		{
			for ( j = 0; (j < mqi_vect_size(div3)) && !rat_found; j++ )
			{

				mqi_poly_eval_xw (coef, det_p3, VECT(div0,i), VECT(div3,j));

				if ( mqi_coef_sign(coef) == 0 )
					rat_found = 1;
			}
		}


		if ( rat_found )
		{
			
			mqi_mat_t q, tmpmat;

			mqi_mat_init_cpy(tmpmat, q1_r);
			mqi_mat_mul_coef(tmpmat, tmpmat, VECT(div0,i));

			mqi_mat_init_cpy(q, q2_r);
			mqi_mat_mul_coef(q, q, VECT(div3,j));

			mqi_mat_add (q, q, tmpmat);
			
			__qi_inter_vanish_four_concurrent_lines_ratplane (rop, q1, q2, q, q1_r, sing_p0, proj_mat);

			/** Free local memory */
			mqi_mat_list_clear(q, tmpmat, NULL);

		}

		/** Free local memory */
		mqi_coef_clear(coef);
		mqi_vect_list_clear(div0, div3, NULL);
		mqi_prime_factorization_clear(f);
		mqi_prime_factorization_clear(g);

	}

	if ( !rat_found )
	{

		/*  Use of inflexion and intersection of inflexion tangent with axis to separate
		 *  the roots. Inflexion is at -b/(3a). The other point is at (9ad-bc)/2(b^2-3ac)
		 */
		mqi_vect_t p1, p2;
		mqi_coef_t coef;

		mqi_vect_list_init_dynamic(2, mqi_mat_typeof(q1), p1, p2, NULL);

		mqi_coef_neg		(VECT(p1,0), POLY(det_p3,2));
		mqi_coef_mul_schar	(VECT(p1,1), POLY(det_p3,3), 3);

		qi_vect_optimize (p1);
		
		mqi_coef_init_dynamic(coef, mqi_poly_typeof(det_p3));
		mqi_poly_eval_xw (coef, det_p3, VECT(p1,0), VECT(p1,1));

		if ( mqi_coef_sign(coef) == 0 )
		{
			/* We have found a rational root */
			mqi_mat_t q, tmpmat;

			mqi_mat_init_cpy(tmpmat, q1_r);
			mqi_mat_mul_coef(tmpmat, tmpmat, VECT(p1,0));

			mqi_mat_init_cpy(q, q2_r);
			mqi_mat_mul_coef(q, q, VECT(p1,1));

			mqi_mat_add (q, q, tmpmat);

			__qi_inter_vanish_four_concurrent_lines_ratplane (rop, q1, q2, q, q1_r, sing_p0, proj_mat);

			/** Free local memory */
			mqi_mat_list_clear(q, tmpmat, NULL);

		}
		else
		{
			
			mqi_coef_linexpr2 (VECT(p2,0),2,	9,POLY(det_p3,3),POLY(det_p3,0),
				       			 	-1,POLY(det_p3,2),POLY(det_p3,1));
			mqi_coef_linexpr2 (VECT(p2,1),2,	2,POLY(det_p3,2),POLY(det_p3,2),
								-6,POLY(det_p3,3),POLY(det_p3,1));

			mqi_coef_mul_int (VECT(p1,0), VECT(p1,0), mqi_coef_sign(VECT(p1,1)));
			mqi_coef_abs (VECT(p1,1), VECT(p1,1));
			mqi_coef_mul_slong (VECT(p2,0), VECT(p2,0), mqi_coef_sign(VECT(p2,1)));
			mqi_coef_abs (VECT(p2,1), VECT(p2,1));
		
			if ( mqi_coef_sign(VECT(p2,1)) != 0 )
				qi_vect_optimize(p2);
	
			/** Reorder the points if needed */
			mqi_coef_linexpr2 (coef,2,	1,VECT(p1,0),VECT(p2,1),	-1,VECT(p2,0),VECT(p1,1));
			
			if ( mqi_coef_sign(coef) > 0 )
				mqi_vect_swap (p1,p2);

			/* Fake polynomial just to be able to use Descartes' rule of signs */
			mqi_hpoly_t fake;

			mqi_hpoly_init_dynamic(fake, mqi_poly_typeof(det_p3));
			mqi_poly_set_homogeneous(fake, 3);

			mqi_poly_eval_xw_int (POLY(fake,0), det_p3, -1, 0);
			mqi_poly_eval_xw (POLY(fake,1), det_p3, VECT(p1,0), VECT(p1,1));
			mqi_poly_eval_xw (POLY(fake,2), det_p3, VECT(p2,0), VECT(p2,1));
			mqi_poly_eval_xw_int (POLY(fake,3), det_p3, 1, 0);

			/* Number of real roots */
			int nbroot;
			int degen_inter;

			nbroot = qi_poly_descartes(fake);

			degen_inter = 0;

			/* 3 real roots */
			if ( nbroot == 3 )
			{

				/* Decide whether a [3 0] is at p1 and p2 */

				/* The matrix associated to p1 */
				mqi_mat_t q, tmpmat;

				mqi_mat_init_cpy(tmpmat, q1_r);
				mqi_mat_mul_coef(tmpmat, tmpmat, VECT(p1,0));

				mqi_mat_init_cpy(q, q2_r);
				mqi_mat_mul_coef(q, q, VECT(p1,1));

				mqi_mat_add(q, q, tmpmat);

				mqi_vect_t in_q;

				qi_mat_signed_inertia(in_q, q);

				/* If signed inertia is [1 2] or [2 1]: proceed with next point */
				if ( (mqi_coef_cmp_schar(VECT(in_q,0),1) == 0) ||
				     (mqi_coef_cmp_schar(VECT(in_q,0),2) == 0) )
				{

					mqi_mat_t tmpmat;

					mqi_mat_init_cpy(tmpmat, q1_r);
					mqi_mat_mul_coef(tmpmat, tmpmat, VECT(p2,0));

					mqi_mat_init_cpy(q, q2_r);
					mqi_mat_mul_coef(q, q, VECT(p2,1));

					mqi_mat_add (q, q, tmpmat);

					/* Its signed inertia */ 
					/* discard old in_q */
					mqi_vect_clear(in_q);
					qi_mat_signed_inertia(in_q, q);

					/** Free local memory */
					mqi_mat_clear(tmpmat);

				}
				
				/* If signed inertia is [0 3] or [3 0]: point */
				if ( (mqi_coef_cmp_schar(VECT(in_q,0),0) == 0) ||
				     (mqi_coef_cmp_schar(VECT(in_q,0),3) == 0) )
				{
					
					degen_inter = 1;

					qi_inter_init(rop);
					qi_inter_set_type(rop,	16,1);

					mqi_curve_param_t c;

					/* Output singular point of all quadrics */
					mqi_curve_param_init_dynamic(c, mqi_vect_size(sing_p0), mqi_vect_typeof(sing_p0));
					mqi_curve_param_set_vect (c, sing_p0);

					qi_inter_component_init (comp, QI_INTER_COMPONENT_2, c);
					qi_inter_component_set_type (comp, QI_INTER_COMPONENT_POINT);

					qi_inter_add_component(rop, comp);
					/* discard comp */
					qi_inter_component_clear(comp);

					/** Free local memory */
					mqi_curve_param_clear(c);

				}

				/** Free local memory */
				mqi_mat_list_clear(q, tmpmat, NULL);
				mqi_vect_clear(in_q);

			}

			if ( !degen_inter )
			{


				mqi_vect_t l, pt;

				mqi_vect_init_dynamic (l, 2, mqi_mat_typeof(q1_r));
				mqi_vect_init_dynamic (pt, 3, mqi_mat_typeof(q1_r));

				mqi_vect_set_at_int (pt,2,	1);

				mqi_mat_t q_rat;

				qi_pencil_quadric_through_ratpoint (q_rat, q1_r, q2_r, pt, l);

				mqi_coef_t coef;

				mqi_coef_init_dynamic (coef, mqi_poly_typeof(det_p3));

				mqi_poly_eval_xw (coef, det_p3, VECT(l,0), VECT(l,1));

				if ( mqi_coef_sign(coef) != 0 )
				{

					mqi_curve_param_t par;
					mqi_mat_t	  p_trans;
					mqi_vect_t	  l;

					mqi_curve_param_init_dynamic(par, 3, mqi_mat_typeof(q1));
					mqi_mat_init_dynamic(p_trans, 3, 3, mqi_mat_typeof(q1));
					mqi_vect_init_dynamic(l, 2, mqi_mat_typeof(q1));

					qi_param_conic_through_ratpoint (q_rat, pt, p_trans, par, l);

					/* Now the param is u*sing_p0 + v*proj_mat*p_trans*par, subject to the 
					 * constraint quart = 0. We put proj_mat and p_trans in a single
					 * transformation matrix and optimize */			
					mqi_mat_t tmpmat;

					/* p_trans is a 3 x 3 matrix so resize first */
					mqi_mat_resize (tmpmat, p_trans, 4, 3);
					/* discard old p_trans */
					mqi_mat_clear(p_trans);

					mqi_mat_mul_init (p_trans, proj_mat, tmpmat);
				
					qi_mat_optimize_3 (p_trans);
					
					/* The param in 3-space */
					mqi_curve_param_t par_3d;

					mqi_curve_param_init_dynamic(par_3d, 4, mqi_mat_typeof(p_trans));
					mqi_curve_param_mul_mat (par_3d, p_trans, par);

					/* Compute the constraint polynomial quart */
					mqi_hpoly_t quart;

					qi_param_plug_cp_in_quadric (quart, par_3d, q1, par_3d);

					if ( mqi_poly_is_zero(quart) )
					{
						/* discard old quart */
						mqi_poly_clear(quart);
						qi_param_plug_cp_in_quadric (quart, par_3d, q2, par_3d);
					}

					qi_hpoly_optimize(quart);

					/* The singularity of all the cones */
					mqi_curve_param_t c0;

					mqi_curve_param_init_dynamic (c0, mqi_vect_size(sing_p0), mqi_vect_typeof(sing_p0));
					mqi_curve_param_set_vect     (c0, sing_p0);

					/** A bit of simplification */
					if ( (mqi_coef_sign(POLY(quart,0)) == 0) || (mqi_coef_sign(POLY(quart,4)) == 0) )
					{

						qi_inter_init(rop);

						mqi_vect_t p_3d;
						mqi_vect_init_dynamic (p_3d, mqi_curve_param_n_equations(par_3d), mqi_mat_typeof(q1));

						if ( mqi_coef_sign(POLY(quart,4)) == 0 )
							mqi_curve_param_eval_int (p_3d, par_3d, 1, 0);
						else
							mqi_curve_param_eval_int (p_3d, par_3d, 0, 1);

						qi_vect_optimize (p_3d);

						if ( nbroot == 1)
							qi_inter_set_type (rop, 16,2);
						else
							qi_inter_set_type (rop, 16,3);

						mqi_curve_param_t line;

						mqi_curve_param_init_dynamic(line, mqi_vect_size(sing_p0), mqi_vect_typeof(sing_p0));
						mqi_curve_param_line_through_two_points (line, sing_p0, p_3d);

						mqi_vect_t cutvect;

						mqi_vect_init_dynamic (cutvect, 2, mqi_vect_typeof(sing_p0));
						qi_param_line_improved1 (cutvect, line);

						qi_vect_optimize_by_half (cutvect);

						qi_inter_cut_init (cut, QI_INTER_CUT_0, VECT(cutvect,0), VECT(cutvect,1));
						qi_inter_component_init(comp, QI_INTER_COMPONENT_2, line);
						qi_inter_component_set_type (comp, QI_INTER_COMPONENT_LINE);
						qi_inter_component_add_cut_parameter (comp, cut);

						qi_inter_cut_set_owner(cut, comp);

						qi_inter_add_component (rop, comp);
						/* discard comp */
						qi_inter_component_clear(comp);

						mqi_hpoly_t div;
						mqi_hpoly_init_dynamic (div, mqi_mat_typeof(q1));

						if ( mqi_coef_sign(POLY(quart,4)) == 0 )
							mqi_poly_set_wi(div, 0);
						else
							mqi_poly_set_x(div);

						mqi_hpoly_t quart2, R;
						mqi_coef_t  coef, coef2;

						mqi_poly_div_qr   (quart2, R, coef, quart, div);

						qi_inter_component_init (comp, QI_INTER_COMPONENT_4, c0, par_3d, quart2);
						qi_inter_component_set_type (comp, QI_INTER_COMPONENT_LINES_WITH_CONSTRAINT);
						mqi_coef_init_cpy(coef, coef2);
						mqi_coef_set_zero(coef);
						mqi_coef_set_schar(coef2, 1);
						qi_inter_cut_init (cut, QI_INTER_CUT_0, coef2, coef);
						qi_inter_component_add_cut_parameter (comp, cut);
						qi_inter_cut_set_owner (cut, comp);

						qi_inter_add_component (rop, comp);
						/* discard comp */
						qi_inter_component_clear(comp);

						/** Free local memory */
						mqi_poly_list_clear(div, quart2, R, NULL);
						mqi_vect_list_clear(cutvect, p_3d, NULL);
						mqi_curve_param_clear(line);
						mqi_coef_list_clear(coef, coef2, NULL);

					}else {

						qi_inter_init(rop);

						if ( nbroot == 1 )
							qi_inter_set_type(rop, 16,2);
						else
							qi_inter_set_type(rop, 16,3);

						mqi_coef_t coef, coef2;

						qi_inter_component_init (comp, QI_INTER_COMPONENT_4, c0, par_3d, quart);
						qi_inter_component_set_type (comp, QI_INTER_COMPONENT_LINES_WITH_CONSTRAINT);
						mqi_coef_init_cpy(coef, coef2);
						mqi_coef_set_zero(coef);
						mqi_coef_set_schar(coef2, 1);
						qi_inter_cut_init (cut, QI_INTER_CUT_0, coef2, coef);
						qi_inter_component_add_cut_parameter (comp, cut);
						qi_inter_cut_set_owner (cut, comp);

						qi_inter_add_component (rop, comp);
						/* discard comp */
						qi_inter_component_clear(comp);

						/** Free local memory */
						mqi_coef_list_clear(coef, coef2, NULL);

					}

					/** Free local memory */
					mqi_mat_list_clear(p_trans, tmpmat, NULL);
					mqi_curve_param_list_clear(par, par_3d, c0, NULL);
					mqi_vect_clear(l);
					mqi_poly_clear(quart);

				}
				else
				{

					/** The point (0,0,1) is on a pair of planes: use these planes to parametrize */

					if ( mqi_coef_sign(VECT(l,0)) == 0 )
						__qi_inter_vanish_four_concurrent_lines_ratplane
							(rop, q1, q2, q_rat, q1_r, sing_p0, proj_mat);
					else
						__qi_inter_vanish_four_concurrent_lines_ratplane
							(rop, q1, q2, q_rat, q2_r, sing_p0, proj_mat);

				}


				/** Free local memory */
				mqi_vect_list_clear(l, pt, NULL);
				mqi_mat_clear(q_rat);
				mqi_coef_clear(coef);

			}

			/** Free local memory */
			mqi_poly_clear(fake);
		}

		/** Free local memory */
		mqi_vect_list_clear(p1, p2, NULL);
		mqi_coef_clear(coef);

	}

	qi_inter_set_optimal (rop);

}


/** Four concurrent lines when we have found one (or two) rational pair(s) of planes */
void __qi_inter_vanish_four_concurrent_lines_ratplane (qi_inter_t rop, mqi_mat_t __IN q1, mqi_mat_t __IN q2,
						       mqi_mat_t __IN q1_r, mqi_mat_t __IN q2_r,
						       mqi_vect_t __IN sing_p0, mqi_mat_t __IN proj_mat)

{

	qi_inter_component_t comp;
	qi_inter_cut_t	     cut;

	mqi_mat_t	m1, m2, singular;
	mqi_coef_t	D;

	/** Here q1_r is necessarily a rational pair of lines -- q2_r is a rational pair
	  * of lines or a conic with rational coefficients */

	mqi_mat_init_dynamic (m1, 3, 2, mqi_mat_typeof(q1));
	mqi_mat_init_dynamic (m2, 3, 1, mqi_mat_typeof(q2));
	mqi_coef_init_dynamic(D, mqi_mat_typeof(m1));

	qi_mat_singular (singular, q1_r);
	qi_param_line_pair (q1_r, (mqi_vect_ptr)COL(singular,0), D, m1, m2);

	/** If D is negative, the solution is reduced to a point */
	if ( mqi_coef_sign(D) == 0 ) {

		mqi_mat_t l1p, l1m;

		/** The two lines are rational */
		mqi_mat_init_cpy (l1p, m1);
		mqi_mat_init_cpy (l1m, m1);

		mqi_mat_add (COL(l1p,0), COL(m1,0), COL(m2,0));
		mqi_mat_add (COL(l1m,0), COL(m1,0), COL(m2,0));

		qi_mat_optimize (l1p);
		qi_mat_optimize (l1m);

		/** l1p'*q2_r*l1p */
		mqi_mat_t p2p, p2m;
		mqi_mat_transformation (p2p, q2_r, l1p, MQI_MAT_NOTRANSCO);

		/** l1m'*q2_r*l1m */
		mqi_mat_transformation (p2m, q2_r, l1m, MQI_MAT_NOTRANSCO);

		/** Parametrizes the points */
		/** Free the old D first */
		/*mqi_coef_clear (D);*/
		mqi_mat_t m1_1d, m2_1d, m1_1d2, m2_1d2;
		mqi_coef_t D2;

		mqi_mat_list_init_dynamic(2, 1, mqi_mat_typeof(q1), m1_1d, m2_1d, m1_1d2, m2_1d2, NULL);
		mqi_coef_init_dynamic(D2, mqi_coef_typeof(D));

		qi_param_2x2 (p2p, D, m1_1d, m2_1d);
		qi_param_2x2 (p2m, D2, m1_1d2, m2_1d2);

		if ( (mqi_coef_sign(D) < 0) && (mqi_coef_sign(D2) < 0) ) {
			
			qi_inter_init(rop);
			qi_inter_set_type (rop, 16,1);

		}else if ( ( (mqi_coef_sign(D) < 0) && (mqi_coef_sign(D2) >= 0) ) ||
			   ( (mqi_coef_sign(D) >= 0) && (mqi_coef_sign(D2) < 0) ) ) {

			qi_inter_init(rop);
			qi_inter_set_type (rop, 16,2);

		}else {
			qi_inter_init(rop);
			qi_inter_set_type (rop, 16,3);
		}

		/** Stack of transformation */

		/** "Resizes" l1p to a 4,2 matrix */
		mqi_mat_t tmpmat, tr_stack;

		mqi_mat_resize (tmpmat, l1p, 4, 2);
		/* discard old l1p */
		mqi_mat_clear(l1p);
		mqi_mat_init_cpy (l1p, tmpmat);

		mqi_mat_mul_init (tr_stack, proj_mat, l1p);

		/* If D is negative and D2 > 0, this means that two of the lines are complex */
		if ( mqi_coef_sign(D) == 0 ) {

			mqi_mat_t pt1, pt2, tmpmat;

			mqi_mat_init_cpy(tmpmat, m1_1d);
			mqi_mat_add (tmpmat, tmpmat, m2_1d);
			mqi_mat_mul_init(pt1, tr_stack, tmpmat);

			mqi_mat_cpy(tmpmat, m1_1d);
			mqi_mat_sub (tmpmat, tmpmat, m2_1d);
			mqi_mat_mul_init(pt2, tr_stack, tmpmat);

			qi_mat_optimize (pt1);
			qi_mat_optimize (pt2);

			/** $$ initpool_dynamic */
			mqi_curve_param_t line1, line2;
			mqi_vect_t	  cut1, cut2;

			mqi_curve_param_init_dynamic(line1, mqi_vect_size(sing_p0), mqi_vect_typeof(sing_p0));
			mqi_curve_param_init_dynamic(line2, mqi_vect_size(sing_p0), mqi_vect_typeof(sing_p0));

			mqi_curve_param_line_through_two_points (line1, sing_p0, (mqi_vect_ptr)COL(pt1,0));
			mqi_curve_param_line_through_two_points (line2, sing_p0, (mqi_vect_ptr)COL(pt2,0));

			qi_param_line_improved1 (cut1, line1);
			qi_param_line_improved1 (cut2, line2);

			qi_vect_optimize_by_half (cut1);
			qi_vect_optimize_by_half (cut2);

			qi_inter_component_init (comp, QI_INTER_COMPONENT_2, line1);
			qi_inter_component_set_type (comp, QI_INTER_COMPONENT_LINE);
			qi_inter_cut_init (cut, QI_INTER_CUT_0, VECT(cut1,0), VECT(cut1,1));
			qi_inter_component_add_cut_parameter (comp, cut);
			qi_inter_cut_set_owner(cut, comp);
			qi_inter_add_component (rop, comp);
			/* discard comp */
			qi_inter_component_clear(comp);

			qi_inter_component_init (comp, QI_INTER_COMPONENT_2, line2);
			qi_inter_component_set_type (comp, QI_INTER_COMPONENT_LINE);
			qi_inter_cut_init (cut, QI_INTER_CUT_0, VECT(cut2,0), VECT(cut2,1));
			qi_inter_component_add_cut_parameter (comp, cut);
			qi_inter_cut_set_owner(cut, comp);
			qi_inter_add_component (rop, comp);
			/* discard comp */
			qi_inter_component_clear(comp);


			/** Free local memory */
			mqi_mat_list_clear (pt1, pt2, tmpmat, NULL);
			mqi_vect_list_clear (cut1, cut2, NULL);
			mqi_curve_param_list_clear (line1, line2, NULL);

	
		} else if ( mqi_coef_sign(D) > 0 ) {
	
			mqi_mat_t pt1, pt2;

			mqi_mat_mul_init (pt1, tr_stack, m1_1d);
			mqi_mat_mul_init (pt2, tr_stack, m2_1d);

			qi_mat_optimize_2 (pt1, pt2);

			/** Zero vector */
			mqi_vect_t vect;
			mqi_vect_init_dynamic (vect, 4, mqi_mat_typeof(pt1));
			mqi_vect_set_zero (vect);
			
			mqi_curve_param_t line_rat, line_D;
			mqi_curve_param_init_dynamic(line_rat, mqi_vect_size(sing_p0), mqi_vect_typeof(sing_p0));
			mqi_curve_param_init_dynamic(line_D, mqi_vect_size(vect), mqi_vect_typeof(vect));

			mqi_curve_param_line_through_two_points (line_rat, sing_p0, (mqi_vect_ptr)COL(pt1,0));
			mqi_curve_param_line_through_two_points (line_D, vect, (mqi_vect_ptr)COL(pt2,0));

			mqi_vect_t cut1;

			qi_param_line_improved2 (cut1, line_rat, line_D, D);
			qi_vect_optimize_by_half (cut1);

			qi_inter_create_components (rop, 2, QI_INTER_COMPONENT_LINE, QI_INTER_COMPONENT_2, line_rat, line_D, D);
			qi_inter_cut_init (cut, QI_INTER_CUT_1, VECT(cut1,0), VECT(cut1,1), D, VECT(cut1,2));
			
			qi_inter_component_add_cut_parameter (rop->components[0], cut);
			qi_inter_component_add_cut_parameter (rop->components[1], cut);

			qi_inter_cut_set_owner(cut, rop->components[0]);


			/** Free local memory */
			mqi_mat_list_clear (pt1, pt2, NULL);
			mqi_vect_list_clear (vect, cut1, NULL);
			mqi_curve_param_list_clear (line_rat, line_D, NULL);

		}

		/** Stack of transformation */

		/** "Resizes" l1m to a 4,2 matrix */
		/* discard old tmpmat */
		mqi_mat_clear (tmpmat);
		mqi_mat_resize (tmpmat, l1m, 4, 2);
		/* discard old l1m */
		mqi_mat_clear(l1m);
		mqi_mat_init_cpy(l1m, tmpmat);

		/* discard old tr_stack */
		mqi_mat_clear(tr_stack);
		mqi_mat_mul_init (tr_stack, proj_mat, l1m);

		if ( mqi_coef_sign(D2) == 0 ) {

			mqi_mat_t pt1, pt2, tmpmat;

			mqi_mat_init_cpy(tmpmat, m1_1d2);
			mqi_mat_add (tmpmat, tmpmat, m2_1d2);
			mqi_mat_mul_init(pt1, tr_stack, tmpmat);

			mqi_mat_cpy(tmpmat, m1_1d2);
			mqi_mat_sub (tmpmat, tmpmat, m2_1d2);
			mqi_mat_mul_init(pt2, tr_stack, tmpmat);

			qi_mat_optimize (pt1);
			qi_mat_optimize (pt2);

			/** $$ initpool_dynamic */
			mqi_curve_param_t line1, line2;
			mqi_vect_t	  cut1, cut2;

			mqi_curve_param_init_dynamic(line1, mqi_vect_size(sing_p0), mqi_vect_typeof(sing_p0));
			mqi_curve_param_init_dynamic(line2, mqi_vect_size(sing_p0), mqi_vect_typeof(sing_p0));

			mqi_curve_param_line_through_two_points (line1, sing_p0, (mqi_vect_ptr)COL(pt1,0));
			mqi_curve_param_line_through_two_points (line2, sing_p0, (mqi_vect_ptr)COL(pt2,0));

			qi_param_line_improved1 (cut1, line1);
			qi_param_line_improved1 (cut2, line2);

			qi_vect_optimize_by_half (cut1);
			qi_vect_optimize_by_half (cut2);

			qi_inter_component_init (comp, QI_INTER_COMPONENT_2, line1);
			qi_inter_component_set_type (comp, QI_INTER_COMPONENT_LINE);
			qi_inter_cut_init (cut, QI_INTER_CUT_0, VECT(cut1,0), VECT(cut1,1));
			qi_inter_component_add_cut_parameter (comp, cut);
			qi_inter_cut_set_owner(cut, comp);
			qi_inter_add_component (rop, comp);
			/* discard comp */
			qi_inter_component_clear(comp);

			qi_inter_component_init (comp, QI_INTER_COMPONENT_2, line2);
			qi_inter_component_set_type (comp, QI_INTER_COMPONENT_LINE);
			qi_inter_cut_init (cut, QI_INTER_CUT_0, VECT(cut2,0), VECT(cut2,1));
			qi_inter_component_add_cut_parameter (comp, cut);
			qi_inter_cut_set_owner(cut, comp);
			qi_inter_add_component (rop, comp);
			/* discard comp */
			qi_inter_component_clear(comp);


			/** Free local memory */
			mqi_mat_list_clear (pt1, pt2, tmpmat, NULL);
			mqi_vect_list_clear (cut1, cut2, NULL);
			mqi_curve_param_list_clear (line1, line2, NULL);



		} else if ( mqi_coef_sign(D2) > 0 ) {
	
			mqi_mat_t pt1, pt2;

			mqi_mat_mul_init (pt1, tr_stack, m1_1d2);
			mqi_mat_mul_init (pt2, tr_stack, m2_1d2);

			qi_mat_optimize_2 (pt1, pt2);

			/** Zero vector */
			mqi_vect_t vect;
			mqi_vect_init_dynamic (vect, 4, mqi_mat_typeof(pt1));
			mqi_vect_set_zero (vect);
			
			mqi_curve_param_t line_rat, line_D;
			mqi_curve_param_init_dynamic(line_rat, mqi_vect_size(sing_p0), mqi_vect_typeof(sing_p0));
			mqi_curve_param_init_dynamic(line_D, mqi_vect_size(vect), mqi_vect_typeof(vect));

			mqi_curve_param_line_through_two_points (line_rat, sing_p0, (mqi_vect_ptr)COL(pt1,0));
			mqi_curve_param_line_through_two_points (line_D, vect, (mqi_vect_ptr)COL(pt2,0));

			mqi_vect_t cut1;

			qi_param_line_improved2 (cut1, line_rat, line_D, D);
			qi_vect_optimize_by_half (cut1);

			qi_inter_create_components (rop, 2, QI_INTER_COMPONENT_LINE, QI_INTER_COMPONENT_2, line_rat, line_D, D);
			qi_inter_cut_init (cut, QI_INTER_CUT_1, VECT(cut1,0), VECT(cut1,1), D, VECT(cut1,2));
			
			qi_inter_component_add_cut_parameter (rop->components[0], cut);
			qi_inter_component_add_cut_parameter (rop->components[1], cut);

			qi_inter_cut_set_owner(cut, rop->components[0]);


			/** Free local memory */
			mqi_mat_list_clear (pt1, pt2, NULL);
			mqi_vect_list_clear (vect, cut1, NULL);
			mqi_curve_param_list_clear (line_rat, line_D, NULL);


		}

		if ( (mqi_coef_sign(D) < 0) && (mqi_coef_sign(D2) < 0) ) {

			/** One component */
			/** Outputs singular point of all quadrics */
			mqi_curve_param_t c;

			mqi_curve_param_init_dynamic(c, mqi_vect_size(sing_p0), mqi_vect_typeof(sing_p0));
			mqi_curve_param_set_vect (c, sing_p0);

			qi_inter_component_init (comp, QI_INTER_COMPONENT_2, c);
			qi_inter_component_set_type (comp, QI_INTER_COMPONENT_POINT);
			qi_inter_add_component (rop, comp);
			/* discard comp */
			qi_inter_component_clear(comp);

			/** Free local memory */
			mqi_curve_param_clear(c);

		}

		/** Set the global optimization flag */
		qi_inter_set_optimal (rop);


		/** Free local memory */
		mqi_mat_list_clear (l1p, l1m, p2p, p2m, tmpmat, tr_stack, m1_1d, m1_1d2,
					m2_1d, m2_1d2, NULL);
		mqi_coef_clear (D2);


	} else if ( mqi_coef_sign(D) > 0 ) {
		
		/** The lines are not rational */

		mqi_mat_t m_1, m_2, m_3;
		mqi_mat_t tmpmat, trans;

		/** Transformations are: m1 +|- sqrt(D)*m2 */
		mqi_mat_transformation	(m_1, q2_r, m1, MQI_MAT_NOTRANSCO);
		mqi_mat_transformation	(m_2, q2_r, m2, MQI_MAT_NOTRANSCO);
		mqi_mat_mul_coef	(m_2, m_2, D);

		mqi_mat_mul_init (tmpmat, q2_r, m2);
		mqi_mat_transpose_init (trans, m1);
		mqi_mat_mul_init (m_3, trans, tmpmat);
		mqi_mat_mul_int (m_3, m_3, 2);

		mqi_coef_t del_rat, del_D, e, delta, e2;

		mqi_coef_list_init_dynamic(mqi_mat_typeof(m_1), del_rat, del_D, e, e2, delta, NULL);

		mqi_coef_linexpr3 (del_rat,4,	4,NULL,MAT(m_1,1,0),MAT(m_1,1,0),
						1,D,MAT(m_3,1,0),MAT(m_3,1,0),
						-4,NULL,MAT(m_1,1,1),MAT(m_1,0,0),
						-4,NULL,MAT(m_1,1,1),MAT(m_2,0,0));
		mqi_coef_linexpr2 (del_D,2,	4,MAT(m_1,1,0),MAT(m_3,1,0),
						-4,MAT(m_1,1,1),MAT(m_3,0,0));

		mqi_coef_set_schar(e, 1);

		mqi_coef_t sq, cofactor;

		qi_coef_extract_square_factors (sq, cofactor, del_rat);
		mqi_coef_cpy (e, sq);
		/* discard old sq & cofactor */
		mqi_coef_list_clear(sq, cofactor, NULL);

		qi_coef_extract_square_factors (sq, cofactor, del_D);
		mqi_coef_gcd (e, e, sq);

		mqi_coef_mul	  (e2, e, e);

		mqi_coef_divexact (del_rat, del_rat, e2);
		mqi_coef_divexact (del_D, del_D, e2);

		/** If delta < 0, there's only 2 real lines */
		mqi_coef_linexpr3 (delta,2,	1,NULL,del_rat,del_rat,		-1,D,del_D,del_D);

		if ( mqi_coef_sign (delta) < 0 ) {
			qi_inter_init(rop);
			qi_inter_set_type	(rop, 16,2);
		}
		else {
			qi_inter_init(rop);
			qi_inter_set_type	(rop, 16,3);
		}

		/* discard old tmpmat */
		mqi_mat_clear(tmpmat);
		mqi_mat_resize (tmpmat, m1, 4, 2);
		/* discard old m1 */
		mqi_mat_clear(m1);
		mqi_mat_init_cpy (m1, tmpmat);

		/* discard old tmpmat */
		mqi_mat_clear(tmpmat);
		mqi_mat_resize (tmpmat, m2, 4, 1);
		/* discard old m2 */
		mqi_mat_clear(m2);
		mqi_mat_init_cpy (m2, tmpmat);

		mqi_mat_t t1, t2;
		mqi_mat_mul_init (t1, proj_mat, m1);
		mqi_mat_mul_init (t2, proj_mat, m2);

		mqi_vect_ptr k1, k2, l1;

		k1 = (mqi_vect_ptr)COL(t1,0);
		k2 = (mqi_vect_ptr)COL(t1,1);
		l1 = (mqi_vect_ptr)COL(t2,0);

	
		/** Solution of quadratic equation is:
		 *  (2 m_1(1,1), -2 m_1(1,0) - eps1*m_3(1,0)*sqrt(D) + eps2*e*sqrt(D'))
		 *  with D' = del_rat + eps1*del_D */
		mqi_vect_t po_1, po_2, po_3;
		mqi_vect_t tmpvect;

		mqi_vect_init_cpy(tmpvect, k2);
		mqi_vect_mul_coef(tmpvect, tmpvect, MAT(m_1,1,0));
		mqi_vect_mul_int(tmpvect, tmpvect, -2);

		mqi_vect_init_cpy(po_1, k1);
		mqi_vect_mul_coef(po_1, po_1, MAT(m_1,1,1));
		mqi_vect_mul_int(po_1, po_1, 2);

		mqi_vect_add(po_1, po_1, tmpvect);

		/* discard old tmpvect */
		mqi_vect_clear(tmpvect);
		mqi_vect_init_cpy(tmpvect, k2);
		mqi_vect_mul_coef(tmpvect, tmpvect, MAT(m_3,1,0));
		mqi_vect_mul_int(tmpvect, tmpvect, -1);

		mqi_vect_init_cpy(po_2, l1);
		mqi_vect_mul_coef(po_2, po_1, MAT(m_1,1,1));
		mqi_vect_mul_int(po_2, po_2, 2);

		mqi_vect_add(po_2, po_2, tmpvect);

		mqi_vect_init_cpy(po_3, k2);
		mqi_vect_mul_coef(po_3, po_3, e);

		qi_vect_optimize_gcd_triplet (po_1, po_2, po_3);


		/** Zero vector */
		mqi_vect_t vect;

		mqi_vect_init_dynamic(vect, 4, mqi_coef_typeof(del_D));
		mqi_vect_set_zero (vect);
		
		/** If the Galois group is C4 or V4, the solution can be written in a simpler way */

		mqi_coef_t Ddelta;

		mqi_coef_init_cpy(Ddelta, D);
		mqi_coef_mul(Ddelta, Ddelta, delta);

		/** V4, first case */
		if ( mqi_coef_sign (del_D) == 0 ) {

			mqi_curve_param_t c1, c2, c3, c4;

			mqi_curve_param_initpool_dynamic(c1, mqi_vect_size(sing_p0), mqi_vect_typeof(sing_p0));
			mqi_curve_param_initpool_dynamic(c2, mqi_vect_size(vect), mqi_vect_typeof(vect));
			mqi_curve_param_initpool_dynamic(c3, mqi_vect_size(vect), mqi_vect_typeof(vect));
			mqi_curve_param_initpool_dynamic(c4, 4, mqi_vect_typeof(vect));

			mqi_curve_param_line_through_two_points (c1, sing_p0, po_1);
			mqi_curve_param_line_through_two_points (c2, vect, po_2);
			mqi_curve_param_line_through_two_points (c3, vect, po_3);

			/** For presentation purpose */
			if ( mqi_coef_cmp (D, del_rat) > 0 )
			{
				mqi_coef_swap(D, del_rat);
				mqi_curve_param_swap(c2, c3);
			}
		
			mqi_coef_t coef;

			mqi_coef_init_dynamic(coef, mqi_coef_typeof(del_rat));
			mqi_coef_set_schar (coef, 0);

			qi_param_line_improved3 (c1, c2, c3, c4, D, del_rat, coef);

			qi_inter_create_components (rop, 4, 
						    QI_INTER_COMPONENT_LINE, QI_INTER_COMPONENT_6,
						    c1, c2, c3, c4, D, del_rat, coef);

			/** Cut parameter at (0, 0) */
			qi_inter_cut_init (cut, QI_INTER_CUT_0, coef, coef);
		
			qi_inter_component_add_cut_parameter (rop->components[0], cut);
			qi_inter_component_add_cut_parameter (rop->components[1], cut);
			qi_inter_component_add_cut_parameter (rop->components[2], cut);
			qi_inter_component_add_cut_parameter (rop->components[3], cut); 

			qi_inter_cut_set_owner(cut, rop->components[0]);

			/** Free local memory */
			mqi_coef_clear(coef);
			mqi_curve_param_list_clear(c1, c2, c3, c4, NULL);


			/** V4, second case */
		} else if ( (mqi_coef_sign(delta) > 0) && (mqi_coef_is_square(delta)) ) {

			mqi_coef_t tmp1, tmp2, dp, ep, dm, em, sq, cofactor;

			mqi_coef_list_init_dynamic(mqi_coef_typeof(delta), tmp1, tmp2, dp, ep, dm, em, sq, NULL);

			mqi_coef_sqrt (sq, delta);

			/** Needs two (non-nested) square roots to define the lines */
			mqi_coef_mul_schar (tmp1, del_rat, 2);
			mqi_coef_mul_schar (tmp2, sq, 2);

			mqi_coef_add	   (dp, tmp1, tmp2);
			mqi_coef_sub	   (dm, tmp1, tmp2);

			mqi_coef_t Da, Db;
			mqi_vect_t r1, r2, r3, r4;

			/** The discriminants of the quadratic equation are sqrt(D') = 1/2*(ep*sqrt(dp) +|- em*sqrt(dm) */
			qi_coef_extract_square_factors (ep, cofactor, dp);
			mqi_coef_cpy (dp, cofactor);
			/* discard cofactor */
			mqi_coef_clear(cofactor);

			qi_coef_extract_square_factors (em, cofactor, dm);
			mqi_coef_cpy (dm, cofactor);
			

			if ( (mqi_coef_sign(dm) == 0) || ((mqi_coef_cmp(dm,D) >= 0) && (mqi_coef_cmp(dm,dp) >= 0)) )
			{

				mqi_coef_t coef;

				/** Smallest square roots are D and dp */
				mqi_vect_init_cpy (r1, po_3);
				mqi_coef_init_cpy (coef, ep);
				mqi_coef_mul	  (coef, coef, coef);
				mqi_coef_mul	  (coef, coef, dp);
				mqi_vect_mul_coef (r1, r1, coef);

				mqi_vect_init_cpy (r2, po_3);
				mqi_coef_cpy	  (coef, del_D);
				mqi_coef_mul_schar(coef, coef, 2);
				mqi_vect_mul_coef (r2, r2, coef);

				mqi_vect_init_cpy (r3, po_1);
				mqi_coef_cpy	  (coef, ep);
				mqi_coef_mul_schar(coef, coef, 2);
				mqi_vect_mul_coef (r3, r3, coef);

				mqi_vect_init_cpy (r4, po_2);
				mqi_coef_cpy	  (coef, ep);
				mqi_coef_mul_schar(coef, coef, 2);
				mqi_vect_mul_coef (r4, r4, coef);

				mqi_coef_init_cpy (Da, D);
				mqi_coef_init_cpy (Db, dp);

				/** Free local memory */
				mqi_coef_clear(coef);

			} else if ( mqi_coef_cmp(dp, D) >= 0 && mqi_coef_cmp(dp, dm) >= 0 ) {

				mqi_coef_t coef;

				/** Smallest roots are D and dm */
				mqi_vect_init_cpy (r1, po_3);
				mqi_coef_init_cpy (coef, em);
				mqi_coef_mul	  (coef, coef, coef);
				mqi_coef_mul	  (coef, coef, dm);
				mqi_vect_mul_coef (r1, r1, coef);

				mqi_vect_init_cpy (r2, po_3);
				mqi_coef_cpy	  (coef, del_D);
				mqi_coef_mul_schar(coef, coef, 2);
				mqi_vect_mul_coef (r2, r2, coef);

				mqi_vect_init_cpy (r3, po_1);
				mqi_coef_cpy	  (coef, em);
				mqi_coef_mul_schar(coef, coef, 2);
				mqi_vect_mul_coef (r3, r3, coef);

				mqi_vect_init_cpy (r4, po_2);
				mqi_coef_cpy	  (coef, em);
				mqi_coef_mul_schar(coef, coef, 2);
				mqi_vect_mul_coef (r4, r4, coef);

				mqi_coef_init_cpy (Da, D);
				mqi_coef_init_cpy (Db, dm);

				/** Free local memory */
				mqi_coef_clear(coef);

			} else {

				mqi_coef_t coef;

				/** D is largest */
				mqi_vect_init_cpy (r1, po_1);
				mqi_coef_init_cpy (coef, del_D);
				mqi_coef_mul_schar(coef, coef, 2);
				mqi_vect_mul_coef (r1, r1, coef);

				mqi_vect_init_cpy (r2, po_3);
				mqi_coef_cpy	  (coef, del_D);
				mqi_coef_mul	  (coef, coef, em);
				mqi_vect_mul_coef (r2, r2, coef);

				mqi_vect_init_cpy (r3, po_3);
				mqi_coef_cpy	  (coef, ep);
				mqi_coef_mul	  (coef, coef, del_D);
				mqi_vect_mul_coef (r3, r3, coef);

				mqi_vect_init_cpy (r4, po_2);
				mqi_coef_cpy	  (coef, ep);
				mqi_coef_mul	  (coef, coef, em);
				mqi_vect_mul_coef (r4, r4, coef);

				mqi_coef_init_cpy (Da, dm);
				mqi_coef_init_cpy (Db, dp);

				/** Free local memory */
				mqi_coef_clear(coef);

			}

			qi_vect_optimize_gcd_quadruplet (r1, r2, r3, r4);

			mqi_curve_param_t c1, c2, c3, c4;
			mqi_curve_param_init_dynamic(c1, mqi_vect_size(r1), mqi_vect_typeof(r1));
			mqi_curve_param_init_dynamic(c2, mqi_vect_size(r2), mqi_vect_typeof(r2));
			mqi_curve_param_init_dynamic(c3, mqi_vect_size(r3), mqi_vect_typeof(r3));
			mqi_curve_param_init_dynamic(c4, mqi_vect_size(r4), mqi_vect_typeof(r4));

			/** Zero vector */
			mqi_vect_t vect;
		
			mqi_vect_init_dynamic(vect, 4, mqi_curve_param_typeof(c1));
			mqi_vect_set_zero (vect);

			mqi_curve_param_line_through_two_points (c1, sing_p0, r1);
			mqi_curve_param_line_through_two_points (c2, vect, r2);
			mqi_curve_param_line_through_two_points (c3, vect, r3);
			mqi_curve_param_line_through_two_points (c4, vect, r4);
			
			/** For presentation purpose */
			if ( mqi_coef_cmp (Da, Db) > 0 )
			{
				mqi_coef_swap(Da, Db);
				mqi_curve_param_swap(c2, c3);
			}

			mqi_coef_t coef;
			
			mqi_coef_init_dynamic(coef, mqi_coef_typeof(Da));
			mqi_coef_set_schar (coef, 0);
			qi_param_line_improved3 (c1, c2, c3, c4, Da, Db, coef);

			qi_inter_create_components (rop, 4, QI_INTER_COMPONENT_LINE, QI_INTER_COMPONENT_6,
							c1, c2, c3, c4, Da, Db, coef);

			/** Cut parameter at (0, 0) */
			qi_inter_cut_init (cut, QI_INTER_CUT_0, coef, coef);
		
			qi_inter_component_add_cut_parameter (rop->components[0], cut);
			qi_inter_component_add_cut_parameter (rop->components[1], cut);
			qi_inter_component_add_cut_parameter (rop->components[2], cut);
			qi_inter_component_add_cut_parameter (rop->components[3], cut);

			qi_inter_cut_set_owner(cut, rop->components[0]);

			/** Free local memory */
			mqi_coef_list_clear(tmp1, tmp2, dp, ep, dm, em, sq, cofactor, Da, Db, coef, NULL);
			mqi_vect_list_clear(r1, r2, r3, r4, vect, NULL);
			mqi_curve_param_list_clear(c1, c2, c3, c4, NULL);


			/** C4 */
		}
		else if ( (mqi_coef_sign(delta) > 0) && (mqi_coef_is_square(Ddelta)) )
		{

			mqi_coef_t sq;
			mqi_vect_t vect;
				
			mqi_coef_init_dynamic (sq, mqi_coef_typeof(Ddelta));
			mqi_coef_sqrt (sq, Ddelta);

			mqi_curve_param_t c1, c2, c3, c4, temp;
			mqi_curve_param_init_dynamic(c1, mqi_vect_size(po_1), mqi_vect_typeof(po_1));
			mqi_curve_param_init_dynamic(c2, mqi_vect_size(po_2), mqi_vect_typeof(po_2));
			mqi_curve_param_init_dynamic(c3, mqi_vect_size(po_3), mqi_vect_typeof(po_3));
			mqi_curve_param_init_dynamic(c4, 4, mqi_vect_typeof(po_3));
			mqi_curve_param_init_dynamic(temp, 4, mqi_vect_typeof(po_3));

			mqi_vect_init_dynamic(vect, 4, mqi_vect_typeof(sing_p0));
			mqi_curve_param_line_through_two_points (c1, sing_p0, po_1);
			mqi_curve_param_line_through_two_points (c2, vect, po_2);
			mqi_curve_param_line_through_two_points (c3, vect, po_3);

			/** Zero curve param */
			mqi_vect_set_zero (vect);
			qi_param_line_improved3 (c1, c2, c3, temp, D, del_rat, del_D);

			qi_inter_create_components (rop, 2, QI_INTER_COMPONENT_LINE, QI_INTER_COMPONENT_6,
							c1, c2, c3, c4, D, del_rat, del_D);

			mqi_vect_t po_1_2, po_2_2, po_3_2;

			mqi_vect_init_cpy (po_1_2, po_1);
			mqi_vect_mul_coef (po_1_2, po_1_2, del_D);

			/* discard old vect */
			mqi_vect_clear(vect);

			mqi_vect_init_cpy (vect, po_2);
			mqi_vect_mul_coef (vect, vect, del_rat);
			mqi_vect_sub	  (po_1_2, po_1_2, vect);
			mqi_vect_mul_coef (po_1_2, po_1_2, D);

			mqi_vect_init_cpy (po_2_2, po_1);
			mqi_vect_mul_coef (po_2_2, po_2_2, del_rat);
			/* discard old vect */
			mqi_vect_clear(vect);
			mqi_vect_init_cpy (vect, po_2);
			mqi_vect_mul_coef (vect, vect, del_D);
			mqi_vect_mul_coef (vect, vect, D);
			mqi_vect_sub	  (po_2_2, po_2_2, vect);
		
			mqi_vect_init_cpy (po_3_2, po_3);
			mqi_vect_mul_coef (po_3_2, po_3_2, sq);

			qi_vect_optimize_gcd_triplet (po_1_2, po_2_2, po_3_2);

			mqi_curve_param_t c1_2, c2_2, c3_2;
			mqi_curve_param_init_dynamic(c1_2, mqi_vect_size(po_1_2), mqi_vect_typeof(po_1_2));
			mqi_curve_param_init_dynamic(c2_2, mqi_vect_size(po_2_2), mqi_vect_typeof(po_2_2));
			mqi_curve_param_init_dynamic(c3_2, mqi_vect_size(po_3_2), mqi_vect_typeof(po_3_2));

			/** $$ Zero vector: is vect still well sized for that purpose ? */
			mqi_vect_set_zero(vect);

			mqi_curve_param_line_through_two_points (c1_2, sing_p0, po_1_2);
			mqi_curve_param_line_through_two_points (c2_2, vect, po_2_2);
			mqi_curve_param_line_through_two_points (c3_2, vect, po_3_2);

			qi_param_line_improved3 (c1_2, c2_2, c3_2, temp, D, del_rat, del_D);

			qi_inter_create_components (rop, 2, QI_INTER_COMPONENT_LINE, QI_INTER_COMPONENT_6,
							c1_2, c2_2, c3_2, c4, D, del_rat, del_D);

			mqi_coef_t coef;

			mqi_coef_init_dynamic(coef, mqi_coef_typeof(del_rat));
			mqi_coef_set_schar (coef, 0);

			/** Cut parameter at (0, 0) */
			qi_inter_cut_init (cut, QI_INTER_CUT_0, coef, coef);
			
			qi_inter_component_add_cut_parameter (rop->components[0], cut);
			qi_inter_component_add_cut_parameter (rop->components[1], cut);
			qi_inter_component_add_cut_parameter (rop->components[2], cut);
			qi_inter_component_add_cut_parameter (rop->components[3], cut);

			qi_inter_cut_set_owner (cut, rop->components[0]);


			/** Free local memory */
			mqi_coef_list_clear(coef, sq, NULL);
			mqi_vect_list_clear(vect, po_1_2, po_2_2, po_3_2, NULL);
			mqi_curve_param_list_clear(c1, c2, c3, c4, c1_2, c2_2, c3_2, temp, NULL);

		}
		else
		{
		
			mqi_curve_param_t c1, c2p, c2m, c3, c4;

			mqi_curve_param_init_dynamic(c1, mqi_vect_size(po_1), mqi_vect_typeof(po_1));
			mqi_curve_param_init_dynamic(c2p, mqi_vect_size(po_2), mqi_vect_typeof(po_2));
			mqi_curve_param_init_dynamic(c2m, mqi_vect_size(po_2), mqi_vect_typeof(po_2));
			mqi_curve_param_init_dynamic(c3, mqi_vect_size(po_3), mqi_vect_typeof(po_3));
			mqi_curve_param_init_dynamic(c4, 4, mqi_vect_typeof(po_3));

			mqi_vect_t vect;

			mqi_vect_init_dynamic(vect, 4, mqi_vect_typeof(po_3));
			mqi_vect_set_zero(vect);

			mqi_curve_param_line_through_two_points (c1, sing_p0, po_1);
			mqi_curve_param_line_through_two_points (c2p, vect, po_2);
			mqi_vect_neg (po_2, po_2);
			mqi_curve_param_line_through_two_points (c2m, vect, po_2);
			mqi_curve_param_line_through_two_points (c3, vect, po_3);

			mqi_coef_t coef;

			mqi_coef_init_dynamic(coef, mqi_vect_typeof(po_3));

			qi_inter_cut_init (cut, QI_INTER_CUT_0, coef, coef);
			
			/** If delta < 0, only two lines are real */
			if ( mqi_coef_sign (delta) < 0) {
			
				if ( mqi_coef_sign (del_D) > 0 )
					qi_inter_create_components (rop, 2, QI_INTER_COMPONENT_LINE,
									QI_INTER_COMPONENT_6,
									c1, c2p, c3, c4, D, del_rat, del_D);
				else {
					mqi_coef_cpy(coef, del_D);
					mqi_coef_neg (coef, coef);
					qi_inter_create_components (rop, 2, QI_INTER_COMPONENT_LINE,
									QI_INTER_COMPONENT_6,
									c1, c2m, c3, c4, D, del_rat, coef);

				}

				qi_inter_component_add_cut_parameter
					(rop->components[0], cut);
				qi_inter_component_add_cut_parameter
					(rop->components[1], cut);

				qi_inter_cut_set_owner (cut, rop->components[0]);

			} else {
				
				qi_inter_create_components (rop, 2, QI_INTER_COMPONENT_LINE,
						QI_INTER_COMPONENT_6,
						c1, c2p, c3, c4, D, del_rat, del_D);
				
				mqi_coef_cpy(coef, del_D);
				mqi_coef_neg (coef, coef);
				qi_inter_create_components (rop, 2, QI_INTER_COMPONENT_LINE,
						QI_INTER_COMPONENT_6,
						c1, c2m, c3, c4, D, del_rat, coef);

				qi_inter_component_add_cut_parameter
					(rop->components[0], cut);
				qi_inter_component_add_cut_parameter
					(rop->components[1], cut);
				qi_inter_component_add_cut_parameter
					(rop->components[2], cut);
				qi_inter_component_add_cut_parameter
					(rop->components[3], cut);

				qi_inter_cut_set_owner (cut, rop->components[0]);

			}

			/** Free local memory */
			mqi_curve_param_list_clear(c1, c2p, c2m, c3, c4, NULL);
			mqi_vect_clear(vect);
			mqi_coef_clear(coef);

		}

		/** Free local memory */
		mqi_mat_list_clear(m_1, m_2, m_3, t1, t2, tmpmat, trans, NULL);
		mqi_vect_list_clear(k1, k2, l1, po_1, po_2, po_3, tmpvect, vect, NULL);
		mqi_coef_list_clear(del_rat, del_D, e, e2, delta, Ddelta, sq, cofactor, NULL);

	}
	else
	{
		/** One point */

		/** One component */
		qi_inter_init(rop);
		qi_inter_set_type	(rop, 16,1);

		/** Output singular point of all quadrics */
		mqi_curve_param_t c;
		mqi_curve_param_init_dynamic(c, mqi_vect_size(sing_p0), mqi_vect_typeof(sing_p0));

		mqi_curve_param_set_vect (c, sing_p0);

		qi_inter_component_init (comp, QI_INTER_COMPONENT_2, c);
		qi_inter_component_set_type (comp, QI_INTER_COMPONENT_POINT);

		qi_inter_add_component		  (rop, comp);
		/* discard comp */
		qi_inter_component_clear(comp);

		/** Free local memory */
		mqi_curve_param_clear(c);

	}

	qi_inter_set_optimal (rop);

	
	/** Free local memory */
	mqi_mat_list_clear(m1, m2, singular, NULL);
	mqi_coef_clear(D);

}


void __qi_inter_vanish_all_positive_divisors (mqi_vect_t __INIT div, mqi_prime_factorization_t f, unsigned long int maxfactor)
{

	size_t	fno;
	size_t	cap;
	size_t	i, j, k, l;
	unsigned short	* factors;
	unsigned char	* exponents, fexp, e;
	unsigned short	  h, base;
	mqi_vect_t	tmp;

	fno		= mqi_prime_factorization_size(f);
	exponents	= mqi_prime_factorization_get_exponents(f);
	factors		= mqi_prime_factorization_get_factors(f);

	for ( k = 0; k < fno; k++ )
		cap *= ( 1 + exponents[k] );

	/* Type "mpz_t" directly set */
	mqi_vect_init_mpz_t (tmp, cap);

	k = 0;
	l = 1;
	mqi_vect_set_at_int (div, 0,	1);

	for ( i = 0; i < fno; i++ )
	{

		h	= 1;
		fexp	= exponents[i];
		base	= factors[i];

		if ( base < maxfactor )
		{

			for ( e = 1; e <= fexp; e++ )			
			{
				h *= base;

				for ( j = 0; j <= k; j++,l++ )
					mqi_coef_mul_slong(VECT(div,l), VECT(div,j), (slong)h);

			}
			k = l - 1;
		}
		/* Possible hack */
		/* Because the factors (base) are sorted from the lowest value
		 * to the highest value. */
		else{
			break;
		}
		

	} /* End for */

	mqi_vect_resize (div, tmp, l);
	mqi_vect_clear  (tmp);

}

void __qi_inter_vanish_all_negative_divisors (mqi_vect_t __INIT div, mqi_prime_factorization_t f, unsigned long int maxfactor)
{
	
	__qi_inter_vanish_all_positive_divisors(div, f, maxfactor);
	mqi_vect_neg(div, div);

}

void __qi_inter_vanish_all_divisors (mqi_vect_t __INIT div, mqi_prime_factorization_t f, unsigned long int maxfactor)
{

	mqi_vect_t pdiv;
	size_t k, ndiv;

	__qi_inter_vanish_all_positive_divisors(pdiv, f, maxfactor);
	ndiv = mqi_vect_size(pdiv);
	mqi_vect_resize(div, pdiv, 2 * ndiv);
	mqi_vect_clear(pdiv);

	for ( k = 0; k < ndiv; k++ )
	{
		mqi_coef_cpy (VECT(div,2 * ndiv - k - 1), VECT(div,k));
		mqi_coef_neg (VECT(div,k), VECT(div,k));
	}

}

