#include "qi_inter.h"

void qi_inter_two_mult (qi_inter_t rop, mqi_mat_t q1, mqi_mat_t q2,
					mqi_hpoly_t det_p, mqi_hpoly_t det_p_orig, mqi_hpoly_t gcd_p)
{

	mqi_coef_t		delta;


	/** The discriminant of the (degree 2) gcd_p */
	mqi_coef_init_dynamic (delta, mqi_poly_typeof(gcd_p));
	qi_hpoly_pseudo_discriminant (delta, gcd_p);

	/** The square root of delta : sqrt_delta */
	
	/** If the discriminant is positive and is a square, the two roots are real and
	  * rational so no need for the big machinery */
	if ( mqi_coef_is_square(delta) ) {

		mqi_coef_t sqrt_delta;

	        mqi_coef_init_dynamic (sqrt_delta, mqi_coef_typeof(delta));
		mqi_coef_sqrt (sqrt_delta, delta);

		/** The two rational roots of the determinantal equation */
		mqi_vect_t root1, root2;
		mqi_vect_init_dynamic (root1, 2, mqi_mat_typeof(q1));
		mqi_vect_init_dynamic (root2, 2, mqi_mat_typeof(q1));

		if ( !mqi_coef_sign(POLY(gcd_p,0)) ) {
			/** Bad case */
			
			/** root1 is [0 1] */
			mqi_vect_set_at_int (root1,1, 1);

			if ( !mqi_coef_sign(POLY(gcd_p,2)) ) {
			
				/** root2 is [1 0] */
				mqi_vect_set_at_int (root2,0, 1);

			}else {
				mqi_vect_set_at (root2,0, POLY(gcd_p,1));
				mqi_coef_neg (VECT(root2,0), VECT(root2,0));
				mqi_vect_set_at (root2,1, POLY(gcd_p,2));
			}

		}else{

			/** root1 = [2a, -b-sqrt_delta] */
			mqi_coef_mul_schar (VECT(root1,0), POLY(gcd_p,0), 2);
			mqi_coef_add	   (VECT(root1,1), POLY(gcd_p,1), sqrt_delta);
			mqi_coef_neg	   (VECT(root1,1), VECT(root1,1));

			/** root2 = [2a, -b+sqrt_delta] */
			mqi_coef_mul_schar (VECT(root2,0), POLY(gcd_p,0), 2);
			mqi_coef_sub	   (VECT(root2,1), sqrt_delta, POLY(gcd_p,1));

		}

		qi_vect_optimize (root1);
		qi_vect_optimize (root2);

		/** The associated matrices */
		mqi_mat_t qa, qb;
	
		mqi_mat_list_init_dynamic(mqi_mat_nrows(q1), mqi_mat_ncols(q1), mqi_mat_typeof(q1),
					  qa, qb, NULL);
		
		mqi_mat_linexpr (qa,2,	VECT(root1,0),q1,	VECT(root1,1),q2);
		mqi_mat_linexpr (qb,2,	VECT(root2,0),q1,	VECT(root2,1),q2);

		/** Inertias */
		mqi_vect_t in_qa, in_qb;

		qi_mat_inertia (in_qa, qa);
		qi_mat_inertia (in_qb, qb);

		/** Ranks */
		mqi_coef_t rank_qa, rank_qb;

		mqi_coef_list_init_dynamic(mqi_mat_typeof(q1), rank_qa, rank_qb, NULL);

		mqi_coef_add (rank_qa, VECT(in_qa,0), VECT(in_qa,1));
		mqi_coef_add (rank_qb, VECT(in_qb,0), VECT(in_qb,1));

		if ( mqi_coef_cmp(rank_qa, rank_qb) < 0 ) {
			mqi_coef_swap (rank_qa, rank_qb);
			mqi_vect_swap (in_qa, in_qb);
			mqi_vect_swap (root1, root2);
			mqi_mat_swap  (qa, qb);
		}

		if ( (mqi_coef_cmp_schar (rank_qa, 3) == 0) &&
		     (mqi_coef_cmp_schar (rank_qb, 3) == 0) )
			/* original name is "cubic_secant_two_rat" */
			__qi_inter_two_mult_cubic_and_secant_line (rop, q1, q2, qa, qb);
		else {
			
			/** The sign of the determinantal equation is the same for any point
			  * other than the roots */
			
			/** A point "outside" the roots */
			mqi_vect_t root_p;

			mqi_vect_init_dynamic (root_p, 2, mqi_mat_typeof(q1));

			if ( mqi_coef_sign(POLY(gcd_p,2)) ) {
				mqi_vect_set_at_int (root_p,0, 1);
			}else{
				mqi_coef_neg(VECT(root_p,0), POLY(gcd_p,0));
				mqi_coef_sub_schar(VECT(root_p,0), VECT(root_p,0), 1);
				mqi_vect_set_at(root_p,1, POLY(gcd_p,1));
			}
				
			/** The sign of det_p outside the roots */
			int s_e;

			mqi_coef_t coef;
			mqi_coef_init_dynamic (coef, mqi_poly_typeof(det_p));

			mqi_poly_eval_xw (coef, det_p, VECT(root_p,0), VECT(root_p,1));
			s_e = mqi_coef_sign(coef);
			
			if ( mqi_coef_cmp_schar(rank_qa, 3) == 0 ) {
				/** Conic and two lines not crossing on the conic */
				__qi_inter_two_mult_conic_2lines_not_crossing
							(rop, q1, q2, qa, qb, mqi_coef_get_int(VECT(in_qa,0)),s_e);
			}
			else {

				if ( s_e == -1 ) {
					__qi_inter_two_mult_four_skew_lines_2points_rational
							(rop, q1, q2, qa, qb, mqi_coef_get_int(VECT(in_qa,0)));
				}
				else{

					/** If inertia of one of the pair of planes is [2 0], empty intersection */

					if ( mqi_coef_cmp_schar (VECT(in_qa,0), 2) == 0 ) {

						qi_inter_init(rop);
						qi_inter_set_type (rop, 14,1);

					}else{
						__qi_inter_two_mult_four_skew_lines_rational (rop, q1, q2, qa, qb);
					}

				}

			}

			/** Free local memory */
			mqi_vect_clear(root_p);
			mqi_coef_clear(coef);

		}
	
		/** Free local memory */
		mqi_coef_list_clear(sqrt_delta, rank_qa, rank_qb, NULL);
		mqi_vect_list_clear(root1, root2, in_qa, in_qb, NULL);
		mqi_mat_list_clear(qa, qb, NULL);

	}else{

		/** The two roots are either complex or algebraically conjugate, so the ranks of the
		  * associated matrices are the same */
		
		/* root = [a,b,c,d]
		 * projective (a,b +/- c*sqrt{d}) if real
		 * projective (a,b +/- c*i*sqrt{-d}) if complex */
		mqi_vect_t root;
		mqi_vect_init_dynamic (root, 4, mqi_mat_typeof(q1));

		mqi_coef_mul_schar (VECT(root,0), POLY(gcd_p,0), 2);
		mqi_coef_neg	   (VECT(root,1), POLY(gcd_p,1));
		mqi_vect_set_at_int(root,2, 1);
		mqi_vect_set_at    (root,3, delta);

		mqi_coef_t square, cofactor;
		qi_coef_extract_square_factors (square, cofactor, VECT(root,3));

		mqi_vect_set_at (root,2, square);
		
		/** $$ Not very pretty */
		mqi_vect_set_at (root,3, square);
		
		qi_vect_optimize (root);

		mqi_vect_set_at (root,3, cofactor);
		
		/* The singular quadrics are a*q1+(b +/- c*sqrt(d))*q2 (and similarly in the
		* complex case). The singular locus is spanned by vectors k1 +/-
		* sqrt(d)*k2, where the aggregate vector (k1 k2) is in the kernel of the 
		* 8x8 matrix:
		*       (a*q1+b*q2   c*d*q2 )
		*       (                   )
		*       (  c*q2    a*q1+b*q2)
		* The size of the kernel is 4 if the singular quadrics have a
		* singular line and only 2 if they are imaginary cones (because if
		* k1+sqrt(d)*k2 is solution of the above, d*k2+sqrt(d)*k1 also is) */
		mqi_mat_t q_rat, q_sq, big_mat, tmpmat, m1, m2;

		mqi_mat_init_dynamic(q_rat, mqi_mat_nrows(q1), mqi_mat_ncols(q1),
					mqi_mat_typeof(q1));
		mqi_mat_linexpr(q_rat,2, VECT(root,0),q1,	VECT(root,1),q2);
		mqi_mat_init_cpy(q_sq, q2);
		mqi_mat_mul_coef(q_sq, q_sq, VECT(root,2));

		/* Initialized automatically in "concat_horizontally" */
		/*mqi_mat_init_dynamic (big_mat, 8, 8, mqi_mat_typeof(q1));*/

		mqi_mat_init_cpy(tmpmat, q_sq);
		mqi_mat_mul_coef (tmpmat, tmpmat, VECT(root,3));

		mqi_mat_concat_vertically (m1, q_rat, q_sq);
		mqi_mat_concat_vertically (m2, tmpmat, q_rat);

		mqi_mat_concat_horizontally (big_mat, m1, m2);
		
		mqi_mat_t sing;
		qi_mat_singular (sing, big_mat);

		int d_sing;
		d_sing = mqi_mat_ncols(sing);

		if ( mqi_coef_sign(delta) > 0 ) {
			/** Roots are real conjugate */
			
			if ( d_sing == 4 ) {

				/** Sign of leading coef of det_p, nevessarily non zero */
				int s_e;
				s_e = mqi_coef_sign(POLY(det_p,0));

				if ( s_e == 1 ) {

					/* Pick a lambda between the roots. If inertia is [4,0] then
					 * intersection is empty */
					mqi_mat_t q;

					mqi_mat_init_dynamic(q, mqi_mat_nrows(q1), mqi_mat_ncols(q1),
								mqi_mat_typeof(q1));
					mqi_mat_linexpr(q,2,	VECT(root,0),q1, 	VECT(root,1),q2);

					mqi_vect_t inertia, inertia2;

					qi_mat_inertia_known_rank (inertia,  q, 4);
					qi_mat_inertia_known_rank (inertia2, q1, 4);

					if ( (mqi_coef_cmp_schar(VECT(inertia,0),4) == 0) ||
					     (mqi_coef_cmp_schar(VECT(inertia2,0),4) == 0) )
					{
						qi_inter_init(rop);
						qi_inter_set_type (rop, 14,1);

					}else
						__qi_inter_two_mult_four_skew_lines_non_rational(rop,q1,q2,sing,VECT(root,3),s_e);

					/** Free local memory */
					mqi_mat_clear(q);
					mqi_vect_list_clear(inertia, inertia2, NULL);

				}else
					/** (s_e != 1) - Two points */
					__qi_inter_two_mult_four_skew_lines_non_rational(rop,q1,q2,sing,VECT(root,3),s_e);
			}
			else
			{

				mqi_mat_t q;

				mqi_mat_init_cpy (q, q1);
				qi_inter_with_22 (rop,q1,q2,det_p_orig,q,3);

				/** Free local memory */
				mqi_mat_clear(q);

			}


		}else{
			/** Roots are complex conjugate */

			if ( d_sing == 4 ) {
				/** Rank 2 */
				mqi_mat_t q;

				mqi_mat_init_cpy(q,q1);
				qi_inter_with_22 (rop,q1,q2,det_p_orig,q,5);

				/** Free local memory */
				mqi_mat_clear(q);

			}else {
				/** Rank 3 */
				mqi_mat_t q;

				mqi_mat_init_cpy (q, q1);
				qi_inter_with_22 (rop,q1,q2,det_p_orig,q,4);

				/** Free local memory */
				mqi_mat_clear(q);
			}

		}

		/** Free local memory */
		mqi_vect_clear(root);
		mqi_mat_list_clear(q_rat, q_sq, big_mat, tmpmat, m1, m2, sing, NULL);
		mqi_coef_list_clear(square, cofactor, NULL);

	}


	/** Free local memory */
	mqi_coef_clear(delta);

	return;

}

/** Conic and two lines not crossing */
void __qi_inter_two_mult_conic_2lines_not_crossing (qi_inter_t rop, mqi_mat_t __IN q1, mqi_mat_t __IN q2,
						    mqi_mat_t __IN qa, mqi_mat_t __IN qb,
						    int img_cone, int s_e)
{
	qi_inter_component_t	comp;
	qi_inter_cut_t		cut;
	mqi_vect_t cone_sing;

	mqi_mat_t singular;

	qi_mat_singular (singular, qa);
	mqi_mat_init_cpy((mqi_mat_ptr)cone_sing, COL(singular,0));
	
	qi_vect_optimize (cone_sing);

	if ( img_cone == 3 ) {
		/** Imaginary cone */

		/** Only one component here */
		qi_inter_init(rop);
		qi_inter_set_type (rop, 13,1);

		/** The point is the singular point of the cone */
		mqi_curve_param_t cone_p;

		mqi_curve_param_init_dynamic (cone_p, mqi_vect_size(cone_sing), mqi_vect_typeof(cone_sing));
		mqi_curve_param_set_vect     (cone_p, cone_sing);

		qi_inter_component_init (comp, QI_INTER_COMPONENT_2, cone_p);
		qi_inter_component_set_type (comp, QI_INTER_COMPONENT_POINT);

		qi_inter_add_component (rop, comp);
		/* discard comp */
		qi_inter_component_clear(comp);

		qi_inter_set_optimal (rop);

		/** Free local memory */
		mqi_curve_param_clear(cone_p);

	}
	else
	{

		/** Either 2 or 3 components */
		if ( s_e == 1 ) {

			qi_inter_init(rop);
			qi_inter_set_type (rop, 13,3);

		}else {

			qi_inter_init(rop);
			qi_inter_set_type (rop, 13,2);

		}

		/** Preliminaries for both lines and conic */

		/** The singular locus of the pair of planes */
		mqi_mat_t plane_sing;

		qi_mat_singular (plane_sing, qb);

		/** Since the vertex of qb falls on qa outside of its singular locus, the two planes
		 *  of qb are rational. */
		mqi_coef_t D;
		mqi_mat_t m1, m2;

		mqi_coef_init_dynamic(D, mqi_mat_typeof(q1));
		mqi_mat_init_dynamic(m1, 4, 3, mqi_mat_typeof(q1));
		mqi_mat_init_dynamic(m2, 4, 1, mqi_mat_typeof(q1));

		qi_param_plane_pair (qb, plane_sing, D, m1, m2);

		/** The two planes are rational so we can directly replace the first column of m1 by m1(0) +/- m2 */
		mqi_mat_t m1p, m1m;

		mqi_mat_init_cpy(m1p, m1);
		mqi_mat_init_cpy(m1m, m1);

		mqi_mat_add (COL(m1p,0), COL(m1,0), COL(m2,0));
		mqi_mat_sub (COL(m1m,0), COL(m1,0), COL(m2,0));

		/* Now we compute the following two matrices: one has inertia [2 1]
		 * and represents the conic, the other has inertia either [1 1] or [2 0] and
		 * represents the (real or imaginary) pair of lines */
		mqi_mat_t ap, am;

		mqi_mat_transformation (ap, qa, m1p, MQI_MAT_NOTRANSCO);
		mqi_mat_transformation (am, qa, m1m, MQI_MAT_NOTRANSCO);

		/** Swap if ap is not the conic of inertia [2 1] */
		mqi_coef_t coef;
		mqi_coef_init_dynamic (coef, mqi_mat_typeof(ap));

		mqi_mat_det (coef, ap);

		if ( ! mqi_coef_sign(coef) ) {
			mqi_mat_swap (ap, am);
			mqi_mat_swap (m1p, m1m);
		}

		/** Cut parameters of the two lines - needed later on */
		mqi_vect_t cut1, cut2;
		mqi_mat_t  linep, linem;
		mqi_vect_init_dynamic (cut1, 2, mqi_mat_typeof(ap));
		mqi_vect_init_dynamic (cut2, 2, mqi_mat_typeof(ap));

		mqi_mat_init_dynamic (linep, 4, 2, mqi_mat_typeof(ap));
		mqi_mat_init_dynamic (linem, 4, 2, mqi_mat_typeof(ap));

		if ( s_e == -1 ) {

			/** The first component is a point */

			/** The point is the singular point of the cone */
			mqi_curve_param_t cone_p;

			mqi_curve_param_init_dynamic (cone_p, mqi_vect_size(cone_sing), mqi_vect_typeof(cone_sing));
			mqi_curve_param_set_vect     (cone_p, cone_sing);

			qi_inter_component_init (comp, QI_INTER_COMPONENT_2, cone_p);
			qi_inter_component_set_type (comp, QI_INTER_COMPONENT_POINT);

			qi_inter_component_set_optimal(comp);
			qi_inter_add_component (rop, comp);
			/* discard comp */
			qi_inter_component_clear(comp);

			/** Set D to a non-zero value, so that it goes in the right loop below */
			mqi_coef_set_schar (D, 1);

			/** Free local memory */
			mqi_curve_param_clear(cone_p);

		}else { /** s_e != -1 */

			/** First component is pair of lines */

			/** The (2D) singular point of the pair of lines (could be inferred from the cone singularity, but ...) */
			mqi_vect_t line_sing;

			/* discard old singular */
			mqi_mat_clear(singular);			
			qi_mat_singular (singular, am);
			mqi_mat_init_cpy((mqi_mat_ptr)line_sing, COL(singular,0));

			qi_vect_optimize (line_sing);

			/** Parametrization of the pair of lines */
			mqi_mat_t m1_l, m2_l;

			mqi_mat_init_dynamic(m1_l, 3, 2, mqi_mat_typeof(q1));
			mqi_mat_init_dynamic(m2_l, 3, 1, mqi_mat_typeof(q1));
			qi_param_line_pair (am, line_sing, D, m1_l, m2_l);

			if ( ! mqi_coef_sign(D) ) {

				/** The two lines are rational */
				mqi_mat_t l1p, l1m;

				mqi_mat_init_cpy(l1p, m1_l);
				mqi_mat_init_cpy(l1m, m1_l);

				mqi_mat_add (COL(l1p,0), COL(m1_l,0), COL(m2_l,0));
				mqi_mat_sub (COL(l1m,0), COL(m1_l,0), COL(m2_l,0));

				qi_mat_optimize (l1p);
				qi_mat_optimize (l1m);

				/** Stack the transformation matrices */

				/* discard old linep, linem */
				mqi_mat_list_clear(linep, linem, NULL);
				mqi_mat_mul_init (linep, m1m, l1p);
				mqi_mat_mul_init (linem, m1m, l1m);

				qi_mat_optimize (linep);
				qi_mat_optimize (linem);

				/** The two lines */
				mqi_curve_param_t line1, line2;

				mqi_curve_param_init_dynamic (line1, mqi_mat_nrows(linep), mqi_mat_typeof(linep));
				mqi_curve_param_init_dynamic (line2, mqi_mat_nrows(linem), mqi_mat_typeof(linem));

				mqi_curve_param_line_through_two_points (line1,
							(mqi_vect_ptr)COL(linep,1), (mqi_vect_ptr)COL(linep,0));
				mqi_curve_param_line_through_two_points (line2,
							(mqi_vect_ptr)COL(linem,1), (mqi_vect_ptr)COL(linem,0));

				qi_param_line_improved1 (cut1, line1);
				qi_param_line_improved1 (cut2, line2);

				qi_vect_optimize_by_half (cut1);
				qi_vect_optimize_by_half (cut2);


				qi_inter_component_init (comp, QI_INTER_COMPONENT_2, line1);
				qi_inter_component_set_type (comp, QI_INTER_COMPONENT_LINE);
				qi_inter_add_component (rop, comp);
				/* discard comp */
				qi_inter_component_clear(comp);

				qi_inter_component_init (comp, QI_INTER_COMPONENT_2, line2);
				qi_inter_component_set_type (comp, QI_INTER_COMPONENT_LINE);
				qi_inter_add_component (rop, comp);
				/* discard comp */
				qi_inter_component_clear(comp);

				/** Free local memory */
				mqi_mat_list_clear(l1p, l1m, NULL);
				mqi_curve_param_list_clear(line1, line2, NULL);

			}else{

				/** The lines are not rational */

				/** Stack the transformations */
				mqi_mat_t tmpmat;

				mqi_mat_init_cpy(tmpmat, m1_l);
				/* discard old m1_l */
				mqi_mat_clear(m1_l);
				mqi_mat_mul_init (m1_l, m1m, tmpmat);
			
				mqi_mat_clear(tmpmat);
				mqi_mat_init_cpy(tmpmat, m2_l);
				mqi_mat_clear(m2_l);
				mqi_mat_mul_init(m2_l, m1m, tmpmat);
				mqi_mat_clear(tmpmat);
				
				mqi_mat_resize (tmpmat, m2_l, 4, 2);
				mqi_mat_clear(m2_l);
				mqi_mat_init_cpy(m2_l, tmpmat);
				

				qi_mat_optimize_2 (m1_l, m2_l);

				/** The components of the lines */
				mqi_curve_param_t line_rat, line_D;

				mqi_curve_param_init_dynamic (line_rat, mqi_mat_nrows(m1_l), mqi_mat_typeof(m1_l));
				mqi_curve_param_init_dynamic (line_D, mqi_mat_nrows(m2_l), mqi_mat_typeof(m2_l));

				mqi_curve_param_line_through_two_points (line_rat,
						(mqi_vect_ptr)COL(m1_l,0), (mqi_vect_ptr)COL(m1_l,1));
				mqi_curve_param_line_through_two_points (line_D,
						(mqi_vect_ptr)COL(m2_l,0), (mqi_vect_ptr)COL(m2_l,1));
				
				mqi_vect_t ignored;

				qi_param_line_improved2 (ignored, line_rat, line_D, D);

				qi_inter_create_components (rop, 2, QI_INTER_COMPONENT_LINE,
								QI_INTER_COMPONENT_3,
								line_rat, line_D, D);
				
				/** Free local memory */
				mqi_mat_clear(tmpmat);
				mqi_vect_clear(ignored);
				mqi_curve_param_list_clear(line_rat, line_D, NULL);

			}

			/** Lines are optimal */
			qi_inter_component_set_optimal (rop->components[0]);
			qi_inter_component_set_optimal (rop->components[1]);

			/** Free local memory */
			mqi_vect_clear(line_sing);
			mqi_mat_list_clear(m1_l, m2_l, NULL);

		} /** if s_e != -1 */

		/** If the lines are rational, their intersection with the singular line of
		 * the pair of planes is a ratioinal point on the conic */

		if ( ! mqi_coef_sign(D) ) {

			mqi_vect_t pp, pm;
			mqi_mat_t  tmpmat;

			qi_mat_linear_intersect (tmpmat, plane_sing, linep);
			mqi_mat_init_cpy ((mqi_mat_ptr)pp, COL(tmpmat,0));

			mqi_mat_clear(tmpmat);
			qi_mat_linear_intersect (tmpmat, plane_sing, linem);
			mqi_mat_init_cpy ((mqi_mat_ptr)pm, COL(tmpmat,0));

			qi_vect_optimize (pp);
			qi_vect_optimize (pm);

			if ( s_e == 1 ) {

				/** Find the parameters on the lines corresponding to pp and pm */
				mqi_mat_t plinep, plinem;

				mqi_mat_list_init_dynamic (4, 2, mqi_vect_typeof(pm), plinep, plinem, NULL);
				
				mqi_curve_param_eval_int ((mqi_vect_ptr)COL(plinep,0), rop->components[0]->c[0], 1,0);
				mqi_curve_param_eval_int ((mqi_vect_ptr)COL(plinep,1), rop->components[0]->c[0], 0,1);

				mqi_mat_t tmpmat, tmpmat2, tmpmat3;
				mqi_vect_t cutp, cutm;

				mqi_mat_transpose_init (tmpmat, plinep);
				mqi_mat_mul_init (tmpmat2, tmpmat, plinep);
				mqi_mat_transco (tmpmat3, tmpmat2);
				mqi_mat_clear(tmpmat2);
				mqi_mat_mul_init (tmpmat2, tmpmat3, tmpmat);
				mqi_mat_mul_init ((mqi_mat_ptr)cutp, tmpmat2, (mqi_mat_ptr)pp);

				qi_vect_optimize (cutp);

				qi_inter_cut_init (cut, QI_INTER_CUT_0, VECT(cut1,0), VECT(cut1,1));
				qi_inter_component_add_cut_parameter(rop->components[0], cut);
				qi_inter_cut_set_owner(cut, rop->components[0]);

				qi_inter_cut_init (cut, QI_INTER_CUT_0, VECT(cutp,0), VECT(cutp,1));
				qi_inter_component_add_cut_parameter(rop->components[0], cut);
				qi_inter_cut_set_owner(cut, rop->components[0]);

				mqi_mat_list_clear(tmpmat, tmpmat2, tmpmat3, NULL);

				mqi_curve_param_eval_int ((mqi_vect_ptr)COL(plinem,0), rop->components[1]->c[0], 1,0);
				mqi_curve_param_eval_int ((mqi_vect_ptr)COL(plinem,1), rop->components[1]->c[0], 0,1);

				mqi_mat_transpose_init (tmpmat, plinem);
				mqi_mat_mul_init (tmpmat2, tmpmat, plinem);
				mqi_mat_transco (tmpmat3, tmpmat2);
				mqi_mat_clear(tmpmat2);
				mqi_mat_mul_init (tmpmat2, tmpmat3, tmpmat);
				mqi_mat_mul_init ((mqi_mat_ptr)cutm, tmpmat2, (mqi_mat_ptr)pp);

				qi_vect_optimize (cutm);

				qi_inter_cut_init (cut, QI_INTER_CUT_0, VECT(cut2,0), VECT(cut2,1));
				qi_inter_component_add_cut_parameter(rop->components[1], cut);
				qi_inter_cut_set_owner(cut, rop->components[1]);

				qi_inter_cut_init (cut, QI_INTER_CUT_0, VECT(cutm,0), VECT(cutm,1));
				qi_inter_component_add_cut_parameter(rop->components[1], cut);
				qi_inter_cut_set_owner(cut, rop->components[1]);

				/** Free local memory */
				mqi_mat_list_clear(tmpmat, tmpmat2, tmpmat3, plinep, plinem, NULL);
				mqi_vect_list_clear(cutp, cutm, NULL);


			} /** end s_e = 1 */		

			/** The point pp is a*plane_sing(0)+b*plane_sing(1): find a and b */
			mqi_vect_t ab;
			mqi_mat_t tmpmat2, tmpmat3;

			mqi_mat_clear (tmpmat);
			mqi_mat_transpose_init (tmpmat, plane_sing);
			mqi_mat_mul_init (tmpmat2, tmpmat, plane_sing);
			mqi_mat_transco (tmpmat3, tmpmat2);
			mqi_mat_clear(tmpmat2);
			mqi_mat_mul_init (tmpmat2, tmpmat3, tmpmat);
			mqi_mat_mul_init ((mqi_mat_ptr)ab, tmpmat2, (mqi_mat_ptr)pp);


			qi_vect_optimize (ab);

			/** (0,ab[0],ab[1]) is a rational point on the conic! */
			mqi_vect_t rat_point2d;

			mqi_vect_init_dynamic (rat_point2d, 3, mqi_vect_typeof(ab));

			mqi_vect_set_at (rat_point2d,1, VECT(ab,0));
			mqi_vect_set_at (rat_point2d,2, VECT(ab,1));

			mqi_mat_t p_trans;
			mqi_curve_param_t conic2d;
			mqi_vect_t l;

			mqi_mat_init_dynamic(p_trans, 3, 3, mqi_mat_typeof(q1));
			mqi_curve_param_init_dynamic(conic2d, 3, mqi_mat_typeof(q1));
			mqi_vect_init_dynamic(l, 2, mqi_mat_typeof(q1));
			qi_param_conic_through_ratpoint (ap,rat_point2d,p_trans,conic2d,l);

			/** Stack the transformations */
	
			mqi_mat_clear(tmpmat);
			mqi_mat_init_cpy(tmpmat, p_trans);
			mqi_mat_clear(p_trans);
			mqi_mat_mul_init (p_trans, m1p, tmpmat);

			/** Rescale so that l[0]*u+l[1]*v is replaced by v */
			mqi_mat_t scalep;

			mqi_mat_init_dynamic (scalep, 3, 3, mqi_vect_typeof(l));

			mqi_coef_mul (MAT(scalep,0,0), VECT(l,1), VECT(l,1));
			mqi_coef_mul (MAT(scalep,1,0), VECT(l,0), VECT(l,0));
			mqi_coef_set_schar (MAT(scalep,1,1), 1);
			mqi_coef_mul_schar (MAT(scalep,1,2), VECT(l,0), -2);
			mqi_coef_linexpr2  (MAT(scalep,2,0),1,	  -1,VECT(l,0),VECT(l,1));
			mqi_coef_cpy (MAT(scalep,2,2), VECT(l,1));

			mqi_mat_clear(tmpmat);
			mqi_mat_init_cpy(tmpmat, p_trans);
			mqi_mat_clear(p_trans);
			mqi_mat_mul_init (p_trans, tmpmat, scalep);

			qi_mat_optimize_3 (p_trans);

			mqi_vect_set_at_int (l,0, 0);
			mqi_vect_set_at_int (l,1, -1);

			mqi_curve_param_t conic;

			mqi_curve_param_init_cpy (conic, conic2d);
			mqi_curve_param_mul_mat  (conic, p_trans, conic);

			/** $$ AMBIGUOUS SECTION: libmqi's way to manage components is a "stack".
			 * A simple call to "add_component" adds a new components without having to
			 * specify for an indice and remember where we stopped.
			 * I think QI's way to manage need to remember the last indice, that's why
			 * this part of the code (and maybe other sections in other parts of the source code)
			 * is splitted using a conditional statement. */
			/**		if ( s_e == -1 )  */

			qi_inter_component_init (comp, QI_INTER_COMPONENT_2, conic);
			qi_inter_component_set_type (comp, QI_INTER_COMPONENT_CONIC);
			qi_inter_component_set_optimal (comp);

			mqi_coef_t coef, coef2;

			mqi_coef_init_cpy(coef, VECT(l,1));
			mqi_coef_neg (coef, coef);
			qi_inter_cut_init (cut, QI_INTER_CUT_0, coef, VECT(l,0));
			qi_inter_component_add_cut_parameter (comp, cut);
			qi_inter_cut_set_owner(cut, comp);

			mqi_coef_set_schar (coef, 0);

			mqi_coef_init_dynamic(coef2, mqi_coef_typeof(coef));
			mqi_coef_set_schar (coef2, 1); 

			qi_inter_cut_init (cut, QI_INTER_CUT_0, coef, coef2);
			qi_inter_component_add_cut_parameter (comp, cut);
			qi_inter_cut_set_owner(cut, comp);

			qi_inter_add_component(rop, comp);
			/* discard comp */
			qi_inter_component_clear(comp);


			/** Free local memory */
			mqi_vect_list_clear (pp, pm, ab, l, rat_point2d, NULL);
			mqi_coef_list_clear (coef, coef2, NULL);
			mqi_curve_param_list_clear (conic, conic2d, NULL);
			mqi_mat_list_clear (tmpmat, tmpmat2, tmpmat3, p_trans, scalep, NULL);

		}else{

			/** The lines are not rational, but we may still end up finding a rational conic */

			/** Parametrize the conic */
			mqi_coef_t D2;
			mqi_mat_t p_trans, p_trans2;
			mqi_curve_param_t par;

			mqi_curve_param_init_dynamic (par, 3, mqi_mat_typeof(q1));
			mqi_mat_list_init_dynamic (3, 3, mqi_mat_typeof(q1), p_trans, p_trans2, NULL);
			
			qi_param_conic (ap, __INIT D2, p_trans, p_trans2, par);

			if ( ! mqi_coef_sign(D2) ) {

				/** We found a rational conic */

				/** Stack the transformations */
				mqi_mat_t tmpmat;

				mqi_mat_init_cpy(tmpmat, p_trans);
				mqi_mat_clear(p_trans);
				mqi_mat_mul (p_trans, m1p, tmpmat);

				qi_mat_optimize_3(p_trans);

				mqi_curve_param_t conic;

				mqi_curve_param_init_cpy (conic, par);
				mqi_curve_param_mul_mat  (conic, p_trans, conic);

				/** $$ Same remark for the management of components */
				qi_inter_component_init (comp, QI_INTER_COMPONENT_2, conic);
				qi_inter_component_set_type (comp, QI_INTER_COMPONENT_CONIC);
				qi_inter_component_set_optimal (comp);

				qi_inter_add_component (rop, comp);
				/* discard comp */
				qi_inter_component_clear(comp);

				/** Free local memory */
				mqi_mat_clear(tmpmat);
				mqi_curve_param_clear(conic);

			}else{

				/** One square root cannot be discarded */

				/** Stack the transformations */
				mqi_mat_t tmpmat;

				mqi_mat_init_cpy(tmpmat, p_trans);
				mqi_mat_clear(p_trans);
				mqi_mat_mul (p_trans, m1p, tmpmat);

				mqi_mat_clear(tmpmat);
				mqi_mat_init_cpy(tmpmat, p_trans2);
				mqi_mat_clear(p_trans2);
				mqi_mat_mul (p_trans2, m1p, tmpmat);

				qi_mat_optimize_4 (p_trans, p_trans2);

				mqi_curve_param_t conic, conic2;

				mqi_curve_param_init_cpy (conic, par);
				mqi_curve_param_init_cpy (conic2, par);

				mqi_curve_param_mul_mat (conic, p_trans, conic);
				mqi_curve_param_mul_mat (conic2, p_trans2, conic2);

				/** $$ Same remark for the management of components */
				qi_inter_component_init (comp, QI_INTER_COMPONENT_3, conic, conic2, D2);
				qi_inter_component_set_type (comp, QI_INTER_COMPONENT_CONIC);

				if ( qi_settings.optimize ) qi_inter_component_set_optimal (comp);

				qi_inter_add_component (rop, comp);
				/* discard comp */
				qi_inter_component_clear (comp);

				/** Free local memory */
				mqi_mat_clear (tmpmat);
				mqi_curve_param_list_clear (conic, conic2, NULL);

			}

			if ( s_e != -1 ) {

				/** Temporary */
				mqi_coef_t coef;

				mqi_coef_init_dynamic (coef, mqi_mat_typeof(q1));
				mqi_coef_set_schar (coef, 0);
				qi_inter_cut_init (cut, QI_INTER_CUT_0, coef, coef);

				qi_inter_component_add_cut_parameter (rop->components[1], cut);
				qi_inter_component_add_cut_parameter (rop->components[1], cut);
				qi_inter_component_add_cut_parameter (rop->components[2], cut);
				qi_inter_component_add_cut_parameter (rop->components[2], cut);

				qi_inter_cut_set_owner (cut, rop->components[1]);

				/** Free local memory */
				mqi_coef_clear(coef);

			}
	
			mqi_coef_clear(D2);
			mqi_mat_list_clear (p_trans, p_trans2, NULL);
			mqi_curve_param_clear (par);

		}

		/** Free local memory */
		mqi_coef_list_clear(D, coef, NULL);
		mqi_vect_list_clear(cut1, cut2, NULL);
		mqi_mat_list_clear(plane_sing, m1, m1p, m1m, m2, ap, am, linep, linem, NULL);

	}

	/** Free local memory */
	mqi_vect_clear (cone_sing);
	mqi_mat_clear  (singular);

}

/** Cubic and secant line */
void __qi_inter_two_mult_cubic_and_secant_line (qi_inter_t rop, mqi_mat_t __IN q1, mqi_mat_t __IN q2, mqi_mat_t __IN qa, mqi_mat_t __IN qb)
{	

	qi_inter_component_t	comp;
	qi_inter_cut_t		cutp;

	/** Two components */
	qi_inter_init(rop);
	qi_inter_set_type (rop, 12,1);

	 /* Both qa and qb are rational cones. The singular point of qa is a rational
	  * point of qb */

	/* Singular points of cones */
	mqi_mat_t singular;
	mqi_vect_t singa, singb;

	qi_mat_singular (singular, qa);
	mqi_vect_init_cpy (singa, (mqi_vect_ptr)COL(singular,0));

	mqi_mat_clear(singular);	
	qi_mat_singular (singular, qb);
	mqi_vect_init_cpy (singb, (mqi_vect_ptr)COL(singular,0));

	qi_vect_optimize (singa);
	qi_vect_optimize (singb);

	mqi_vect_t rat_point;

	/* This statement is strange, because rat_point is
	 * initialized later in any case.
	 */
	/*mqi_vect_init_cpy (rat_point, singa);*/

	/** The line param */
	mqi_curve_param_t line_par;
	mqi_curve_param_init_dynamic (line_par, mqi_vect_size(singa), mqi_vect_typeof(singa));
	mqi_curve_param_line_through_two_points (line_par, singa, singb);

	/** Cut parameters */

	/** Try to find a point of small height on the line by looking at the
	  * parameter values such that one of the components is zero. */
	mqi_vect_t cut;

	qi_param_line_improved1 (cut, line_par);
	qi_vect_optimize_by_half(cut);

	/* Slight simplification from the original code */
	mqi_vect_init_dynamic (rat_point, mqi_curve_param_n_equations(line_par), mqi_curve_param_typeof(line_par));
	
	mqi_curve_param_eval_int (rat_point, line_par, 1,0);
	if ( mqi_vect_equals (singb, rat_point) )
	{
		mqi_curve_param_eval_int (rat_point, line_par, 0,1);
	}

	/** Parametrize the cone */
	mqi_surface_param_t par;
	mqi_mat_t p_trans;
	mqi_vect_t l;

	mqi_vect_init_dynamic(l, 2, mqi_mat_typeof(qa));
	qi_param_cone_through_ratpoint (qb,singb,rat_point,p_trans,par,l);

	qi_mat_optimize_3_update (p_trans, l);

	/** Do that anyway, otherwise tmp might not divide res (constant factor larger) */
	qi_vect_optimize (l);

	/** The quadric in the canonical frame of the cone */
	mqi_mat_t q_tmp;
	mqi_mat_transformation (q_tmp, qa, p_trans, MQI_MAT_NOTRANSCO);

	/** Plug in the other quadric */
	mqi_hhpoly_t res;
	qi_param_plug_sp_in_quadric (res, par, q_tmp, par);

	/** The common factor of res (see below) */
	mqi_hpoly_t tmp;
	mqi_vect_t m;
	mqi_vect_init_dynamic (m, 2, mqi_mat_typeof(q_tmp));
	
	if ( mqi_coef_sign(VECT(l,0)) ) {
		mqi_coef_mul (VECT(m,0), VECT(l,0), HHCOEF(res,1,2));
		mqi_coef_linexpr2 (VECT(m,1), 2,		1,VECT(l,0),HHCOEF(res,1,1),	-1,VECT(l,1),HHCOEF(res,1,2));
	}else {
		mqi_coef_cpy (VECT(m,0), HHCOEF(res,1,1));
		mqi_coef_cpy (VECT(m,1), HHCOEF(res,1,0));
	}

	qi_vect_optimize (m);

	/* res has the form:
	*       ---> u*(l[0]*s+l[1]*t)*(c1*p1(s,t)*u + c2*p2(s,t)*v),
	* where p1(s,t) has degree 3 and p2(s,t) = m[0]*s+m[1]*t has degree 1

	* The two solutions (-l[1],l[0]) and (-m[1],m[0]) correspond to the two points of
	* intersection of the line with the cubic. Let us rescale the cubic so that
	* these two points correspond to the parameters (1,0) and (0,1). Since these
	* points are quite ``simple'', the cubic should have a much nicer param.

	* Now rewrite the par accordingly, i.e. replace l[0]*s+l[1]*t by t and
	* m[0]*s+m[1]*t by s */

	mqi_coef_mul		(SPCOEF(par,0,1,2), VECT(l,1), VECT(l,1));
	mqi_coef_linexpr2	(SPCOEF(par,0,1,1), 1,	-2,VECT(m,1),VECT(l,1));
	mqi_coef_mul		(SPCOEF(par,0,1,0),VECT(m,1),VECT(m,1));
	mqi_coef_mul		(SPCOEF(par,1,1,2),VECT(l,0),VECT(l,0));
	mqi_coef_linexpr2	(SPCOEF(par,1,1,1), 1,	-2,VECT(m,0),VECT(l,0));
	mqi_coef_mul		(SPCOEF(par,1,1,0), VECT(m,0), VECT(m,0));
	mqi_coef_linexpr2	(SPCOEF(par,2,1,2),1,	-1,VECT(l,0),VECT(l,1));
	mqi_coef_linexpr2	(SPCOEF(par,2,1,1),2,	1,VECT(l,0),VECT(m,1),	 1,VECT(m,0),VECT(l,1));
	mqi_coef_linexpr2	(SPCOEF(par,2,1,0),1,	-1,VECT(m,0),VECT(m,1));

	/** And do the plug again */
	/* discard old res */
	mqi_hhpoly_clear(res);
	qi_param_plug_sp_in_quadric (res, par, q_tmp, par);

	/* At this stage, res has the form
	 *      ---> u*t*(c1*p1(s,t)*t*u + c2*s*v)
	 * where p1(s,t) has degree 2 */
	mqi_hpoly_init_dynamic (tmp, mqi_mat_typeof(q_tmp));
	mqi_poly_set_wi (tmp, 1);

	mqi_vect_set_at_int (l,0, 0);
	mqi_vect_set_at_int (l,1, -1);
	
	mqi_vect_set_at_int (m,0, 1);
	mqi_vect_set_at_int (m,1, 0);

	mqi_hpoly_t p1, p2, R;
	mqi_coef_t coef;
	
	mqi_poly_div_qr (p1, R, coef, HHPOLY(res,2), tmp);
	mqi_poly_div_qr (p2, R, coef, HHPOLY(res,1), tmp);
	mqi_poly_neg (p2, p2);

	
	/** Expand param */
	mqi_surface_param_mul_mat (par, p_trans, par);

	mqi_curve_param_t cubic;
	mqi_curve_param_init_dynamic (cubic, 4, mqi_mat_typeof(p_trans));
	
	mqi_surface_param_eval_poly (cubic, par, p2, p1);

	qi_curve_param_optimize (cubic);

	qi_inter_component_init (comp, QI_INTER_COMPONENT_2, cubic);
	qi_inter_component_set_type (comp, QI_INTER_COMPONENT_CUBIC);
	
	mqi_coef_neg (VECT(l,1), VECT(l,1));
	mqi_coef_neg (VECT(m,1), VECT(m,1));

	qi_inter_cut_init (cutp, QI_INTER_CUT_0, VECT(l,1), VECT(l,0));
	qi_inter_component_add_cut_parameter (comp, cutp);
	qi_inter_cut_set_owner(cutp, comp);

	qi_inter_cut_init (cutp, QI_INTER_CUT_0, VECT(m,1), VECT(m,0));
	qi_inter_component_add_cut_parameter (comp, cutp);
	qi_inter_cut_set_owner(cutp, comp);

	qi_inter_add_component (rop, comp);
	/* discard comp */
	qi_inter_component_clear(comp);

	qi_inter_component_init (comp, QI_INTER_COMPONENT_2, line_par);
	qi_inter_component_set_type (comp, QI_INTER_COMPONENT_LINE);
	
	qi_inter_cut_init (cutp, QI_INTER_CUT_0, VECT(cut,0), VECT(cut,1));
	qi_inter_component_add_cut_parameter (comp, cutp);
	qi_inter_cut_set_owner(cutp, comp);

	qi_inter_cut_init (cutp, QI_INTER_CUT_0, VECT(cut,2), VECT(cut,3));
	qi_inter_component_add_cut_parameter (comp, cutp);
	qi_inter_cut_set_owner(cutp, comp);

	qi_inter_add_component (rop, comp);
	/* discard comp */
	qi_inter_component_clear(comp);

	qi_inter_set_optimal (rop);


	/** Free local memory */
	mqi_mat_list_clear (singular, p_trans, q_tmp, NULL);
	mqi_vect_list_clear(singa, singb, rat_point, cut, l, m, NULL);
	mqi_curve_param_list_clear(line_par, cubic, NULL);
	mqi_coef_clear(coef);
	mqi_surface_param_clear(par);
	mqi_hhpoly_clear(res);
	mqi_poly_list_clear(tmp, p1, p2, R, NULL);

}

/** Four skew lines */
void __qi_inter_two_mult_four_skew_lines_rational (qi_inter_t rop, mqi_mat_t __IN q1, mqi_mat_t __IN q2,
						   mqi_mat_t __IN qa, mqi_mat_t __IN qb)
{

	qi_inter_component_t	comp;
	qi_inter_cut_t		cut;

	/** Four components here */
	qi_inter_init(rop);
	qi_inter_set_type (rop, 14,2);

	/* The four vertices of the quadrilateral are the intersections of the singular
	 * line of one pair with the other pair */
	mqi_mat_t singa, singb;
	qi_mat_singular (singa, qa);
	qi_mat_singular (singb, qb);

	/* Now compute the following matrices */
	mqi_mat_t mab, mba;
	mqi_mat_transformation (mab, qb, singa, MQI_MAT_NOTRANSCO);
	mqi_mat_transformation (mba, qa, singb, MQI_MAT_NOTRANSCO);

	/** Discriminants */
	mqi_coef_t Dab, Dba;
	mqi_coef_list_init_dynamic (mqi_mat_typeof(mab), Dab, Dba, NULL);

	mqi_coef_linexpr2 (Dab, 2,	1,MAT(mab,0,1),MAT(mab,0,1),	-1,MAT(mab,0,0),MAT(mab,1,1));
	mqi_coef_linexpr2 (Dba, 2,	1,MAT(mba,0,1),MAT(mba,0,1),	-1,MAT(mba,0,0),MAT(mba,1,1));

	/** The (1D) points: the solutions are pab1 and pab2 if the discrim is a square,
	  * pab1 +/- sqrt(Dab)*pab2 otherwise */
	mqi_mat_t pab1, pab2, pba1, pba2;
	mqi_mat_list_init_dynamic(2, 1, mqi_mat_typeof(mab), pab1, pab2, pba1, pba2, NULL);

	mqi_coef_t sqrt_Dab, sqrt_Dba;
	mqi_coef_list_init_dynamic (mqi_coef_typeof(Dab), sqrt_Dab, sqrt_Dba, NULL);

	int sq_Dab = mqi_coef_is_square(Dab);
	int sq_Dba = mqi_coef_is_square(Dba);

	/** The square roots of discriminants */
	if ( sq_Dab ) {
		
		mqi_coef_sqrt (sqrt_Dab, Dab);

		if ( mqi_coef_sign(MAT(mab,0,0)) ) {

			/* Use the "set_at" routines when it is possible for it
			 * performs indice checkings. */
			mqi_coef_sub (MAT(pab1,0,0), sqrt_Dab, MAT(mab,0,1));
			mqi_mat_set_at (pab1,1,0, MAT(mab,0,0));
			mqi_coef_add (MAT(pab2,0,0), MAT(mab,0,1), sqrt_Dab);
			mqi_coef_neg (MAT(pab2,0,0), MAT(pab2,0,0));
			mqi_mat_set_at (pab2,1,0, MAT(mab,0,0));

		}else{

			mqi_mat_set_at_int (pab1,0,0, 1);
			mqi_mat_set_at	   (pab2,0,0, MAT(mab,1,1));
			mqi_coef_mul_schar (MAT(pab2,1,0), MAT(mab,0,1), -2);

		}

		mqi_mat_t tmpmat;

		mqi_mat_init_cpy(tmpmat, pab1);
		mqi_mat_clear(pab1);
		mqi_mat_mul_init (pab1, singa, tmpmat);

		mqi_mat_clear(tmpmat);
		mqi_mat_init_cpy(tmpmat, pab2);
		mqi_mat_clear(pab2);
		mqi_mat_mul_init (pab2, singa, tmpmat);


		qi_mat_optimize (pab1);
		qi_mat_optimize (pab2);

		/** Free local memory */
		mqi_mat_clear(tmpmat);

	}else{

		mqi_coef_neg (MAT(pab1,0,0), MAT(mab,0,1));
		mqi_mat_set_at (pab1,1,0, MAT(mab,0,0));
		mqi_mat_set_at_int (pab2,0,0, 1);

		mqi_mat_t tmpmat;

		mqi_mat_init_cpy(tmpmat, pab1);
		mqi_mat_clear(pab1);
		mqi_mat_mul_init (pab1, singa, tmpmat);

		mqi_mat_clear(tmpmat);
		mqi_mat_init_cpy(tmpmat, pab2);
		mqi_mat_clear(pab2);
		mqi_mat_mul_init (pab2, singa, tmpmat);

		/** Replace Dab by e^2*Dab'  and optimize */
		mqi_coef_t square, cofactor;

		qi_coef_extract_square_factors (square, cofactor, Dab);

		/** Dab --> Dab' */
		mqi_coef_cpy (Dab, cofactor);

		/** Multiply pab2 by e = square */
		mqi_mat_mul_coef (pab2, pab2, square);

		qi_mat_optimize_2 (pab1, pab2);

		/** Free local memory */
		mqi_mat_clear(tmpmat);
		mqi_coef_list_clear(square, cofactor, NULL);

	}

	if ( sq_Dba ) {
		
		mqi_coef_sqrt (sqrt_Dba, Dba);

		if ( mqi_coef_sign(MAT(mba,0,0)) ) {

			/* Use the "set_at" routines when it is possible for it
			 * performs indice checkings. */
			mqi_coef_sub (MAT(pba1,0,0), sqrt_Dba, MAT(mba,0,1));
			mqi_mat_set_at (pba1,1,0, MAT(mba,0,0));
			mqi_coef_add (MAT(pba2,0,0), MAT(mba,0,1), sqrt_Dba);
			mqi_coef_neg (MAT(pba2,0,0), MAT(pba2,0,0));
			mqi_mat_set_at (pba2,1,0, MAT(mba,0,0));

		}else{

			mqi_mat_set_at_int (pba1,0,0, 1);
			mqi_mat_set_at	   (pba2,0,0, MAT(mba,1,1));
			mqi_coef_mul_schar (MAT(pba2,1,0), MAT(mba,0,1), -2);

		}

		mqi_mat_t tmpmat;

		mqi_mat_init_cpy(tmpmat, pba1);
		mqi_mat_clear(pba1);
		mqi_mat_mul_init (pba1, singa, tmpmat);

		mqi_mat_clear(tmpmat);
		mqi_mat_init_cpy(tmpmat, pba2);
		mqi_mat_clear(pba2);
		mqi_mat_mul_init (pba2, singa, tmpmat);


		qi_mat_optimize (pba1);
		qi_mat_optimize (pba2);

		/** Free local memory */
		mqi_mat_clear(tmpmat);

	}else{

		mqi_coef_neg (MAT(pba1,0,0), MAT(mba,0,1));
		mqi_mat_set_at (pba1,1,0, MAT(mba,0,0));
		mqi_mat_set_at_int (pba2,0,0, 1);

		mqi_mat_t tmpmat;

		mqi_mat_init_cpy(tmpmat, pba1);
		mqi_mat_clear(pba1);
		mqi_mat_mul_init (pba1, singa, tmpmat);

		mqi_mat_clear(tmpmat);
		mqi_mat_init_cpy(tmpmat, pba2);
		mqi_mat_clear(pba2);
		mqi_mat_mul_init (pba2, singa, tmpmat);

		/** Replace Dba by e^2*Dba'  and optimize */
		mqi_coef_t square, cofactor;

		qi_coef_extract_square_factors (square, cofactor, Dba);

		/** Dba --> Dba' */
		mqi_coef_cpy (Dba, cofactor);

		/** Multiply pba2 by e = square */
		mqi_mat_mul_coef (pba2, pba2, square);

		qi_mat_optimize_2 (pba1, pba2);

		/** Free local memory */
		mqi_mat_clear(tmpmat);
		mqi_coef_list_clear(square, cofactor, NULL);

	}


	/** Swap if needed */
	if ( sq_Dba && !sq_Dab ) {
		
		/** $$ New swapping functionality appeared in libmqi 0.2.0 */
		mqi_mat_swap (pba1, pab1);
		mqi_mat_swap (pba2, pab2);
		mqi_coef_swap(Dab, Dba);
		/* Swap without a third variable (for fun) */
		sq_Dba += sq_Dab;
		sq_Dab = sq_Dba - sq_Dab;
		sq_Dba = sq_Dba - sq_Dab;

	}

	if ( sq_Dab && sq_Dba ) {

		/** The four lines are rational */
		mqi_curve_param_t line1, line2, line3, line4;
		mqi_curve_param_init_dynamic (line1, mqi_mat_nrows(pab1), mqi_mat_typeof(pab1));
		mqi_curve_param_init_dynamic (line2, mqi_mat_nrows(pab1), mqi_mat_typeof(pab1));
		mqi_curve_param_init_dynamic (line3, mqi_mat_nrows(pab2), mqi_mat_typeof(pab2));
		mqi_curve_param_init_dynamic (line4, mqi_mat_nrows(pab2), mqi_mat_typeof(pab2));

		mqi_curve_param_line_through_two_points (line1,
						(mqi_vect_ptr)COL(pab1,0), (mqi_vect_ptr)COL(pba1,0));
		mqi_curve_param_line_through_two_points (line2,
						(mqi_vect_ptr)COL(pab1,0), (mqi_vect_ptr)COL(pba2,0));
		mqi_curve_param_line_through_two_points (line3,
						(mqi_vect_ptr)COL(pab2,0), (mqi_vect_ptr)COL(pba1,0));
		mqi_curve_param_line_through_two_points (line4,
						(mqi_vect_ptr)COL(pab2,0), (mqi_vect_ptr)COL(pba2,0));

		/** Cut parameters of the lines */
		mqi_vect_t cut1, cut2, cut3, cut4;
		qi_param_line_improved1 (cut1, line1);
		qi_param_line_improved1 (cut2, line2);
		qi_param_line_improved1 (cut3, line3);
		qi_param_line_improved1 (cut4, line4);
		
		qi_vect_optimize_by_half (cut1);
		qi_vect_optimize_by_half (cut2);
		qi_vect_optimize_by_half (cut3);
		qi_vect_optimize_by_half (cut4);

		qi_inter_component_init (comp, QI_INTER_COMPONENT_LINE, line1);
		qi_inter_component_set_type (comp, QI_INTER_COMPONENT_LINE);
		qi_inter_cut_init (cut, QI_INTER_CUT_0, VECT(cut1,0), VECT(cut1,1));
		qi_inter_component_add_cut_parameter (comp, cut);
		qi_inter_cut_set_owner(cut, comp);
		qi_inter_cut_init (cut, QI_INTER_CUT_0, VECT(cut1,2), VECT(cut1,3));
		qi_inter_component_add_cut_parameter (comp, cut);
		qi_inter_cut_set_owner(cut, comp);
		
		qi_inter_add_component (rop, comp);
		/* discard comp */
		qi_inter_component_clear(comp);

		qi_inter_component_init (comp, QI_INTER_COMPONENT_LINE, line2);
		qi_inter_component_set_type (comp, QI_INTER_COMPONENT_LINE);
		qi_inter_cut_init (cut, QI_INTER_CUT_0, VECT(cut2,0), VECT(cut2,1));
		qi_inter_component_add_cut_parameter (comp, cut);
		qi_inter_cut_set_owner(cut, comp);
		qi_inter_cut_init (cut, QI_INTER_CUT_0, VECT(cut2,2), VECT(cut2,3));
		qi_inter_component_add_cut_parameter (comp, cut);
		qi_inter_cut_set_owner(cut, comp);
		
		qi_inter_add_component (rop, comp);
		/* discard comp */
		qi_inter_component_clear(comp);

		qi_inter_component_init (comp, QI_INTER_COMPONENT_LINE, line3);
		qi_inter_component_set_type (comp, QI_INTER_COMPONENT_LINE);
		qi_inter_cut_init (cut, QI_INTER_CUT_0, VECT(cut3,0), VECT(cut3,1));
		qi_inter_component_add_cut_parameter (comp, cut);
		qi_inter_cut_set_owner(cut, comp);
		qi_inter_cut_init (cut, QI_INTER_CUT_0, VECT(cut3,2), VECT(cut3,3));
		qi_inter_component_add_cut_parameter (comp, cut);
		qi_inter_cut_set_owner(cut, comp);
		
		qi_inter_add_component (rop, comp);
		/* discard comp */
		qi_inter_component_clear(comp);

		qi_inter_component_init (comp, QI_INTER_COMPONENT_LINE, line4);
		qi_inter_component_set_type (comp, QI_INTER_COMPONENT_LINE);
		qi_inter_cut_init (cut, QI_INTER_CUT_0, VECT(cut4,0), VECT(cut4,1));
		qi_inter_component_add_cut_parameter (comp, cut);
		qi_inter_cut_set_owner(cut, comp);
		qi_inter_cut_init (cut, QI_INTER_CUT_0, VECT(cut4,2), VECT(cut4,3));
		qi_inter_component_add_cut_parameter (comp, cut);
		qi_inter_cut_set_owner(cut, comp);
		
		qi_inter_add_component (rop, comp);
		/* discard comp */
		qi_inter_component_clear(comp);

		/** Free local memory */
		mqi_curve_param_list_clear(line1, line2, line3, line4, NULL);
		mqi_vect_list_clear(cut1, cut2, cut3, cut4, NULL);

	}else if ( sq_Dab ) {

		/** Two points are rational, the two others are not */
		mqi_vect_t zero;
		mqi_vect_init_dynamic (zero, 4, mqi_coef_typeof(Dab));

		mqi_curve_param_t line1_rat, line2_rat, line_sq1, line_sq2;
		mqi_curve_param_init_dynamic (line1_rat, mqi_mat_nrows(pab1), mqi_mat_typeof(pab1));
		mqi_curve_param_init_dynamic (line2_rat, mqi_mat_nrows(pab1), mqi_mat_typeof(pab1));
		mqi_curve_param_init_dynamic (line_sq1,  mqi_mat_nrows(pba2), mqi_mat_typeof(pba2));
		
		mqi_curve_param_line_through_two_points (line1_rat,
						(mqi_vect_ptr)COL(pab1,0), (mqi_vect_ptr)COL(pba1,0));
		mqi_curve_param_line_through_two_points (line2_rat,
						(mqi_vect_ptr)COL(pab2,0), (mqi_vect_ptr)COL(pba1,0));
		mqi_curve_param_line_through_two_points (line_sq1, zero, (mqi_vect_ptr)COL(pba2,0));
		mqi_curve_param_init_cpy (line_sq2, line_sq1);

		mqi_vect_t cut1, cut2;
		qi_param_line_improved2 (cut1, line1_rat, line_sq1, Dba);
		qi_param_line_improved2 (cut2, line2_rat, line_sq2, Dba);
	
		qi_vect_optimize_by_half (cut1);
		qi_vect_optimize_by_half (cut2);

		qi_inter_create_components (rop, 2, QI_INTER_COMPONENT_LINE, QI_INTER_COMPONENT_3,
					    line1_rat, line_sq1, Dba);

		qi_inter_cut_init (cut, QI_INTER_CUT_1, VECT(cut1,0), VECT(cut1,1), Dba, VECT(cut1,2));
		qi_inter_component_add_cut_parameter (rop->components[0], cut);
		qi_inter_cut_set_owner(cut, rop->components[0]);

		qi_inter_cut_init (cut, QI_INTER_CUT_1, VECT(cut1,3), VECT(cut1,4), Dba, VECT(cut1,5));
		qi_inter_component_add_cut_parameter (rop->components[0], cut);
		qi_inter_cut_set_owner(cut, rop->components[0]);

		mqi_coef_neg (VECT(cut1,1), VECT(cut1,1));
		mqi_coef_neg (VECT(cut1,4), VECT(cut1,4));

		qi_inter_cut_init (cut, QI_INTER_CUT_1, VECT(cut1,0), VECT(cut1,1), Dba, VECT(cut1,2));
		qi_inter_component_add_cut_parameter (rop->components[1], cut);

		qi_inter_cut_init (cut, QI_INTER_CUT_1, VECT(cut1,3), VECT(cut1,4), Dba, VECT(cut1,5));
		qi_inter_component_add_cut_parameter (rop->components[1], cut);
		qi_inter_cut_set_owner(cut, rop->components[1]);


		qi_inter_create_components (rop, 2, QI_INTER_COMPONENT_LINE, QI_INTER_COMPONENT_3,
					    line2_rat, line_sq2, Dba);

		qi_inter_cut_init (cut, QI_INTER_CUT_1, VECT(cut2,0), VECT(cut2,1), Dba, VECT(cut2,2));
		qi_inter_component_add_cut_parameter (rop->components[2], cut);
		qi_inter_cut_set_owner(cut, rop->components[2]);

		qi_inter_cut_init (cut, QI_INTER_CUT_1, VECT(cut2,3), VECT(cut2,4), Dba, VECT(cut2,5));
		qi_inter_component_add_cut_parameter (rop->components[2], cut);
		qi_inter_cut_set_owner(cut, rop->components[2]);

		mqi_coef_neg (VECT(cut2,1), VECT(cut2,1));
		mqi_coef_neg (VECT(cut2,4), VECT(cut2,4));

		qi_inter_cut_init (cut, QI_INTER_CUT_1, VECT(cut2,0), VECT(cut2,1), Dba, VECT(cut2,2));
		qi_inter_component_add_cut_parameter (rop->components[3], cut);

		qi_inter_cut_init (cut, QI_INTER_CUT_1, VECT(cut2,3), VECT(cut2,4), Dba, VECT(cut2,5));
		qi_inter_component_add_cut_parameter (rop->components[3], cut);
		qi_inter_cut_set_owner(cut, rop->components[3]);


		/** Free local memory */
		mqi_vect_list_clear(zero, cut1, cut2, NULL);
		mqi_curve_param_list_clear(line1_rat, line2_rat, line_sq1, line_sq2, NULL);


	}else{

		/* None of the points are rational, but the degree might still be 2 if 
      		 * sqrt(Dab) and sqrt(Dba) are in the same extension */
		mqi_coef_t g;
		mqi_coef_init_cpy (g, Dab);
		mqi_coef_gcd	  (g, g, Dba);
		
		mqi_coef_t coef, coef2;
		mqi_coef_init_cpy (coef, Dab);
		mqi_coef_divexact (coef, coef, g);

		mqi_coef_init_cpy (coef2, Dba);
		mqi_coef_divexact (coef2, coef2, g);

		mqi_coef_t sq_ab, sq_ba;

		mqi_coef_list_init_dynamic(mqi_coef_typeof(Dab), sq_ab, sq_ba, NULL);

		if ( mqi_coef_is_square (coef) && mqi_coef_is_square (coef2) ) {

			/** Degree 2 */
			mqi_coef_sqrt (sq_ab, coef);
			mqi_coef_sqrt (sq_ba, coef2);
		
			if ( mqi_coef_cmp(Dab, Dba) != 0 ) {

				mqi_mat_mul_coef (pba1, pba1, sq_ab);
				mqi_mat_mul_coef (pba2, pba2, sq_ba);

				qi_mat_optimize_2 (pba1, pba2);

			}

			mqi_curve_param_t line_rat1, line_rat2, line1_sq, line2_sq;
			mqi_curve_param_init_dynamic (line_rat1, mqi_mat_nrows(pab1), mqi_mat_typeof(pab1));
			mqi_curve_param_init_cpy     (line_rat2, line_rat1);
			mqi_curve_param_init_dynamic (line1_sq, mqi_mat_nrows(pab2), mqi_mat_typeof(pab2));
			mqi_curve_param_init_dynamic (line2_sq, mqi_mat_nrows(pab2), mqi_mat_typeof(pab2));
		
			mqi_curve_param_line_through_two_points (line_rat1,
						(mqi_vect_ptr)COL(pab1,0), (mqi_vect_ptr)COL(pba1,0));
			mqi_curve_param_line_through_two_points (line1_sq,
						(mqi_vect_ptr)COL(pab2,0), (mqi_vect_ptr)COL(pba2,0));
			mqi_vect_t vect;

			mqi_vect_init_cpy(vect, (mqi_vect_ptr)COL(pba2,0));
			mqi_vect_neg(vect,vect);
			mqi_curve_param_line_through_two_points (line2_sq,
						(mqi_vect_ptr)COL(pab2,0), vect);

			mqi_vect_t cut1, cut2;
			qi_param_line_improved2 (cut1, line_rat1, line1_sq, Dab);
			qi_param_line_improved2 (cut2, line_rat2, line2_sq, Dab);

			qi_vect_optimize_by_half (cut1);
			qi_vect_optimize_by_half (cut2);


			qi_inter_create_components (rop, 2, QI_INTER_COMPONENT_LINE, QI_INTER_COMPONENT_3,
					line_rat1, line1_sq, Dab);

			qi_inter_cut_init (cut, QI_INTER_CUT_1, QI_INTER_CUT_1, VECT(cut1,0), VECT(cut1,1), Dba, VECT(cut1,2));
			qi_inter_component_add_cut_parameter (rop->components[0], cut);
			qi_inter_cut_set_owner(cut, rop->components[0]);

			qi_inter_cut_init (cut, QI_INTER_CUT_1, VECT(cut1,3), VECT(cut1,4), Dba, VECT(cut1,5));
			qi_inter_component_add_cut_parameter (rop->components[0], cut);
			qi_inter_cut_set_owner(cut, rop->components[0]);

			mqi_coef_neg (VECT(cut1,1), VECT(cut1,1));
			mqi_coef_neg (VECT(cut1,4), VECT(cut1,4));

			qi_inter_cut_init (cut, QI_INTER_CUT_1, VECT(cut1,0), VECT(cut1,1), Dba, VECT(cut1,2));
			qi_inter_component_add_cut_parameter (rop->components[1], cut);

			qi_inter_cut_init (cut, QI_INTER_CUT_1, VECT(cut1,3), VECT(cut1,4), Dba, VECT(cut1,5));
			qi_inter_component_add_cut_parameter (rop->components[1], cut);
			qi_inter_cut_set_owner(cut, rop->components[1]);


			qi_inter_create_components (rop, 2, QI_INTER_COMPONENT_LINE, QI_INTER_COMPONENT_3,
					line_rat2, line2_sq, Dab);

			qi_inter_cut_init (cut, QI_INTER_CUT_1, VECT(cut2,0), VECT(cut2,1), Dab, VECT(cut2,2));
			qi_inter_component_add_cut_parameter (rop->components[2], cut);
			qi_inter_cut_set_owner(cut, rop->components[2]);

			qi_inter_cut_init (cut, QI_INTER_CUT_1, VECT(cut2,3), VECT(cut2,4), Dab, VECT(cut2,5));
			qi_inter_component_add_cut_parameter (rop->components[2], cut);
			qi_inter_cut_set_owner(cut, rop->components[2]);

			mqi_coef_neg (VECT(cut2,1), VECT(cut2,1));
			mqi_coef_neg (VECT(cut2,4), VECT(cut2,4));

			qi_inter_cut_init (cut, QI_INTER_CUT_1, VECT(cut2,0), VECT(cut2,1), Dba, VECT(cut2,2));
			qi_inter_component_add_cut_parameter (rop->components[3], cut);

			qi_inter_cut_init (cut, QI_INTER_CUT_1, VECT(cut2,3), VECT(cut2,4), Dba, VECT(cut2,5));
			qi_inter_component_add_cut_parameter (rop->components[3], cut);
			qi_inter_cut_set_owner(cut, rop->components[3]);


			/** Free local memory */
			mqi_vect_list_clear(vect, cut1, cut2, NULL);
			mqi_curve_param_list_clear(line_rat1, line_rat2, line1_sq, line2_sq, NULL);


		}else{
			
			/** Swap for presentation */
			if ( mqi_coef_cmp(Dab, Dba) > 0 ) {
				mqi_mat_swap (pba1, pab1);
				mqi_mat_swap (pba2, pab2);
				mqi_coef_swap(Dab, Dba);
			}
			
			mqi_vect_t zero;
			mqi_vect_init_dynamic (zero, 4, mqi_mat_typeof(pba1));
			
			mqi_curve_param_t line_rat, line1_sq, line2_sq, zer;
			mqi_curve_param_init_dynamic (line_rat, mqi_mat_nrows(pab1), mqi_mat_typeof(pab1));
			mqi_curve_param_init_dynamic (line1_sq, mqi_mat_nrows(pab2), mqi_mat_typeof(pab2));
			mqi_curve_param_init_dynamic (line2_sq, mqi_mat_nrows(pba2), mqi_mat_typeof(pba2));
			mqi_curve_param_init_dynamic (zer, mqi_vect_size(zero), mqi_vect_typeof(zero));

			mqi_curve_param_line_through_two_points (line_rat,
					(mqi_vect_ptr)COL(pab1,0), (mqi_vect_ptr)COL(pba1,0));
			mqi_curve_param_line_through_two_points (line1_sq,
					(mqi_vect_ptr)COL(pab2,0), zero);
			mqi_curve_param_line_through_two_points (line2_sq,
					zero, (mqi_vect_ptr)COL(pba2,0));
			mqi_curve_param_set_vect(zer, zero);

			mqi_coef_t coef;
			mqi_coef_init_dynamic (coef, mqi_coef_typeof(Dab));
			mqi_coef_set_zero     (coef);
			qi_param_line_improved3 (line_rat, line1_sq, line2_sq, zer, Dab, Dba, coef);

			/** $$ Warning: zer might have changed */
			qi_inter_create_components (rop, 4, QI_INTER_COMPONENT_LINE, QI_INTER_COMPONENT_6,
						    line_rat, line1_sq, Dab, line2_sq, zer, Dba, coef);

			/** $$ Temporary */
			qi_inter_cut_init (cut, QI_INTER_CUT_0, coef, coef);

			qi_inter_component_add_cut_parameter (rop->components[0], cut);
			qi_inter_component_add_cut_parameter (rop->components[0], cut);
			qi_inter_component_add_cut_parameter (rop->components[1], cut);
			qi_inter_component_add_cut_parameter (rop->components[1], cut);
			qi_inter_component_add_cut_parameter (rop->components[2], cut);
			qi_inter_component_add_cut_parameter (rop->components[2], cut);
			qi_inter_component_add_cut_parameter (rop->components[3], cut);
			qi_inter_component_add_cut_parameter (rop->components[3], cut);

			qi_inter_cut_set_owner(cut, rop->components[0]);

			/** Free local memory */
			mqi_coef_clear(coef);
			mqi_curve_param_list_clear(line_rat, line1_sq, line2_sq, zer, NULL);
			mqi_vect_clear(zero);

		}

		/** Free local memory */
		mqi_coef_list_clear (sq_ab, sq_ba, coef, coef2, g);

	}


	/** Free local memory */
	mqi_mat_list_clear(singa, singb, mab, mba, pab1, pab2, pba1, pba2, NULL);
	mqi_coef_list_clear(Dab, Dba, sqrt_Dab, sqrt_Dba, NULL);

	qi_inter_set_optimal (rop);
	

}

/** Four skew lines when the real part is 2 points */
void __qi_inter_two_mult_four_skew_lines_2points_rational (qi_inter_t rop, mqi_mat_t __IN q1, mqi_mat_t __IN q2,
							   mqi_mat_t __IN qa_cp, mqi_mat_t __IN qb_cp,
							   int in_qa)
{

	qi_inter_component_t	comp;
	

	/** Two components here */
	qi_inter_init(rop);
	qi_inter_set_type (rop, 14,3);

	/** qa_cp and qb_cp are const: make copies */
	mqi_mat_t qa, qb;
	mqi_mat_init_cpy (qa, qa_cp);
	mqi_mat_init_cpy (qb, qb_cp);

	/** If qb is the imaginary pair of planes, swap */
	if ( in_qa == 1 )
		mqi_mat_swap (qa, qb);

	/** Now qa is the imaginary cone */
	mqi_mat_t singa;
	qi_mat_singular (singa, qa);

	/** Find the two points by intersecting with qb */
	mqi_mat_t mab;
	mqi_mat_transformation (mab, qb, singa, MQI_MAT_NOTRANSCO);

	/** The discriminant */
	mqi_coef_t Dab;
	mqi_coef_init_dynamic (Dab, mqi_mat_typeof(mab));
	mqi_coef_linexpr2     (Dab, 2,		1,MAT(mab,0,1),MAT(mab,0,1),		-1,MAT(mab,0,0),MAT(mab,1,1));

	/** The (1D) points: the solutions are pab1 and pab2 if the discrim is a square,
	  * pab1 +/- sqrt(Dab)*pab2 otherwise */
	mqi_mat_t pab1, pab2;
	mqi_mat_list_init_dynamic (2, 1, mqi_mat_typeof(mab), pab1, pab2, NULL);

	mqi_coef_t sqrt_Dab;
	mqi_coef_init_dynamic(sqrt_Dab, mqi_coef_typeof(Dab));

	/** The discriminants */
	if ( mqi_coef_is_square (Dab) ) {
		
		mqi_coef_sqrt (sqrt_Dab, Dab);
		
		if ( mqi_coef_sign(MAT(mab,0,0)) ) {
			mqi_coef_sub (MAT(pab1,0,0), sqrt_Dab, MAT(mab,0,1));
			mqi_mat_set_at (pab1,1,0, MAT(mab,0,0));
			mqi_coef_add (MAT(pab2,0,0), MAT(mab,0,1), sqrt_Dab);
			mqi_coef_neg (MAT(pab2,0,0), MAT(pab2,0,0));
			mqi_mat_set_at (pab2,1,0, MAT(mab,0,0));
		}else {
			mqi_mat_set_at (pab1,0,0, MAT(mab,1,1));
			mqi_mat_set_at (pab2,0,0, MAT(mab,1,1));
			mqi_coef_mul_schar (MAT(pab2,1,0), MAT(mab,0,1), -2);
		}

		mqi_mat_t tmpmat;

		mqi_mat_init_cpy(tmpmat, pab1);
		mqi_mat_clear(pab1);
		mqi_mat_mul_init (pab1, singa, tmpmat);
		mqi_mat_clear(tmpmat);
		mqi_mat_init_cpy(tmpmat, pab2);
		mqi_mat_clear(pab2);
		mqi_mat_mul_init (pab2, singa, tmpmat);

		qi_mat_optimize (pab1);
		qi_mat_optimize (pab2);

		/** The two points */
		mqi_curve_param_t point1, point2;
		mqi_curve_param_init_dynamic (point1, mqi_mat_nrows(pab1), mqi_mat_typeof(pab1));
		mqi_curve_param_init_dynamic (point2, mqi_mat_nrows(pab2), mqi_mat_typeof(pab2)); 
	
		mqi_curve_param_set_vect (point1, (mqi_vect_ptr)COL(pab1,0));
		mqi_curve_param_set_vect (point2, (mqi_vect_ptr)COL(pab2,0));

		qi_inter_component_init (comp, QI_INTER_COMPONENT_2, point1);
		qi_inter_component_set_type (comp, QI_INTER_COMPONENT_POINT);
		qi_inter_add_component (rop, comp);
		/* discard comp */
		qi_inter_component_clear(comp);

		qi_inter_component_init (comp, QI_INTER_COMPONENT_2, point2);
		qi_inter_component_set_type (comp, QI_INTER_COMPONENT_POINT);
		qi_inter_add_component (rop, comp);
		/* discard comp */

		/** Free local memory */
		mqi_mat_clear(tmpmat);
		mqi_curve_param_list_clear(point1, point2, NULL);


	}else{

		mqi_coef_set_zero (sqrt_Dab);
		mqi_coef_neg (MAT(pab1,0,0), MAT(mab,0,1));
		mqi_mat_set_at (pab1,1,0, MAT(mab,0,0));
		mqi_mat_set_at_int (pab1,0,0, 1);

		mqi_mat_t tmpmat;

		mqi_mat_init_cpy(tmpmat, pab1);
		mqi_mat_clear(pab1);
		mqi_mat_mul_init (pab1, singa, tmpmat);
		mqi_mat_clear(tmpmat);
		mqi_mat_init_cpy(tmpmat, pab2);
		mqi_mat_clear(pab2);
		mqi_mat_mul_init (pab2, singa, tmpmat);


		/** Replace Dab by e^2*Dab' - and optimize */
		mqi_coef_t square, cofactor;
		qi_coef_extract_square_factors (square, cofactor, Dab);

		/** Dab --> Dab' */
		mqi_coef_cpy (Dab, cofactor);

		/** Multiply pab2 by e = square */
		mqi_mat_mul_coef (pab2, pab2, square);

		qi_mat_optimize_2 (pab1, pab2);

		mqi_curve_param_t point_rat, point_sq;
		mqi_curve_param_init_dynamic (point_rat, mqi_mat_nrows(pab1), mqi_mat_typeof(pab1));
		mqi_curve_param_init_dynamic (point_sq, mqi_mat_nrows(pab2), mqi_mat_typeof(pab2));
	
		mqi_curve_param_set_vect (point_rat, (mqi_vect_ptr)COL(pab1,0));
		mqi_curve_param_set_vect (point_sq, (mqi_vect_ptr)COL(pab2,0));

		qi_inter_create_components (rop, 2, QI_INTER_COMPONENT_POINT, QI_INTER_COMPONENT_3,
					    point_rat, point_sq, Dab);

		/** Free local memory */
		mqi_mat_clear(tmpmat);
		mqi_coef_list_clear(square, cofactor, NULL);
		mqi_curve_param_list_clear(point_rat, point_sq, NULL);

	}

	qi_inter_set_optimal (rop);

	/** Free local memory */
	mqi_mat_list_clear(qa, qb, singa, mab, pab1, pab2, NULL);
	mqi_coef_list_clear(Dab, sqrt_Dab, NULL);

}

/** Four skew lines when the real part is four lines or two points, and the roots are non-rational */
/** $$ One of the most complex function of QI: 103 local variables ! */
void __qi_inter_two_mult_four_skew_lines_non_rational (qi_inter_t rop, mqi_mat_t __IN q1, mqi_mat_t __IN q2, mqi_mat_t __IN sing, mqi_coef_t __IN d,
						       int s_e)
{

	qi_inter_component_t	comp;
	qi_inter_cut_t		cut;


	/* sing has four columns: among them, at least two columns are independent
	* (though the others might be linear combinations of the two points).
	* Also, if the k1i + sqrt(d)*k2i span the first line, then the k1i -
	* sqrt(d)*k2i span the second! */
	mqi_mat_t k11, k12, k21, k22, tmp1, tmp2;

	mqi_mat_list_init_dynamic(4,1, mqi_mat_typeof(sing), k11,k12,k21,k22, NULL);
	mqi_mat_list_init_dynamic(8,2, mqi_mat_typeof(sing), tmp1,tmp2, NULL);

	mqi_mat_get_submat (tmp1, sing, 0,0, mqi_mat_nrows(tmp1), 2);
	mqi_mat_get_submat (tmp2, sing, 0,2, mqi_mat_nrows(tmp2), 2);

	mqi_mat_get_submat (k11, tmp1, 0, 0, mqi_mat_nrows(k11), mqi_mat_ncols(k11));
	mqi_mat_get_submat (k12, tmp1, 0, mqi_mat_ncols(k11), mqi_mat_nrows(k12), mqi_mat_ncols(k12));
	mqi_mat_get_submat (k21, tmp1, mqi_mat_nrows(k11), 0, mqi_mat_nrows(k21), mqi_mat_ncols(k21));
	mqi_mat_get_submat (k22, tmp1, mqi_mat_nrows(k11), mqi_mat_ncols(k11), mqi_mat_nrows(k21), mqi_mat_ncols(k21));

	/** Pick another column if the first two are in the same span */
	if ( qi_are_equal(k11, k21, k12, k22, d) )
	{
		mqi_mat_get_submat (k12, tmp2, 0, 0, mqi_mat_nrows(k12), mqi_mat_ncols(k12));
		mqi_mat_get_submat (k22, tmp2, mqi_mat_nrows(k12), 0, mqi_mat_nrows(k22), mqi_mat_ncols(k22));
	}

	/** The result of plugging u*(k11+eps1*sqrt(d)*k21)+v*(k12+eps1*sqrt(d)*k22) in
  	  * q1 is of the form (u  v)*(p_rat + eps1*sqrt(d)*p_sq)*(u  v)^T */
	mqi_mat_t p_rat, p_sq;
	mqi_mat_init_dynamic (p_rat, 2,2, mqi_mat_typeof(k11));
	mqi_mat_init_dynamic (p_sq,  2,2, mqi_mat_typeof(k11));


	mqi_mat_t tmpmat, tmpmat2;

	mqi_mat_transformation (tmpmat,  q1, k11, MQI_MAT_NOTRANSCO);
	mqi_mat_transformation (tmpmat2, q1, k21, MQI_MAT_NOTRANSCO);
	mqi_mat_mul_coef       (tmpmat2, tmpmat2, d);
	mqi_mat_add	       (tmpmat, tmpmat, tmpmat2);
	mqi_mat_set_at	       (p_rat,0,0,  MAT(tmpmat,0,0));

	mqi_mat_list_clear(tmpmat, tmpmat2, NULL);

	qi_mat_transformation_int  (tmpmat,  1,k11, q1, k12);
	qi_mat_transformation_int  (tmpmat2, 1,k21, q1, k22);
	mqi_mat_mul_coef       (tmpmat2, tmpmat2, d);
	mqi_mat_add	       (tmpmat, tmpmat, tmpmat2);
	mqi_mat_set_at	       (p_rat,0,1,  MAT(tmpmat,0,0));

	mqi_mat_list_clear(tmpmat, tmpmat2, NULL);

	
	mqi_mat_set_at (p_rat,1,0, MAT(p_rat,0,1));

	
	mqi_mat_transformation (tmpmat,  q1, k12, MQI_MAT_NOTRANSCO);
	mqi_mat_transformation (tmpmat2, q1, k22, MQI_MAT_NOTRANSCO);
	mqi_mat_mul_coef       (tmpmat2, tmpmat2, d);
	mqi_mat_add	       (tmpmat, tmpmat, tmpmat2);
	mqi_mat_set_at	       (p_rat,1,1,  MAT(tmpmat,0,0));

	mqi_mat_list_clear(tmpmat, tmpmat2, NULL);

	qi_mat_transformation_int (tmpmat, 2, k21, q1, k11);
	mqi_mat_set_at	       (p_sq,0,0,  MAT(tmpmat,0,0));

	mqi_mat_clear(tmpmat);

	qi_mat_transformation_int (tmpmat,  1,k21, q1, k12);
	qi_mat_transformation_int (tmpmat2, 1,k11, q1, k22);
	mqi_mat_add (tmpmat, tmpmat, tmpmat2);
	mqi_mat_set_at (p_sq,0,1, MAT(tmpmat,0,0));
	
	/* Do not clear tmpmat2 so that we know what to clear at
	 * the end of the function. */
	mqi_mat_clear(tmpmat);


	mqi_coef_cpy (MAT(p_sq,1,0), MAT(p_sq,0,1));

	qi_mat_transformation_int (tmpmat, 2, k12, q1, k22);
	mqi_mat_set_at	       (p_sq,1,1,  MAT(tmpmat,0,0));


	
	/** The reduced discriminants of the quadratic equation are e^2*(D1 +/- sqrt(d)*D2) */
	mqi_coef_t e, D1, D2;

	mqi_coef_list_init_dynamic (mqi_coef_typeof(d), e, D1, D2, NULL);

	mqi_coef_set_schar (e, 1);	
	mqi_coef_linexpr3 (D1, 4, 	1,MAT(p_rat,0,1),MAT(p_rat,0,1),NULL,	1,d,MAT(p_sq,0,1),MAT(p_sq,0,1),
					-1,NULL,MAT(p_rat,0,0),MAT(p_rat,1,1),	-1,d,MAT(p_sq,0,0),MAT(p_sq,1,1));
	mqi_coef_linexpr2 (D2, 3,	2,MAT(p_rat,0,1),MAT(p_sq,0,1),	  -1,MAT(p_sq,0,0),MAT(p_rat,1,1),   -1,MAT(p_sq,1,1),MAT(p_rat,0,0));

	mqi_coef_t square, square2, cofactor, cofactor2;
	qi_coef_extract_square_factors (square, cofactor, D1);
	qi_coef_extract_square_factors (square2, cofactor2, D2);

	mqi_coef_gcd (e, square, square2);

	mqi_coef_t e2;
	mqi_coef_init_cpy (e2, e);
	mqi_coef_mul	  (e2, e2, e2);

	mqi_coef_divexact (D1, D1, e2);
	mqi_coef_divexact (D2, D2, e2);

	/* The points in 3D are:
	 *         po_rat + eps1*sqrt(d)*po_sq + eps2*(k11+eps1*sqrt(d)*k21)*e*sqrt(Delta), 
  	 * where Delta = D1 + eps1*sqrt(d)*D2 */
	
	mqi_mat_t po_rat, po_sq;
	mqi_coef_t coef1, coef2, coef3, coef4;

	mqi_coef_list_init_dynamic(mqi_coef_typeof(D1), coef1,coef2,coef3,coef4, NULL);
	mqi_mat_list_init_dynamic(mqi_mat_nrows(k11), mqi_mat_ncols(k11), mqi_mat_typeof(k11),
					po_rat, po_sq, NULL);

	/* -p_rat(0,1) */
	mqi_mat_get_at (coef1, p_rat,0,1);
	mqi_coef_neg(coef1,coef1);

	/* p_rat(0,0) */
	mqi_mat_get_at (coef2, p_rat,0,0);

	/* -d*p_sq(0,1) */
	mqi_coef_cpy(coef3, MAT(p_sq,0,1));
	mqi_coef_neg(coef3,coef3);
	mqi_coef_mul(coef3, coef3, d);

	/* d*p_sq(0,0) */
	mqi_coef_cpy(coef4, MAT(p_sq,0,0));
	mqi_coef_mul(coef4, coef4, d);

	/* Using linexpr is far easier and safer */
	mqi_mat_linexpr (po_rat,4,	coef1,k11, coef2,k12, coef3,k21, coef4,k22);

	/* -p_sq(0,1) */
	mqi_coef_cpy(coef4,coef2); /* save p_rat(0,0) in coef4 */
	mqi_mat_get_at (coef2, p_sq,0,1);
	mqi_coef_neg(coef2,coef2);

	/* p_sq(0,0) */
	mqi_mat_get_at (coef3, p_sq,0,0);
	
	mqi_mat_linexpr (po_sq,4,	coef1,k21, coef2,k11, coef3,k12, coef4,k22);

	 /* Warning: the case when p_rat(0,0) = p_sq(0,0) = 0 can happen. In that case,
	 * the solution is not (-b+eps2*sqrt(Delta),2*a) but (2*c,-b+eps2*sqrt(Delta)).
	 * We handle it later since it implies that the solution contains rational lines

	 * Can we simplify that a bit? */
	mqi_vect_content (coef1, (mqi_vect_ptr)COL(po_rat,0));
	mqi_vect_content (coef2,(mqi_vect_ptr)COL(po_sq,0));

	mqi_coef_t ct;
	mqi_coef_init_cpy (ct, coef1);
	mqi_coef_gcd	  (ct, ct, coef2);
	mqi_coef_gcd	  (ct, ct, e);
	
	mqi_coef_divexact (e, e, ct);
	mqi_mat_divexact_coef (po_rat, po_rat, ct);
	mqi_mat_divexact_coef (po_sq, po_sq, ct);

	if ( s_e != 1 ) {
		/** Two points */

		/** Two components */
		qi_inter_init(rop);
		qi_inter_set_type (rop, 14,3);

		/* One of the singular quadrics is imaginary. Which one? (hint: the one with D1
      		 * +/- d*D2 < 0) */
		mqi_coef_t eps1;
		mqi_coef_init_dynamic (eps1, mqi_coef_typeof(d));

		if ( qi_coef_sign(D1, D2, d) == 1 ) /** D1+sqrt(d)*D2 > 0 */
			mqi_coef_set_schar (eps1, 1);
		else
			mqi_coef_set_schar (eps1, -1);

		/* The points correspond to the value of eps1 such that Delta > 0 */
		mqi_curve_param_t c1, c2, c3, c4;
		mqi_vect_t tmpvect;

		mqi_curve_param_init_dynamic (c1, mqi_mat_nrows(po_rat), mqi_mat_typeof(po_rat));
		mqi_curve_param_init_dynamic (c2, mqi_mat_nrows(po_sq),  mqi_mat_typeof(po_sq));
		mqi_curve_param_init_dynamic (c3, mqi_mat_nrows(k11),    mqi_mat_typeof(k11));
		mqi_curve_param_init_dynamic (c4, mqi_mat_nrows(k21), 	 mqi_mat_typeof(k21));

		mqi_curve_param_set_vect (c1, (mqi_vect_ptr)COL(po_rat,0));

		mqi_vect_init_cpy(tmpvect, (mqi_vect_ptr)COL(po_sq,0));
		mqi_vect_mul_coef(tmpvect, tmpvect, eps1);
		mqi_curve_param_set_vect (c2, tmpvect);

		mqi_vect_clear(tmpvect);

		mqi_vect_init_cpy(tmpvect, (mqi_vect_ptr)COL(k11,0));
		mqi_vect_mul_coef(tmpvect, tmpvect, e);
		mqi_curve_param_set_vect (c3, tmpvect);

		mqi_vect_clear(tmpvect);

		mqi_vect_init_cpy(tmpvect, (mqi_vect_ptr)COL(k21,0));
		mqi_vect_mul_coef(tmpvect, tmpvect, e);
		mqi_vect_mul_coef(tmpvect, tmpvect, eps1);
		mqi_curve_param_set_vect(c4, tmpvect);
	
		mqi_coef_mul(coef1, eps1, D2);

		qi_inter_create_components (rop, 2, QI_INTER_COMPONENT_POINT, QI_INTER_COMPONENT_6,
					   c1, c2, d, c3, c4, D1, coef1);

		/** Free local memory */
		mqi_coef_clear(eps1);
		mqi_vect_clear(tmpvect);
		mqi_curve_param_list_clear(c1, c2, c3, c4, NULL);


	}else{

		/** Four components here */
		qi_inter_init(rop);
		qi_inter_set_type (rop, 14,2);

		/* The lines link the two points with eps1 = 1 to the two points with eps1 = -1

		* Notations between here and paper:
		*                    d  <-->  Delta
		*   del2 = D1^2-d*D2^2  <-->  c

		* There are two cases according to the value of d*(D1^2-d*D2^2) */
		mqi_coef_t del2, sq, coef;
		mqi_coef_init_cpy (del2, D1);
		mqi_coef_mul (del2, del2, del2);

		mqi_coef_init_cpy (coef, D2);
		mqi_coef_mul (coef, coef, coef);
		mqi_coef_mul (coef, coef, d);

		mqi_coef_sub (del2, del2, coef);

		mqi_coef_mul (coef, d, del2);

		if ( mqi_coef_is_square(coef) ) {
			
			mqi_coef_sqrt(sq, coef);

			/** The four lines are defined on the same extension */
			mqi_curve_param_t c1p, c1m, c2p, c2m, c3p, c3m, c4p, c4m;

			mqi_curve_param_init_dynamic(c1p, mqi_mat_nrows(po_rat), mqi_mat_typeof(po_rat));
			mqi_curve_param_init_dynamic(c2p, mqi_mat_nrows(po_sq),  mqi_mat_typeof(po_sq));
			mqi_curve_param_list_init_dynamic(mqi_mat_nrows(k11), mqi_mat_typeof(k11),
							  c3p, c3m, c4p, c4m, NULL);

			mqi_vect_t tmpvect;

			mqi_vect_init_cpy(tmpvect, (mqi_vect_ptr)COL(k21,0));
			mqi_vect_mul_coef(tmpvect, tmpvect, sq);
			mqi_vect_mul_coef(tmpvect, tmpvect, e);
			mqi_vect_neg(tmpvect, tmpvect);

			mqi_curve_param_line_through_two_points (c1p, 
					(mqi_vect_ptr)COL(po_rat,0), tmpvect);
			mqi_curve_param_init_cpy(c1m, c1p);

			/* discard tmpvect */
			mqi_vect_clear(tmpvect);

			mqi_vect_init_cpy(tmpvect, (mqi_vect_ptr)COL(k11,0));
			mqi_vect_mul_coef(tmpvect, tmpvect, sq);
			mqi_vect_mul_coef(tmpvect, tmpvect, e);
			mqi_vect_divexact_coef(tmpvect, tmpvect, d);

			mqi_curve_param_line_through_two_points (c2p, 
					(mqi_vect_ptr)COL(po_sq,0), tmpvect);
			mqi_curve_param_init_cpy(c2m, c2p);

			/* discard tmpvect */
			mqi_vect_clear(tmpvect);

			mqi_vect_init_cpy(tmpvect, (mqi_vect_ptr)COL(k11,0));
			mqi_vect_mul_coef(tmpvect, tmpvect, e);

			mqi_curve_param_line_through_two_points(c3p, tmpvect, (mqi_vect_ptr)COL(po_rat,0));

			/* discard tmpvect */
			mqi_vect_clear(tmpvect);

			mqi_vect_init_cpy(tmpvect, (mqi_vect_ptr)COL(k21,0));
			mqi_vect_mul_coef(tmpvect, tmpvect, e);

			mqi_vect_t tmpvect2;
			mqi_vect_init_cpy(tmpvect2, (mqi_vect_ptr)COL(po_sq,0));
			mqi_vect_neg(tmpvect2,tmpvect2);
			mqi_curve_param_line_through_two_points(c4p, tmpvect, tmpvect2);

			/* discard tmpvect(2) */
			mqi_vect_list_clear(tmpvect, tmpvect2, NULL);

			mqi_vect_init_cpy(tmpvect, (mqi_vect_ptr)COL(k11,0));
			mqi_vect_mul_coef(tmpvect, tmpvect, e);

			mqi_vect_init_cpy(tmpvect2, (mqi_vect_ptr)COL(po_rat,0));
			mqi_vect_neg(tmpvect2,tmpvect2);

			mqi_curve_param_line_through_two_points(c3m, tmpvect, tmpvect2);

			/* discard tmpvect, let tmpvect2 uncleared */
			mqi_vect_clear(tmpvect);

			mqi_vect_init_cpy(tmpvect, (mqi_vect_ptr)COL(k21,0));
			mqi_vect_mul_coef(tmpvect, tmpvect, e);

			mqi_curve_param_line_through_two_points(c4m, tmpvect, (mqi_vect_ptr)COL(po_sq,0));


			qi_param_line_improved3 (c1p, c2p, c3p, c4p, d, D1, D2);
			qi_param_line_improved3 (c1m, c2m, c3m, c4m, d, D1, D2);


			qi_inter_create_components (rop, 2, QI_INTER_COMPONENT_LINE, QI_INTER_COMPONENT_6,
						    c1p, c2p, d, c3p, c4p, D1, D2);
			qi_inter_create_components (rop, 2, QI_INTER_COMPONENT_LINE, QI_INTER_COMPONENT_6,
						    c1m, c2m, d, c3m, c4m, D1, D2);


		}else {

			/** Two cases according to the value of D1^2-d*D2^2 */

			if ( mqi_coef_is_square (del2) ) {

				mqi_coef_sqrt (sq, del2);

				/* D2 can be equal to zero here. If sqrt(D1) and sqrt(d) are in the
				* same extension, only two lines are non-rational and we proceed in
				* the next loop. Otherwise, the four lines are non-rational and we
				* go in the second loop. */
				mqi_coef_t dsq, cd, cd1, g;

				if ( !mqi_coef_sign(D2) ) {
					mqi_coef_init_cpy (g, d);
					mqi_coef_gcd	  (g, g, D1);
				}
			
				mqi_coef_divexact (coef1, D1, g);
				mqi_coef_divexact (coef2, d, g);
				mqi_coef_linexpr1 (coef3,2,	2,D1, 2,dsq);
				mqi_coef_linexpr1 (coef4,2,	2,D1, -2,dsq);

				if ( (!mqi_coef_sign(D2) && mqi_coef_is_square(coef1) && mqi_coef_is_square(coef2))
					||
				     (mqi_coef_sign(D2) && ( mqi_coef_is_square(coef3) || mqi_coef_is_square(coef4)))
				   )
				{
	
					/** Two lines are rational and the two others have one square root */
					mqi_mat_t r1p, r1m, s1p, s1m;

					/** We handle here the case we have left aside earlier */
					if ( !mqi_coef_sign(MAT(p_rat,0,0)) && !mqi_coef_sign(MAT(p_sq,0,0)) ) {
						mqi_coef_t b1, b2, c1, c2;

						mqi_coef_list_init_dynamic(mqi_mat_typeof(p_rat), b1, b2, c1, c2, NULL);

						mqi_mat_get_at (b1, p_rat,0,1);
						mqi_mat_get_at (b2, p_sq,0,1);
						mqi_mat_get_at (c1, p_rat,1,1);
						mqi_mat_get_at (c2, p_sq,1,1);

						mqi_mat_init_cpy (r1p, k11);
						mqi_mat_init_cpy (s1p, k21);

						/** I */
						mqi_coef_neg(coef1,c1);
						mqi_coef_linexpr2(coef2,1,  -1,d,c2);
						mqi_coef_linexpr1(coef3,1,  2,b1);
						mqi_coef_linexpr2(coef4,1,  2,d,b2);

						mqi_mat_init_dynamic(r1m, 4, 1, mqi_mat_typeof(k11));
						
						mqi_mat_linexpr (r1m,4,	 coef1,k11, coef2,k21, coef3,k12, coef4,k22);

						/** II */
						mqi_coef_neg(coef1,c2);
						mqi_coef_neg(coef2,c1);
						mqi_coef_linexpr1(coef4,1,  2,b2);

						mqi_mat_init_dynamic(s1m, 4, 1, mqi_mat_typeof(k11));
						
						mqi_mat_linexpr (s1m,4,	 coef1,k11, coef2,k21, coef3,k22, coef4,k12);


						/** Free local memory */
						mqi_coef_list_clear (b1, b2, c1, c2, NULL);


					}else {

						if ( mqi_coef_sign(dsq) )
						{

							mqi_coef_t co1, co2, co3;

							mqi_coef_list_init_dynamic(mqi_coef_typeof(dsq),
									co1, co2, co3, NULL);

							mqi_coef_linexpr1(co1,1,	2,dsq);
							mqi_coef_linexpr3(co2,1,	1,e,dsq,dsq);
							mqi_coef_linexpr2(co3,1,	2,e,D2);

							mqi_mat_t m1r, m2r;

							mqi_mat_init_cpy(m1r, po_rat);
							mqi_mat_mul_coef (m1r, m1r, co1);

							mqi_coef_linexpr2(coef1,1,	1,d,co3);

							mqi_mat_linexpr(m2r,2,	co2,k11,  coef1,k21);

							mqi_mat_init_cpy (r1p, m1r);
							mqi_mat_add	 (r1p, r1p, m2r);
							mqi_mat_init_cpy (r1m, m1r);
							mqi_mat_sub	 (r1m, r1m, m2r);

							mqi_mat_t m1s, m2s;
							mqi_mat_init_cpy (m1s, po_sq);
							mqi_mat_mul_coef (m1s, m1s, co1);

							mqi_mat_linexpr(m2s,2,	co2,k21, co3,k11);

							mqi_mat_init_cpy (s1p, m1s);
							mqi_mat_add	 (s1p, s1p, m2s);
							mqi_mat_init_cpy (s1m, m1s);
							mqi_mat_sub	 (s1m, s1m, m2s);
						
							/** Free local memory */
							mqi_coef_list_clear(co1, co2, co3, NULL);
							mqi_mat_list_clear(m1r, m2r, m1s, m2s, NULL);

						}
						else
						{
							
							mqi_coef_t co;
							mqi_coef_init_cpy (co, cd);
							mqi_coef_mul	  (co, co, e);

							mqi_mat_t m1r, m2r, m1s, m2s;
							mqi_mat_init_cpy (m1r, po_rat);
							mqi_mat_mul_coef (m1r, m1r, cd1);

							mqi_coef_linexpr2(coef1,1, 1,d,co);
							mqi_mat_linexpr(m2r,1, coef1,k21);

							mqi_mat_init_cpy (r1p, m1r);
							mqi_mat_add	 (r1p, r1p, m2r);
							mqi_mat_init_cpy (r1m, m1r);
							mqi_mat_sub	 (r1m, r1m, m2r);

							mqi_mat_init_cpy (m1s, po_sq);
							mqi_mat_mul_coef (m1s, m1s, cd1);

							mqi_mat_init_cpy (m2s, k11);
							mqi_mat_mul_coef (m2s, m2s, co);
	
							mqi_mat_init_cpy (s1p, m1s);
							mqi_mat_add	 (s1p, s1p, m2s);
							mqi_mat_init_cpy (s1m, m1s);
							mqi_mat_sub	 (s1m, s1m, m2s);
						
							/** Free local memory */
							mqi_coef_clear(co);
							mqi_mat_list_clear(m1r, m2r, m1s, m2s, NULL);

						}

					}

					/** First the two rational lines */
					mqi_curve_param_t line1, line2;
					mqi_curve_param_init_dynamic (line1, mqi_mat_nrows(r1p), mqi_mat_typeof(r1p));
					mqi_curve_param_init_dynamic (line2, mqi_mat_nrows(r1m), mqi_mat_typeof(r1m));

					mqi_curve_param_line_through_two_points (line1,
							(mqi_vect_ptr)COL(r1p,0), (mqi_vect_ptr)COL(s1p,0));
					mqi_curve_param_line_through_two_points (line2,
							(mqi_vect_ptr)COL(r1m,0), (mqi_vect_ptr)COL(s1m,0));

					qi_curve_param_optimize (line1);
					qi_curve_param_optimize (line2);

					/** Reparametrize the lines */
					
					/** $$ Cut parameters */
					mqi_vect_t cut1, cut2;
					qi_param_line_improved1 (cut1, line1);
					qi_param_line_improved1 (cut2, line2);

					qi_inter_component_init (comp, QI_INTER_COMPONENT_2, line1);
					qi_inter_component_set_type (comp, QI_INTER_COMPONENT_LINE);
					qi_inter_add_component (rop, comp);
					/* discard comp */
					qi_inter_component_clear(comp);

					qi_inter_component_init (comp, QI_INTER_COMPONENT_2, line2);
					qi_inter_component_set_type (comp, QI_INTER_COMPONENT_LINE);
					qi_inter_add_component (rop, comp);
					/* discard comp */
					qi_inter_component_clear(comp);

					/** Now the two non rational lines */
					mqi_curve_param_t c_r, c_s;
					mqi_curve_param_init_dynamic (c_r, mqi_mat_nrows(r1p), mqi_mat_typeof(r1p));
					mqi_curve_param_init_dynamic (c_s, mqi_mat_nrows(s1p), mqi_mat_typeof(s1p));

					mqi_curve_param_line_through_two_points (c_r,
							(mqi_vect_ptr)COL(r1p,0), (mqi_vect_ptr)COL(r1m,0));
					
					mqi_vect_t tmpvect;
					mqi_vect_init_cpy(tmpvect, (mqi_vect_ptr)COL(s1m,0));
					mqi_vect_neg(tmpvect,tmpvect);
					mqi_curve_param_line_through_two_points (c_s,
							(mqi_vect_ptr)COL(s1p,0), tmpvect);

					qi_curve_param_optimize_gcd (c_r, c_s); 

					mqi_vect_t ignored;
					qi_param_line_improved2 (ignored, c_r, c_s, d);

					qi_inter_create_components (rop, 2, QI_INTER_COMPONENT_LINE, QI_INTER_COMPONENT_3, 
									c_r, c_s, d);


					/** Free local memory */
					mqi_curve_param_list_clear(line1, line2, c_r, c_s, NULL);
					mqi_vect_list_clear(cut1, cut2, tmpvect, ignored, NULL);

				}else {
					
					/* D2 might be zero here. In this case, D1 = sq and dm = em = 0
			                * so special care has to be taken

			                * Need one square root per line */
					mqi_coef_t tmp1, tmp2, dp, ep, dm, em;

					mqi_coef_list_init_dynamic(mqi_coef_typeof(D1),
							tmp1, tmp2, dp, ep, dm, em, NULL);
					mqi_coef_init_cpy (tmp1, D1);
					mqi_coef_init_cpy (tmp2, sq);
					mqi_coef_mul_schar (tmp1, tmp1, 2);
					mqi_coef_mul_schar (tmp2, tmp2, 2);

					mqi_coef_init_cpy (dp, tmp1);
					mqi_coef_add	  (dp, dp, tmp2);
					mqi_coef_init_cpy (dm, tmp1);
					mqi_coef_sub	  (dm, dm, tmp2);

					/* $$ Why initializing to one if it's overwritten afterwards ... ? */
					mqi_coef_set_schar(ep,1);
					mqi_coef_set_schar(em,1);
					
					/* The discriminants of the planes are 
					* sqrt(Delta) = 1/2*(ep*sqrt(dp) +/- em*sqrt(dm))
					* We have: ep*em*sqrt(dp)*sqrt(dm) = 2*D2*sqrt(d) (if D2 != 0)  */
					qi_coef_extract_square_factors (square, cofactor, dp);

					mqi_coef_cpy (ep, square);
					mqi_coef_cpy (dp, cofactor);

					if ( !mqi_coef_sign(D2) )
						mqi_coef_mul (dm, d, dp);

					mqi_coef_list_clear(square, cofactor, NULL);
					qi_coef_extract_square_factors (square, cofactor, dm);
					
					mqi_coef_cpy (em, square);
					mqi_coef_cpy (dm, cofactor);

					mqi_mat_t r1p, r1m, r2p, r2m, r3p, r3m, r4p, r4m;

					if ( mqi_coef_sign(D2) ) {

						mqi_mat_t k;
						mqi_coef_mul_schar(coef1, D2,2);
						mqi_mat_linexpr(k,1,	coef1,k11);

						mqi_coef_linexpr3(coef1,1, 1,em,em,dm);
						mqi_coef_linexpr3(coef2,1, 1,ep,ep,dp);

						mqi_mat_t tmp_v1, tmp_v2;
						mqi_mat_linexpr(tmp_v1,1, coef1,k21);
						mqi_mat_linexpr(tmp_v2,1, coef2,k21);

						mqi_mat_add (tmp_v1, tmp_v1, k);
						mqi_mat_add (tmp_v2, tmp_v2, k);

						mqi_mat_mul_coef (tmp_v1, tmp_v1, e);
						mqi_mat_mul_coef (tmp_v2, tmp_v2, e);

						/** I */
						mqi_mat_init_cpy(r1p,po_rat);
						mqi_coef_cpy (coef, D2);
						mqi_coef_mul_schar (coef, coef, 4);
						mqi_mat_mul_coef (r1p, r1p, coef);

						mqi_mat_init_cpy (r2p, tmp_v2);

						mqi_mat_init_cpy (r3p, tmp_v1);
						mqi_mat_mul_coef (r3p, r3p, ep);

						mqi_mat_init_cpy (r4p, po_sq);
						mqi_coef_cpy (coef, ep);
						mqi_coef_mul_schar (coef, coef, 2);
						mqi_mat_mul_coef (r4p, r4p, coef);

						/** II */
						mqi_mat_init_cpy (r1m, r1p);

						mqi_mat_init_cpy (r2m, tmp_v1);

						mqi_mat_init_cpy (r3m, tmp_v2);
						mqi_mat_mul_coef (r3m, r3m, em);

						mqi_mat_init_cpy (r4m, po_sq);
						mqi_coef_cpy (coef, em);
						mqi_coef_mul_schar (coef, coef, 2);
						mqi_mat_mul_coef (r4m, r4m, coef);

						/** Free local memory */
						mqi_mat_list_clear(k, tmp_v1, tmp_v2, NULL);

					}else{

						/** I */
						mqi_mat_init_cpy (r1p, po_rat);
						mqi_mat_mul_int(r1p, r1p, 2);

						mqi_mat_init_cpy (r2p, po_sq);
						mqi_mat_mul_int(r2p, r2p, 2);

						mqi_mat_init_cpy (r3p, k11);
						mqi_coef_init_cpy (coef, e);
						mqi_coef_mul (coef, coef, ep);
						mqi_mat_mul_coef (r3p, r3p, coef);
						
						mqi_mat_init_cpy (r4p, k21);
						mqi_mat_mul_coef (r4p, r4p, coef);

						/** II */
						mqi_mat_init_cpy (r1m, r1p);

						mqi_mat_init_cpy  (r2m, po_sq);
						mqi_coef_init_cpy (coef2, d);
						mqi_coef_mul_int(coef2, coef2, 2);
						mqi_mat_mul_coef  (r2m, r2m, coef2);

						mqi_coef_mul (coef2, coef, em);
						mqi_mat_initpool_cpy (r3m, k21);
						mqi_mat_mul_coef (r3m, r3m, coef2);

						mqi_mat_initpool_cpy (r4m, k11);
						mqi_mat_mul_coef (r4m, r4m, coef2);

					}

					qi_mat_optimize_2 (r1p, r3p);
					qi_mat_optimize_2 (r2p, r4p);
					qi_mat_optimize_2 (r1m, r3m);
					qi_mat_optimize_2 (r2m, r4m);

					mqi_curve_param_t c_ratp, c_ratm, c_sqp, c_sqm;
					mqi_curve_param_init_dynamic (c_ratp, mqi_mat_nrows(r1p), mqi_mat_typeof(r1p));
					mqi_curve_param_init_dynamic (c_ratm, mqi_mat_nrows(r1m), mqi_mat_typeof(r1m));
					mqi_curve_param_init_dynamic (c_sqp,  mqi_mat_nrows(r3p), mqi_mat_typeof(r3p));
					mqi_curve_param_init_dynamic (c_sqm,  mqi_mat_nrows(r3m), mqi_mat_typeof(r3m));

					mqi_curve_param_line_through_two_points (c_ratp,
							(mqi_vect_ptr)COL(r1p,0), (mqi_vect_ptr)COL(r2p,0));
					mqi_curve_param_line_through_two_points (c_sqp,
							(mqi_vect_ptr)COL(r3p,0), (mqi_vect_ptr)COL(r4p,0));
					mqi_curve_param_line_through_two_points (c_ratm,
							(mqi_vect_ptr)COL(r1m,0), (mqi_vect_ptr)COL(r2m,0));
					mqi_curve_param_line_through_two_points (c_sqm,
							(mqi_vect_ptr)COL(r3m,0), (mqi_vect_ptr)COL(r4m,0));

					mqi_vect_t ignored;
					qi_param_line_improved2 (ignored, c_ratp, c_sqp, dp);
					mqi_vect_clear(ignored);
					qi_param_line_improved2 (ignored, c_ratm, c_sqm, dm);

					qi_inter_create_components (rop, 2, QI_INTER_COMPONENT_LINE, QI_INTER_COMPONENT_3,
									c_ratp, c_sqp, dp);
					qi_inter_create_components (rop, 2, QI_INTER_COMPONENT_LINE, QI_INTER_COMPONENT_3,
									c_ratm, c_sqm, dm);

					/** Free local memory */
					mqi_coef_list_clear(tmp1, tmp2, dp, dm, ep, dm, NULL);
					mqi_vect_clear(ignored);
					mqi_mat_list_clear(r1p, r1m, r2p, r2m, r3p, r3m, r4p, r4m, NULL);
					mqi_curve_param_list_clear(c_ratp, c_ratm, c_sqp, c_sqm, NULL);

				}
				
				/** Free local memory */
				mqi_coef_list_clear(dsq, cd, cd1, g, NULL);


			}
		       	else
			{

				mqi_coef_t f, nd1, nd2, g;

				mqi_coef_list_init_dynamic(mqi_coef_typeof(d), f, nd1, nd2, g, NULL);

				mqi_coef_set_schar (f,1);
				mqi_coef_cpy(nd1, D1);
				mqi_coef_mul_schar(nd1, nd1, 2);
				mqi_coef_set_schar (nd2, 2);
				mqi_coef_set_schar (g, 1);
				
				mqi_coef_t square, cofactor;
				qi_coef_extract_square_factors (square, cofactor, del2);

				mqi_coef_cpy (g, square);
				mqi_coef_cpy (del2, cofactor);
				mqi_coef_mul (nd2, nd2, g);

				mqi_coef_t gc;
				mqi_coef_init_cpy (gc, nd1);
				mqi_coef_gcd (gc, gc, nd2);

				mqi_coef_list_clear(square, cofactor, NULL);
				qi_coef_extract_square_factors (square, cofactor, gc);

				mqi_coef_cpy (f, square);

				mqi_coef_t f2;
				mqi_coef_init_cpy (f2, f);
				mqi_coef_mul	  (f2, f2, f2);

				mqi_coef_divexact (nd1, nd1, f2);
				mqi_coef_divexact (nd2, nd2, f2);

				mqi_vect_t zero;
				mqi_vect_init_dynamic (zero, 4, mqi_coef_typeof(nd1));

				mqi_coef_t ef, efg;
				mqi_coef_init_cpy (ef, e);
				mqi_coef_mul (ef, ef, f);

				mqi_coef_init_cpy (efg, ef);
				mqi_coef_mul (efg, efg, g);

				/** I */
				mqi_vect_t k1p, k3p, k4p, k1m, k3m, k4m;
				mqi_mat_t  tmpmat;

				mqi_mat_init_cpy(tmpmat, po_rat);
				mqi_coef_cpy(coef1,D2);
				mqi_coef_mul_schar(coef1,coef1,2);
				mqi_mat_mul_coef(tmpmat, tmpmat, coef1);
				mqi_vect_init_cpy(k1p, (mqi_vect_ptr)COL(tmpmat,0));

				mqi_mat_clear(tmpmat);
				mqi_mat_init_dynamic(tmpmat, mqi_mat_nrows(k11), mqi_mat_ncols(k11),
							mqi_mat_typeof(k11));
				mqi_mat_linexpr(tmpmat,2, D2,k11, D1,k21);
				mqi_mat_mul_coef(tmpmat, tmpmat, ef);

				mqi_vect_init_cpy(k3p, (mqi_vect_ptr)COL(tmpmat,0));

				mqi_vect_init_cpy(k4p, (mqi_vect_ptr)COL(k21,0));
				mqi_vect_mul_coef(k4p, k4p, efg);
				mqi_vect_neg(k4p, k4p);


				mqi_mat_clear(tmpmat);
				mqi_mat_init_cpy(tmpmat, po_sq);
				mqi_coef_cpy(coef1,D2);
				mqi_coef_mul_schar(coef1,coef1,2);
				mqi_coef_mul(coef1,coef1,d);
				mqi_mat_mul_coef(tmpmat, tmpmat, coef1);
				mqi_vect_init_cpy(k1m, (mqi_vect_ptr)COL(tmpmat,0));

				mqi_mat_clear(tmpmat);
				mqi_mat_init_dynamic(tmpmat, mqi_mat_nrows(k11), mqi_mat_ncols(k11),
							mqi_mat_typeof(k11));
				mqi_coef_cpy(coef1,D2);
				mqi_coef_mul(coef1,coef1,d);
				mqi_mat_linexpr(tmpmat,2, coef1,k21, D1,k11);
				mqi_mat_mul_coef(tmpmat, tmpmat, ef);
				mqi_vect_init_cpy(k3m, (mqi_vect_ptr)COL(tmpmat,0));

				mqi_vect_init_cpy(k4m, (mqi_vect_ptr)COL(k11,0));
				mqi_vect_mul_coef(k4m, k4m, efg);
				mqi_vect_neg(k4p, k4p);


				qi_vect_optimize_gcd_triplet (k1p, k3p, k4p);
				qi_vect_optimize_gcd_triplet (k1m, k3m, k4m);

				mqi_curve_param_t c1p, c1m, c2p, c2m, c3p, c3m, c4p, c4m;
				mqi_curve_param_init_dynamic (c1p, mqi_vect_size(k1p), mqi_vect_typeof(k1p));
				mqi_curve_param_line_through_two_points (c1p, k1p, k1m);
				mqi_curve_param_init_cpy (c1m, c1p);
				mqi_curve_param_init_dynamic (c2p, mqi_vect_size(zero), mqi_vect_typeof(zero));
				mqi_curve_param_line_through_two_points (c2p, zero, zero);
				mqi_curve_param_init_cpy (c2m, c2p);
				mqi_curve_param_init_dynamic (c3p, mqi_vect_size(k3p), mqi_vect_typeof(k3p));
				mqi_curve_param_line_through_two_points (c3p, k3p, k3m);
				mqi_curve_param_init_cpy (c3m, c3p);
				mqi_curve_param_init_dynamic (c4p, mqi_vect_size(k4p), mqi_vect_typeof(k4p));
				mqi_curve_param_line_through_two_points (c4p, k4p, k4m);

				mqi_vect_neg (k4p, k4p);
				mqi_vect_neg (k4m, k4m);

				mqi_curve_param_init_dynamic (c4m, mqi_vect_size(k4p), mqi_vect_typeof(k4p));
				mqi_curve_param_line_through_two_points (c4m, k4p, k4m);

				qi_param_line_improved3 (c1p, c2p, c3p, c4p, del2, nd1, nd2);
				mqi_coef_neg (nd2, nd2);
				qi_param_line_improved3 (c1m, c2m, c3m, c4m, del2, nd1, nd2);

				mqi_coef_neg (nd2, nd2);
				qi_inter_create_components (rop, 2, QI_INTER_COMPONENT_LINE, QI_INTER_COMPONENT_6,
								c1p, c2p, del2, c3p, c4p, nd1, nd2);
				mqi_coef_neg (nd2, nd2);
				qi_inter_create_components (rop, 2, QI_INTER_COMPONENT_LINE, QI_INTER_COMPONENT_6,
								c1m, c2m, del2, c3m, c4m, nd1, nd2);
				
				/** Free local memory */
				mqi_coef_list_clear(f, f2, nd1, nd2, g, square, cofactor, gc, ef, efg, NULL);
				mqi_vect_list_clear(zero, k1p, k1m, k3p, k3m, k4p, k4m, NULL);
				mqi_mat_clear(tmpmat);
				mqi_curve_param_list_clear(c1p, c1m, c2p, c2m, c3p, c3m, c4p, c4m, NULL);				

			}

		}

		/** Temporary */

		mqi_coef_set_zero (coef);	

		qi_inter_cut_init (cut, QI_INTER_CUT_0, coef,coef);

		qi_inter_component_add_cut_parameter (rop->components[0], cut);
		qi_inter_component_add_cut_parameter (rop->components[0], cut);
		qi_inter_component_add_cut_parameter (rop->components[1], cut);
		qi_inter_component_add_cut_parameter (rop->components[1], cut);
		qi_inter_component_add_cut_parameter (rop->components[2], cut);
		qi_inter_component_add_cut_parameter (rop->components[2], cut);
		qi_inter_component_add_cut_parameter (rop->components[3], cut);
		qi_inter_component_add_cut_parameter (rop->components[3], cut);

		qi_inter_cut_set_owner(cut, rop->components[0]);


		/** Free local memory */
		mqi_coef_list_clear(del2, sq, coef, NULL);

	}	

	qi_inter_set_optimal(rop);


	/** Free local memory */
	mqi_coef_list_clear(e, D1, D2, square, square2, cofactor, cofactor2, e2,
			    coef1, coef2, coef3, coef4, ct, NULL);
	mqi_mat_list_clear(k11, k12, k21, k22, tmp1, tmp2, p_rat, p_sq, tmpmat,
			   tmpmat2, po_rat, po_sq, NULL);

}

