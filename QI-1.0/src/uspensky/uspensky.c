#include "qi_uspensky.h"
#include <mqi/mqi_log.h>
#include <cmm/mempool/mempool.h>
#include "qi_utils.h"

unsigned long usign;
int b1 = 0, b2 = 0;

/** Output things in the right order */
void _qi_uspensky_clean_output(interval *roots, unsigned long __IN deg, unsigned int __IN nbroot)
{

	interval roots2[deg];
	int i, j;
	unsigned int k;

	if ((nbroot != 0) && (mpz_sgn(roots[nbroot-1].c) < 0))
	{
	
		j = 0;

		for (i = nbroot-1; i >= 0; i--)
		{
			if (mpz_sgn(roots[i].c) >= 0)
				break;

			mpz_init_set(roots2[j].c,roots[i].c);
			roots2[j].k = roots[i].k;
			roots2[j].isexact = roots[i].isexact;
			j++;
		}

		for (k = 0; k < nbroot-j; k++)
		{
			mpz_init_set(roots2[k+j].c,roots[k].c);
			roots2[k+j].k = roots[k].k;
			roots2[k+j].isexact = roots[k].isexact;
		}

		// Copy roots2 in roots and delete roots2
		for (k = 0; k < nbroot; k++)
		{
			mpz_set(roots[k].c,roots2[k].c);
			mpz_clear(roots2[k].c);
			roots[k].k = roots2[k].k;
			roots[k].isexact = roots2[k].isexact;
		}

	}

}


/** Deprecated as we use libmqi now */
// Convert between LiDIA and gmp format, then launch Uspensky
/*void qi_uspensky_Uspensky(interval **roots, const hom_polynomial <bigint> &det_p, const unsigned long deg,
unsigned int &nbroot);*/
int _qi_uspensky_remove_content(mpz_t *P, unsigned long __IN deg)
{

	unsigned long cont, i, z;

	i = 0; while (mpz_sgn(P[i]) == 0) i++;
	cont = vali(P[i]);

	for( ; (i <= deg) && cont; i++)
	{
		if (mpz_sgn(P[i]) != 0)
		{
			z = vali(P[i]);
			if (z < cont) cont = z;
		}
	}

	if (cont == 0) return 0;

	for (i = 0; i <= deg; i++)
		mpz_fdiv_q_2exp(P[i], P[i], cont);

	return cont;

}

/* Johnson's bound : 2*max(abs(-ai/an)^(1/(n-i)),
   the maximum being taken over the i with sgn(a_i) != sgn(an) */
long _qi_uspensky_bound_roots(mpz_t *t, unsigned long __IN deg)
{

	unsigned long i;
	long maxpow, currpow, currpow2, lan, tpos = 1;

	currpow = currpow2 = 0;
	lan = ilog2(t[deg]) - 1; /* power of 2 < an */

	maxpow = -lan;

	for (i = 0; i < deg; i++)
	{
		if (mpz_sgn(t[deg]) != mpz_sgn(t[i]))
		{
			tpos = 0;
			currpow = ilog2(t[i]);
			currpow -= lan; /* 2^currpow >= abs(-ai/an) */

			if (currpow > 0)
				currpow2 = currpow / (deg - i);
			else
				currpow2 = ((-currpow) / (deg - i));

			/** Bug fix Sylvain in parentheses */
			if (currpow2 * ((long) (deg - i)) != currpow) currpow2++;
			/* 2^currpow2 >= abs(-ai/an)^(1/(n-i)) */

			if (currpow2 > maxpow) maxpow = currpow2;
		}
	}

	if (tpos == 1) return -1;

	/* here 2^maxpow > max(abs(-ai/an)^(1/(n-i)), add one to get the bound */
	maxpow++;

	return(maxpow);

}

/* Computes P(X*2^k) */
int _qi_uspensky_homoth(mpz_t *P, long __IN k, unsigned long __IN deg)
{

	unsigned long i;
	long j;

	if (k > 0) {
		j = k;
		for (i = 1; i <= deg; i++, j += k)
			mpz_mul_2exp(P[i], P[i], j);
	}
	else
	{
		j = deg * (-k);
		for (i = 0; i < deg; i++, j += k)
			mpz_mul_2exp(P[i], P[i], j);
	}

	/* Remove possible large power of 2 in content */
	return _qi_uspensky_remove_content(P, deg);

}

/* Replaces P by the polynomial P(X+1) */
void _qi_uspensky_x2xp1(mpz_t *P, unsigned long __IN deg)
{

	long i, j;

	for (i = 0; i <= (long)deg-1; i++)
		for (j = deg-1 ; j >= i; j--)
			mpz_add(P[j], P[j], P[j+1]);
}

/* Number of sign changes in the coefficients of P(1/(X+1)) (Descartes' rule) */
unsigned long _qi_uspensky_descartes(mpz_t *P, unsigned long __IN deg, long __IN sigh, long *flag)
{

	unsigned long nb = 0;
	long s, t;
	unsigned long i, j;
	mpz_t	Q[deg + 1];	

	/* 
	   Prune the computation if all the coefficients are of the sign of P[deg] 
	   In that case any subsequent interval shall have the same property, 
	   we put flag at 1 to point this to Uspensky_rec.
	   */
	j = deg; t = mpz_sgn(P[j]);
	while (j >= 0 && mpz_sgn(P[j]) == t)
	{
		if (j == 0)
			break;

		j--;
	}

	if (j < 0)
	{
		*flag = -1;
		return nb;
	}

	for (i = 0; i <= deg; i++)
		mpz_init_set(Q[i], P[i]);

	for (j = 0; j <= deg-1; j++)
		mpz_add(Q[j+1], Q[j+1], Q[j]);

	s = mpz_sgn(Q[deg]);

	*flag = s && (s == mpz_sgn(P[0])) && (s == -sigh);

	for (i = 1; i <= deg-1; i++)
	{
		/* 
		   Prune the computation if all further coefficients are of the sign of 
		   Q[deg-i] 
		   */
		j = deg - i; t = s;
		while (j >= 0 && t == 0) { t = mpz_sgn(Q[j]); j--; }
		while (j >= 0 && mpz_sgn(Q[j]) == t)
		{
			if (j == 0)
				break;

			j--;
		}
		if (j < 0)
		{
			for (i = 0; i <= deg; i++)
				mpz_clear(Q[i]);

			return nb;
		}

		for (j = 0; j <= deg - i - 1; j++)
			mpz_add(Q[j+1], Q[j+1], Q[j]);

		if (s == 0) { s = mpz_sgn(Q[deg-i]); }
		else
			if (s == -mpz_sgn(Q[deg-i]))
			{
				if ((nb == 1 && !flag) || nb == 2)
				{
					for (i = 0; i <= deg; i++)
						mpz_clear(Q[i]);

					return (nb + 1);
				}

				nb++; s = -s;
			}
	}

	if (s == -mpz_sgn(Q[0])) nb++;

	for (i = 0; i <= deg; i++)
		mpz_clear(Q[i]);

	return nb;

}

/* Returns the sign of P(1/2) */
long _qi_uspensky_evalhalf(mpz_t *P, unsigned long __IN deg)
{

	int ret;
	long j;
	mpz_t x, y;

	mpz_init_set(x, P[deg]);
	mpz_init(y);

	for (j = deg - 1; j >= 0; j--)
	{
		mpz_mul_2exp(y, P[j], deg - j);
		mpz_add(x, x, y);
	}

	mpz_clear(y);
	ret = mpz_sgn (x);
	mpz_clear(x);

	return ret;

}

void _qi_uspensky_add_root(interval *roots, mpz_ptr __IN c, int __IN k, unsigned int __IN flag,
              		   unsigned int __IN nbroot)
{

	int b = (usign ? b1 : b2);

	mpz_init(roots[nbroot].c);

	if (k <= b)
	{
		if (usign)
		{
			mpz_neg(roots[nbroot].c, c);
			/** Bug fix Sylvain when root is exact */
			if (!flag)
				mpz_sub_ui(roots[nbroot].c, roots[nbroot].c, 1);
			mpz_mul_2exp(roots[nbroot].c, roots[nbroot].c, b-k);
		}
		else
			mpz_mul_2exp(roots[nbroot].c, c, b-k);

		roots[nbroot].k = k - b;
		roots[nbroot].isexact = flag;
	}
	else
	{
		if (usign)
		{
			mpz_neg(roots[nbroot].c, c);
			/** Bug fix Sylvain when root is exact */
			if (!flag)
				mpz_sub_ui(roots[nbroot].c, roots[nbroot].c, 1);
		}
		else
			mpz_set(roots[nbroot].c, c);

		roots[nbroot].k = k - b;

		roots[nbroot].isexact = flag;
	}

}

/** Check interval [c/2^k, (c+1)/2^k]. The value of k is returned, this
    is necessary to know from where we come [i.e., what exactly is the 
    current polynomial P] when several recursive calls return in a row. 
    In practice, this is used to update the polynomial at HERE */
long _qi_uspensky_rec(mpz_t *P, mpz_ptr __IN c, unsigned long __IN k, unsigned long *Deg,
                  	 interval *roots, unsigned int *nbroot)
{

	unsigned long i, j, nb;
	long oldk;
	long shalf, flag;
	mpz_t tmp;

	if (k > DEPTH_MAX)
	{
		mqi_log_warning( "Maximal depth reached. Check that your polynomial is squarefree or increase DEPTH_MAX\n");
		exit(-1);
	}

	mpz_init(tmp);

	/* Check whether c/2^k is a root */
	if (mpz_cmp_ui(P[0], 0) == 0)
	{
		i = 1; while(mpz_cmp_ui(P[i], 0) == 0) { i++; }

		for (j = 0; j < i; j++)
		{
			_qi_uspensky_add_root(roots, c, k, 1, *nbroot);
			(*nbroot)++;
		}

		Deg -= i; /* Update the polynomial */
		for (j = 0; j <= *Deg; j++, i++)
			mpz_set(P[j], P[i]);
	}

	/* 
	   Compute the sign of P(1/2) ; thus if Descartes bound is 2, 
	   whereas sign(P(0)) = sign(P(1)) = -sign(P(1/2)) we have
	   found two roots.
	   */
	shalf = _qi_uspensky_evalhalf(P, *Deg);

	/* Compute Descartes' bound */
	nb = _qi_uspensky_descartes(P, *Deg, shalf, &flag);
	if (flag == TOT_POS)
	{
		mpz_clear(tmp);
		return TOT_POS;
	}

	switch (nb)
	{
		case 0: /* no root */
			mpz_clear(tmp);
			return k;

		case 1: /* exactly one root */
			_qi_uspensky_add_root(roots, c, k, 0, *nbroot);
			(*nbroot)++;
			mpz_clear(tmp);
			return k;

		case 2: /* if flag != 0, one root in each half of the current interval */
			if (flag)
			{
				mpz_set(tmp, c);

				mpz_mul_2exp(tmp, tmp, 1);
				_qi_uspensky_add_root(roots, tmp, k+1, 0, *nbroot);
				(*nbroot)++;

				mpz_add_ui(tmp, tmp, 1);
				_qi_uspensky_add_root(roots, tmp, k+1, 0, *nbroot);
				(*nbroot)++;

				mpz_clear(tmp);
				return k;
			}

		default: /* recursive call on each half of the interval */
			mpz_set(tmp, c);

			mpz_mul_2exp(tmp, tmp, 1);
			_qi_uspensky_homoth(P, -1, *Deg);
			oldk = _qi_uspensky_rec(P, tmp, k+1, Deg, roots, nbroot);
			if (oldk == TOT_POS)
			{
				mpz_clear(tmp);
				return TOT_POS;
			}

			mpz_add_ui(tmp, tmp, 1);
			_qi_uspensky_x2xp1(P, *Deg);

			if (oldk > (long)k + 1)
				_qi_uspensky_homoth(P, oldk - (k + 1), *Deg);

			oldk = _qi_uspensky_rec(P, tmp, k+1, Deg, roots, nbroot);
			if (oldk == TOT_POS)
			{
				mpz_clear(tmp);
				return TOT_POS;
			}

			mpz_clear(tmp);
			return oldk;
	}

}

void _qi_uspensky_gmp(interval *roots, mpz_t *Q, unsigned long __IN deg, unsigned int *nbroot)
{

	unsigned long deg1 = deg;
	long i, j;
	
	mpz_t e;
	mpz_t P[deg]; /** $$ Allocate more: to avoid dynamic allocation */
	int nb_z;

	mpz_init_set_ui(e, 0);
	*nbroot = 0;

	/* Remove 0 roots in all cases */
	nb_z = 0; while (mpz_sgn(Q[nb_z]) == 0) nb_z++;
	for (j = 0; j < nb_z; j++)
	{
		_qi_uspensky_add_root(roots, e, 0, 1, *nbroot);
		(*nbroot)++;
	}

	deg1 = deg - nb_z; /* Update the polynomial */

	for (j = 0; j <= (long)deg1; j++)
		mpz_init_set(P[j], Q[nb_z + j]);

	/* First work on the positive roots. */
	b2 = _qi_uspensky_bound_roots(P, deg1);

	if (b2 < 0) goto NEGATIVE;

	_qi_uspensky_homoth(P, b2, deg1);

	usign = 0;
	_qi_uspensky_rec(P, e, 0, &deg1, roots, nbroot);

	/* Change P into P(-X) to look for negative roots */
NEGATIVE:
	deg1 = deg - nb_z;
	for (i = deg1; i >= 0; i--)
	{
		if (i % 2 == 1)
			mpz_neg(P[i], Q[nb_z + i]);
		else mpz_set(P[i], Q[nb_z + i]);
	}

	b1 = _qi_uspensky_bound_roots(P, deg1);

	if (b1 >= 0)
	{
		_qi_uspensky_homoth(P, b1, deg1);
		mpz_set_ui(e, 0);
		usign = 1;
		_qi_uspensky_rec(P, e, 0, &deg1, roots, nbroot);
	}

	/* Free memory. */
	for (i = deg-nb_z; i >= 0; i--)
		mpz_clear(P[i]);

	mpz_clear(e);

}

void qi_uspensky_pick_point_outside_roots(mqi_vect_t __INIT rop, interval *roots, int __IN *infinity_flag,
                                          mqi_hpoly_t __IN det_p,
                                          mqi_hpoly_t __IN deriv,
					  unsigned int __IN nbroot, unsigned int __IN index)
{

	mqi_vect_t point;
	mqi_coef_t tmp1, tmp2;

	mqi_vect_init_dynamic (point, 2, mqi_poly_typeof(det_p));
	mqi_coef_list_init_dynamic(mqi_vect_typeof(point), tmp1, tmp2, NULL);


	if ( index == 0 ) {

#ifdef TRACEFUNC
		tracefunc("index == 0");
#endif

		/** Only root is infinity */
		if ( (*infinity_flag) && (nbroot == 1) )
		{
			mqi_coef_set_schar (VECT(point,0), 1);
		}else{
			if ( roots[0].k <= 0 )
				mqi_coef_set_schar (VECT(point,1), 1);
			else{
				mqi_coef_set_schar (tmp1, 2);
				mqi_coef_pow	   (VECT(point,1), tmp1, roots[0].k);
			}

			mqi_coef_set_mpz_t (tmp1, roots[0].c);

			if ( roots[0].isexact ) {
				/** Take lower end of the interval minus 1 as point */
				mqi_coef_set_schar (tmp2, 1);
				mqi_coef_sub_schar (VECT(point,0), tmp1, 1);
			}else {
				/** Take lower end of the interval as point */

				mqi_coef_cpy (VECT(point,0), tmp1);
			}

		}

	}
	else if ( (index == nbroot - 1) && (*infinity_flag) )
	{

#ifdef TRACEFUNC
		tracefunc("index == nbroot - 1  AND  infinity_flag is set");
#endif

		/** Take upper end of the interval plus 1 as point */
		if ( roots[index - 1].k > 0 ) {
			mqi_coef_set_mpz_t (VECT(point,0), roots[index-1].c);
			mqi_coef_add_schar (VECT(point,0), VECT(point,0), 2);

			mqi_coef_set_schar(tmp2, 2);
			mqi_coef_pow (VECT(point,1), tmp2, roots[index-1].k);
		}else{
			mqi_coef_set_schar (tmp1, 2);
			mqi_coef_pow (VECT(point,0), tmp1, -roots[index-1].k);

			mqi_coef_add_mpz_t (VECT(point,0), VECT(point,0), roots[index-1].c);
			mqi_coef_add_schar (VECT(point,0), VECT(point,0), 1);
			mqi_vect_set_at_int(point,1, 1);
		}
	}
	else
	{

		mqi_vect_t lower_end;
		mqi_vect_t higher_end;

#ifdef TRACEFUNC
		tracefunc("3rd block");
#endif

		mqi_vect_list_initpool_dynamic(2, mqi_poly_typeof(det_p), lower_end, higher_end, NULL);

		if ( roots[index-1].isexact )
		{
			mqi_coef_set_mpz_t (VECT(lower_end,0), roots[index-1].c);

			if ( roots[index-1].k <= 0)
				mqi_vect_set_at_int (lower_end,1, 1);
			else
			{
				mqi_coef_set_schar (tmp1, 2);
				mqi_coef_pow	   (VECT(lower_end,1), tmp1, roots[index-1].k);
			}
		}
		else
		{
			if ( roots[index-1].k <= 0 )
			{
				mqi_coef_set_schar (tmp1, 2);
				mqi_coef_pow	   (VECT(lower_end,0), tmp1, -roots[index-1].k);
				mqi_coef_add_mpz_t (VECT(lower_end,0), VECT(lower_end,0), roots[index-1].c);
				mqi_coef_set_schar (VECT(lower_end,1), 1);

			}
			else
			{
				mqi_coef_set_mpz_t (tmp1, roots[index-1].c);
				mqi_coef_add_schar (VECT(lower_end,0), tmp1, 1);
				mqi_coef_set_schar (tmp1, 2);
				mqi_coef_pow	   (VECT(lower_end,1), tmp1, roots[index-1].k);
			}
		}

		mqi_coef_set_mpz_t (VECT(higher_end,0), roots[index].c);

		if ( roots[index].k <= 0 )
			mqi_vect_set_at_int (higher_end,1, 1);
		else
		{
			mqi_coef_set_schar (tmp1, 2);
			mqi_coef_pow (VECT(higher_end,1), tmp1, roots[index].k);
		}

		/** Are the two points the same ? */
		mqi_coef_t equal_end;

		mqi_coef_init_dynamic(equal_end, mqi_vect_typeof(lower_end));
		mqi_coef_mul (tmp1, VECT(lower_end,0), VECT(higher_end,1));
		mqi_coef_mul (tmp2, VECT(lower_end,1), VECT(higher_end,0));
		mqi_coef_sub (equal_end, tmp1, tmp2);

		if ( mqi_coef_sign(equal_end) != 0 )
		{

			/** They're different: pick a point in the middle */
			if ( mqi_coef_cmp (VECT(lower_end,1), VECT(higher_end,1)) == 0 )
			{
				/** No need to find common denominator */			
				mqi_coef_add (VECT(point,0), VECT(lower_end,0), VECT(higher_end,0));
				mqi_coef_mul_schar (VECT(point,1), VECT(lower_end,1), 2);
			}
			else
			{
				mqi_coef_mul (tmp1, VECT(higher_end,0), VECT(lower_end,1));
				mqi_coef_mul (tmp2, VECT(higher_end,1), VECT(lower_end,0));
				mqi_coef_add (VECT(point,0), tmp1, tmp2);

				mqi_coef_mul (tmp1, VECT(lower_end,1), VECT(higher_end,1));
				mqi_coef_mul_schar (VECT(point,1), tmp1, 2);
			}
		}else if ( (!roots[index-1].isexact) && (!roots[index].isexact) ) {
			/** None of the two roots are exact: pick the higher_end */
			mqi_vect_cpy (point, higher_end);
		}
		else
		{
			/** One of the two roots is exact and equal to the endpoint of the other interval */
			if ( roots[index-1].isexact )
			{

				mqi_vect_t ex_root;
				int sg_der;

				/** Left is exact, refine right interval */
				/** Exact root */
				mqi_vect_init_dynamic (ex_root, 2, mqi_poly_typeof(det_p));
				mqi_coef_set_mpz_t    (VECT(ex_root,0), roots[index-1].c);

				if ( roots[index-1].k <= 0 )
					mqi_coef_set_schar (VECT(ex_root,1), 1);
				else{
					mqi_coef_set_schar (tmp1, 2);
					mqi_coef_pow (VECT(ex_root,1), tmp1, roots[index-1].k);
				}

				/** Evaluate sign of derivative at exact root */
				mqi_poly_eval_xw (tmp1, deriv, VECT(ex_root,0), VECT(ex_root,1));

				sg_der = mqi_coef_sign(tmp1);

				/** Evaluate at midpoint of interval and loop by cutting in half until the right sign (sg_der) is reached */

				mqi_coef_t l_0, l_1, m;

				mqi_coef_list_init_dynamic(mqi_poly_typeof(det_p), l_0, l_1, m, NULL);
				if ( roots[index].k > 0 )
				{
					mqi_coef_set_schar    (l_1, 1);
					mqi_coef_set_schar    (tmp1, 2);
					mqi_coef_pow	      (m, tmp1, roots[index].k + 1);
				}
				else
				{
					mqi_coef_set_schar    (tmp1, 2);
					mqi_coef_pow	      (l_1, tmp1, -roots[index].k);
					mqi_coef_set_schar    (m, 2);
				}

				mqi_coef_set_mpz_t    (tmp1, roots[index].c);
				mqi_coef_mul_schar    (l_0, tmp1, 2);

				mqi_coef_add (VECT(point,0), l_0, l_1);
				mqi_coef_cpy (VECT(point,1), m);

				/** Recurse */
				for ( ; ; )
				{
					mqi_poly_eval_xw (tmp1, det_p, VECT(point,0), VECT(point,1));
					if ( mqi_coef_sign(tmp1) == sg_der ) break;

					mqi_coef_mul_schar (l_0, l_0, 2);
					mqi_coef_mul_schar (m, m, 2);

					mqi_coef_add	   (VECT(point,0), l_0, l_1);
					mqi_coef_cpy	   (VECT(point,1), m);
				}

				/** Free local memory */
				mqi_vect_clear(ex_root);
				mqi_coef_list_clear(l_0, l_1, m, NULL);
			}
			else
			{
				mqi_vect_t ex_root;
				mqi_coef_t l_0, l_1, m;
				int sg_der;

				/** Right is exact, refine left interval */
				/** Exact root */
				mqi_vect_init_dynamic (ex_root, 2, mqi_poly_typeof(det_p));
				mqi_coef_set_mpz_t    (VECT(ex_root,0), roots[index].c);

				if ( roots[index].k <= 0 )
					mqi_coef_set_schar (VECT(ex_root,1), 1);
				else{
					mqi_coef_set_schar (tmp1, 2);
					mqi_coef_pow (VECT(ex_root,1), tmp1, roots[index].k);
				}

				/** Evaluate sign of derivative at exact root */
				mqi_poly_eval_xw (tmp1, deriv, VECT(ex_root,0), VECT(ex_root,1));

				sg_der = mqi_coef_sign(tmp1);

				mqi_coef_list_init_dynamic (mqi_poly_typeof(det_p), l_0, l_1, m, NULL);

				if ( roots[index-1].k > 0 )
				{
					mqi_coef_set_mpz_t (tmp1, roots[index-1].c);
					mqi_coef_add_schar (l_0, tmp1, 1);
					mqi_coef_mul_schar (l_0, l_0, 2);
					mqi_coef_set_schar (l_1, -1);

					mqi_coef_set_schar (tmp1, 2);
					mqi_coef_pow	   (m, tmp1, roots[index-1].k + 1);
				}
				else
				{
					mqi_coef_set_schar (tmp1, 2);
					mqi_coef_pow	   (l_0, tmp1, -roots[index-1].k);
					mqi_coef_cpy	   (l_1, l_0);
					mqi_coef_neg	   (l_1, l_1);
					mqi_coef_add_mpz_t (l_0, l_0, roots[index-1].c);
					mqi_coef_mul_schar (l_0, l_0, 2);
					mqi_coef_set_schar (m, 2);
				}

				mqi_coef_add (VECT(point,0), l_0, l_1);
				mqi_coef_cpy (VECT(point,1), m);

				/** Recurse */
				for ( ; ; ) {

					mqi_poly_eval_xw (tmp1, det_p, VECT(point,0), VECT(point,1));
					if ( mqi_coef_sign(tmp1) == -sg_der ) break;

					mqi_coef_mul_schar (l_0, l_0, 2);
					mqi_coef_mul_schar (m, m, 2);

					mqi_coef_add (VECT(point,0), l_0, l_1);
					mqi_coef_cpy (VECT(point,1), m);

				}

				/** Free local memory */
				mqi_vect_clear(ex_root);
				mqi_coef_list_clear(l_0, l_1, m, NULL);
			}
		}

		/** Free local memory */
		mqi_vect_list_clear(lower_end, higher_end, NULL);
		mqi_coef_clear(equal_end);

	}

	mqi_vect_init_cpy(rop, point);

	/** Free local memory */
	mqi_vect_clear(point);
	mqi_coef_list_clear(tmp1, tmp2, NULL);

}

void qi_uspensky (interval **roots, mqi_hpoly_t detp, unsigned long __IN deg, unsigned int *nbroot)
{
	
	mpz_t P[deg + 1];
	unsigned long i;

	*roots = (interval *)calloc( deg, sizeof(interval) );
	
	for (i = 0; i <= deg; i++)
		mpz_init_set (P[i], mqi_coef_get_mpz_t(POLY(detp,i)));/*(mpz_ptr)POLY(detp,i)->value);*/
	
	_qi_uspensky_gmp (*roots, P, deg, nbroot);

	/** Frees memory */
	for (i = 0; i <= deg; i++)
		mpz_clear (P[i]);

	_qi_uspensky_clean_output (*roots, deg, *nbroot);
		
}

void qi_uspensky_print_root (interval z)
{
	mpz_t tmp;
	mpz_init(tmp);

	if (z.isexact != 1) { mqi_log_printf ("]"); }

  if (z.k <= 0)
    	gmp_printf ("%Zd", z.c);
  else
	gmp_printf ("%Zd/2^%ld", z.c, z.k);

  if (z.isexact != 1)
    {
  	mqi_log_printf (", ");  

      if (z.k <= 0)
        {
          mpz_set_ui(tmp, 1);
          mpz_mul_2exp(tmp, tmp, -z.k);
          mpz_add(tmp, z.c, tmp);
          gmp_printf ("%Zd", tmp);
        }
      else
        {
          mpz_add_ui(tmp, z.c, 1);
	  gmp_printf ("%Zd/2^%ld", tmp, z.k);
        }

	mqi_log_printf ("[");
    }

  mpz_clear(tmp);

}

void qi_uspensky_print_roots (interval *roots, int nbroot)
{
	int i;
	
	for (i = 0; i < nbroot; i++)
	{
		qi_uspensky_print_root(roots[i]);
		if (i < nbroot - 1) mqi_log_printf (", ");
	}
}

