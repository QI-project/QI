#include "qi_elem.h"

/** Finds inertia of quadric using Descartes' rule **/
void qi_mat_inertia 		(mqi_vect_t __INIT rop, mqi_mat_t __IN op) {

	size_t n_rows;
	int eigen_p;
	int eigen_m;
	size_t k;	
	mqi_mat_t	identity;
	mqi_poly_t	detpencil;

	n_rows = mqi_mat_nrows(op);

	mqi_mat_init_dynamic (identity, n_rows, n_rows, mqi_mat_typeof(op));
	mqi_mat_set_id	     (identity);
	
	qi_pencil_det (__INIT detpencil, op, identity);

	eigen_p = qi_poly_descartes (detpencil);

	/* Inertia is a vector with 2 elements */
	mqi_vect_init_dynamic(rop, 2, mqi_mat_typeof(op));

	if ( eigen_p == n_rows ) {
		mqi_vect_set_at_int(rop, 0, n_rows);
		mqi_vect_set_at_int(rop, 1, 0);
		/* Don't blame me for putting gotos, they're justified in
		 * many situations, and an assembly program is nothing
		 * but a succession of jumps (gotos) ...
		 */
		goto inertia_end;
	}

	for ( k = 0; k <= n_rows; k += 2 )  
		mqi_coef_neg ( POLY(detpencil,k), POLY(detpencil,k) );	

	eigen_m = qi_poly_descartes (detpencil);		
	
	if ( eigen_m > eigen_p ) {
		mqi_vect_set_at_int (rop, 0, eigen_m);
		mqi_vect_set_at_int (rop, 1, eigen_p);
	}
	else {
		mqi_vect_set_at_int (rop, 0, eigen_p);
		mqi_vect_set_at_int (rop, 1, eigen_m);
	}
	
inertia_end:
	mqi_mat_clear(identity);
	mqi_poly_clear(detpencil);

}

void qi_mat_signed_inertia	(mqi_vect_t __INIT rop, mqi_mat_t op) {

	size_t n_rows;
	int descartes;
	size_t k;	
	mqi_mat_t	identity;
	mqi_poly_t	detpencil;

	n_rows = mqi_mat_nrows(op);

	mqi_mat_init_dynamic (identity, n_rows, n_rows, mqi_mat_typeof(op));
	mqi_mat_set_id	     (identity);
	
	qi_pencil_det (detpencil, op, identity);

	descartes = qi_poly_descartes(detpencil);

	/* Inertia is a vector with 2 elements */
	mqi_vect_init_dynamic(rop, 2, mqi_mat_typeof(op));

	mqi_vect_set_at_int (rop, 0, descartes);

	if ( descartes == n_rows ) {
		mqi_vect_set_at_int (rop, 1, 0);
		goto sinertia_end;
	}

	for ( k = 0; k <= n_rows; k += 2 )  
		mqi_coef_neg ( POLY(detpencil,k), POLY(detpencil,k) );	

	descartes = qi_poly_descartes (detpencil);		
	
	mqi_vect_set_at_int (rop, 1, descartes);

sinertia_end:
	mqi_mat_clear(identity);
	mqi_poly_clear(detpencil);

}

/** Finds inertia of quadric using Descartes' rule when we know its rank */
void qi_mat_inertia_known_rank	(mqi_vect_t __INIT rop, mqi_mat_t op, size_t rank) {

	size_t n_rows;
	int eigen_p;
	mqi_mat_t	identity;
	mqi_poly_t	detpencil;

	n_rows = mqi_mat_nrows(op);

	mqi_mat_init_dynamic (identity, n_rows, n_rows, mqi_mat_typeof(op));
	mqi_mat_set_id	     (identity);
	
	mqi_poly_init_dynamic (detpencil, mqi_mat_typeof(op));
	
	qi_pencil_det (detpencil, op, identity);

	eigen_p = qi_poly_descartes (detpencil);

	/* Inertia is a vector with 2 elements */
	mqi_vect_init_dynamic(rop, 2, mqi_mat_typeof(op));


	if ( 2 * eigen_p > rank ) {
		mqi_vect_set_at_int (rop, 0, eigen_p);
		mqi_vect_set_at_int (rop, 1, rank - eigen_p);
		goto inertia_kr_end;
	}
	
	mqi_vect_set_at_int (rop, 0, rank - eigen_p);
	mqi_vect_set_at_int (rop, 1, eigen_p);

inertia_kr_end:
	mqi_mat_clear(identity);
	mqi_poly_clear(detpencil);

}

/** Finds the inertia of a non-singular non-rational conic Q whose 3x3 matric is 
 *  q1+sqrt(D).q2. Being non-singular, the inertia is [3,0] or [2,1]. 
 *  Output 0 if inertia [3,0] and 1 otherwise */
int qi_mat_inertia_non_rational_conic(mqi_mat_t q1, mqi_mat_t q2, mqi_coef_t D) {
	
	mqi_coef_t	a0, a1, a2, b0, b1, b2;
	int		inertia_21;
	int		sign_c0, sign_c2;
	mempool_t pool; 

	mempool_init(pool); 

	/* The characteric polynomial det(q1+sqrt(D).q2-x.I)= 
	 *  P(x) = -x^3 + c2*x^2 +c1*x +c0 
	 *  P(-x) = x^3 + c2*x^2 -c1*x +c0 
	 * The inertia of Q is [3,0] iff P has 0 positive roots or 0 negative roots 
	 * (since all 3 roots are real Descartes gives the exact number of positive roots)
	 * tha is by Descarte rule iff
	 *  c2<=0, c1<=0, c0<=0 or 
	 *  c2>=0, c1<=0, c0>=0. 
	 * Let ci = ai +bi*sqrt(D)
	 * Recall that sign(a+b*sqrt(D)) =  sign(bigint a, bigint b, bigint D)

	 * Computed with Maple c0, c1 c2 are as follows. */

	mqi_coef_initpool_dynamic(a1, mqi_mat_typeof(q1));

	mqi_coef_linexpr1(a1,6,	-1,MAT(q2,0,0),MAT(q2,1,1),	1,MAT(q2,0,2),MAT(q2,0,2),
				-1,MAT(q2,0,0),MAT(q2,2,2),	-1,MAT(q2,1,1),MAT(q2,2,2),
				1,MAT(q2,0,1),MAT(q2,0,1),	1,MAT(q2,1,2),MAT(q2,1,2));

	mqi_coef_linexpr1(a1,7,	1,D,a1,		-1,MAT(q1,0,0),MAT(q1,2,2),	-1,MAT(q1,0,0),MAT(q1,1,1),
						1,MAT(q1,0,2),MAT(q1,0,2),	-1,MAT(q1,1,1),MAT(q1,2,2),
						1,MAT(q1,0,1),MAT(q1,0,1),	1,MAT(q1,1,2),MAT(q1,1,2));

	mqi_coef_initpool_dynamic(b1, mqi_mat_typeof(q1));

	mqi_coef_linexpr1(b1,9,	-1,MAT(q2,0,0),MAT(q1,2,2),	2,MAT(q1,0,2),MAT(q2,0,2),
				-1,MAT(q1,1,1),MAT(q2,2,2),	-1,MAT(q2,0,0),MAT(q1,1,1),
				2,MAT(q1,1,2),MAT(q2,1,2),	-1,MAT(q1,0,0),MAT(q2,1,1),
				-1,MAT(q2,1,1),MAT(q1,2,2),	-1,MAT(q1,0,0),MAT(q2,2,2),
				2,MAT(q1,0,1),MAT(q2,0,1));

	/** Inertia [2,1] */
	if ( qi_coef_sign(a1, b1, D) > 0 ) { inertia_21 = 1; goto inertia_nrconic_end; }


	mqi_coef_list_initpool_dynamic (mqi_mat_typeof(q1), a0, b0, a2, b2, NULL);

	mqi_coef_linexpr1 (a2,3,	1,MAT(q1,2,2),	1,MAT(q1,1,1),	1,MAT(q1,0,0)	);
	mqi_coef_linexpr1 (b2,3,	1,MAT(q2,2,2),	1,MAT(q2,1,1),	1,MAT(q2,0,0)	);

	mqi_coef_linexpr3(a0,12,	-1,MAT(q2,0,1),MAT(q2,0,1),MAT(q1,2,2),		-2,MAT(q2,0,0),MAT(q1,1,2),MAT(q2,1,2),
					-2,MAT(q1,0,2),MAT(q2,0,2),MAT(q2,1,1),		-1,MAT(q2,0,2),MAT(q2,0,2),MAT(q1,1,1),
					1,MAT(q2,0,0),MAT(q1,1,1),MAT(q2,2,2),		2,MAT(q1,0,1),MAT(q2,0,2),MAT(q2,1,2),
					-1,MAT(q1,0,0),MAT(q2,1,2),MAT(q2,1,2),		2,MAT(q2,0,1),MAT(q2,0,2),MAT(q1,1,2),
					1,MAT(q2,0,0),MAT(q2,1,1),MAT(q1,2,2),		-2,MAT(q1,0,1),MAT(q2,0,1),MAT(q2,2,2),
					2,MAT(q2,0,1),MAT(q1,0,2),MAT(q2,1,2),		1,MAT(q1,0,0),MAT(q2,1,1),MAT(q2,2,2));
	mqi_coef_linexpr3(a0,6,		1,D,a0,NULL,
					-1,MAT(q1,0,2),MAT(q1,0,2),MAT(q1,1,1),
					-1,MAT(q1,0,1),MAT(q1,0,1),MAT(q1,2,2),
					-1,MAT(q1,0,0),MAT(q1,1,2),MAT(q1,1,2),
					1,MAT(q1,0,0),MAT(q1,1,1),MAT(q1,2,2),
					2,MAT(q1,0,1),MAT(q1,0,2),MAT(q1,1,2));

	mqi_coef_linexpr3(b0,5,		-1,MAT(q2,0,1),MAT(q2,0,1),MAT(q2,2,2),		1,MAT(q2,0,0),MAT(q2,1,1),MAT(q2,2,2),
					-1,MAT(q2,0,0),MAT(q2,1,2),MAT(q2,1,2),		-1,MAT(q2,0,2),MAT(q2,0,2),MAT(q2,1,1),
					2,MAT(q2,0,1),MAT(q2,0,2),MAT(q2,1,2));

	mqi_coef_linexpr3(b0,13,	1,D,b0,NULL,
					-1,MAT(q2,0,0),MAT(q1,1,2),MAT(q1,1,2),		1,MAT(q2,0,0),MAT(q1,1,1),MAT(q1,2,2),
					1,MAT(q1,0,0),MAT(q1,1,1),MAT(q2,2,2),		2,MAT(q1,0,1),MAT(q2,0,2),MAT(q1,1,2),
					2,MAT(q2,0,1),MAT(q1,0,2),MAT(q1,1,2),		1,MAT(q1,0,0),MAT(q2,1,1),MAT(q1,2,2),
					2,MAT(q1,0,1),MAT(q1,0,2),MAT(q2,1,2),		-2,MAT(q1,0,0),MAT(q1,1,2),MAT(q2,1,2),
					-1,MAT(q1,0,1),MAT(q1,0,1),MAT(q2,2,2),		-2,MAT(q1,0,2),MAT(q2,0,2),MAT(q1,1,1),
					-2,MAT(q1,0,1),MAT(q2,0,1),MAT(q1,2,2),		-1,MAT(q1,0,2),MAT(q1,0,2),MAT(q2,1,1));
	
	sign_c2 = qi_coef_sign(a2, b2, D);
	sign_c0 = qi_coef_sign(a0, b0, D);

	/** Inertia [3,0] */
	if ( ((sign_c2 >= 0) && (sign_c0 >= 0)) || ((sign_c2 <= 0) && (sign_c0 <= 0)) )
		inertia_21 = 0;
	/** Inertia [2,1] */
	else 	inertia_21 = 1;
	

inertia_nrconic_end:
	mempool_clear(); 

	return (inertia_21);
	
}


/** Computes the singular locus of a matrix - Output is a matrix of vectors forming
 *  a basis of singular locus.
 */
void qi_mat_singular		(mqi_mat_t __INIT rop, mqi_mat_t op) {
	mqi_mat_kernel(rop,op);	
}

/** Intersects two linear spaces */
void qi_mat_linear_intersect	(mqi_mat_t __INIT rop, mqi_mat_t op1, mqi_mat_t op2) {

	mqi_mat_t	compose;
	mqi_mat_t	kernel, tmp;
	mqi_coef_t	coef;
	int isnull;


	mqi_coef_init_dynamic 		(coef, mqi_mat_typeof(op1));
	mqi_mat_concat_horizontally	(__INIT compose, op1, op2);
	mqi_mat_kernel	    		(__INIT kernel, compose);	

	isnull = 1;

	if ( !mqi_mat_is_zero (COL(kernel,0)) )
			isnull = 0;

	if ( isnull )
		mqi_mat_init_dynamic (rop, mqi_mat_nrows(kernel), 0, mqi_mat_typeof(kernel));	
	else
	{
		mqi_mat_init_dynamic (tmp, mqi_mat_ncols(op1), mqi_mat_ncols(kernel), mqi_mat_typeof(kernel));
		mqi_mat_get_submat (tmp, kernel, 0, 0, mqi_mat_ncols(op1), mqi_mat_ncols(kernel));
		mqi_mat_mul_init  (rop, op1, tmp);
		mqi_mat_clear (tmp);
	}
	
	mqi_coef_clear(coef);
	mqi_mat_list_clear(compose, kernel, NULL);

}

/** Computes a 4x4 projective transformation sending infinity (0 0 0 1) to the point */
void qi_mat_send_to_infinity	(mqi_mat_t __INIT rop, mqi_vect_t point) {
	
	size_t size;
	mqi_mat_t	transpose;
	mqi_mat_t	ker;
	
	size = mqi_vect_size(point);

	mqi_mat_init_dynamic 	(rop, size, size, mqi_vect_typeof(point));

	mqi_mat_cpy_col 	(rop, (mqi_mat_ptr)point, 0, 0);
	mqi_mat_transpose_init  (__INIT transpose, rop);
	mqi_mat_kernel 		(__INIT ker, transpose);

	/** Set the point to the last column */
	mqi_mat_swap_cols (rop, 0, size - 1);
	
	mqi_mat_set_submat (rop, ker, 0, 0);

	mqi_mat_list_clear(transpose, ker, NULL);

}

/** Computes a 4x4 projective transformation sending the line point1-point2 to z = w = 0 */
void qi_mat_send_to_zw		(mqi_mat_t __INIT rop, mqi_vect_t point1, mqi_vect_t point2) {
	
	mqi_mat_t	transpose;
	mqi_mat_t	ker;

	/** rop is persistent: don't call initpool */
	mqi_mat_init_dynamic (rop, 4, 4, mqi_vect_typeof(point1));
	
	/** Set the point at the first column of the transformation matrix */
	mqi_mat_cpy_col (rop, (mqi_mat_ptr)point1, 0, 0);
	mqi_mat_cpy_col (rop, (mqi_mat_ptr)point2, 0, 1);

	mqi_mat_transpose_init	(__INIT transpose, rop);
	mqi_mat_kernel 		(__INIT ker, transpose);

	/** Set the points to the two last columns of the matrix */
	mqi_mat_swap_cols (rop, 0, 2);
	mqi_mat_swap_cols (rop, 1, 3);
	
	/** Complete the matrix with the vectors of the kernel */
	mqi_mat_set_submat (rop, ker, 0, 0);
	
	mqi_mat_list_clear(transpose, ker, NULL);

}


	
/** Gauss reduction of quadratic form (used only in qi_inter_with_22) */
void qi_mat_gauss		(mqi_mat_t __INIT can, mqi_mat_t op, mqi_mat_t __INIT tm) {

	size_t d;
	size_t i, j, k;
	mqi_coef_t tmpi, tmpj;
	mqi_vect_t inertia;
	mqi_mat_t  tmp;

	d = mqi_mat_ncols(op);

	mqi_mat_init_cpy 	(can, op);
	mqi_mat_init_dynamic 	(tmp, d, d, mqi_mat_typeof(op));

	mqi_coef_list_init_dynamic(mqi_mat_typeof(op), tmpi, tmpj, NULL);
	mqi_mat_set_id 		(tmp);

	
	for ( j = 0; j < d; j++ ) { 
	
		k = j + 1;
		while ( (mqi_coef_sign(MAT(can,j,j)) == 0) && (k < d) ) {
			mqi_mat_linexpr_col_int (can, 1, 	1, j,	1, k);
			mqi_mat_linexpr_row_int (can, 1,	1, j,	1, k);
			mqi_mat_linexpr_row_int (tmp, 1,	1, j,	1, k);
			k++;
		}
		
		if ( mqi_coef_sign(MAT(can,j,j)) == 0 ) break;
		
		for ( i = j + 1; i < d; i++ ) {

			mqi_coef_cpy (tmpi, MAT(can,j,j));
			mqi_coef_neg (tmpj, MAT(can,i,j));

			/* "NULL" specifies a divisor equals to one */
			mqi_mat_linexpr_col (can, NULL, tmpi, i,	tmpj, j);
			mqi_mat_linexpr_row (can, NULL,	tmpi, i,	tmpj, j);
			mqi_mat_linexpr_row (tmp, NULL,	tmpi, i,	tmpj, j);

		}
		
	}
	
	qi_mat_signed_inertia (__INIT inertia, can);
	
	for ( i = 0; i < d - 1; i++ ) { 
		for ( j = i + 1; j < d; j++ ) { 
			
			if ( ((mqi_coef_sign(MAT(can,i,i)) == 0) && (mqi_coef_sign(MAT(can,j,j)) == 0)) ||
			     ((mqi_coef_sign(MAT(can,i,i)) < 0) && (mqi_coef_sign(MAT(can,j,j)) > 0) &&
			      (mqi_coef_cmp (VECT(inertia,0), VECT(inertia,1)) >= 0)) ||
			     ((mqi_coef_sign(MAT(can,i,i)) > 0) && (mqi_coef_cmp_schar(MAT(can,j,j), -1) < 0) &&
			      (mqi_coef_cmp (VECT(inertia,1), VECT(inertia,0)) > 0))
			   )
			{
				
				mqi_mat_swap_cols (can, i, j);
				mqi_mat_swap_rows (can, i, j);
				mqi_mat_swap_rows (tmp, i, j);
				
			}
		}	
	}
	
	if ( mqi_coef_sign (MAT(can,0,0)) < 0 ) mqi_mat_neg (can, can);

	mqi_mat_transpose_init (__INIT tm, tmp);
	
	mqi_coef_list_clear(tmpi, tmpj, NULL);
	mqi_mat_clear	   (tmp);

}

