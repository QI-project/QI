#include "qi_elem.h"

/** Sign of the algebraic number a+b*sqrt(c) */
int qi_coef_sign			(mqi_coef_t a, mqi_coef_t b, mqi_coef_t c) {
	
	mqi_coef_t	tmp;
	int result = 0;

	if ( mqi_coef_sign (a) == 0 ) 				return mqi_coef_sign(b);
	if ( mqi_coef_sign (a) > 0 && mqi_coef_sign(b) > 0 )	return 1;
	if ( mqi_coef_sign (a) < 0 && mqi_coef_sign(b) < 0 )	return -1;

	mqi_coef_init_cpy (tmp,  a);
	
	/* a^2 - c*b^2 */
	mqi_coef_linexpr3 (tmp,2,  1,a,a,NULL,	-1,b,b,c);
	
	result = mqi_coef_sign(a) * mqi_coef_sign(tmp);
	mqi_coef_clear(tmp);

	return ( result );
	
}

