#include "qi_elem.h"

/** Compares two projective points */
int qi_point_equals 	(mqi_vect_t point1, mqi_vect_t point2) {

/*	unsigned short k = 0;
	unsigned short size;
	unsigned char  res;
	mqi_coef_t     alpha, beta;
	mqi_vect_t     tmp1, tmp2;

	size = mqi_vect_size(point1);
	
	mqi_coef_init_dynamic (alpha, mqi_vect_typeof(point1));
	
	// Find the first non nul indice: necessarily exists.
	for ( k = 0; k < size; k++ ) { 
		
		mqi_vect_get (alpha, point1, k);
		if ( mqi_coef_sign(alpha) != 0 ) break;
		
	}

	mqi_coef_init_cpy (beta, VECT(point2,k));
	
	mqi_vect_init_cpy (tmp1, point1);
	mqi_vect_init_cpy (tmp2, point2);

	mqi_vect_mul_coef (tmp1, tmp1, beta);
	mqi_vect_mul_coef (tmp2, tmp2, alpha);

	res = mqi_vect_equals (tmp1, tmp2);

	mqi_coef_clear(alpha);
	mqi_coef_clear(beta);
	mqi_vect_clear(tmp1);
	mqi_vect_clear(tmp2);

	return (res);*/
	return mqi_vect_are_multiple(point1, point2);
	
}

/** Tells if k1+sqrt(d)*k2 = (a+sqrt(d)*b)*(k1'+sqrt(d)*k2') */
int qi_are_equal	(mqi_mat_t k1, mqi_mat_t k2, mqi_mat_t k1p, mqi_mat_t k2p, mqi_coef_t d) {
	
	size_t rows;
	size_t cols;
	size_t rank;
	mqi_mat_t      col1, col2, col3, coltmp, tmp, comp;

	rows = mqi_mat_nrows(k1);
	cols = mqi_mat_ncols(k1);
	
	mqi_mat_init_cpy 	(tmp, k2);
	mqi_mat_mul_coef	(tmp, tmp, d);
	
	mqi_mat_concat_vertically	(__INIT col1, k1, k2);
	mqi_mat_concat_vertically	(__INIT col2, tmp, k1);
	mqi_mat_concat_vertically	(__INIT col3, k1p, k2p);
	
	mqi_mat_concat_horizontally	(__INIT coltmp, col1, col2);
	mqi_mat_concat_horizontally	(__INIT comp, coltmp, col3);

	rank = mqi_mat_rank (comp);
	
	mqi_mat_list_clear(col1, col2, col3, coltmp, tmp, comp, NULL);

	if ( rank == 3 ) return 0;
	else		 return 1;
	
}

/** Converts a list of coefficients
 *  (x^2, xy, xz, xw, y^2, yz, yw, z^2, zw, w^2) into a matrix representing
 *  a quadric.
 */
void qi_vect_to_quad 		(mqi_mat_t __INIT rop, __IN mqi_vect_t v) {

	
	/** rop is a persistent data */
	mqi_mat_init_dynamic (rop, 4, 4, mqi_vect_typeof(v));

	if ( mqi_coef_even (VECT(v,1)) && mqi_coef_even (VECT(v,2)) &&
	     mqi_coef_even (VECT(v,3)) && mqi_coef_even (VECT(v,5)) &&
	     mqi_coef_even (VECT(v,6)) && mqi_coef_even (VECT(v,8)) ) {

		mqi_coef_cpy (MAT(rop,0,0), VECT(v,0));
		
		mqi_coef_cpy (MAT(rop,0,1), VECT(v,1));
		mqi_coef_divexact_int (MAT(rop,0,1), MAT(rop,0,1), 2);
		
		mqi_coef_cpy (MAT(rop,1,0), VECT(v,1));
		mqi_coef_divexact_int (MAT(rop,1,0), MAT(rop,1,0), 2);
		
		mqi_coef_cpy (MAT(rop,0,2), VECT(v,2));
		mqi_coef_divexact_int (MAT(rop,0,2), MAT(rop,0,2), 2);
		
		mqi_coef_cpy (MAT(rop,2,0), VECT(v,2));
		mqi_coef_divexact_int (MAT(rop,2,0), MAT(rop,2,0), 2);

		mqi_coef_cpy (MAT(rop,0,3), VECT(v,3));
		mqi_coef_divexact_int (MAT(rop,0,3), MAT(rop,0,3), 2);

		mqi_coef_cpy (MAT(rop,3,0), VECT(v,3));
		mqi_coef_divexact_int (MAT(rop,3,0), MAT(rop,3,0), 2);

		mqi_coef_cpy (MAT(rop,1,1), VECT(v,4));
	
		mqi_coef_cpy (MAT(rop,1,2), VECT(v,5));
		mqi_coef_divexact_int (MAT(rop,1,2), MAT(rop,1,2), 2);

		mqi_coef_cpy (MAT(rop,2,1), VECT(v,5));
		mqi_coef_divexact_int (MAT(rop,2,1), MAT(rop,2,1), 2);

		mqi_coef_cpy (MAT(rop,1,3), VECT(v,6));
		mqi_coef_divexact_int (MAT(rop,1,3), MAT(rop,1,3), 2);

		mqi_coef_cpy (MAT(rop,3,1), VECT(v,6));
		mqi_coef_divexact_int (MAT(rop,3,1), MAT(rop,3,1), 2);

		mqi_coef_cpy (MAT(rop,2,2), VECT(v,7));

		mqi_coef_cpy (MAT(rop,2,3), VECT(v,8));
		mqi_coef_divexact_int (MAT(rop,2,3), MAT(rop,2,3), 2);

		mqi_coef_cpy (MAT(rop,3,2), VECT(v,8));
		mqi_coef_divexact_int (MAT(rop,3,2), MAT(rop,3,2), 2);

		mqi_coef_cpy (MAT(rop,3,3), VECT(v,9));
		
	}
	else {

		mqi_coef_cpy (MAT(rop,0,0), VECT(v,0));
		mqi_coef_mul_int (MAT(rop,0,0), MAT(rop,0,0), 2);
		
		mqi_coef_cpy (MAT(rop,0,1), VECT(v,1));
		
		mqi_coef_cpy (MAT(rop,1,0), VECT(v,1));
		
		mqi_coef_cpy (MAT(rop,0,2), VECT(v,2));
		
		mqi_coef_cpy (MAT(rop,2,0), VECT(v,2));

		mqi_coef_cpy (MAT(rop,0,3), VECT(v,3));

		mqi_coef_cpy (MAT(rop,3,0), VECT(v,3));

		mqi_coef_cpy (MAT(rop,1,1), VECT(v,4));
		mqi_coef_mul_int (MAT(rop,1,1), MAT(rop,1,1), 2);
	
		mqi_coef_cpy (MAT(rop,1,2), VECT(v,5));

		mqi_coef_cpy (MAT(rop,2,1), VECT(v,5));

		mqi_coef_cpy (MAT(rop,1,3), VECT(v,6));

		mqi_coef_cpy (MAT(rop,3,1), VECT(v,6));

		mqi_coef_cpy (MAT(rop,2,2), VECT(v,7));
		mqi_coef_mul_int (MAT(rop,2,2), MAT(rop,2,2), 2);
		
		mqi_coef_cpy (MAT(rop,2,3), VECT(v,8));

		mqi_coef_cpy (MAT(rop,3,2), VECT(v,8));

		mqi_coef_cpy (MAT(rop,3,3), VECT(v,9));
		mqi_coef_mul_int (MAT(rop,3,3), MAT(rop,3,3), 2);

	}
	
}

/** Converts a matrix representing a quadric into a list of coefficients */
void qi_quad_to_vect		(mqi_vect_t __INIT rop, mqi_mat_t __IN matrix) {


	/** rop is a persistent data */
	mqi_vect_init_dynamic (rop, 10, mqi_mat_typeof(matrix));

	mqi_coef_cpy (VECT(rop,0), MAT(matrix,0,0));
	
	mqi_coef_cpy (VECT(rop,1), MAT(matrix,0,1));
	mqi_coef_add (VECT(rop,1), VECT(rop,1), VECT(rop,1));

	mqi_coef_cpy (VECT(rop,2), MAT(matrix,0,2));
	mqi_coef_add (VECT(rop,2), VECT(rop,2), VECT(rop,2));

	mqi_coef_cpy (VECT(rop,3), MAT(matrix,0,3));
	mqi_coef_add (VECT(rop,3), VECT(rop,3), VECT(rop,3));

	mqi_coef_cpy (VECT(rop,4), MAT(matrix,1,1));

	mqi_coef_cpy (VECT(rop,5), MAT(matrix,1,2));
	mqi_coef_add (VECT(rop,5), VECT(rop,5), VECT(rop,5));

	mqi_coef_cpy (VECT(rop,6), MAT(matrix,1,3));
	mqi_coef_add (VECT(rop,6), VECT(rop,6), VECT(rop,6));

	mqi_coef_cpy (VECT(rop,7), MAT(matrix,2,2));

	mqi_coef_cpy (VECT(rop,8), MAT(matrix,2,3));
	mqi_coef_add (VECT(rop,8), VECT(rop,8), VECT(rop,8));

	mqi_coef_cpy (VECT(rop,9), MAT(matrix,3,3));

}

