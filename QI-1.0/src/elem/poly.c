#include "qi_elem.h"

/** Descartes: count number of sign changes in sequence */
int qi_poly_descartes (mqi_poly_t poly) {

	int count = 0;
	int sign_old   = 0;
	int sign_new   = 0;
	size_t k;
	

	for ( k = 0; k <= mqi_poly_highest_x_degree(poly); k++ ) { 

		if ( ! sign_old ) { sign_old = mqi_coef_sign(POLY(poly,k)); continue; }
		
		sign_new = mqi_coef_sign(POLY(poly,k));

		if ( (sign_new != 0) && (sign_new * sign_old < 0) ) {
			sign_old = sign_new;
			count ++;
		}	
	
	}

	return (count);
	
}

