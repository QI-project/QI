#include "qi_number.h"
#include "qi_utils.h"

/** Optimizes by dividing each column by its content */
void qi_mat_optimize (mqi_mat_t matrix) {

	mqi_coef_t content;
	mqi_mat_ptr col;
	size_t j;

	mqi_coef_init_dynamic 		(content, mqi_mat_typeof(matrix));
	
	for ( j = 0; j < mqi_mat_ncols(matrix); j++ )
	{
		col = COL(matrix, j);
		mqi_vect_content(content,(mqi_vect_ptr)col);
		mqi_vect_divexact_coef((mqi_vect_ptr)col, (mqi_vect_ptr)col, content);
	}

	mqi_coef_clear(content);

}

/** optimizes by dividing each matrix column by the gcd of the contents
 *  of the corresponding matrix columns. */
void qi_mat_optimize_2 (mqi_mat_t matrix1, mqi_mat_t matrix2)
{

	mqi_coef_t content;
	mqi_coef_t content2;
	mqi_mat_ptr col, col2;
	size_t j;

	mqi_coef_list_init_dynamic 	(mqi_mat_typeof(matrix1), content, content2, NULL);
	
	for ( j = 0; j < mqi_mat_ncols(matrix1); j++ ) { 

		col = COL(matrix1, j);
		mqi_vect_content(content, (mqi_vect_ptr)col);

		col2 = COL(matrix2, j);
		mqi_vect_content(content2, (mqi_vect_ptr)col2);

		mqi_coef_gcd		(content, content, content2);
		
		mqi_mat_divexact_coef (col, col, content);
		mqi_mat_divexact_coef (col2, col2, content);

	}

	mqi_coef_list_clear(content, content2, NULL);

}
	
/** Optimize by dividing columns 1, 2, 3 by a, b, c such that a b = c^2
 *  Divide remaining columns by their content.
 *  Update additional line values in "line" */
void qi_mat_optimize_3_update (mqi_mat_t matrix, mqi_vect_t line) {

	mqi_coef_t A, B, C;
	mqi_coef_t gcdAB, gcdABC;
	mqi_coef_t a0, b0;
	mqi_coef_t alpha, beta;
	mqi_coef_t a, b, c;
	mqi_coef_t coef;
	mqi_coef_t content;
	size_t j;
	
	mqi_coef_list_init_dynamic (mqi_mat_typeof(matrix),
			   	    A, B, C, gcdAB, gcdABC, a0, b0, alpha, beta, a, b, c, coef, content, NULL);
	

	mqi_vect_content(A, (mqi_vect_ptr)COL(matrix,0));
	mqi_vect_content(B, (mqi_vect_ptr)COL(matrix,1));
	mqi_vect_content(C, (mqi_vect_ptr)COL(matrix,2));
	mqi_coef_gcd   (gcdAB, A, B);

	mqi_coef_divexact     (A, A, gcdAB);
	mqi_coef_divexact     (B, B, gcdAB);

	/* coef holds the cofactor, not used in what follows */	
	qi_coef_extract_square_factors (a0, coef, A);
	qi_coef_extract_square_factors (b0, coef, B);	

	mqi_coef_gcd	      (gcdABC, gcdAB, C);
	mqi_coef_divexact     (C, C, gcdABC);
	mqi_coef_gcd	      (alpha, a0, C);
	mqi_coef_gcd	      (beta,  b0, C);

	mqi_coef_linexpr3 (a, 1,   1,alpha,alpha,gcdABC); /* alpha^2*gcdABC    */
	mqi_coef_linexpr3 (b, 1,   1,beta,beta,gcdABC);   /* beta^2*gcdABC     */
	mqi_coef_linexpr3 (c, 1,   1,alpha,beta,gcdABC);  /* alpha*beta*gcdABC */

	/** Simplification occurs only when c != 1 */
	if ( mqi_coef_cmp_schar (c, 1) != 0 ) {

		mqi_mat_divexact_coef(COL(matrix,0), COL(matrix,0), a);
		mqi_mat_divexact_coef(COL(matrix,1), COL(matrix,1), b);
		mqi_mat_divexact_coef(COL(matrix,2), COL(matrix,2), c);

	}
			
	/** Divide remaining columns by their content */
	for ( j = 3; j < mqi_mat_ncols(matrix); j++ ) { 
	
		mqi_vect_content(content, (mqi_vect_ptr)COL(matrix,j));
		mqi_mat_divexact_coef(COL(matrix,j),COL(matrix,j),content);
	
	}

	mqi_vect_get_at (coef, line, 0);
	mqi_coef_mul (coef, coef, c);
	mqi_vect_set_at (line, 0, coef);

	mqi_vect_get_at (coef, line, 1);
	mqi_coef_mul (coef, coef, a);
	mqi_vect_set_at (line, 1, coef);

	/** Free local memory */
	mqi_coef_list_clear (A, B, C, gcdAB, gcdABC, a0, b0, alpha, beta, a, b, c, coef, content, NULL);

}

/** Optimize by dividing columns 1, 2, 3 by a, b, c such that a b = c^2
 *  Divide remaining columns by their content */
void qi_mat_optimize_3 (mqi_mat_t matrix) {

	mqi_vect_t vect;

	mqi_vect_init_dynamic (vect, mqi_mat_nrows(matrix), mqi_mat_typeof(matrix));
	qi_mat_optimize_3_update (matrix, vect);

	mqi_vect_clear(vect);

}


/** Load balancing of columns of a matrix such that a*b = c*d
 *  Configuration:
 *
 *  	0: Balance only the two first columns
 *  	1: Balance the four columns
 *
 *  	(matrices are supposed to be 4x4)
 */
void qi_mat_load_balancing (mqi_mat_t matrix, const int configuration) {

	qi_vect_load_balancing ((mqi_vect_ptr)COL(matrix,0), (mqi_vect_ptr)COL(matrix,1));
		
	if ( configuration == 1 )
		qi_vect_load_balancing ((mqi_vect_ptr)COL(matrix,2), (mqi_vect_ptr)COL(matrix,3));
	
}

/** Optimize by dividing columns 1, 2, 3 by a, b, c such that a b = c^2
 *  Divide remaining columns by their content.
 *  Here the two matrices should be divided simultaneously */
void qi_mat_optimize_4 (mqi_mat_t matrix1, mqi_mat_t matrix2) {

	mqi_coef_t A, B, C;
	mqi_coef_t gcdAB, gcdABC;
	mqi_coef_t a0, b0;
	mqi_coef_t alpha, beta;
	mqi_coef_t a, b, c;
	mqi_coef_t coef;
	mqi_coef_t content;
	mempool_t pool;
	size_t j;	

	mempool_init (pool);

	mqi_coef_list_initpool_dynamic (mqi_mat_typeof(matrix1),
			A, B, C, gcdAB, gcdABC, a0, b0, alpha, beta, a, b, c, coef, content, NULL);

	mqi_vect_content (A, (mqi_vect_ptr)COL(matrix1,0));
	mqi_vect_content (B, (mqi_vect_ptr)COL(matrix2,0));
	mqi_coef_gcd(A, A, B);

	mqi_vect_content (B, (mqi_vect_ptr)COL(matrix1,1));
	mqi_vect_content (C, (mqi_vect_ptr)COL(matrix2,1));
	mqi_coef_gcd(B, B, C);

	mqi_vect_content (C, (mqi_vect_ptr)COL(matrix1,2));
	mqi_vect_content (coef, (mqi_vect_ptr)COL(matrix2,2));
	mqi_coef_gcd(C, C, coef);

	mqi_coef_gcd(gcdAB, A, B);

	mqi_coef_divexact(A, A, gcdAB);
	mqi_coef_divexact(B, B, gcdAB);

	/* Note that "coef" holds the cofactor of the decomposition,
	 * and we do not use it in what follows.
	 */
	qi_coef_extract_square_factors (a0, coef, A);
	qi_coef_extract_square_factors (b0, coef, B);

	mqi_coef_gcd(gcdABC, gcdAB, C);
	mqi_coef_divexact(C, C, gcdABC);
	mqi_coef_gcd(alpha, a0, C);
	mqi_coef_gcd(beta,  b0, C);

	mqi_coef_linexpr3 (a, 1,   1,alpha,alpha,gcdABC); /* alpha^2*gcdABC    */
	mqi_coef_linexpr3 (b, 1,   1,beta,beta,gcdABC);   /* beta^2*gcdABC     */
	mqi_coef_linexpr3 (c, 1,   1,alpha,beta,gcdABC);  /* alpha*beta*gcdABC */

	/** Simplification occurs only when c != 1 */
	if ( mqi_coef_cmp_schar (c, 1) != 0 ) {

		mqi_mat_divexact_coef(COL(matrix1,0), COL(matrix1,0), a);
		mqi_mat_divexact_coef(COL(matrix1,1), COL(matrix1,1), b);
		mqi_mat_divexact_coef(COL(matrix1,2), COL(matrix1,2), c);

		mqi_mat_divexact_coef(COL(matrix2,0), COL(matrix2,0), a);
		mqi_mat_divexact_coef(COL(matrix2,1), COL(matrix2,1), b);
		mqi_mat_divexact_coef(COL(matrix2,2), COL(matrix2,2), c);

	}
			
	/** Divide remaining columns by their content */
	for ( j = 3; j < mqi_mat_ncols(matrix1); j++ ) { 

		mqi_vect_content(content, (mqi_vect_ptr)COL(matrix1,j));
		mqi_vect_content(A,	  (mqi_vect_ptr)COL(matrix2,j));
		mqi_coef_gcd   (content, content, A);
	
		mqi_mat_divexact_coef(COL(matrix1,j),COL(matrix1,j),content);
		mqi_mat_divexact_coef(COL(matrix2,j),COL(matrix2,j),content);
		
	}
	
	mempool_clear();

}

/* Function used in __qi_inter_two_mult_four_skew_lines_non_rational.
 * Calculates:
 *
 * 	alpha * m' * p * q
 *
 */
void qi_mat_transformation (mqi_mat_t __INIT rop, mqi_coef_t alpha, mqi_mat_t __IN m, mqi_mat_t __IN p, mqi_mat_t __IN q)
{

	mqi_mat_t trans;
	mqi_mat_t tmpmat;

	mqi_mat_transpose_init(trans,m);
	mqi_mat_mul_init(tmpmat,trans,p);
	mqi_mat_mul_init(rop,tmpmat,q);
	mqi_mat_mul_coef(rop,rop,alpha);
	mqi_mat_list_clear(trans,tmpmat,NULL);

}

/* Same with alpha as an int */
void qi_mat_transformation_int (mqi_mat_t __INIT rop, int alpha, mqi_mat_t __IN m, mqi_mat_t __IN p, mqi_mat_t __IN q)
{

	mqi_mat_t trans;
	mqi_mat_t tmpmat;

	mqi_mat_transpose_init(trans,m);
	mqi_mat_mul_init(tmpmat,trans,p);
	mqi_mat_mul_init(rop,tmpmat,q);
	mqi_mat_mul_int(rop,rop,alpha);
	mqi_mat_list_clear(trans,tmpmat,NULL);

}


