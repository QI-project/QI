#include "qi_number.h"

/** Extracts the square factors of a bigint:
 *
 * 	number = a^2 * b
 *
 * Optimization level is managed through a global variable */
void qi_coef_extract_square_factors (mqi_coef_t __INIT square, mqi_coef_t __INIT cofactor, mqi_coef_t number) {

	mqi_coef_t   coef;
	mqi_coef_ptr prime_cofactor;
	mqi_prime_factorization_t pf;
	long maxfactor;
        unsigned short * bases;
	unsigned char  * exponents;
	mqi_coef_t     tmp_base;
	size_t n_factors;
	size_t k;
	unsigned long  tmp;

	mqi_coef_list_init_dynamic(mqi_coef_typeof(number), square, cofactor, coef, NULL);

	mqi_coef_set_int     (cofactor, mqi_coef_sign(number));
	mqi_coef_abs	     (coef, number);
	
	/** Is "number" a perfect square already ? */
	if ( mqi_coef_is_square (coef) ) {
		mqi_coef_sqrt (square, coef);
		mqi_coef_clear(coef);
		return;
	}

	/** Else, we need to extract the square factors */
	mqi_coef_set_schar (square, 1);
	mqi_coef_cbrt (coef, coef);

	/** Ceiling of the cubic root */
	mqi_coef_add_schar (coef, coef, 1);	

	if ( mqi_coef_cmp_sshort (coef, qi_settings.maxfactor) < 0 )
	{
		maxfactor = mqi_coef_get_slong(coef);
	}
	else
	{
		maxfactor = qi_settings.maxfactor;
	}

	mqi_prime_factorization_init(pf);

	mqi_primefactor (pf, number, maxfactor);

	bases 		= mqi_prime_factorization_get_factors(pf);
	exponents	= mqi_prime_factorization_get_exponents(pf);
	n_factors	= mqi_prime_factorization_size(pf);
	prime_cofactor	= mqi_prime_factorization_get_cofactor(pf);

	mqi_coef_init_dynamic (tmp_base, mqi_coef_typeof(coef));
	
	for ( k = 0; k < n_factors; k++ ) { 
	
		mqi_coef_set_slong (tmp_base, (slong)bases[k]);

		tmp = exponents[k];
		
		/** Even exponent */
		if ( ! (tmp % 2) ) {
			/*if ( tmp != 2 )	tmp /= 2;*/
			tmp /= 2;

			mqi_coef_pow (coef, tmp_base, tmp);
		}
		/** Odd exponent */
		else {
			if ( tmp != 1 ) {
			
				tmp --;			
				tmp /= 2;
				mqi_coef_pow	   (coef, tmp_base, tmp);

			}
			mqi_coef_mul (cofactor, cofactor, tmp_base);
			continue;	

		}

		mqi_coef_mul (square, square, coef);		
		
	}
	
	/** If a cofactor has been detected, try to extract squares from it */
	if ( mqi_coef_cmp_schar (prime_cofactor, 1) != 0 ) {
				
		if ( mqi_coef_is_square (prime_cofactor) ) {
			mqi_coef_sqrt (coef, prime_cofactor);			
			mqi_coef_mul (square, square, coef);
		}
		else 	{
			mqi_coef_mul (cofactor, cofactor, prime_cofactor);	
		}
	}

	mqi_coef_list_clear(coef, tmp_base, NULL);
	mqi_prime_factorization_clear(pf);

}

