#include "qi_parser.h"

char *trim (const char *str, size_t n) {
        
        size_t k,l;
        char *out; 
                
        out = (char *)calloc( n+1, sizeof(char) );
                
        for ( k = 0, l = 0; k < n; k++ ) {
                        
                if ( (str[k] != ' ') &&
                     (str[k] != '\t') ) {
                        out[l++] = str[k];
		}

        }

	out[l] = '\0';

	return out;

}

