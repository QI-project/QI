#include "qi_parser.h"
#include <mqi/mqi_log.h>

/** Invokes the core parser to recognize an homogeneous (or not) polynomial */
void _qi_parse_equation (mqi_vect_ptr rop, const char *equation_desc) {

	unsigned short k;
	char * modifiable;

	/** Need to have a modifiable copy of "equation_desc" */
	modifiable = strdup ((char *)equation_desc);

	parser_set_input_stream (modifiable);
	parser_set_error_stream (&error[0]);
	parser_reset ();
	parser_parse ();

	ON_ERROR ( mqi_log_error ("An error occured during parsing.\n"); return; );

	mqi_vect_init_mpz_t (rop, 10);

	for ( k = 0; k < 10; k++ )
		mqi_coef_set_str (VECT(rop,k), coefficients[k]);
	
	return;

}

