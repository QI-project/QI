#include "qi_backend_t.h"

/* Special symbols bindings */
static const char * const backend_symbols[] =
 {
	"&real",			/* Example: "&real" in html, "\mathbb{R}" in latex */
	"<b>C</b>",			/* Example: "\mathbb{C}" in latex */
	"&cup",				/* Example: "&cup" in html */
	"&infin",			/* Example: "&infin" in html, "\infty" in latex */
 };

/* You *cannot* nest different styles, there're
 * exclusive.
 */

/* The following enumeration is used to manage
 * markup languages like html, for which it is
 * necessary to declare the end of a section with
 * a specific style, for example:
 * <b>BOLD TEXT</b>
 */
typedef enum
 {

	STYLE_DEFAULT,	/* In default style section */
	STYLE_MATH,	/* In math style section */
	STYLE_INFO,	/* In info style section */
	STYLE_NONE	/* Not in a section (all markups are closed) */

 } Style;

static Style current_style = STYLE_NONE;

static void style_close_if_needed (const char * const close_str)
{
	if ( current_style != STYLE_NONE )
	{
		mqi_log_printf("%s", close_str);
		current_style = STYLE_NONE;
	}
}


static void begin_document (void)
{
	mqi_log_printf (
	"<html>\n"
	" <head>\n"
	"  <link rel=\"stylesheet\" type=\"text/css\" href=\"style_output.css\">\n"
	" <body>"
	);
	current_style = STYLE_NONE;
}

static void end_document (void)
{
	style_close_if_needed("</b>");
	mqi_log_printf (
	" </body>\n"
	"</html>"
	);
}

static void style_default (void)
{
	/* The previous code in QI 0.9
	 * was performing a substitution
	 * of every '\n' in '<br>'.
	 * But the new way to handle output
	 * is a state machine so this
	 * conversion will have to be done
	 * by the web script collecting
	 * the output. */
	style_close_if_needed("</b>");
	current_style = STYLE_DEFAULT;
	mqi_log_printf ("<b class=\"default\">");
}

static void style_math (void)
{
	style_close_if_needed("</b>");
	current_style = STYLE_MATH;
	mqi_log_printf("<b class=\"math\">");
}
static void style_info (void)
{
	style_close_if_needed("</b>");
	current_style = STYLE_INFO;
	mqi_log_printf("<b class=\"info\">");
}
static void exponent (const char * const text)
{
	mqi_log_printf ("<sup>%s</sup>", text);
}

static void symbol (qi_backend_sym sym)
{
	if ( sym > SYM_INFINITE )
	{
		fprintf (stderr,
		"%s error: invalid special symbol code (%d)\n",
		__func__, sym);
	}
	mqi_log_printf ("%s", backend_symbols[sym]);
}

static void newline (void)
{
	mqi_log_printf ("<br>");
}

static void section (const char * const text)
{
	mqi_log_printf ("<div class=\"section\">%s</div>\n", text);
}
static void subsection (const char * const text)
{
	mqi_log_printf ("<div class=\"subsection\">%s</div>\n", text);
}


/* Static initialization of the <name> backend
 * structure.
 */
DEF_BACKEND(html);

