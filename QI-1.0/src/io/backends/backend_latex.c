#include "qi_backend_t.h"

/* Special symbols bindings */
static const char * const backend_symbols[] =
 {
	"\\mathbb{R}",			/* Example: "&real" in html, "\mathbb{R}" in latex */
	"\\mathbb{C}",			/* Example: "\mathbb{C}" in latex */
	"\\cup",			/* Example: "&cup" in html */
	"\\infty",			/* Example: "&infin" in html, "\infty" in latex */
 };

/* You *cannot* nest different styles, there're
 * exclusive.
 */

/* The following enumeration is used to manage
 * markup languages like html, for which it is
 * necessary to declare the end of a section with
 * a specific style, for example:
 * <b>BOLD TEXT</b>
 */
typedef enum
 {

	STYLE_DEFAULT,	/* In default style section */
	STYLE_MATH,	/* In math style section */
	STYLE_INFO,	/* In info style section */
	STYLE_NONE	/* Not in a section (all markups are closed) */

 } Style;

static Style current_style = STYLE_NONE;

static void style_close_if_needed (const char * const close_str)
{
	if ( current_style != STYLE_NONE )
	{
		mqi_log_printf("%s", close_str);
		current_style = STYLE_NONE;
	}
}

static void begin_document (void)
{
	current_style = STYLE_NONE;
}

/* In what follows, an example of <close_string>
 * is "</b>" in html
 */
static void end_document (void)
{
	style_close_if_needed("");
}


static void style_default (void)
{
	style_close_if_needed("");
	current_style = STYLE_DEFAULT;
}	

static void style_math (void)
{
	style_close_if_needed("");
	current_style = STYLE_MATH;
}

static void style_info (void)
{
	style_close_if_needed("");
	current_style = STYLE_INFO;
}

static void exponent (const char * const text)
{

}

static void symbol (qi_backend_sym sym)
{
	if ( sym > SYM_INFINITE )
	{
		fprintf (stderr,
		"%s error: invalid special symbol code (%d)\n",
		__func__, sym);
	}
	mqi_log_printf ("%s", backend_symbols[sym]);
}

static void newline (void)
{

}

static void section (const char * const text)
{

}
static void subsection (const char * const text)
{

}


/* Static initialization of the <name> backend
 * structure.
 */
DEF_BACKEND(latex);

