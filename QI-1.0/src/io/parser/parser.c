#include "parser.h"

char * token		= NULL;

char * input_str 	= NULL;
char * error_str	= NULL;
size_t input_length	= 0;

unsigned short _current_symbol_index = 0;

/** Current indice in the token string */
unsigned short token_indice	     = 0;

/** Number of memory blocks occupied by "token" */
unsigned short token_n_blocks  	     = 0;

/** Are we interested in storing the current 
 *  read characters inside "token" ? */
unsigned char tokenize		     = 0;

/** Reinitializes every variable involved in the parser, except
 *  the streams */
void parser_reset (void) {

	_current_symbol_index = 0;
	token_indice	      = 0;
	token_n_blocks	      = 0;
	tokenize	      = 0;

	if ( token ) { free ( token ); token = NULL; }
}


/** Passes the parser a filename instead of a char pointer.
 *  Uses open and mmap
 */
void parser_set_input_filename (const char *filename) {

	int fdin;
	struct stat s;
	
	if ((fdin = open (filename, O_RDONLY)) < 0) {
		fprintf (stderr, "Cannot open file: %s for reading\n", filename);
		exit (EXIT_FAILURE);
	}

	if (fstat (fdin,&s) < 0) {
		fprintf (stderr, "Cannot stat file: %s\n", filename);
		exit (EXIT_FAILURE);
	}

	input_length = s.st_size;
	
	if ((input_str = mmap (0, input_length+1, PROT_READ, MAP_SHARED, fdin, 0))
			   == (caddr_t) -1) {
		fprintf (stderr, "Cannot map file: %s in memory\n", filename);
		exit (EXIT_FAILURE);
	}

	
}

/** Same for the error */
void parser_set_error_filename (const char *filename) {

	int fdout;
	struct stat s;
	
	if ((fdout = open (filename, O_CREAT|O_APPEND)) < 0) {
		fprintf (stderr, "Cannot open file: %s for writing\n", filename);
		exit (EXIT_FAILURE);
	}

	if (fstat (fdout,&s) < 0) {
		fprintf (stderr, "Cannot stat file: %s\n", filename);
		exit (EXIT_FAILURE);
	}

	input_length = s.st_size;
	
	if ((error_str = mmap (0, input_length, PROT_READ, MAP_SHARED, fdout, 0))
			   == (caddr_t) -1) {
		fprintf (stderr, "Cannot map file: %s in memory\n", filename);
		exit (EXIT_FAILURE);
	}
	
}


void parser_parse_terminal_string(char * stringptr) {

	while ( *stringptr != '\0' ) {			
		parser_parse_terminal(*stringptr);	
		ON_ERROR ( break; );			
		stringptr ++;	
	}

}

