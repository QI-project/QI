#include "qi_param.h"


/** ************** */
/** Local routines */
/** ************** */


/** Parameterizes a 2 x 2 matrix when the determinant is 0. */
void _qi_param_2x2sing                   (mqi_mat_t __IN q, mqi_mat_t m1) {

	mqi_coef_t a, b, c;

	mqi_coef_init_cpy (a, MAT(q,0,0));
	mqi_coef_init_cpy (b, MAT(q,0,1));
	mqi_coef_init_cpy (c, MAT(q,1,1));
	mqi_coef_neg      (b, b);

	/** $$ Let m1 and m2 be initialized before calling this function */
	/*   $ mqi_mat_init_dynamic (m1, 2, 2, mqi_mat_typeof(q));*/

	if ( mqi_coef_sign(a) != 0 ) {
		mqi_mat_set_at  (m1, 0, 0, b);
		mqi_mat_set_at  (m1, 1, 0, a);
	}else {
		mqi_mat_set_at  (m1, 0, 0, c);
		mqi_mat_set_at  (m1, 1, 0, b);
	}

	mqi_coef_list_clear (a, b, c, NULL);

}


/** *************** */
/** Public routines */
/** *************** */



/** Plugs a surface_param in the equation of a quadric */
void qi_param_plug_sp_in_quadric        (mqi_hhpoly_t __INIT rop, mqi_surface_param_t __IN s1,
					 mqi_mat_t __IN q, mqi_surface_param_t __IN s2) {

	mqi_surface_param_t tmp;

	mqi_surface_param_init_cpy (tmp, s2);
	mqi_surface_param_mul_mat  (tmp, q, s2);

	mqi_hhpoly_init_cpy	   (rop, SP(s1,0)); 

	mqi_surface_param_scalar_product (rop, s1, tmp);

	mqi_surface_param_clear(tmp);

}

/** Plugs a curve_param in the equation of a quadric */
void qi_param_plug_cp_in_quadric        (mqi_hpoly_t __INIT rop, mqi_curve_param_t __IN c1, mqi_mat_t __IN q, mqi_curve_param_t __IN c2) {

	mqi_curve_param_t tmp;

	mqi_curve_param_init_dynamic (tmp, mqi_mat_ncols(q), mqi_mat_typeof(q));
	mqi_curve_param_mul_mat	     (tmp, q, c2);

	mqi_poly_init_cpy	     (rop, CP(c2,0));

	mqi_curve_param_scalar_product(rop, c1, tmp);

	mqi_curve_param_clear(tmp);

}

/** Computes the parameterization of 2 x 2 matrix, used for lines and planes
  * Output is m1*[u] +/- sqrt(D)*m2*[u] if D is not a square
  * Output is m1*[u] +/- m2*[u] if D is a square
  * When b^2-ac is a square, D is set to 0 */
void qi_param_2x2                       (mqi_mat_t __IN q, mqi_coef_t __INIT D, mqi_mat_t m1, mqi_mat_t m2)
{

	mqi_coef_t a, b, c, g;
	mqi_coef_t tmp, sq, cofactor;

	mqi_coef_init_cpy (a, MAT(q,0,0));
	mqi_coef_init_cpy (b, MAT(q,0,1));
	mqi_coef_init_cpy (c, MAT(q,1,1));
	mqi_coef_init_dynamic (g, mqi_coef_typeof(a));

	/** $$ Let m1 and m2 be initialized before calling this function     */
	/* $mqi_mat_init_dynamic (m1, 2, 2, mqi_mat_typeof(q));
	   $ mqi_mat_init_dynamic (m2, 2, 2, mqi_mat_typeof(q));*/

	/** gcd(a,b,c) */
	mqi_coef_gcd (g,a,b);
	mqi_coef_gcd (g,g,c);

	mqi_coef_divexact (a, a, g);
	mqi_coef_divexact (b, b, g);
	mqi_coef_divexact (c, c, g);

	/** D = b^2-ac */
	mqi_coef_init_cpy (tmp, a);
	
	/** D is a persistent data */
	mqi_coef_init_cpy (D, b);

	mqi_coef_mul (D, D, D);
	mqi_coef_mul (tmp, tmp, c);
	mqi_coef_sub (D, D, tmp);

	mqi_coef_neg (b,b);

	if ( mqi_coef_is_square(D) ) {
		mqi_coef_init_cpy (sq, D);
		mqi_coef_sqrt	  (sq, D);
		mqi_coef_set_schar(D, 0);

		if ( mqi_coef_sign(a) != 0 ) {
			mqi_mat_set_at (m1,0,0,b);
			mqi_mat_set_at (m1,1,0,a);
			mqi_mat_set_at (m2,0,0,sq);
		}
		else if ( mqi_coef_sign(c) != 0 ) {
			mqi_mat_set_at (m1,0,0,c);
			mqi_mat_set_at (m1,1,0,b);
			mqi_mat_set_at (m2,1,0,sq);
		}
		else {
			mqi_mat_set_at_int (m1,0,0,1);
			mqi_mat_set_at_int (m1,1,0,1);
			mqi_mat_set_at_int (m2,0,0,1);
			mqi_mat_set_at_int (m2,1,0,-1);
		}

	}else {
		mqi_coef_init_dynamic (cofactor, mqi_coef_typeof(a));
		mqi_coef_init_dynamic (sq, mqi_coef_typeof(a));
		qi_coef_extract_square_factors (sq, cofactor, D);
		mqi_coef_cpy (D, cofactor);

		if ( mqi_coef_sign(a) != 0 ) {
			mqi_mat_set_at (m1,0,0,b);
			mqi_mat_set_at (m1,1,0,a);
			mqi_mat_set_at (m2,0,0,sq);
		}
		else {
			mqi_mat_set_at(m1,0,0,c);
			mqi_mat_set_at(m1,1,0,b);
			mqi_mat_set_at(m2,1,0,sq);
		}
	        mqi_coef_clear(cofactor);
	}

	mqi_coef_list_clear(a, b, c, g, tmp, sq, NULL);

}

