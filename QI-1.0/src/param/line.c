#include "qi_param.h"

/** Computes the parameterization of a pair of lines in the plane
  * Output is m1*[u v] +/- sqrt(D)*m2*[u] if D is non zero
  * Output is m1*[u v] +/- m2*[u] otherwise */
void qi_param_line_pair                 (mqi_mat_t __IN q, mqi_vect_t __IN sing, mqi_coef_t __INIT D, mqi_mat_t m1, mqi_mat_t m2)
{
	
	mqi_mat_t p_trans, q_r, q_r22;

	/** Rotates the plane by sending one of its singular line to infinity */
	qi_mat_send_to_infinity (p_trans, sing);

	mqi_mat_transformation (q_r, q, p_trans, MQI_MAT_NOTRANSCO);

	/** Resizes to a 2D matrix */
	mqi_mat_resize (q_r22, q_r, 2, 2);

	/** Parametrizes the reduced 2D matrix */
	qi_param_2x2 (q_r22, __INIT D, m1, m2);

	/** $$ Bug here: destination matrix is not sized properly ! */
	/** Augments the result and stacks the transformation matrices
	 * Adds a "1" at the right places in m1 for the two new variables of "par" */
	/** $$ I don't know what the previous comment refers to.
	 *     The problem is the word "augment" from the original comment of S.Petitjean
	 */
	mqi_mat_set_at_int (m1, 2, 1, 1);

	mqi_mat_mul (m1, p_trans, m1);
	mqi_mat_mul (m2, p_trans, m2);

	mqi_mat_list_clear(p_trans, q_r, q_r22, NULL);

}

/** Computes the parameterization of a double line
  * Output is m1*[u u] */
void qi_param_line_double               (mqi_mat_t __IN q, mqi_mat_t __IN sing, mqi_mat_t m1) {

	mqi_mat_t p_trans, q_r, q_r22;

	/** Rotates the plane by sending one of its singular line to infinity */
	qi_mat_send_to_infinity (__INIT p_trans, (mqi_vect_ptr)COL(sing,0));

	mqi_mat_transformation (q_r, q, p_trans, MQI_MAT_NOTRANSCO);

	/** Resizes to a 2D matrix */
	mqi_mat_resize (q_r22, q_r, 2, 2);

	/** Parametrizes the reduced 2D matrix */
	_qi_param_2x2sing (__IN q_r22, m1);

	/** $$ Bug here: destination matrix not sized properly ! */
	/** Augments the result and stacks the transformation matrices
	 * Adds a "1" at the right places in m1 for the two new variables of "par" */
	mqi_mat_set_at_int (m1, 2, 1, 1);

	mqi_mat_mul (m1, p_trans, m1);

	mqi_mat_list_clear(p_trans, q_r, q_r22, NULL);

}

/** Attempts to reparameterize a rational line by taking as "endpoints" (ie. the points
  * (u,v) = (1,0) and (0,1)) points on the planes x = 0, y = 0, z = 0, w = 0. 
  * In other words, one of the coordinates is 0 when u=0 and another is zero when
  * v=0. This assumes that the current endpoints of line_par have already been
  * optimized. Otherwise p0 and end1 (say) might represent the same point but with
  * different scales.
  * Returns the value of the new parameters corresponding to the first endpoint of
  * the old parameterization */
void qi_param_line_improved1            (mqi_vect_t __INIT rop, mqi_curve_param_t line_par) {

	mqi_vect_t p[4];
	mqi_vect_t end1, end2, tmpvect;
	mqi_coef_t c[4];
	mqi_coef_t sum1, sum2;
	mqi_coef_t u1, u2, v1, v2, co1, co2;
	mqi_coef_t zero, one, tmp;
	mqi_curve_param_t line_new;
	mempool_t pool;
	size_t k;

	mempool_init(pool);

	for ( k = 0; k < 4; k++ ) { 
	
		mqi_vect_initpool_dynamic (p[k], mqi_curve_param_n_equations(line_par),
					mqi_curve_param_typeof(line_par));

		if ( mqi_poly_is_zero (CP(line_par,k)) ) continue;

		mqi_curve_param_eval (p[k], line_par,
					POLY(CP(line_par,k),0),
					POLY(CP(line_par,k),1));
		mqi_vect_content (c[k], p[k]);

		if ( mqi_coef_cmp_schar(c[k], 1) != 0 )
			mqi_vect_divexact_coef (p[k], p[k], c[k]);
	}

	mqi_coef_list_initpool_dynamic (mqi_curve_param_typeof(line_par),
			u1, u2, v1, v2, co1, co2, zero, one, sum1, sum2, tmp, NULL);


	mqi_coef_set_schar    (u1, 1);
	mqi_coef_set_schar    (v2, 1);
	mqi_coef_set_schar    (co1, 1);
	mqi_coef_set_schar    (co2, 1);
	mqi_coef_set_schar    (one, 1);
	mqi_coef_set_schar    (u2, 0);
	mqi_coef_set_schar    (v1, 0);
	mqi_coef_set_schar    (zero, 0);
	

	mqi_vect_list_initpool_dynamic (mqi_curve_param_typeof(line_par), mqi_curve_param_n_equations(line_par), end1, end2, NULL);

	mqi_curve_param_eval (end1, line_par, one, zero);
	mqi_curve_param_eval (end2, line_par, zero, one);
		
	mqi_vect_sum_of_squares (sum1, end1);
	mqi_vect_sum_of_squares (sum2, end2);

	if ( mqi_coef_cmp (sum2, sum1) < 0 ) {
		mqi_vect_initpool_cpy (tmpvect, end1);
		mqi_vect_cpy	  (end1, end2);
		mqi_vect_cpy	  (end2, tmpvect);
		mqi_coef_cpy	  (tmp, sum1);
		mqi_coef_cpy	  (sum1, sum2);
		mqi_coef_cpy	  (sum2, tmp);
		mqi_coef_cpy	  (tmp, u1);
		mqi_coef_cpy	  (u1, u2);
		mqi_coef_cpy	  (u2, tmp);
		mqi_coef_cpy	  (tmp, v1);
		mqi_coef_cpy	  (v1, v2);
		mqi_coef_cpy	  (v2, tmp);
	}

	for ( k = 0; k < 4; k++ ) { 
	
		mqi_vect_sum_of_squares (tmp, p[k]);
	
		if ( mqi_coef_sign (tmp) > 0 && mqi_coef_cmp(tmp, sum1) < 0 ) {

			mqi_coef_cpy (sum2, sum1);
			mqi_vect_cpy (end2, end1);
			mqi_coef_cpy (sum1, tmp);
			mqi_vect_cpy (end1, p[k]);
			mqi_coef_cpy (u2, u1);
			mqi_coef_cpy (u1, POLY(CP(line_par,k),0));
			mqi_coef_cpy (v2, v1);
			mqi_coef_cpy (v1, POLY(CP(line_par,k),1));
			mqi_coef_neg (v1, v1);
			mqi_coef_cpy (co2, co1);
			mqi_coef_cpy (co1, c[k]);

		}else if ( mqi_coef_cmp (sum1, tmp) < 0 && mqi_coef_cmp (tmp, sum2) < 0 ) {

			mqi_coef_cpy (sum2, tmp);
			mqi_vect_cpy (end2, p[k]);
			mqi_coef_cpy (u2, POLY(CP(line_par,k), 0));
			mqi_coef_cpy (v2, POLY(CP(line_par,k), 1));
			mqi_coef_neg (v2, v2);
			mqi_coef_cpy (co2, c[k]);

		}

		/** $$ initpool or initpool_dynamic ? */
		/** I think initpool_dynamic is necessary to init the hosted polynomials */
		mqi_curve_param_initpool_dynamic (line_new, mqi_vect_size(end1), mqi_vect_typeof(end1));
		mqi_curve_param_line_through_two_points (line_new, end1, end2);

		mqi_curve_param_cpy (line_par, line_new);

		/** rop is a persistent data */
		mqi_vect_init_dynamic (rop, 4, mqi_curve_param_typeof(line_par));

		mqi_coef_mul 	 (VECT(rop,0), 	 co1, v2);
		mqi_coef_linexpr1(VECT(rop,1),1, -1,co2,v1);
		mqi_coef_mul	 (VECT(rop,2),	 co1,u2);
		mqi_coef_linexpr1(VECT(rop,3),1, -1,co2,u1);


	}

	mempool_clear();

}

/** Attempts to reparameterize a line c = c1+sqrt(xi). c2 such that 
  * one of the coordinates is 0 when u=0 and another is zero when v=0
  * Returns the parameters corresponding to the initial endpoints */
void qi_param_line_improved2            (mqi_vect_t __INIT rop, mqi_curve_param_t c1, mqi_curve_param_t c2, mqi_coef_t __IN xi) {


	/* c[i] = c1[i] + sqrt(xi). c2[i]
	*       = c1[i][1].u + c1[i][0].v + sqrt(xi). c2[i][1].u + sqrt(xi). c2[i][0].v
	*       = (c1[i][1] + sqrt(xi). c2[i][1]). u + (c1[i][0] + sqrt(xi). c2[i][0]). v
	* We consider  for i from 0 to 3 the point Mi = c(u,v) where 
	* u = (c1[i][0] + sqrt(xi). c2[i][0]) and  v = -(c1[i][1] + sqrt(xi). c2[i][1])
	* The jth coordinate of this point is 
	*    -(c1[j][1] + sqrt(xi). c2[j][1]).(c1[i][0] + sqrt(xi). c2[i][0])  
	*    + (c1[j][0] + sqrt(xi). c2[j][0]).(c1[i][1] + sqrt(xi). c2[i][1])
	* = -c1[j][1]*c1[i][0] - xi*c2[j][1]*c2[i][0] + c1[j][0]*c1[i][1] +
	* xi*c2[j][0]*c2[i][1] +sqrt(xi)*(-c1[j][1]*c2[i][0] - c2[j][1]*c1[i][0] +
	* c1[j][0]*c2[i][1] + c2[j][0]*c1[i][1]) 
	* which is zero for j=i. 
	* Let pi and qi be the points such that Mi = pi + sqrt(xi). qi. Their
	* coordinates are  pi[j] = -c1[j][1]*c1[i][0] - xi*c2[j][1]*c2[i][0] +
	* c1[j][0]*c1[i][1] + xi*c2[j][0]*c2[i][1] qi[j] = -c1[j][1]*c2[i][0] -
	* c2[j][1]*c1[i][0] + c1[j][0]*c2[i][1] + c2[j][0]*c1[i][1] 
	* pi = c1.eval(-c1[i][0], c1[i][1])  + xi*c2.eval(-c2[i][0], c2[i][1])
	* qi = c1.eval(-c2[i][0], c2[i][1])  +      c2.eval(-c1[i][0], c1[i][1]) */	
	/*  u = (c1[i][0] + sqrt(xi). c2[i][0]) and  v = -(c1[i][1] + sqrt(xi). c2[i][1]) */

	mqi_mat_t 	p, q, u, v, pi, qi;
	mqi_coef_t	gcd_tmp[4], tmp, tmp2, one, zero;
	mqi_coef_t	sumA, sumB, co1, co2;
	mqi_vect_t	A1, A2, B1, B2, tmpvect, u1, u2, v1, v2;
	mqi_curve_param_t line_new1, line_new2;
	mempool_t 	pool;
	size_t 		i, j, k;

	mempool_init(pool);

	mqi_mat_list_initpool_dynamic(mqi_coef_typeof(xi), 4, 4, p, u, v, NULL);

	for ( i = 0; i < 4; i++ )
	{

		mqi_coef_list_initpool_dynamic (mqi_coef_typeof(xi), gcd_tmp[i], tmp, tmp2, NULL);
		mqi_coef_set_schar	(gcd_tmp[i], 0);

		for ( j = 0; j < 4; j++ ) { 
			
			if ( i == j ) continue;

			/** The point pi (resp. qi) is the ith line of the matrix p
			 *  (resp. q) */
			if ( !mqi_poly_is_zero(CP(c1,i)) && !mqi_poly_is_zero(CP(c1,j)) )
				mqi_coef_linexpr1(MAT(p,i,j),2,	1,CPCOEF(c1,j,1),CPCOEF(c1,i,0),
								-1,CPCOEF(c1,j,0),CPCOEF(c1,i,1));

			if ( !mqi_poly_is_zero(CP(c2,i)) && !mqi_poly_is_zero(CP(c2,j)) )
				mqi_coef_linexpr3(MAT(p,i,j),3,	1,MAT(p,i,j),NULL,NULL,
								1,xi,CPCOEF(c2,j,1),CPCOEF(c2,i,0),
								-1,xi,CPCOEF(c2,j,0),CPCOEF(c2,i,1));


			if ( !mqi_poly_is_zero(CP(c1,j)) && !mqi_poly_is_zero(CP(c2,i)) )
				mqi_coef_linexpr1(MAT(q,i,j),2,	1,CPCOEF(c1,j,1),CPCOEF(c2,i,0),
								-1,CPCOEF(c1,j,0),CPCOEF(c2,i,1));

			if ( !mqi_poly_is_zero(CP(c1,i)) && !mqi_poly_is_zero(CP(c2,j)) )
				mqi_coef_linexpr1(MAT(q,i,j),3,	1,MAT(q,i,j),NULL,
								1,CPCOEF(c2,j,1),CPCOEF(c1,i,0),
								-1,CPCOEF(c2,j,0),CPCOEF(c1,i,1));


			mqi_coef_gcd (gcd_tmp[i], gcd_tmp[i], MAT(p,i,j));
			mqi_coef_gcd (gcd_tmp[i], gcd_tmp[i], MAT(q,i,j));

		}

		/** gcd_tmp is the gcd of the coeffs of the points pi and qi */
		if ( mqi_coef_cmp_schar (gcd_tmp[i], 1) != 0 &&
		     mqi_coef_sign (gcd_tmp[i]) != 0 ) {

			for ( j = 0; j < 4; j++ ) { 
			
				mqi_coef_divexact (tmp, MAT(p,i,j), gcd_tmp[i]);
				mqi_mat_set_at (p, i, j, tmp);
				mqi_coef_divexact (tmp, MAT(q,i,j), gcd_tmp[i]);
				mqi_mat_set_at (q, i, j, tmp);

			}
		}

		/** Stores the (u,v) parameters corresponding to the points */
		if ( !mqi_poly_is_zero (CP(c1,i)) ) {
			mqi_mat_set_at (u, i, 0, POLY(CP(c1,i), 0));
			mqi_mat_set_at (v, i, 0, POLY(CP(c1,i), 1));
			mqi_coef_neg(MAT(v,i,0),MAT(v,i,0));
		}

		if ( !mqi_poly_is_zero (CP(c2,i)) ) {
			mqi_mat_set_at (u, i, 1, POLY(CP(c2,i), 0));
			mqi_mat_set_at (v, i, 1, POLY(CP(c2,i), 1));
			mqi_coef_neg(MAT(v,i,1),MAT(v,i,1));
		}

	} /* End for */


	/* If the parameterization c of the line has its ith coordinate = 0 (ie. c[i] = 0)
	* then the point Mi that we computed has all coordinates to 0 (and thus 
	* is not a projective point). 
	* Thus 2 points Mi and Mj (that have not all coordinates to 0) are equal 
	* iff Mi[j] = Mj[i]=0
	* Similarly the point (below) A (or B) on the line  is equal to Mi iff A[i] = 0
	* that is A1[i] = A2[i] = 0
	* Hence if points A and B are used for parameterizing the line (as below)
	* we want to consider point Mi  for reparametizing the line iff 
	* Mi != [0,0,0,0] that is sum_of_squares!=0 and if Mi is different from A and B
	* that is iff A[i] != 0 and B[i] != 0 */

	/* Initialize with endpoints of c (Two endpoints A1 + sqrt(xi).A2 and B1 + sqrt(xi).B2) */

	mqi_coef_list_initpool_dynamic(mqi_coef_typeof(xi), one, zero, sumA, sumB, NULL);
	mqi_vect_initpool_dynamic (A1, mqi_curve_param_n_equations(c1), mqi_coef_typeof(xi));
	mqi_vect_initpool_dynamic (A2, mqi_curve_param_n_equations(c2), mqi_coef_typeof(xi));
	mqi_vect_initpool_dynamic (B1, mqi_curve_param_n_equations(c1), mqi_coef_typeof(xi));
	mqi_vect_initpool_dynamic (B2, mqi_curve_param_n_equations(c2), mqi_coef_typeof(xi));

	mqi_curve_param_eval (A1, c1, one, zero);
	mqi_curve_param_eval (A2, c2, one, zero);
	mqi_curve_param_eval (B1, c1, zero, one);
	mqi_curve_param_eval (B2, c2, zero, one);

	mqi_vect_sum_of_squares (sumA, A1);
	mqi_vect_sum_of_squares (tmp, A2);
	mqi_coef_mul (tmp, xi, tmp);
	mqi_coef_add (sumA, sumA, tmp);

	mqi_vect_sum_of_squares (sumB, B1);
	mqi_vect_sum_of_squares (tmp, B2);
	mqi_coef_mul (tmp, xi, tmp);
	mqi_coef_add (sumB, sumB, tmp);

	mqi_vect_list_initpool_dynamic (mqi_coef_typeof(xi), 2, u1, v1, u2, v2, NULL);
	mqi_vect_set_at_int (u1, 0, 1);
	mqi_vect_set_at_int (v2, 0, 1);

	mqi_coef_list_initpool_dynamic (mqi_coef_typeof(xi), co1, co2, NULL);
	mqi_coef_set_schar (co1, 1);
	mqi_coef_set_schar (co2, 1);

	if ( mqi_coef_cmp (sumB, sumA) < 0 ) {
		mqi_vect_initpool_cpy (tmpvect, A1);
		mqi_vect_cpy (A1, B1);
		mqi_vect_cpy (B1, tmpvect);
		mqi_vect_cpy (tmpvect, A2);
		mqi_vect_cpy (A2, B2);
		mqi_vect_cpy (B2, tmpvect);
		mqi_coef_cpy (tmp, sumA);
		mqi_coef_cpy (sumA, sumB);
		mqi_coef_cpy (sumB, tmp);
		mqi_vect_cpy (tmpvect, u1);
		mqi_vect_cpy (u1, u2);
		mqi_vect_cpy (u2, tmpvect);
		mqi_vect_cpy (tmpvect, v1);
		mqi_vect_cpy (v1, v2);
		mqi_vect_cpy (v2, tmpvect);
	}

	mqi_mat_initpool_dynamic (pi, 1, mqi_mat_ncols(p), mqi_mat_typeof(p));
	mqi_mat_initpool_dynamic (qi, 1, mqi_mat_ncols(q), mqi_mat_typeof(q));
	mqi_vect_list_initpool_dynamic(mqi_mat_ncols(u), mqi_mat_typeof(u), u1, u2, NULL);
	mqi_vect_list_initpool_dynamic(mqi_mat_ncols(v), mqi_mat_typeof(v), v1, v2, NULL);

	for ( k = 0; k < 4; k++ ) {

		/** We consider replacing B by Mi only if A!=Mi that is A[i]!=0 
	     	 *  (otherwise the line AMi is not well defined) */
		if ( mqi_coef_sign (VECT(A1,k)) == 0 ||
		     mqi_coef_sign (VECT(A2,k)) == 0 )
			continue;

		mqi_mat_get_submat (pi, p, k, 0, 1, mqi_mat_ncols(p));
		mqi_mat_get_submat (qi, q, k, 0, 1, mqi_mat_ncols(q));

		/** A vector is a matrix in libmqi */
		mqi_vect_sum_of_squares (tmp,  (mqi_vect_ptr)pi);
		mqi_vect_sum_of_squares (tmp2, (mqi_vect_ptr)qi);
		mqi_coef_mul (tmp2, xi, tmp2);
		mqi_coef_add (tmp, tmp, tmp2);	

		if ( mqi_coef_sign(tmp) > 0 && mqi_coef_cmp(tmp, sumA) < 0 ) {

			/** Replace B by Mi (by replacing B by A and A by Mi) */
			mqi_coef_cpy (sumB, sumA);
			mqi_coef_cpy (sumA, tmp);
			mqi_vect_cpy (B1, A1);
			mqi_vect_cpy (B2, A2);
			mqi_vect_cpy (A1, (mqi_vect_ptr)pi);
			mqi_vect_cpy (A2, (mqi_vect_ptr)qi);
			mqi_coef_cpy (co2, co1);
			mqi_coef_cpy (co1, gcd_tmp[k]);	
			mqi_vect_cpy (u2, (mqi_vect_ptr)u1);
			mqi_vect_cpy (v2, (mqi_vect_ptr)v1);
			mqi_mat_get_submat ((mqi_mat_ptr)u1, u, k, 0, 1, mqi_mat_ncols(u));
			mqi_mat_get_submat ((mqi_mat_ptr)v1, v, k, 0, 1, mqi_mat_ncols(v));
		}
		else if ( mqi_coef_cmp (sumA, tmp) < 0 && mqi_coef_cmp(tmp, sumB) < 0) {

			/** Replace B by Mi */
			mqi_coef_cpy (sumB, tmp);
			mqi_vect_cpy (B1, (mqi_vect_ptr)pi);
			mqi_vect_cpy (B2, (mqi_vect_ptr)qi);
			mqi_coef_cpy (co2, gcd_tmp[k]);
			mqi_mat_get_submat ((mqi_mat_ptr)u2, u, k, 0, 1, mqi_mat_ncols(u));
			mqi_mat_get_submat ((mqi_mat_ptr)v2, v, k, 0, 1, mqi_mat_ncols(v));

		}
		
	}

	/** $$ Same remarks: initpooldynamic seems to be adequate */
/*	mqi_curve_param_initpool(line_new1, mqi_vect_size(A1));
	mqi_curve_param_initpool(line_new2, mqi_vect_size(A2));*/
	mqi_curve_param_initpool_dynamic(line_new1, mqi_vect_size(A1), mqi_vect_typeof(A1));
	mqi_curve_param_initpool_dynamic(line_new2, mqi_vect_size(A2), mqi_vect_typeof(A2));

	mqi_curve_param_line_through_two_points (line_new1, A1, B1);
	mqi_curve_param_line_through_two_points (line_new2, A2, B2);

	/* discard old c1 & c2 */
	mqi_curve_param_list_clear(c2, c2, NULL);
	mqi_curve_param_init_cpy (c1, line_new1);
	mqi_curve_param_init_cpy (c2, line_new2);

	/** rop is a persistent data */
	mqi_vect_init_dynamic (rop, 6, mqi_coef_typeof(xi));
	
	if ( mqi_coef_sign(VECT(v1,0)) == 0 && mqi_coef_sign(VECT(v1,1)) == 0 )
		mqi_vect_set_at_int (rop, 0, 1);
	else {
		
		mqi_coef_linexpr4(VECT(rop,0),2,	1,co1,VECT(v1,0),VECT(v2,0),NULL,
							-1,co1,xi,VECT(v1,1),VECT(v2,1));
		mqi_coef_linexpr3(VECT(rop,1),2,	1,co1,VECT(v1,0),VECT(v2,1),
							-1,co1,VECT(v1,1),VECT(v2,0));
		mqi_coef_linexpr4(VECT(rop,2),2,	-1,co2,VECT(v1,0),VECT(v1,0),NULL,
							1,co2,xi,VECT(v1,1),VECT(v1,1));
	}

	if ( mqi_coef_sign(VECT(u1,0)) == 0 && mqi_coef_sign(VECT(u1,1)) == 0 )
		mqi_vect_set_at_int (rop, 1, 3);
	else {

		mqi_coef_linexpr4(VECT(rop,3),2,	1,co1,VECT(u1,0),VECT(u2,0),NULL,
							-1,co1,xi,VECT(u1,1),VECT(u2,1));
		mqi_coef_linexpr3(VECT(rop,4),2,	1,co1,VECT(u1,0),VECT(u2,1),
							-1,co1,VECT(u1,1),VECT(u2,0));
		mqi_coef_linexpr4(VECT(rop,5),2,	-1,co2,VECT(u1,0),VECT(u1,0),NULL,
							1,co2,xi,VECT(u1,1),VECT(u1,1));
	}

	mempool_clear();

}

/** Attempts to reparameterize a line c = c1+sqrt(xi). c2 + sqrt(D). c3 + sqrt(D.xi). c4
  * such that one of the coordinates is 0 when u=0 and another is zero when v=0
  * (D = D1 + sqrt(xi). D2) */
void qi_param_line_improved3            (mqi_curve_param_t c1, mqi_curve_param_t c2, mqi_curve_param_t c3, mqi_curve_param_t c4,
                                         mqi_coef_t __IN xi, mqi_coef_t __IN D1, mqi_coef_t __IN D2) {

	/* c[i] = c1[i] + sqrt(xi). c2[i] + sqrt(D). c3[i] + sqrt(D.xi). c4[i]
  	*    = (c1[i][1] + sqrt(xi). c2[i][1] + sqrt(D). c3[i][1] + sqrt(D.xi). c4[i][1]). u
  	*    = (c1[i][0] + sqrt(xi). c2[i][0] + sqrt(D). c3[i][0] + sqrt(D.xi). c4[i][0]). v
  	* We consider  for i from 0 to 3 the point Mi = c(u,v) such that c[i]=0, that
  	* is where  
  	* u = -(c1[i][0] + sqrt(xi). c2[i][0] + sqrt(D). c3[i][0] + sqrt(D.xi). c4[i][0])  and
  	* v = (c1[i][1] + sqrt(xi). c2[i][1] + sqrt(D). c3[i][1] + sqrt(D.xi). c4[i][1])
	* The jth coordinate of this point is 
	*    - (c1[j][1] + sqrt(xi). c2[j][1] + sqrt(D). c3[j][1] + sqrt(D.xi). c4[j][1]). 
	*       (c1[i][0] + sqrt(xi). c2[i][0] + sqrt(D). c3[i][0] + sqrt(D.xi). c4[i][0])
	*    + (c1[j][0] + sqrt(xi). c2[j][0] + sqrt(D). c3[j][0] + sqrt(D.xi). c4[j][0]).
	*       (c1[i][1] + sqrt(xi). c2[i][1] + sqrt(D). c3[i][1] + sqrt(D.xi). c4[i][1])
	* 
	* =  (-c1[j][1]*c1[i][0] + c1[j][0]*c1[i][1] - xi*c2[j][1]*c2[i][0] +
	* xi*c2[i][1]*c2[j][0] -D1*c3[j][1]*c3[i][0] + D1*c3[i][1]*c3[j][0] 
	*       -D1*xi*c4[j][1]*c4[i][0] + D1*xi*c4[i][1]*c4[j][0] 
	*       -D2*xi*c3[j][1]*c4[i][0] -D2*xi*c3[i][0]*c4[j][1]
	*       +D2*xi*c3[i][1]*c4[j][0]  +D2*xi*c3[j][0]*c4[i][1]) 
	*        +sqrt(xi)*(-c2[j][1]*c1[i][0] -c1[j][1]*c2[i][0] + c2[i][1]*c1[j][0]
	*        +c1[i][1]*c2[j][0] -D2*c3[j][1]*c3[i][0] + D2*c3[i][1]*c3[j][0] 
	*              -D2*xi*c4[j][1]*c4[i][0] + D2*xi*c4[i][1]*c4[j][0] 
	*              -D1*c3[j][1]*c4[i][0] -D1*c3[i][0]*c4[j][1] 
	*              +D1*c3[i][1]*c4[j][0]  +D1*c3[j][0]*c4[i][1])
	*        +sqrt(D)*(-c3[j][1]*c1[i][0] -c1[j][1]*c3[i][0] + c3[i][1]*c1[j][0]
	*        +c1[i][1]*c3[j][0] -xi*c2[j][1]*c4[i][0] -xi*c2[i][0]*c4[j][1] +
	*        xi*c2[i][1]*c4[j][0] +xi*c2[j][0]*c4[i][1])
	*        +sqrt(xi)*sqrt(D)*(-c4[j][1]*c1[i][0] -c1[j][1]*c4[i][0] +
	*        c4[i][1]*c1[j][0] +c1[i][1]*c4[j][0] -c3[j][1]*c2[i][0]
	*        -c3[i][0]*c2[j][1] + c3[i][1]*c2[j][0] + c3[j][0]*c2[i][1]) 
	* 
	* which is zero for j=i. 
	* Let pi, qi, ri, si be the points such that Mi = pi + sqrt(xi). qi +
	* sqrt(D).ri + sqrt(D).sqrt(xi). si 
	* Their  coordinates are 
	* pi[j] = -c1[j][1]*c1[i][0] + c1[j][0]*c1[i][1] - xi*c2[j][1]*c2[i][0] +
	* xi*c2[i][1]*c2[j][0] -D1*c3[j][1]*c3[i][0] + D1*c3[i][1]*c3[j][0] 
	a*       -D1*xi*c4[j][1]*c4[i][0] + D1*xi*c4[i][1]*c4[j][0] 
	*       -D2*xi*c3[j][1]*c4[i][0] -D2*xi*c3[i][0]*c4[j][1]
	*       +D2*xi*c3[i][1]*c4[j][0]  +D2*xi*c3[j][0]*c4[i][1] 
	* qi[j] = -c2[j][1]*c1[i][0] -c1[j][1]*c2[i][0] + c2[i][1]*c1[j][0] +c1[i][1]*c2[j][0]
	*              -D2*c3[j][1]*c3[i][0] + D2*c3[i][1]*c3[j][0] 
	*              -D2*xi*c4[j][1]*c4[i][0] + D2*xi*c4[i][1]*c4[j][0] 
	*              -D1*c3[j][1]*c4[i][0] -D1*c3[i][0]*c4[j][1] 
	*              +D1*c3[i][1]*c4[j][0]  +D1*c3[j][0]*c4[i][1]
	* ri[j] = -c3[j][1]*c1[i][0] -c1[j][1]*c3[i][0] + c3[i][1]*c1[j][0] +c1[i][1]*c3[j][0]
	*             -xi*c2[j][1]*c4[i][0] -xi*c2[i][0]*c4[j][1] +
	*             xi*c2[i][1]*c4[j][0] +xi*c2[j][0]*c4[i][1] 
	* si[j] = -c4[j][1]*c1[i][0] -c1[j][1]*c4[i][0] + c4[i][1]*c1[j][0] +c1[i][1]*c4[j][0]
	*                      -c3[j][1]*c2[i][0] -c3[i][0]*c2[j][1] +
	*                      c3[i][1]*c2[j][0] + c3[j][0]*c2[i][1] */

	mqi_mat_t 	p, q, r, s;
	mqi_vect_t	pi, qi, ri, si;
	mqi_coef_t	gcd_tmp, acc, zero, one, d, sumA, sumB, tmp, sum_tmp;
	mqi_vect_t	A1, A2, A3, A4, B1, B2, B3, B4, tmpvect;
	mempool_t	pool;
	size_t		i, j;
	
	mempool_init(pool);

	mqi_coef_list_initpool_dynamic(mqi_coef_typeof(xi), gcd_tmp, acc, NULL);
	mqi_mat_list_initpool_dynamic(mqi_coef_typeof(xi), 4, 4, p, q, r, s, NULL);

	/** Shortcut */
#define CTEST(n,p,k,l)	( !mqi_poly_is_zero(CP(c##n,k)) && !mqi_poly_is_zero(CP(c##p,l)) )

	for ( i = 0; i < 4; i++ ) {
		mqi_coef_set_schar (gcd_tmp, 0);

		for ( j = 0; j < 4; j++ ) {
			if ( i == j ) continue; /** if i=j, p[i,j]=q[i,j]=0 */
			/** The points pi (resp. qi) is  the ith line of matrix p (resp. q). */
			
			if ( CTEST(1,1,i,j) )
				mqi_coef_linexpr1(MAT(p,i,j),2,	-1,CPCOEF(c1,j,1),CPCOEF(c1,i,0),   1,CPCOEF(c1,j,0),CPCOEF(c1,i,0));
			if ( CTEST(2,2,i,j) )
				mqi_coef_linexpr3(MAT(p,i,j),3,	1,MAT(p,i,j),NULL,NULL,	 -1,xi,CPCOEF(c2,j,1),CPCOEF(c2,i,0),
								1,xi,CPCOEF(c2,j,0),CPCOEF(c2,i,1));
			if ( CTEST(3,3,i,j) )
				mqi_coef_linexpr3(MAT(p,i,j),3,	1,MAT(p,i,j),NULL,NULL,	-1,D1,CPCOEF(c3,j,1),CPCOEF(c3,i,0),
								1,D1,CPCOEF(c3,i,1),CPCOEF(c3,j,0));
			if ( CTEST(4,4,i,j) )
				mqi_coef_linexpr4(MAT(p,i,j),3,	1,MAT(p,i,j),NULL,NULL,NULL,	-1,D1,xi,CPCOEF(c4,j,1),CPCOEF(c4,i,0),
								1,D1,xi,CPCOEF(c4,i,1),CPCOEF(c4,j,0));
			if ( CTEST(3,4,j,i) )
				mqi_coef_linexpr4(MAT(p,i,j),3,	1,MAT(p,i,j),NULL,NULL,NULL,	-1,D2,xi,CPCOEF(c3,j,1),CPCOEF(c4,i,0),
								1,D2,xi,CPCOEF(c3,j,0),CPCOEF(c4,i,1));
			if ( CTEST(3,4,i,j) )
				mqi_coef_linexpr4(MAT(p,i,j),3,	1,MAT(p,i,j),NULL,NULL,NULL,	-1,D2,xi,CPCOEF(c3,i,0),CPCOEF(c4,j,1),
								1,D2,xi,CPCOEF(c3,i,1),CPCOEF(c4,j,0));

			if ( CTEST(1,2,j,i) )
				mqi_coef_linexpr1(MAT(q,i,j),2,	-1,CPCOEF(c1,j,1),CPCOEF(c2,i,0),  1,CPCOEF(c1,j,0),CPCOEF(c2,i,1));
			if ( CTEST(1,2,i,j) )
				mqi_coef_linexpr1(MAT(q,i,j),3,	1,MAT(q,i,j),NULL,	-1,CPCOEF(c2,j,1),CPCOEF(c1,i,0),
								CPCOEF(c2,j,0),CPCOEF(c1,i,1));
			if ( CTEST(3,3,i,j) )
				mqi_coef_linexpr3(MAT(q,i,j),3,	1,MAT(q,i,j),NULL,NULL,	-1,D2,CPCOEF(c3,j,1),CPCOEF(c3,i,0),
								1,D2,CPCOEF(c3,i,1),CPCOEF(c3,j,0));
			if ( CTEST(4,4,i,j) )
				mqi_coef_linexpr4(MAT(q,i,j),3,	1,MAT(q,i,j),NULL,NULL,NULL,	-1,D2,xi,CPCOEF(c4,j,1),CPCOEF(c4,i,0),
								1,D2,xi,CPCOEF(c4,i,1),CPCOEF(c4,j,0));
			if ( CTEST(3,4,j,i) ) 
				mqi_coef_linexpr3(MAT(q,i,j),3,	1,MAT(q,i,j),NULL,NULL,	 -1,D1,CPCOEF(c3,j,1),CPCOEF(c4,i,0),
								1,D1,CPCOEF(c3,j,0),CPCOEF(c4,i,1));
			if ( CTEST(3,4,i,j) )
				mqi_coef_linexpr3(MAT(q,i,j),3,	1,MAT(q,i,j),NULL,NULL,	 -1,D1,CPCOEF(c3,i,0),CPCOEF(c4,j,1),
								1,D1,CPCOEF(c3,i,1),CPCOEF(c4,j,0));
			
			if ( CTEST(1,3,i,j) )
				mqi_coef_linexpr1(MAT(r,i,j),2,	-1,CPCOEF(c3,j,1),CPCOEF(c1,i,0),    1,CPCOEF(c1,i,1),CPCOEF(c3,j,0));
			if ( CTEST(1,3,j,i) )
				mqi_coef_linexpr1(MAT(r,i,j),3,	-1,CPCOEF(c1,j,1),CPCOEF(c3,i,0),NULL,
								1,CPCOEF(c3,i,1),CPCOEF(c1,j,0));
			if ( CTEST(2,4,i,j) )
				mqi_coef_linexpr3(MAT(r,i,j),3,	1,MAT(r,i,j),NULL,NULL,	 -1,xi,CPCOEF(c2,i,0),CPCOEF(c4,j,1),
								1,xi,CPCOEF(c2,i,1),CPCOEF(c4,j,0));
			if ( CTEST(2,4,j,i) )
				mqi_coef_linexpr3(MAT(r,i,j),3,	1,MAT(r,i,j),NULL,NULL,	 -1,xi,CPCOEF(c2,j,1),CPCOEF(c4,i,0),
								1,xi,CPCOEF(c2,j,0),CPCOEF(c4,i,1));

			if ( CTEST(1,4,i,j) )
				mqi_coef_linexpr1(MAT(s,i,j),2,	-1,CPCOEF(c4,j,1),CPCOEF(c1,i,0),   1,CPCOEF(c1,i,1),CPCOEF(c4,j,0));
			if ( CTEST(1,4,j,i) )
				mqi_coef_linexpr1(MAT(s,i,j),2,	-1,CPCOEF(c1,j,1),CPCOEF(c4,i,0),   1,CPCOEF(c4,i,1),CPCOEF(c1,j,0));
			if ( CTEST(2,3,i,j) )
				mqi_coef_linexpr1(MAT(s,i,j),2,	-1,CPCOEF(c3,j,1),CPCOEF(c2,i,0),   1,CPCOEF(c3,j,0),CPCOEF(c2,i,1));
			if ( CTEST(2,3,j,i) )
				mqi_coef_linexpr1(MAT(s,i,j),2,	-1,CPCOEF(c3,i,0),CPCOEF(c2,j,1),   1,CPCOEF(c3,i,1),CPCOEF(c2,j,0));

			mqi_coef_gcd (gcd_tmp, gcd_tmp, MAT(p,i,j));
			mqi_coef_gcd (gcd_tmp, gcd_tmp, MAT(q,i,j));
			mqi_coef_gcd (gcd_tmp, gcd_tmp, MAT(r,i,j));
			mqi_coef_gcd (gcd_tmp, gcd_tmp, MAT(s,i,j));

		} /** End for j */

		/** gcd_tmp is the gcd of the coeffs of the points pi and qi */
		if ( mqi_coef_cmp_schar (gcd_tmp, 1) != 0 ) {
			for ( j = 0; j < 4; j++ ) {
				mqi_coef_divexact (MAT(p,i,j), MAT(p,i,j), gcd_tmp);
				mqi_coef_divexact (MAT(q,i,j), MAT(q,i,j), gcd_tmp);
				mqi_coef_divexact (MAT(r,i,j), MAT(r,i,j), gcd_tmp);
				mqi_coef_divexact (MAT(s,i,j), MAT(s,i,j), gcd_tmp);
			}
		}

	} /** End for i */

	/* If the parameterization c of the line has its ith coordinate =0 (ie. c[i]=0)
	 * then the point Mi that we computed has all coordinates to 0 (and thus 
	 *  is not a projective point). 
	 * Thus 2 points Mi and Mj (that have not all coordinates to 0) are equal 
	 * iff Mi[j]=Mj[i]=0
	 * Similarly the point (below) A (or B) on the line  is equal to Mi iff A[i]=0
	 * that is A1[i]=A2[i]=A3[i]=A4[i]=0
	 * Hence if points A and B are used for parameterizing the line (as below)
	 * we want to consider point Mi  for reparametizing the line iff 
	 * Mi!=[0,0,0,0] that is sum_of_squares!=0 and if Mi is different from A and B
	 * that is iff A[i]!=0 and B[i]!=0

	 * Initialize with "endpoints" of c (for (u,v) = (1,0) and (0,1))
	 * (Two endpoints A1 + sqrt(xi).A2 + sqrt(D).A3 + sqrt(D).sqrt(xi).A4 
	 * and B1 + sqrt(xi).B2 + sqrt(D).B3 + sqrt(D).sqrt(xi).B4 ) */

	mqi_vect_initpool_dynamic (A1, mqi_curve_param_n_equations(c1), mqi_coef_typeof(xi));
	mqi_vect_initpool_dynamic (A2, mqi_curve_param_n_equations(c2), mqi_coef_typeof(xi));
	mqi_vect_initpool_dynamic (A3, mqi_curve_param_n_equations(c3), mqi_coef_typeof(xi));
	mqi_vect_initpool_dynamic (A4, mqi_curve_param_n_equations(c4), mqi_coef_typeof(xi));
	mqi_vect_initpool_dynamic (B1, mqi_curve_param_n_equations(c1), mqi_coef_typeof(xi));
	mqi_vect_initpool_dynamic (B2, mqi_curve_param_n_equations(c2), mqi_coef_typeof(xi));
	mqi_vect_initpool_dynamic (B3, mqi_curve_param_n_equations(c3), mqi_coef_typeof(xi));
	mqi_vect_initpool_dynamic (B4, mqi_curve_param_n_equations(c4), mqi_coef_typeof(xi));

	mqi_coef_initpool_dynamic (zero, mqi_coef_typeof(xi));
	mqi_coef_initpool_dynamic (one, mqi_coef_typeof(xi));
	mqi_coef_set_schar    (zero, 0);
	mqi_coef_set_schar    (one, 1);

	mqi_curve_param_eval (A1, c1, one, zero);
	mqi_curve_param_eval (A2, c2, one, zero);
	mqi_curve_param_eval (A3, c3, one, zero);
	mqi_curve_param_eval (A4, c4, one, zero);
	mqi_curve_param_eval (B1, c1, zero, one);
	mqi_curve_param_eval (B2, c2, zero, one);
	mqi_curve_param_eval (B3, c3, zero, one);
	mqi_curve_param_eval (B4, c4, zero, one);

	mqi_coef_initpool_cpy (tmp, D1);
	mqi_coef_abs	  (tmp, tmp);
	mqi_coef_initpool_cpy (d, D2);
	mqi_coef_abs	  (d, d);
	mqi_coef_add	  (d, tmp, d);

	mqi_vect_sum_of_squares (sumA, A1);
	mqi_vect_sum_of_squares (tmp, A2);
	mqi_coef_add (sumA, sumA, tmp);
	mqi_vect_sum_of_squares (tmp, A3);
	mqi_coef_add (sumA, sumA, tmp);
	mqi_vect_sum_of_squares (tmp, A4);
	mqi_coef_add (sumA, sumA, tmp);

	mqi_vect_sum_of_squares (sumB, B1);
	mqi_vect_sum_of_squares (tmp, B2);
	mqi_coef_add (sumB, sumB, tmp);
	mqi_vect_sum_of_squares (tmp, B3);
	mqi_coef_add (sumB, sumB, tmp);
	mqi_vect_sum_of_squares (tmp, B4);
	mqi_coef_add (sumB, sumB, tmp);

	if ( mqi_coef_cmp (sumB, sumA) < 0 ) {

		mqi_vect_initpool_cpy (tmpvect, A1);
		mqi_vect_cpy (A1, B1);
		mqi_vect_cpy (B1, tmpvect);
		
		mqi_vect_cpy (tmpvect, A2);
		mqi_vect_cpy (A2, B2);
		mqi_vect_cpy (B2, tmpvect);
	
		mqi_vect_cpy (tmpvect, A3);
		mqi_vect_cpy (A3, B3);
		mqi_vect_cpy (B3, tmpvect);

		mqi_vect_cpy (tmpvect, A4);
		mqi_vect_cpy (A4, B4);
		mqi_vect_cpy (B4, tmpvect);

		mqi_coef_cpy (tmp, sumA);
		mqi_coef_cpy (sumA, sumB);
		mqi_coef_cpy (sumB, tmp);

	}

	mqi_coef_initpool_dynamic (sum_tmp, mqi_coef_typeof(xi));

	mqi_vect_list_initpool_dynamic(mqi_mat_ncols(p), mqi_mat_typeof(p),
					pi, qi, ri, si, NULL);

	for ( i = 0; i < 4; i++ ) {

		/** We consider replacing B by Mi only if A!=Mi that is A[i]!=0 
		  * (otherwise the line AMi is not well defined */
		if ( mqi_coef_sign(VECT(A1,i)) != 0 ||
		     mqi_coef_sign(VECT(A2,i)) != 0 ||
		     mqi_coef_sign(VECT(A3,i)) != 0 ||
		     mqi_coef_sign(VECT(A4,i)) != 0 )
		{

			mqi_mat_get_submat ((mqi_mat_ptr)pi, p, i, 0, 1, mqi_mat_ncols(p));
			mqi_mat_get_submat ((mqi_mat_ptr)qi, q, i, 0, 1, mqi_mat_ncols(p));
			mqi_mat_get_submat ((mqi_mat_ptr)ri, r, i, 0, 1, mqi_mat_ncols(p));
			mqi_mat_get_submat ((mqi_mat_ptr)si, s, i, 0, 1, mqi_mat_ncols(p));

			mqi_vect_sum_of_squares (sum_tmp, pi);
			mqi_vect_sum_of_squares (tmp, qi);
			mqi_coef_add (sum_tmp, sum_tmp, tmp);
			mqi_vect_sum_of_squares (tmp, ri);
			mqi_coef_add (sum_tmp, sum_tmp, tmp);
			mqi_vect_sum_of_squares (tmp, si);
			mqi_coef_add (sum_tmp, sum_tmp, tmp);

			if ( mqi_coef_sign(sum_tmp) > 0 && mqi_coef_cmp(sum_tmp, sumA) < 0 ) {
				/** Replace B by Mi (by replacing B by A and A by Mi) */
				mqi_coef_cpy (sumB, sumA);
				mqi_coef_cpy (sumA, sum_tmp);
				mqi_vect_cpy (B1, A1);
				mqi_vect_cpy (B2, A2);
				mqi_vect_cpy (B3, A3);
				mqi_vect_cpy (B4, A4);
				mqi_vect_cpy (A1, pi);
				mqi_vect_cpy (A2, qi);
				mqi_vect_cpy (A3, ri);
				mqi_vect_cpy (A4, si);

			}else if ( mqi_coef_cmp (sumA, sum_tmp) < 0 && mqi_coef_cmp (sum_tmp, sumB) < 0 ) {
				/** Replace B by Mi */
				mqi_coef_cpy (sumB, sum_tmp);
				mqi_vect_cpy (B1, pi);
				mqi_vect_cpy (B2, qi);
				mqi_vect_cpy (B3, ri);
				mqi_vect_cpy (B4, si);

			}

		}

	} /**  End for i */

	/* discard old curve params */
	int size = mqi_curve_param_n_equations(c1);
	mqi_curve_param_list_clear(c1, c2, c3, c4, NULL);

	mqi_curve_param_list_init_dynamic(mqi_coef_typeof(xi), size, c1, c2, c3, c4, NULL);
	mqi_curve_param_line_through_two_points (c1, A1, B1);
	mqi_curve_param_line_through_two_points (c2, A2, B2);
	mqi_curve_param_line_through_two_points (c3, A3, B3);
	mqi_curve_param_line_through_two_points (c4, A4, B4);

	mempool_clear();

}

