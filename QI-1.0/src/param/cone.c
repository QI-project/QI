#include "qi_param.h"

/** Computes the parameterization of a cone through a rational point
  *  Output is p_trans*[u*s^2 u*t^2 u*s*t v]
  *  s*l[0]+t*l[1] gives the line (sing,rat_point) on the cone */
void qi_param_cone_through_ratpoint     (mqi_mat_t __IN q, mqi_vect_t __IN sing, mqi_vect_t __IN rat_point,
					 mqi_mat_t __INIT p_trans, mqi_surface_param_t __INIT p, mqi_vect_t __IN l) {

	mqi_mat_t q_r;
	mqi_curve_param_t par;
	mqi_mat_t p_trans_2d, p_trans2d_44, tmp;
	mqi_hpoly_t pol_s;

	qi_mat_send_to_zw (__INIT p_trans, rat_point, sing);

	/** p_trans'*q*p_trans */
	mqi_mat_transformation (tmp, q, p_trans, MQI_MAT_NOTRANSCO);	

	/** Resizes to a 3x3 matrix */
	mqi_mat_resize (q_r, tmp, 3, 3);

	/** Parametrizes the conic */
	mqi_curve_param_init_dynamic(par, 4, mqi_mat_typeof(q));
	qi_param_conic_through_origin (q_r, __INIT p_trans_2d, par, l);

	/** Stacks the transformations */
	mqi_mat_init_dynamic (p_trans2d_44, 4, 4, mqi_mat_typeof(q));
	mqi_mat_cpy (p_trans2d_44, p_trans_2d);
	mqi_mat_set_at_int (p_trans2d_44, 3, 3, 1);
	mqi_mat_mul (p_trans, p_trans, p_trans2d_44);

	/** Completes the curve param to make a surface param */

	/** "Resizes" "par" to "par4" */
/*	mqi_curve_param_init_dynamic (par4, 4, mqi_mat_typeof(q));
	mqi_curve_param_cpy  (par4, par);*/

	mqi_poly_init_dynamic (pol_s, mqi_mat_typeof(q));
	mqi_poly_set_x (pol_s);

	mqi_surface_param_init_dynamic (p, mqi_curve_param_n_equations(par),
					mqi_curve_param_typeof(par));
	mqi_surface_param_inject_poly_in_cp (p, par, pol_s);

	/** $$ Missing assign_y */	
	/** $$ Not sure */	
	mqi_hhpoly_set_zero 	(SP(p,3));
	mqi_hhpoly_set_degree	(SP(p,3), 1);
	mqi_poly_set_at_int	(SPPOLY(p,3,0), 0, 1);

	mqi_curve_param_clear(par);
	mqi_mat_list_clear(q_r, p_trans_2d, p_trans2d_44, tmp, NULL);
	mqi_poly_clear(pol_s);

}

/** Computes the parameterization of a cone
  * Output is p_trans*[u*s^2 u*t^2 u*s*t v] + sqrt(D)*p_trans2*[u*s^2 u*t^2 u*s*t v] */
void qi_param_cone                      (mqi_mat_t __IN q, mqi_vect_t __IN sing, mqi_coef_t __INIT D, mqi_mat_t __INIT p_trans, mqi_mat_t __INIT p_trans2,
                                         mqi_surface_param_t __INIT p)
{

	mqi_mat_t q_r;
	mqi_mat_t p_inf, tmp;
	mqi_curve_param_t par;
	mqi_mat_t p_trans2d, p_trans2d2, p_trans2d_44, p_trans2d2_44;
	mqi_hpoly_t pol_s;

	qi_mat_send_to_infinity (p_inf, sing);
	
	/** q_inf'*q*p_inf */
	mqi_mat_transformation  (tmp, q, p_inf, MQI_MAT_NOTRANSCO);

	/** Resizes to a 3x3 matrix */
	mqi_mat_resize (q_r, tmp, 3, 3);

	/** $$ BEWARE OF THE opt_level argument: obsolete ! */
	mqi_curve_param_init_dynamic(par, 4, mqi_mat_typeof(q));
	qi_param_conic (q_r, D, p_trans2d, p_trans2d2, par);

	/** Stacks the transformations */
	mqi_mat_init_dynamic (p_trans2d_44, 4, 4, mqi_mat_typeof(q));
	mqi_mat_cpy (p_trans2d_44, p_trans2d);
	mqi_mat_set_at_int (p_trans2d_44, 3, 3, 1);

	/* $$ I suggest p_trans and p_trans2 to be initialized ... */
/*	mqi_mat_mul (p_trans, p_inf, p_trans2d_44);*/
	mqi_mat_mul_init (p_trans, p_inf, p_trans2d_44);

	if ( mqi_coef_sign(D) != 0 ) {
		mqi_mat_init_dynamic (p_trans2d2_44, 4, 4, mqi_mat_typeof(q));
		mqi_mat_cpy (p_trans2d2_44, p_trans2d2);
		mqi_mat_set_at_int (p_trans2d2_44, 3, 3, 1);

		/* $$ Same remark */
	/*	mqi_mat_mul (p_trans2, p_inf, p_trans2d2_44);*/
		mqi_mat_mul_init (p_trans2, p_inf, p_trans2d2_44);
	}
	
	/** Completes the curve param to make a surface param */

	/** "Resizes" "par" to "par4"  */
/*	mqi_curve_param_init_dynamic (par4, 4, mqi_mat_typeof(q));
	mqi_curve_param_cpy  	     (par4, par);*/

	mqi_hpoly_init_dynamic (pol_s, mqi_mat_typeof(q));
	mqi_poly_set_x (pol_s);

	mqi_surface_param_init_dynamic (p,
					mqi_curve_param_n_equations(par),
					mqi_curve_param_typeof(par));
	mqi_surface_param_inject_poly_in_cp (p, par, pol_s);

	/** $$ Missing assign_y */
	/** Not sure */
	mqi_hhpoly_set_zero (SP(p,3));
	mqi_hhpoly_set_degree (SP(p,3), 1);

	mqi_poly_set_at_int (SPPOLY(p,3,0), 0, 1);

	mqi_mat_list_clear(q_r, p_inf, p_trans2d, p_trans2d2, p_trans2d_44, p_trans2d2_44, tmp, NULL);
	mqi_curve_param_clear(par);
	mqi_poly_clear(pol_s);

}

