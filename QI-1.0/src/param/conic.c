#include "qi_param.h"
#include <mqi/mqi_log.h>

/** ************** */
/** Local routines */
/** ************** */


/** Parameterization by discriminant D = e^2-a*f > 0
  * The param is (p_trans + sqrt(D)*p_trans2) * [u^2 v^2 uv]
  * par = [u^2 v^2 uv] */
void _qi_param_conic_1 (mqi_mat_t __IN q, mqi_coef_t __INIT D, mqi_vect_t __IN nd, mqi_mat_t p_trans, mqi_mat_t p_trans2,
                        mqi_curve_param_t par) {


	mqi_coef_mul(MAT(p_trans,0,0), MAT(q,0,0), MAT(q,0,2));
	mqi_coef_mul(MAT(p_trans,0,1), MAT(q,1,1), MAT(q,0,2));
	mqi_coef_linexpr1(MAT(p_trans,0,2),1,  2,MAT(q,0,0),MAT(q,1,2));
	mqi_coef_linexpr1(MAT(p_trans,1,1),2,  2,MAT(q,0,0),MAT(q,1,2),	-2,MAT(q,0,1),MAT(q,0,2));
	mqi_coef_linexpr1(MAT(p_trans,2,0),1, -1,MAT(q,0,0),MAT(q,0,0));
	mqi_coef_linexpr1(MAT(p_trans,2,1),1, -1,MAT(q,0,0),MAT(q,1,1));
	mqi_coef_linexpr1(MAT(p_trans,2,2),1, -2,MAT(q,0,0),MAT(q,0,1));

	mqi_coef_neg(MAT(p_trans2,0,0), MAT(q,0,0));
	mqi_coef_cpy(MAT(p_trans2,0,1), MAT(q,1,1));
	mqi_coef_mul_schar(MAT(p_trans2,1,1),MAT(q,0,1),-2);
	mqi_coef_mul_schar(MAT(p_trans2,1,2),MAT(q,0,0),-2);
	mqi_mat_mul_coef(p_trans2, p_trans2, VECT(nd,0));
	
	mqi_poly_set_xi (CP(par,0), 2);
	mqi_poly_set_wi (CP(par,1), 2);
	mqi_poly_set_xw (CP(par,2));

	/** $$ D seems to be a persistent data */
	mqi_coef_init_cpy (D, VECT(nd,1));

	if ( mqi_coef_cmp_schar (D, 1) == 0 ) {
		/** Rational param */
		mqi_coef_set_schar    (D, 0);
		mqi_mat_add	      (p_trans, p_trans, p_trans2);
	}


}

/** Parameterization by -f*p > 0
  * Equation is (a*f-e^2)*(f*z+e*x+d*y)^2 + (x*(a*f-e^2)+y*(b*f-d*e))^2 + f*p*y^2 = 0
  * The param is (p_trans + sqrt(D)*p_trans2) * [u^2 v^2 uv]
  * par = [u^2 v^2 uv] */
void _qi_param_conic_2                   (mqi_mat_t __IN q, mqi_coef_t __INIT D, mqi_coef_t __IN de, mqi_coef_t __IN p3, mqi_vect_t __IN nd,
                                          mqi_vect_t __IN np, mqi_mat_t p_trans, mqi_mat_t p_trans2, mqi_curve_param_t par)
{

	/** Conic is ax^2 + 2bxy + cy^2 + 2dyz + 2exz + fz^2
	  * Inertia is supposed to be [2 1] */

	mqi_coef_t t1, t2, tmp, square, cofactor;

	mqi_coef_list_init_dynamic(mqi_coef_typeof(D), t1, t2, tmp, NULL);

	mqi_coef_linexpr1(t1,2,	1,MAT(q,0,1),MAT(q,2,2),	-1,MAT(q,1,2),MAT(q,0,2));
	mqi_coef_linexpr1(t2,2,	1,MAT(q,0,0),MAT(q,1,2),	-1,MAT(q,0,1),MAT(q,0,2));


	/** e^2 - af < 0 */
	if ( mqi_coef_sign(VECT(nd,1)) < 0 ) {

		mqi_coef_mul(MAT(p_trans,0,0), MAT(q,2,2), t1);
		mqi_coef_linexpr1(MAT(p_trans,0,1),1,  -1,MAT(p_trans,0,0),VECT(nd,1));
		mqi_coef_mul(MAT(p_trans,1,0),de,MAT(q,2,2));
		mqi_coef_linexpr1(MAT(p_trans,1,1),1,  -1,MAT(p_trans,1,0),VECT(nd,1));
		mqi_coef_mul(MAT(p_trans,2,0),MAT(q,2,2),t2);
		mqi_coef_linexpr1(MAT(p_trans,2,1),1,  -1,MAT(p_trans,2,0),VECT(nd,1));

		mqi_coef_neg(MAT(p_trans2,0,0),MAT(q,2,2));
		mqi_coef_mul(MAT(p_trans2,0,1),MAT(p_trans2,0,0),VECT(nd,1));
		mqi_coef_cpy(MAT(p_trans2,2,0),MAT(q,0,2));
		mqi_coef_mul(MAT(p_trans2,2,1),MAT(p_trans2,2,0),VECT(nd,1));
		mqi_coef_linexpr1(MAT(p_trans2,2,2),1,  2,VECT(nd,0),VECT(nd,1));

	}else {
		
		mqi_coef_init_cpy (tmp, de);
		mqi_coef_mul (tmp, tmp, VECT(np,1));
		qi_coef_extract_square_factors (__INIT square, __INIT cofactor, tmp);

		mqi_coef_mul(MAT(p_trans,0,0),MAT(q,2,2),p3);
		mqi_coef_mul(MAT(p_trans,0,1),MAT(p_trans,0,0),cofactor);
		mqi_coef_linexpr1(MAT(p_trans,2,0),1,  -1,MAT(q,0,2),p3);
		mqi_coef_mul(MAT(p_trans,2,1),MAT(p_trans,2,0),cofactor);

		mqi_coef_mul(MAT(p_trans2,0,0),MAT(q,2,2),t1);
		mqi_coef_linexpr1(MAT(p_trans2,0,1),1,  -1,MAT(p_trans2,0,0),cofactor);
		mqi_coef_mul(MAT(p_trans2,1,0),MAT(q,2,2),de);
		mqi_coef_linexpr1(MAT(p_trans2,1,1),1,  -1,MAT(p_trans2,1,0),cofactor);
		mqi_coef_mul(MAT(p_trans2,2,0),MAT(q,2,2),t2);
		mqi_coef_linexpr1(MAT(p_trans2,2,1),1,  -1,MAT(p_trans2,2,0),cofactor);
		mqi_coef_linexpr3(MAT(p_trans2,2,2),1,  -2,VECT(np,0),square,cofactor);

	}

	mqi_mat_mul_coef (p_trans2, p_trans2, VECT(np,0));

	mqi_poly_set_xi (CP(par,0), 2);
	mqi_poly_set_wi (CP(par,1), 2);
	mqi_poly_set_xw (CP(par,2));

	/** $$ D seems to be a persistent data */
	mqi_coef_init_cpy (D, VECT(np,1));

	if ( mqi_coef_cmp_schar (D, 1) == 0 ) {
		/** Rational param */
		mqi_coef_set_schar    (D, 0);
		mqi_mat_add	      (p_trans, p_trans, p_trans2);
	}

	mqi_coef_list_clear(t1, t2, tmp, square, cofactor, NULL);

}

/** Parameterization of the conic C1+sqrt(delta)*C2 with  discriminant D = (eg)
  * e^2-a*f > 0 
  * The param is :
  * (p_trans0 + sqrt(delta)*p_trans1 + sqrt(D)*p_trans2 +
                sqrt(delta)*sqrt(D)*p_trans3) * [u^2 v^2 uv] 
  * par = [u^2 v^2 uv]
  * D = D[0] + sqrt(delta)*D[1] */
void _qi_param_conic_nr1                  (mqi_mat_t __IN C1, mqi_mat_t __IN C2, mqi_coef_t __IN delta, mqi_vect_t D, mqi_vect_t __IN nd,
                                           mqi_mat_t p_trans0, mqi_mat_t p_trans1, mqi_mat_t p_trans2, mqi_mat_t p_trans3,
                                           mqi_curve_param_t par) {

	/** Here we simply recopy the procedure qi_param_conic_1() after replacing conic q by
   	  * C1+sqrt(delta)*C2 
   	  * Conic is ax^2 + 2bxy + cy^2 + 2dyz + 2exz + fz^2
   	  * a = a[0] + a[1]*sqrt(delta) b=...
   	  * Inertia of the conic is supposed to be [2 1]  */

  /** Computed with Maple from the parameterization of conic_param1() the
    * parameterization matrix is  
    * p(0,0) = a[0]*e[0] +delta*a[1]*e[1] + sqrtdelta*(a[0]*e[1]+a[1]*e[0])
    *      -a[0]*sqrtD  -a[1]*sqrtdelta*sqrtD 
    * p(0,1) = +c[0]*e[0]+delta*c[1]*e[1] + sqrtdelta*(c[0]*e[1]+c[1]*e[0])
    *      +c[0]*sqrtD +c[1]*sqrtdelta*sqrtD 
    * p(0,2) = 2*a[0]*d[0]+2*a[1]*d[1]*delta + sqrtdelta*(2*a[0]*d[1]+2*a[1]*d[0])
    * p(1,0) = 0
    * p(1,1) = +2*a[0]*d[0]+2*a[1]*d[1]*delta-2*b[0]*e[0]-2*delta*b[1]*e[1] 
    *              + sqrtdelta*(2*a[0]*d[1]+2*a[1]*d[0]-2*b[0]*e[1]-2*b[1]*e[0])
    *              -2*b[0]*sqrtD -2*b[1]*sqrtdelta*sqrtD
    * p(1,2) = -2*a[0]*sqrtD  -2*a[1]*sqrtdelta*sqrtD
    * p(2,0) = -a[0]^2-a[1]^2*delta  -2*a[0]*a[1]*sqrtdelta
    * p(2,1) = -a[0]*c[0]-a[1]*c[1]*delta + sqrtdelta*(-a[0]*c[1]-a[1]*c[0])
    * p(2,2) = -2*a[0]*b[0]-2*a[1]*b[1]*delta + sqrtdelta*(-2*a[0]*b[1]-2*a[1]*b[0]) */

	
	/** p(0,0) = a[0]*e[0] +delta*a[1]*e[1] + sqrtdelta*(a[0]*e[1]+a[1]*e[0])
	  * 	     -a[0]*sqrtD  -a[1]*sqrtdelta*sqrtD */
	mqi_coef_linexpr3(MAT(p_trans0,0,0),2,	1,MAT(C1,0,0),MAT(C1,0,2),NULL,	1,delta,MAT(C2,0,0),MAT(C2,0,2));
	mqi_coef_linexpr1(MAT(p_trans1,0,0),2,	1,MAT(C1,0,0),MAT(C2,0,2),	1,MAT(C2,0,0),MAT(C1,0,2));
	mqi_coef_neg(MAT(p_trans2,0,0), MAT(C1,0,0));
	mqi_coef_neg(MAT(p_trans3,0,0), MAT(C2,0,0));

	/** p(0,1) = +c[0]*e[0]+delta*c[1]*e[1] + sqrtdelta*(c[0]*e[1]+c[1]*e[0])
	  *	     +c[0]*sqrtD +c[1]*sqrtdelta*sqrtD */
	mqi_coef_linexpr3(MAT(p_trans0,0,1),2,	1,MAT(C1,1,1),MAT(C1,0,2),NULL, 1,delta,MAT(C2,1,1),MAT(C2,0,2));
	mqi_coef_linexpr1(MAT(p_trans1,0,1),2,	1,MAT(C1,1,1),MAT(C2,0,2),	1,MAT(C2,1,1),MAT(C1,0,2));
	mqi_coef_cpy(MAT(p_trans2,0,1), MAT(C1,1,1));
	mqi_coef_cpy(MAT(p_trans3,0,1), MAT(C2,1,1));
		
	/** p(0,2) = 2*a[0]*d[0]+2*a[1]*d[1]*delta + sqrtdelta*(2*a[0]*d[1]+2*a[1]*d[0])*/
	mqi_coef_linexpr3(MAT(p_trans0,0,2),2,	2,MAT(C1,0,0),MAT(C1,1,2),NULL,	 2,MAT(C2,0,0),MAT(C2,1,2),delta);
	mqi_coef_linexpr1(MAT(p_trans1,0,2),2,	2,MAT(C1,0,0),MAT(C2,1,2),	 2,MAT(C2,0,0),MAT(C1,1,2));
	mqi_coef_set_zero(MAT(p_trans2,0,2));
	mqi_coef_set_zero(MAT(p_trans3,0,2));

	/** p(1,0) = 0 */
	mqi_coef_set_zero(MAT(p_trans0,1,0));
	mqi_coef_set_zero(MAT(p_trans1,1,0));
	mqi_coef_set_zero(MAT(p_trans2,1,0));
	mqi_coef_set_zero(MAT(p_trans3,1,0));

	/** p(1,1) = +2*a[0]*d[0]+2*a[1]*d[1]*delta-2*b[0]*e[0]-2*delta*b[1]*e[1] 
  	  *          + sqrtdelta*(2*a[0]*d[1]+2*a[1]*d[0]-2*b[0]*e[1]-2*b[1]*e[0])
  	  *          -2*b[0]*sqrtD -2*b[1]*sqrtdelta*sqrtD	*/
	mqi_coef_linexpr3(MAT(p_trans0,1,1),4,	2,MAT(C1,0,0),MAT(C1,1,2),NULL,	 2,MAT(C2,0,0),MAT(C2,1,2),delta,
						-2,MAT(C1,0,1),MAT(C1,0,2),NULL, -2,delta,MAT(C2,0,1),MAT(C2,0,2));
	mqi_coef_linexpr1(MAT(p_trans1,1,1),4,	2,MAT(C1,0,0),MAT(C2,1,2),	2,MAT(C2,0,0),MAT(C1,1,2),
						-2,MAT(C1,0,1),MAT(C2,0,2),	-2,MAT(C2,0,1),MAT(C1,0,2));
	mqi_coef_mul_schar(MAT(p_trans2,1,1), MAT(C1,0,1), -2);
	mqi_coef_mul_schar(MAT(p_trans3,1,1), MAT(C2,0,1), -2);

	/** p(1,2) = -2*a[0]*sqrtD  -2*a[1]*sqrtdelta*sqrtD */
	mqi_coef_set_zero(MAT(p_trans0,1,2));
	mqi_coef_set_zero(MAT(p_trans1,1,2));
	mqi_coef_mul_schar(MAT(p_trans2,1,2), MAT(C1,0,0), -2);
	mqi_coef_mul_schar(MAT(p_trans3,1,2), MAT(C2,0,0), -2);

	/** p(2,0) = -a[0]^2-a[1]^2*delta  -2*a[0]*a[1]*sqrtdelta */
	mqi_coef_linexpr3(MAT(p_trans0,2,0),2,	-1,MAT(C1,0,0),MAT(C1,0,0),NULL,	-1,MAT(C2,0,0),MAT(C2,0,0),delta);
	mqi_coef_linexpr1(MAT(p_trans1,2,0),1,	-2,MAT(C1,0,0),MAT(C2,0,0));
	mqi_coef_set_zero(MAT(p_trans2,2,0));
	mqi_coef_set_zero(MAT(p_trans3,2,0));

	/** p(2,1) = -a[0]*c[0]-a[1]*c[1]*delta + sqrtdelta*(-a[0]*c[1]-a[1]*c[0]) */	
	mqi_coef_linexpr3(MAT(p_trans0,2,1),2,	-1,MAT(C1,0,0),MAT(C1,1,1),NULL,	-1,MAT(C2,0,0),MAT(C2,1,1),delta);
	mqi_coef_linexpr1(MAT(p_trans1,2,1),2,	-1,MAT(C1,0,0),MAT(C2,1,1),		-1,MAT(C2,0,0),MAT(C1,1,1));
      	mqi_coef_set_zero(MAT(p_trans2,2,1));
	mqi_coef_set_zero(MAT(p_trans3,2,1));	

	/** p(2,2) = -2*a[0]*b[0]-2*a[1]*b[1]*delta + sqrtdelta*(-2*a[0]*b[1]-2*a[1]*b[0]) */
	mqi_coef_linexpr3(MAT(p_trans0,2,2),2,	-2,MAT(C1,0,0),MAT(C1,0,1),NULL,	-2,MAT(C2,0,0),MAT(C2,0,1),delta);
	mqi_coef_linexpr1(MAT(p_trans1,2,2),2,	-2,MAT(C1,0,0),MAT(C2,0,1),		-2,MAT(C2,0,0),MAT(C1,0,1));
	mqi_coef_set_zero(MAT(p_trans2,2,2));
	mqi_coef_set_zero(MAT(p_trans3,2,2));

	/** The parametrization */
	mqi_poly_set_x2 (CP(par,0));
	mqi_poly_set_wi (CP(par,1),2);
	mqi_poly_set_xw (CP(par,2));

	/** nd is = to eg v1 such that the subdiscriminant dxy =
  	  * nd[2]^2*(nd[0]+sqrt(delta)*nd[1]) */
	mqi_coef_cpy (VECT(D,0), VECT(nd,0));
	mqi_coef_cpy (VECT(D,1), VECT(nd,1));

	if ( mqi_coef_cmp_schar (VECT(nd,2), 1) != 0 ) {
		mqi_mat_mul_coef (p_trans2, p_trans2, VECT(nd,2));
		mqi_mat_mul_coef (p_trans3, p_trans3, VECT(nd,2));
	}
	
}

/** Parameterization by -f*p > 0
  * Equation is (a*f-e^2)*(f*z+e*x+d*y)^2+(x*(a*f-e^2)+y*(b*f-d*e))^2+f*p*y^2 = 0
  * The param is 
  * (p_trans0 + sqrt(delta)*p_trans1 + sqrt(D)*p_trans2 +
                  sqrt(delta)*sqrt(D)*p_trans3) * [u^2 v^2 uv] 
  * par = [u^2 v^2 uv]
  * D = D[0] + sqrt(delta)*D[1] */
void _qi_param_conic_nr2                  (mqi_mat_t __IN C1, mqi_mat_t __IN C2, mqi_coef_t __IN delta, mqi_vect_t D,
                                           mqi_vect_t __IN de, mqi_vect_t __IN p3, mqi_vect_t __IN nd, mqi_vect_t __IN np,
                                           mqi_mat_t p_trans0, mqi_mat_t p_trans1, mqi_mat_t p_trans2, mqi_mat_t p_trans3,
                                           mqi_curve_param_t par)
{

	/** Here we simply recopy the procedure qi_param_conic_2() after replacing conic q by
	  * C1+sqrt(delta)*C2 
	  * Conic is ax^2 + 2bxy + cy^2 + 2dyz + 2exz + fz^2
	  * a = a[0] + a[1]*sqrt(delta) b=...
	  * Inertia of the conic is supposed to be [2 1] */
	mqi_vect_t t1, t2, vf;
	mqi_coef_t square, cofactor, gcd;

	mqi_vect_init_dynamic (t1, 2, mqi_mat_typeof(C1));
	mqi_vect_init_dynamic (t2, 2, mqi_mat_typeof(C1));
	mqi_vect_init_dynamic (vf, 3, mqi_mat_typeof(C1));	

	/** Computed from the parameterization of conic_param2() 
	  * the  parameterization matrix is as follows
	  * Let   t1 = b*f-d*e;   t2 = a*d-b*e;
	  * t1 = (b[0]+sqrt(delta)*b[1])*(f[0]+sqrt(delta)*f[1])
	  *          -(d[0]+sqrt(delta)*d[1])*(e[0]+sqrt(delta)*e[1])
	  * t2 = (a[0]+sqrt(delta)*a[1])*(d[0]+sqrt(delta)*d[1])
	  *          -(b[0]+sqrt(delta)*b[1])*(e[0]+sqrt(delta)*e[1])	*/
	mqi_coef_linexpr1(VECT(t1,0),2,	1,MAT(C2,0,1),MAT(C2,2,2),	-1,MAT(C2,1,2),MAT(C2,0,2));
	mqi_coef_linexpr1(VECT(t1,0),3,	1,delta,VECT(t1,0),		1,MAT(C1,0,1),MAT(C1,2,2),
					-1,MAT(C1,1,2),MAT(C1,0,2));
	mqi_coef_linexpr1(VECT(t1,1),4,	1, MAT(C1,0,1),MAT(C2,2,2),	1,MAT(C2,0,1), MAT(C1,2,2),
					-1, MAT(C1,1,2),MAT(C2,0,2),	-1,MAT(C2,1,2), MAT(C1,0,2));
	mqi_coef_linexpr1(VECT(t2,0),2,	1,MAT(C2,0,0),MAT(C2,1,2),	-1,MAT(C2,0,1),MAT(C2,0,2));
	mqi_coef_linexpr1(VECT(t2,0),3,	1,delta,VECT(t2,0),	1, MAT(C1,0,0), MAT(C1,1,2),
					-1, MAT(C1,0,1), MAT(C1,0,2));
	mqi_coef_linexpr1(VECT(t2,1),4,	1, MAT(C1,0,0),MAT(C2,1,2),	1,MAT(C2,0,0), MAT(C1,1,2),
					-1, MAT(C1,0,1),MAT(C2,0,2),	-1,MAT(C2,0,1), MAT(C1,0,2));

	/* p = (p_trans0 + sqrt(delta)*p_trans1 + sqrt(D)*p_trans2 +
	 * 	 sqrt(delta)*sqrt(D)*p_trans3) * [u^2 v^2 uv]  */
	
	/** e^2 - af < 0 */
	if ( qi_coef_sign (VECT(nd,0), VECT(nd,1), delta) < 0 ) {
		
		mqi_coef_linexpr3(MAT(p_trans0,0,0),2,	1, MAT(C1,2,2),VECT(t1,0),NULL,	 1,delta,MAT(C2,2,2),VECT(t1,1));
		mqi_coef_linexpr1(MAT(p_trans1,0,0),2,	1, MAT(C1,2,2),VECT(t1,1),	 1,MAT(C2,2,2),VECT(t1,0));

		mqi_coef_linexpr3(MAT(p_trans0,0,1),2,	-1,MAT(p_trans0,0,0),VECT(nd,0),NULL,	-1,delta,MAT(p_trans1,0,0),VECT(nd,1));
		mqi_coef_linexpr1(MAT(p_trans1,0,1),2,	-1,MAT(p_trans1,0,0),VECT(nd,0),	-1,MAT(p_trans0,0,0),VECT(nd,1));

		mqi_coef_linexpr3(MAT(p_trans0,1,0),2,	1, MAT(C1,2,2),VECT(de,0),NULL,	 1,delta,MAT(C2,2,2),VECT(de,1));
		mqi_coef_linexpr1(MAT(p_trans1,1,0),2,	1, MAT(C1,2,2),VECT(de,1),	 1,MAT(C2,2,2),VECT(de,1));

		mqi_coef_linexpr3(MAT(p_trans0,1,1),2,	-1,MAT(p_trans0,1,0),VECT(nd,0),NULL,	-1,delta,MAT(p_trans1,1,0),VECT(nd,1));
		mqi_coef_linexpr1(MAT(p_trans1,1,1),2,	-1,MAT(p_trans1,1,0),VECT(nd,0),	-1,MAT(p_trans0,1,0),VECT(nd,1));

		mqi_coef_linexpr3(MAT(p_trans0,2,0),2,	1, MAT(C1,2,2),VECT(t2,0),NULL,	 1,delta,MAT(C2,2,2),VECT(t2,1));
		mqi_coef_linexpr1(MAT(p_trans1,2,0),2,	1, MAT(C1,2,2),VECT(t2,1),	 1,MAT(C2,2,2),VECT(t2,0));

		mqi_coef_linexpr3(MAT(p_trans0,2,1),2,	-1,MAT(p_trans0,2,0),VECT(nd,0),NULL,	-1,delta,MAT(p_trans1,2,0),VECT(nd,1));
		mqi_coef_linexpr1(MAT(p_trans1,2,1),2,	-1,MAT(p_trans1,2,0),VECT(nd,0),	-1,MAT(p_trans0,2,0),VECT(nd,1));

		mqi_coef_neg(MAT(p_trans2,0,0), MAT(C1,2,2));
		mqi_coef_neg(MAT(p_trans3,0,0), MAT(C2,2,2));

		mqi_coef_linexpr3(MAT(p_trans2,0,1),2,	1,MAT(p_trans2,0,0),VECT(nd,0),NULL,	1,delta,MAT(p_trans3,0,0),VECT(nd,1));
		mqi_coef_linexpr1(MAT(p_trans3,0,1),2,	1,MAT(p_trans3,0,0),VECT(nd,0),		1,MAT(p_trans2,0,0),VECT(nd,1));

		mqi_coef_cpy(MAT(p_trans2,2,0), MAT(C1,0,2));
		mqi_coef_cpy(MAT(p_trans3,2,0), MAT(C2,0,2));

		mqi_coef_linexpr3(MAT(p_trans2,2,1),2,	1,MAT(p_trans2,2,0),VECT(nd,0),NULL,	1,delta,MAT(p_trans3,2,0),VECT(nd,1));
		mqi_coef_linexpr1(MAT(p_trans3,2,1),2,	1,MAT(p_trans3,2,0),VECT(nd,0),		1,MAT(p_trans2,2,0),VECT(nd,1));

		mqi_coef_linexpr1(MAT(p_trans2,2,2),1,	2,VECT(nd,0),VECT(nd,2));
		mqi_coef_linexpr1(MAT(p_trans3,2,2),1,	2,VECT(nd,1),VECT(nd,2));

	}else {
		/** base_vector <bigint> vf = extract_square_factors(de*np[1],1,cout);
		 * de*np[1] is here = (de[0]+sqrt(delta)*de[1]) * (np[0]+sqrt(delta)*np[1])
		 * = (de[0]*np[0] + de[1]*np[1]*delta) +sqrt(delta)*(de[0]*np[1]+de[1]*np[0]) */
		mqi_coef_list_init_dynamic(mqi_coef_typeof(delta), square, cofactor, gcd, NULL);

		mqi_coef_linexpr3(VECT(vf,0),2,		1,VECT(de,0),VECT(np,0),NULL,	1,VECT(de,1),VECT(np,1),delta);
		mqi_coef_linexpr1(VECT(vf,1),2,		1,VECT(de,0),VECT(np,1),	1,VECT(de,1),VECT(np,0));
		mqi_vect_set_at_int (vf, 2, 1);

		mqi_coef_gcd (gcd, VECT(vf,0), VECT(vf,1));
		qi_coef_extract_square_factors (__INIT square, __INIT cofactor, gcd);	
	
		if ( mqi_coef_cmp_schar (square, 1) != 0 ) {
			mqi_vect_set_at	       (vf, 2, square);
			mqi_coef_mul (square, square, square);
			mqi_coef_divexact (VECT(vf,0), VECT(vf,0), square);
			mqi_coef_divexact (VECT(vf,1), VECT(vf,1), square);
		}

		mqi_coef_linexpr3(MAT(p_trans0,0,0),2,	1,MAT(C1,2,2),VECT(p3,0),NULL,		1,delta,MAT(C2,2,2),VECT(p3,1));
		mqi_coef_linexpr1(MAT(p_trans1,0,0),2,	1,MAT(C1,2,2),VECT(p3,1),		1,MAT(C2,2,2),VECT(p3,0));
		
		mqi_coef_linexpr3(MAT(p_trans0,0,1),2,	1,MAT(p_trans0,0,0),VECT(vf,0),NULL,	1,delta,MAT(p_trans1,0,0),VECT(vf,1));
		mqi_coef_linexpr1(MAT(p_trans1,0,1),2,	1,MAT(p_trans1,0,0),VECT(vf,0),		1,MAT(p_trans0,0,0),VECT(vf,1));

		mqi_coef_linexpr3(MAT(p_trans0,2,0),2,	-1,MAT(C1,0,2),VECT(p3,0),NULL,		-1,delta,MAT(p_trans1,2,0),VECT(vf,1));
		mqi_coef_linexpr1(MAT(p_trans1,2,0),2,	-1,MAT(C1,0,2),VECT(p3,1),		-1,MAT(C2,0,2),VECT(p3,0));

		mqi_coef_linexpr3(MAT(p_trans0,2,1),2,	1,MAT(p_trans0,2,0),VECT(vf,0),NULL,	1,delta,MAT(p_trans1,2,0),VECT(vf,1));
		mqi_coef_linexpr1(MAT(p_trans1,2,1),2,	1,MAT(p_trans1,2,0),VECT(vf,0),		1,MAT(p_trans0,2,0),VECT(vf,1));



		mqi_coef_linexpr3(MAT(p_trans2,0,0),2,	1, MAT(C1,2,2),VECT(t1,0),NULL,		1,delta,MAT(C2,2,2),VECT(t1,1));
		mqi_coef_linexpr1(MAT(p_trans3,0,0),2,	1, MAT(C1,2,2),VECT(t1,1),		1,MAT(C2,2,2),VECT(t1,0));

		mqi_coef_linexpr3(MAT(p_trans2,0,1),2,	-1,MAT(p_trans2,0,0),VECT(vf,0),NULL,	-1,delta,MAT(p_trans3,0,0),VECT(vf,1));
		mqi_coef_linexpr1(MAT(p_trans3,0,1),2,	-1,MAT(p_trans3,0,0),VECT(vf,0),	-1,MAT(p_trans2,0,0),VECT(vf,1));

		mqi_coef_linexpr3(MAT(p_trans2,1,0),2,	1, MAT(C1,2,2),VECT(de,0),NULL,		1,delta,MAT(C2,2,2),VECT(de,1));
		mqi_coef_linexpr1(MAT(p_trans3,1,0),2,	1, MAT(C1,2,2),VECT(de,1),		1,MAT(C2,2,2),VECT(de,0));

		mqi_coef_linexpr3(MAT(p_trans2,1,1),2,	-1,MAT(p_trans2,1,0),VECT(vf,0),NULL,	-1,delta,MAT(p_trans3,1,0),VECT(vf,1));
		mqi_coef_linexpr1(MAT(p_trans3,1,1),2,	-1,MAT(p_trans3,1,0),VECT(vf,0),	-1,MAT(p_trans2,1,0),VECT(vf,1));

		mqi_coef_linexpr3(MAT(p_trans2,2,0),2,	1, MAT(C1,2,2),VECT(t2,0),NULL,		1,delta,MAT(C2,2,2),VECT(t2,1));
		mqi_coef_linexpr1(MAT(p_trans3,2,0),2,	1, MAT(C1,2,2),VECT(t2,1),		1,MAT(C2,2,2),VECT(t2,0));

		mqi_coef_linexpr3(MAT(p_trans2,2,1),2,	-1,MAT(p_trans2,2,0),VECT(vf,0),NULL,	-1,delta,MAT(p_trans3,2,0),VECT(vf,1));
		mqi_coef_linexpr1(MAT(p_trans3,2,1),2,	1,MAT(p_trans3,2,0),VECT(vf,0),		-1,MAT(p_trans2,2,0),VECT(vf,1));

		mqi_coef_linexpr3 (MAT(p_trans2,2,2),1,	-2,VECT(np,2),VECT(vf,2),VECT(vf,0));
		mqi_coef_linexpr3 (MAT(p_trans3,2,2),1,	-2,VECT(np,2),VECT(vf,2),VECT(vf,1));

	}


	/** The parametrization */
	/** This has been invented !
	$ mqi_curve_param_init_dynamic (par, 4, QI_POLY_MAXDEG, 0, mqi_mat_typeof(C1));
	*/

	mqi_poly_set_x2 (CP(par,0));
	mqi_poly_set_wi (CP(par,1), 2);
	mqi_poly_set_xw (CP(par,2));

	/** This has been invented !
	$ mqi_vect_init_dynamic (D, 2, mqi_mat_typeof(C1));
	*/

	/** np is = to eg v1 such that the subdiscriminant dxy =
  	  * np[2]^2*(np[0]+sqrt(delta)*np[1]) */
	mqi_coef_cpy (VECT(D,0), VECT(np,0));
	mqi_coef_cpy (VECT(D,1), VECT(np,1));

	if ( mqi_coef_cmp_schar (VECT(np,2), 1) != 0 ) {
		mqi_mat_mul_coef (p_trans2, p_trans2, VECT(np,2));
		mqi_mat_mul_coef (p_trans3, p_trans3, VECT(np,2));
	}
	
	mqi_vect_list_clear(t1, t2, vf, NULL);
	mqi_coef_list_clear(square, cofactor, gcd, NULL);

}

/** Computes the parameterization of a non-singular conic when there is no rational
    point or we were unable to find a simple one. */
void _qi_param_conic_no_ratpoint         (mqi_mat_t __IN q, mqi_coef_t __INIT D, mqi_mat_t p_trans, mqi_mat_t p_trans2, mqi_curve_param_t par,
                                          mqi_coef_t __IN dxyv, mqi_coef_t __IN dxzv, mqi_coef_t __IN dyzv, mqi_coef_t __IN p1v,
                                          mqi_coef_t __IN p2v, mqi_coef_t __IN p3v)
{

	/** If qi_settings.optimize = 1 (previously known as "optlevel"), we know there is no rational point. Otherwise, there still
	  * is a chance that we find one such point. */
	mqi_coef_t dxy, dxz, dyz, p1, p2, p3;
	mqi_coef_t tmp, p, sq, cofactor;
	mqi_vect_t v1, v2, v3, v4, v5, v6;
	mqi_mat_t  tr, tmp_mat;
	mempool_t pool;
	int id;

	mempool_init(pool);

	mqi_coef_initpool_dynamic (tmp, mqi_mat_typeof(q));

	if ( qi_settings.optimize == 1 ) {
		
		mqi_coef_list_initpool_dynamic (mqi_mat_typeof(q), dxy, dxz, dyz, p, p1, p2, p3, NULL);	
		
		mqi_coef_linexpr1(dxy,2,	 1,MAT(q,0,1),MAT(q,0,1),	-1,MAT(q,0,0),MAT(q,1,1));
		mqi_coef_linexpr1(dxz,2,	 1,MAT(q,0,2),MAT(q,0,2),	-1,MAT(q,0,0),MAT(q,2,2));
		mqi_coef_linexpr1(dyz,2,	 1,MAT(q,1,2),MAT(q,1,2),	-1,MAT(q,1,1),MAT(q,2,2));

		mqi_mat_det (p, q);

		mqi_coef_linexpr1(p1,1, -1,MAT(q,0,0),p);
		mqi_coef_linexpr1(p2,1, -1,MAT(q,1,1),p);
		mqi_coef_linexpr1(p3,1, -1,MAT(q,2,2),p);
		

	}else {

		mqi_coef_initpool_cpy (dxy, dxyv);
		mqi_coef_initpool_cpy (dxz, dxzv);
		mqi_coef_initpool_cpy (dyz, dyzv);
		mqi_coef_initpool_cpy (p1, p1v);
		mqi_coef_initpool_cpy (p2, p2v);
		mqi_coef_initpool_cpy (p3, p3v);

	}
	
	/** Try to find the smallest square root */
	mqi_vect_list_initpool_dynamic (mqi_mat_typeof(q), 2, v1, v2, v3, v4, v5, v6, NULL);

	mqi_vect_set_at_int (v1, 0, 1);
	mqi_vect_set_at_int (v2, 0, 1);
	mqi_vect_set_at_int (v3, 0, 1);
	mqi_vect_set_at_int (v4, 0, 1);
	mqi_vect_set_at_int (v5, 0, 1);
	mqi_vect_set_at_int (v6, 0, 1);

	mqi_vect_set_at	   (v1, 1, dxy);
	mqi_vect_set_at    (v2, 1, dxz);
	mqi_vect_set_at	   (v3, 1, dyz);
	mqi_vect_set_at	   (v4, 1, p1);
	mqi_vect_set_at	   (v5, 1, p2);
	mqi_vect_set_at	   (v6, 1, p3);


	mqi_coef_initpool_dynamic (cofactor, mqi_mat_typeof(q));
	mqi_coef_initpool_dynamic (sq, mqi_coef_typeof(cofactor));

#define optimize_vects(vect)								\
	if ( mqi_coef_sign (VECT(vect,1)) > 0 ) {					\
		qi_coef_extract_square_factors (sq, cofactor, VECT(vect,1));		\
		mqi_coef_cpy (VECT(vect,0), sq);					\
		mqi_coef_cpy (VECT(vect,1), cofactor);					\
	}

	optimize_vects(v1)
	optimize_vects(v2)
	optimize_vects(v3)
	optimize_vects(v4)
	optimize_vects(v5)
	optimize_vects(v6)

#undef optimize_vects

	if ( mqi_coef_sign (VECT(v1,1)) < 0 ) {
		id = 4;
		mqi_coef_cpy (sq, VECT(v4,1));
	}
	else {
		id = 1;
		mqi_coef_cpy (sq, VECT(v1,1));	
	}

	if ( mqi_coef_cmp(VECT(v2,1), sq) < 0 && mqi_coef_sign(VECT(v2,1)) > 0 ) {
		id = 2;
		mqi_coef_cpy (sq, VECT(v2,1));
	}	
	
	if ( mqi_coef_cmp(VECT(v3,1), sq) < 0 && mqi_coef_sign(VECT(v3,1)) > 0 ) {
		id = 3;
		mqi_coef_cpy (sq, VECT(v3,1));
	}

	if ( ( id != 4 ) && mqi_coef_cmp(VECT(v4,1), sq) < 0 && mqi_coef_sign(VECT(v4,1)) > 0 ) {
		id = 4;
		mqi_coef_cpy (sq, VECT(v4,1));
	}
	
	if ( mqi_coef_cmp(VECT(v5,1), sq) < 0 && mqi_coef_sign(VECT(v5,1)) > 0 ) {
		id = 5;
		mqi_coef_cpy (sq, VECT(v5,1));
	}

	if ( mqi_coef_cmp(VECT(v6,1), sq) < 0 && mqi_coef_sign(VECT(v6,1)) > 0 ) {
		id = 6;
	}


	switch ( id ) {

		case 1:
			/** Switch y and z */
			mqi_mat_initpool_dynamic (tr, 3, 3, mqi_mat_typeof(q));
			mqi_mat_set_at_int    (tr, 0, 1, 0);
			mqi_mat_set_at_int    (tr, 2, 1, 1);
			mqi_mat_set_at_int    (tr, 1, 1, 2);

			mqi_mat_initpool_cpy     (tmp_mat, q);
			mqi_mat_mul	     (tmp_mat, tmp_mat, tr);
			mqi_mat_mul	     (tmp_mat, tr, tmp_mat);

			_qi_param_conic_1      (tmp_mat, D, v1, p_trans, p_trans2, par);

			mqi_mat_mul (p_trans, tr, p_trans);
			mqi_mat_mul (p_trans2, tr, p_trans2);	
		
			break;

		case 2:
			_qi_param_conic_1 (q, D, v2, p_trans, p_trans2, par);
			break;

		case 3:
			/** Switch x and y */
			mqi_mat_initpool_dynamic (tr, 3, 3, mqi_mat_typeof(q));
			mqi_mat_set_at_int    (tr, 1, 1, 0);
			mqi_mat_set_at_int    (tr, 0, 1, 1);
			mqi_mat_set_at_int    (tr, 2, 1, 2);

			mqi_mat_initpool_cpy     (tmp_mat, q);
			mqi_mat_mul	     (tmp_mat, tmp_mat, tr);
			mqi_mat_mul	     (tmp_mat, tr, tmp_mat);

			_qi_param_conic_1      (tmp_mat, D, v3, p_trans, p_trans2, par);

			mqi_mat_mul (p_trans, tr, p_trans);
			mqi_mat_mul (p_trans2, tr, p_trans2);	
		
			break;
		
		case 4:
			/** Switch x and z */
			mqi_mat_initpool_dynamic (tr, 3, 3, mqi_mat_typeof(q));
			mqi_mat_set_at_int    (tr, 2, 1, 0);
			mqi_mat_set_at_int    (tr, 1, 1, 1);
			mqi_mat_set_at_int    (tr, 0, 1, 2);

			mqi_mat_initpool_cpy     (tmp_mat, q);
			mqi_mat_mul	     (tmp_mat, tmp_mat, tr);
			mqi_mat_mul	     (tmp_mat, tr, tmp_mat);

			_qi_param_conic_2      (tmp_mat, D, dxz, p1, v2, v4, p_trans, p_trans2, par);

			mqi_mat_mul (p_trans, tr, p_trans);
			mqi_mat_mul (p_trans2, tr, p_trans2);	
		
			break;
	
		case 5:
			/** Switch y and z */
			mqi_mat_initpool_dynamic (tr, 3, 3, mqi_mat_typeof(q));
			mqi_mat_set_at_int    (tr, 0, 1, 0);
			mqi_mat_set_at_int    (tr, 2, 1, 1);
			mqi_mat_set_at_int    (tr, 1, 1, 2);

			mqi_mat_initpool_cpy     (tmp_mat, q);
			mqi_mat_mul	     (tmp_mat, tmp_mat, tr);
			mqi_mat_mul	     (tmp_mat, tr, tmp_mat);

			_qi_param_conic_2      (tmp_mat, D, dxy, p2, v1, v5, p_trans, p_trans2, par);

			mqi_mat_mul (p_trans, tr, p_trans);
			mqi_mat_mul (p_trans2, tr, p_trans2);	
		
			break;

		case 6:
			_qi_param_conic_2     (q, D, dxz, p3, v2, v6, p_trans, p_trans2, par);
			break;

		default:
			mqi_log_error ("In %s: case not recognized: %d\n", __func__, id);
			break;
	

	}	

	mempool_clear();

}


/** Parametrization of conics without optimization. (cf old version: opt_level = 0) */
void _qi_param_conic_no_opt              (mqi_mat_t __IN q, mqi_coef_t __INIT D, mqi_mat_t p_trans, mqi_mat_t p_trans2, mqi_curve_param_t par) {

	size_t k;
	mqi_vect_t     rat_point, l, v, v2;
	mqi_coef_t     dxy, dxz, dyz;
	mqi_coef_t     p, p1, p2, p3, sq;
	mqi_mat_t      tr, tmp_mat;
	mempool_t  pool;


	mempool_init(pool);

	/** If one of the diagonal elements is zero, we know how to get a rational param */
	for ( k = 0; k < 3; k++) {
		
		if ( mqi_coef_sign (MAT(q,k,k)) == 0 ) {
			mqi_vect_initpool_dynamic (rat_point, 3, mqi_mat_typeof(q));
			mqi_vect_set_at_int    (rat_point, k, 1);
			
			mqi_vect_initpool_dynamic (l, 2, mqi_mat_typeof(q));
			qi_param_conic_through_ratpoint (q, rat_point, p_trans, par, l);

			/** Sets D to zero */
			mqi_coef_init_dynamic (D, mqi_mat_typeof(q));

			mempool_clear();
			return;
		}

	}


	/** Now, if one of the following 2x2 discriminants is a square, we also have
  	  * a rational param. */

	mqi_coef_list_initpool_dynamic (mqi_mat_typeof(q), dxy, dxz, dyz, p, p1, p2, p3, NULL);

	mqi_coef_linexpr1(dxy,2,	1,MAT(q,0,1),MAT(q,0,1),	-1,MAT(q,0,0),MAT(q,1,1));
	mqi_coef_linexpr1(dxz,2,	1,MAT(q,0,2),MAT(q,0,2),	-1,MAT(q,0,0),MAT(q,2,2));
	mqi_coef_linexpr1(dyz,2,	1,MAT(q,1,2),MAT(q,1,2),	-1,MAT(q,1,1),MAT(q,2,2));

	mqi_mat_det (p, q);

	mqi_coef_linexpr1(p1,1,	-1,MAT(q,0,0),p);
	mqi_coef_linexpr1(p2,1,	-1,MAT(q,1,1),p);
	mqi_coef_linexpr1(p3,1,	-1,MAT(q,2,2),p);

	if ( mqi_coef_sign(dxy) > 0 && mqi_coef_is_square (dxy) ) {
		mqi_coef_initpool_cpy (sq, dxy);
		mqi_coef_sqrt	  (sq, sq);

		/** Switch y and z */
		mqi_mat_initpool_dynamic (tr, 3, 3, mqi_mat_typeof(q));
		mqi_mat_set_at_int    (tr, 0, 1, 0);
		mqi_mat_set_at_int    (tr, 2, 1, 1);
		mqi_mat_set_at_int    (tr, 1, 1, 2);

		mqi_mat_initpool_cpy     (tmp_mat, q);
		mqi_mat_mul	     (tmp_mat, tmp_mat, tr);
		mqi_mat_mul	     (tmp_mat, tr, tmp_mat);

		mqi_vect_initpool_dynamic (v, 2, mqi_mat_typeof(q));
		mqi_vect_set_at	       (v, 0, sq);
		mqi_vect_set_at_int    (v, 1,  1);

		_qi_param_conic_1      (tmp_mat, D, v, p_trans, p_trans2, par);

		mqi_mat_mul (p_trans, tr, p_trans);
		mqi_mat_mul (p_trans2, tr, p_trans2);	
	
		goto _qi_param_conic_no_opt_end;

	}
	else if ( mqi_coef_is_square (dxz) ) {
		mqi_coef_initpool_cpy (sq, dxz);
		mqi_coef_sqrt	  (sq, sq);

		mqi_vect_initpool_dynamic (v, 2, mqi_mat_typeof(q));
		mqi_vect_set_at	      (v, 0, sq);
		mqi_vect_set_at_int   (v, 1, 1);

		_qi_param_conic_1      (q, D, v, p_trans, p_trans2, par);

		goto _qi_param_conic_no_opt_end;
	
	}
	else if ( mqi_coef_is_square (dyz) ) {

		mqi_coef_initpool_cpy (sq, dyz);
		mqi_coef_sqrt	  (sq, sq);

		/** Switch x and y */
		mqi_mat_initpool_dynamic (tr, 3, 3, mqi_mat_typeof(q));
		mqi_mat_set_at_int    (tr, 1, 1, 0);
		mqi_mat_set_at_int    (tr, 0, 1, 0);
		mqi_mat_set_at_int    (tr, 2, 1, 2);

		mqi_mat_initpool_cpy     (tmp_mat, q);
		mqi_mat_mul	     (tmp_mat, tmp_mat, tr);
		mqi_mat_mul	     (tmp_mat, tr, tmp_mat);

		mqi_vect_initpool_dynamic (v, 2, mqi_mat_typeof(q));
		mqi_vect_set_at	      (v, 0, sq);
		mqi_vect_set_at_int    (v, 1,  1);

		_qi_param_conic_1      (tmp_mat, D, v, p_trans, p_trans2, par);

		mqi_mat_mul (p_trans, tr, p_trans);
		mqi_mat_mul (p_trans2, tr, p_trans2);	
	
		goto _qi_param_conic_no_opt_end;

	}
	else if ( mqi_coef_is_square (p1) ) {
		mqi_coef_initpool_cpy (sq, p1);
		mqi_coef_sqrt	  (sq, sq);

		/** Switch x and z */
		mqi_mat_initpool_dynamic (tr, 3, 3, mqi_mat_typeof(q));
		mqi_mat_set_at_int    (tr, 2, 1, 0);
		mqi_mat_set_at_int    (tr, 1, 1, 1);
		mqi_mat_set_at_int    (tr, 0, 1, 2);

		mqi_mat_initpool_cpy     (tmp_mat, q);
		mqi_mat_mul	     (tmp_mat, tmp_mat, tr);
		mqi_mat_mul	     (tmp_mat, tr, tmp_mat);

		mqi_vect_initpool_dynamic (v, 2, mqi_mat_typeof(q));
		mqi_vect_set	      (v, 0, sq);
		mqi_vect_set_at_int    (v, 1,  1);

		mqi_vect_initpool_dynamic (v2, 2, mqi_mat_typeof(q));
		qi_coef_extract_square_factors (VECT(v2,0), VECT(v2,1), dxz);

		_qi_param_conic_2      (tmp_mat, D, dxz, p1, v2, v, p_trans, p_trans2, par);

		mqi_mat_mul (p_trans, tr, p_trans);
		mqi_mat_mul (p_trans2, tr, p_trans2);	
	
		goto _qi_param_conic_no_opt_end;

	}
	else if ( mqi_coef_is_square (p2) ) {
		mqi_coef_initpool_cpy (sq, p2);
		mqi_coef_sqrt	  (sq, sq);

		mqi_mat_initpool_dynamic (tr, 3, 3, mqi_mat_typeof(q));
		mqi_mat_set_at_int    (tr, 2, 1, 0);
		mqi_mat_set_at_int    (tr, 1, 1, 1);
		mqi_mat_set_at_int    (tr, 0, 1, 2);

		mqi_mat_initpool_cpy     (tmp_mat, q);
		mqi_mat_mul	     (tmp_mat, tmp_mat, tr);
		mqi_mat_mul	     (tmp_mat, tr, tmp_mat);

		mqi_vect_initpool_dynamic (v, 2, mqi_mat_typeof(q));
		mqi_vect_set	      (v, 0, sq);
		mqi_vect_set_at_int    (v, 1,  1);

		mqi_vect_initpool_dynamic (v2, 2, mqi_mat_typeof(q));
		qi_coef_extract_square_factors (VECT(v2,0), VECT(v2,1), dxy);

		_qi_param_conic_2      (tmp_mat, D, dxy, p2, v2, v, p_trans, p_trans2, par);

		mqi_mat_mul (p_trans, tr, p_trans);
		mqi_mat_mul (p_trans2, tr, p_trans2);	
	
		goto _qi_param_conic_no_opt_end;

	}
	else if ( mqi_coef_is_square (p3) ) {
		mqi_coef_initpool_cpy (sq, p3);
		mqi_coef_sqrt	  (sq, sq);

		mqi_vect_initpool_dynamic (v, 2, mqi_mat_typeof(q));
		mqi_vect_set_at	      (v, 0, sq);
		mqi_vect_set_at_int   (v, 1,  1);

		mqi_vect_initpool_dynamic (v2, 2, mqi_mat_typeof(q));
		qi_coef_extract_square_factors (VECT(v2,0), VECT(v2,1), dxz);

		_qi_param_conic_2      (q, D, dxz, p3, v2, v, p_trans, p_trans2, par);
	
		goto _qi_param_conic_no_opt_end;

	}else {
		_qi_param_conic_no_ratpoint (q, D, p_trans, p_trans2, par, dxy, dxz, dyz, p1, p2, p3);
	}



_qi_param_conic_no_opt_end:
	mempool_clear();

}



/** *************** */
/** Public routines */
/** *************** */


/** Computes the parameterization of a non-singular conic through a rational point
  * Conic should be in the plane (x,y,z) */
void qi_param_conic_through_ratpoint    (mqi_mat_t __IN q, mqi_vect_t __IN rat_point, mqi_mat_t p_trans, mqi_curve_param_t p,
                                         mqi_vect_t l) {

	mqi_mat_t	p_trans2, tmp;
	mqi_mat_t	proj_mat;
	mqi_mat_t	q_r;
	mempool_t	pool;

	mempool_init(pool);

	if ( mqi_coef_sign (VECT(rat_point,0)) == 0 &&
	     mqi_coef_sign (VECT(rat_point,1)) == 0 )
	{
		/** No need to rotate */
		mqi_log_printf ("No need to rotate: go through origin\n");
		qi_param_conic_through_origin (q, p_trans, p, l);

		goto _qi_param_conic_through_ratpoint_end;
	}
	
	if ( mqi_coef_sign (VECT(rat_point,0)) == 0 &&
	     mqi_coef_sign (VECT(rat_point,2)) == 0 )
	{
		/** The conic goes through [0,1,0], hence switch y and z */
		mqi_log_printf ("Switch y and z: go through [0,1,0]\n");
		mqi_mat_initpool_dynamic (p_trans2, 3, 3, mqi_mat_typeof(q));

		mqi_mat_set_at_int   (p_trans2,0, 0, 1);
		mqi_mat_set_at_int   (p_trans2,1, 2, 1);
		mqi_mat_set_at_int   (p_trans2,2, 1, 1);

		mqi_mat_initpool_cpy    (tmp, p_trans2);

		mqi_mat_transpose   (tmp, tmp);
		mqi_mat_mul	    (tmp, tmp, q);
		mqi_mat_mul	    (tmp, tmp, p_trans2);
		qi_param_conic_through_origin (tmp, p_trans, p, l);
		mqi_mat_mul	    (p_trans, p_trans2, p_trans);

		goto _qi_param_conic_through_ratpoint_end;
	}
	
	if ( mqi_coef_sign (VECT(rat_point,0)) == 0 &&
	     mqi_coef_sign (VECT(rat_point,2)) == 0 )
	{
		/** The conic goes through [1,0,0], hence switch x and z */
		mqi_log_printf ("Switch x and z: go through [1,0,0]\n");
		mqi_mat_initpool_dynamic (p_trans2, 3, 3, mqi_mat_typeof(q));

		mqi_mat_set_at_int   (p_trans2,1, 1, 1);
		mqi_mat_set_at_int   (p_trans2,0, 2, 1);
		mqi_mat_set_at_int   (p_trans2,2, 0, 1);

		mqi_mat_initpool_cpy    (tmp, p_trans2);

		mqi_mat_transpose   (tmp, tmp);
		mqi_mat_mul	    (tmp, tmp, q);
		mqi_mat_mul	    (tmp, tmp, p_trans2);
		qi_param_conic_through_origin (tmp, p_trans, p, l);
		mqi_mat_mul	    (p_trans, p_trans2, p_trans);

		goto _qi_param_conic_through_ratpoint_end;
	}

	/** Rotate and param */
	mqi_log_printf ("General case\n");
	qi_mat_send_to_infinity (proj_mat, rat_point);
	
	mqi_mat_initpool_cpy (q_r, q);
	
	mqi_mat_mul (q_r, q, proj_mat);
	
	mqi_mat_initpool_cpy (tmp, proj_mat);
	mqi_mat_transpose (tmp, tmp);

	mqi_mat_mul (q_r,tmp, q_r);
	
	qi_param_conic_through_origin (q_r, p_trans, p, l);
	
	mqi_mat_mul (p_trans, proj_mat, p_trans);

_qi_param_conic_through_ratpoint_end:
	mempool_clear();

}

/** Computes the parameterization of a non-singular conic going through the point [0,0,1] */
void qi_param_conic_through_origin      (mqi_mat_t __IN q, mqi_mat_t p_trans, mqi_curve_param_t p, mqi_vect_t l) {

	/** The curve_param (u^2, v^2, uv) */
	mqi_poly_set_x2 (CP(p,0));
	mqi_poly_set_wi (CP(p,1), 2);
	mqi_poly_set_xw (CP(p,2));

	/** Param is u^2 [e,0,-a] + v^2 [0,d,-c] + uv [d,e,-b] */
	mqi_coef_mul_schar (MAT(p_trans,0,0), MAT(q,0,2), 2);
	mqi_coef_neg	   (MAT(p_trans,2,0), MAT(q,0,0));
	mqi_coef_mul_schar (MAT(p_trans,1,1), MAT(q,1,2), 2);
	mqi_coef_neg	   (MAT(p_trans,2,1), MAT(q,1,1));
	mqi_coef_mul_schar (MAT(p_trans,0,2), MAT(q,1,2), 2);
	mqi_coef_mul_schar (MAT(p_trans,1,2), MAT(q,0,2), 2);
	mqi_coef_mul_schar (MAT(p_trans,2,2), MAT(q,0,1), -2);
	
	/** The ``line'' values (parameters for the ratpoint) */
	mqi_coef_cpy (VECT(l,0), MAT(q,0,2));
	mqi_coef_cpy (VECT(l,1), MAT(q,1,2));
	
	qi_vect_optimize (l);
	
}

/** Parametrizes conics when no rational point is known a priori,
  * but there may be some.
  * The param is (p_trans + sqrt(D)*p_trans2) * [u^2 v^2 uv], par = [u^2 v^2 uv] */
void qi_param_conic                     (mqi_mat_t __IN q, mqi_coef_t D, mqi_mat_t p_trans, mqi_mat_t p_trans2,
					 mqi_curve_param_t par)
{
	
	mqi_coef_t zero;

	if ( qi_settings.optimize == 1 ) {
		mqi_log_printf ("FindRationalPointOnConic Not Yet Implemented, as qi_legendre hasn't been ported yet => ignoring the optimization flag.\n");

		mqi_coef_init_dynamic (zero, mqi_mat_typeof(q));	
		_qi_param_conic_no_ratpoint(q, D, p_trans, p_trans2, par, 
				zero, zero, zero, zero, zero, zero);
		mqi_coef_clear(zero);
	}
	else	
		_qi_param_conic_no_opt (q, D, p_trans, p_trans2, par);

}

/** Parametrizes a conic whose corresponding 3x3 matrix is 
  * C1 +sqrt(delta).C2 when no rational point is known
  * The param is
           (p_trans0 + sqrt(delta)*p_trans1 + sqrt(D)*p_trans2 
                      + sqrt(delta)*sqrt(D)*p_trans3) * [u^2 v^2 uv]
  * par = [u^2 v^2 uv]
  * D = D[0] + sqrt(delta)*D[1] */
void qi_param_conic_nr                  (mqi_mat_t __IN C1, mqi_mat_t __IN C2, mqi_coef_t __IN delta, mqi_vect_t D,
                                         mqi_mat_t p_trans0, mqi_mat_t p_trans1, mqi_mat_t p_trans2, mqi_mat_t p_trans3,
					 mqi_curve_param_t par)
{

	mqi_vect_t 	dxy, dxz, dyz, detq, p0, p1, p2;
	mqi_vect_t	v1, v2, v3, v4, v5, v6;
	mqi_vect_ptr	v;
	mqi_coef_t	abs_vect, abs_vect2, tmp, sq, cofactor, acc;
	mqi_mat_t	C1p, C2p, tr;
	mempool_t	pool;
	int id;


	mempool_init(pool);

	/** The conic has no rational point because they would necessarily be on the 
	 * rational singular line of the pair of planes which is impossible since the
	 * intersection curve on the other plane is entirely imaginary

	 * Now, if one of the following 2x2 discriminants is a square, we also have
	 * a rational param. 
	 * dxy, dxz, dyz are minus the 2x2 discriminants 
	 * (ie -det(minor(C1+sqrt(delta)*C2, 2,2)), -det(minor(C1+sqrt(delta)*C2, 1,1)),
	 * -det(minor(C1+sqrt(delta)*C2, 0,0))) dxy = dxy[0] +sqrt(delta).dxy[1] */

	mqi_vect_list_initpool_dynamic (mqi_mat_typeof(C1), 2, dxy, dxz, dyz, detq, p0, p1, p2, NULL);
	
	mqi_coef_linexpr1(VECT(dxy,0),2,	 -1,MAT(C2,0,0),MAT(C2,1,1),	1,MAT(C2,0,1),MAT(C2,0,1));
	mqi_coef_linexpr1(VECT(dxy,0),3,	 1,delta,VECT(dxy,0),	-1,MAT(C1,0,0),MAT(C1,1,1),
					1,MAT(C1,0,1),MAT(C1,0,1));
	mqi_coef_linexpr1(VECT(dxy,1),3,	 -1,MAT(C1,0,0),MAT(C2,1,1),	-1,MAT(C2,0,0),MAT(C1,1,1),
					2,MAT(C1,0,1),MAT(C2,0,1));

	mqi_coef_linexpr1(VECT(dxz,0),2,	 -1,MAT(C2,0,0),MAT(C2,2,2),	1,MAT(C2,0,2),MAT(C2,0,2));
	mqi_coef_linexpr1(VECT(dxz,0),3,	 1,delta,VECT(dxz,0),	-1,MAT(C1,0,0),MAT(C1,2,2),
					1,MAT(C1,0,2),MAT(C1,0,2));
	mqi_coef_linexpr1(VECT(dxz,1),3,	 -1,MAT(C1,0,0),MAT(C2,2,2),	-1,MAT(C2,0,0),MAT(C1,2,2),
					2,MAT(C1,0,2),MAT(C2,0,2));

	mqi_coef_linexpr1(VECT(dyz,0),2,	 -1,MAT(C2,1,1),MAT(C2,2,2),	1,MAT(C2,1,2),MAT(C2,1,2));
	mqi_coef_linexpr1(VECT(dyz,0),3,	 1,delta,VECT(dyz,0),	-1,MAT(C1,1,1),MAT(C1,2,2),
					 1,MAT(C1,1,2),MAT(C1,1,2));
	mqi_coef_linexpr1(VECT(dyz,1),3,	 -1,MAT(C1,1,1),MAT(C2,2,2),	-1,MAT(C2,1,1),MAT(C1,2,2),
					2,MAT(C1,1,2),MAT(C2,1,2));

	mqi_coef_linexpr3(VECT(detq,0), 12,	-1,MAT(C2,0,1),MAT(C2,0,1),MAT(C1,2,2),
						-2,MAT(C2,0,0),MAT(C1,1,2),MAT(C2,1,2),
						-2,MAT(C1,0,2),MAT(C2,0,2),MAT(C2,1,1),
						-1,MAT(C2,0,2),MAT(C2,0,2),MAT(C1,1,1),
						1,MAT(C2,0,0),MAT(C1,1,1),MAT(C2,2,2),
						2,MAT(C1,0,1),MAT(C2,0,2),MAT(C2,1,2),
						-1,MAT(C1,0,0),MAT(C2,1,2),MAT(C2,1,2),
						2,MAT(C2,0,1),MAT(C2,0,2),MAT(C1,1,2),
						1,MAT(C2,0,0),MAT(C2,1,1),MAT(C1,2,2),
						-2,MAT(C1,0,1),MAT(C2,0,1),MAT(C2,2,2),
						2,MAT(C2,0,1),MAT(C1,0,2),MAT(C2,1,2),
						1,MAT(C1,0,0),MAT(C2,1,1),MAT(C2,2,2));
	mqi_coef_linexpr3(VECT(detq,0), 6,	1,delta,VECT(detq,0),NULL,
						-1,MAT(C1,0,2),MAT(C1,0,2),MAT(C1,1,1),
						-1,MAT(C1,0,1),MAT(C1,0,1),MAT(C1,2,2),
						-1,MAT(C1,0,0),MAT(C1,1,2),MAT(C1,1,2),
						1,MAT(C1,0,0),MAT(C1,1,1),MAT(C1,2,2),
						2,MAT(C1,0,1),MAT(C1,0,2),MAT(C1,1,2));

	mqi_coef_linexpr3(VECT(detq,1),5,	-1,MAT(C2,0,1),MAT(C2,0,1),MAT(C2,2,2),
						1,MAT(C2,0,0),MAT(C2,1,1),MAT(C2,2,2),
						-1,MAT(C2,0,0),MAT(C2,1,2),MAT(C2,1,2),
						-1,MAT(C2,0,2),MAT(C2,0,2),MAT(C2,1,1),
						2,MAT(C2,0,1),MAT(C2,0,2),MAT(C2,1,2));
	mqi_coef_linexpr3(VECT(detq,1),13,	1,delta,VECT(detq,1),NULL,
						-1,MAT(C2,0,0),MAT(C1,1,2),MAT(C1,1,2),
						1,MAT(C2,0,0),MAT(C1,1,1),MAT(C1,2,2),
						1,MAT(C1,0,0),MAT(C1,1,1),MAT(C2,2,2),
						2,MAT(C1,0,1),MAT(C2,0,2),MAT(C1,1,2),
						2,MAT(C2,0,1),MAT(C1,0,2),MAT(C1,1,2),
						1,MAT(C1,0,0),MAT(C2,1,1),MAT(C1,2,2),
						2,MAT(C1,0,1),MAT(C1,0,2),MAT(C2,1,2),
						-2,MAT(C1,0,0),MAT(C1,1,2),MAT(C2,1,2),
						-1,MAT(C1,0,1),MAT(C1,0,1),MAT(C2,2,2),
						-2,MAT(C1,0,2),MAT(C2,0,2),MAT(C1,1,1),
						-2,MAT(C1,0,1),MAT(C2,0,1),MAT(C1,2,2),
						-1,MAT(C1,0,2),MAT(C1,0,2),MAT(C2,1,1));

	/** pi = -q(0,0)* det(q) = (-C1(i,i) - sqrt(delta). C2(i,i)) * (detq[0] +
	 * sqrt(delta). detq[1]) = -C1(i,i)*detq[0] -delta*C2(i,i)*detq[1]
	 * +sqrt(delta)*[-C2(i,i)*detq[0] -C1(i,i)*detq[1]] */
	mqi_coef_linexpr3(VECT(p0,0),2, -1,MAT(C1,0,0),VECT(detq,0),NULL,	-1,MAT(C2,0,0),VECT(detq,1),delta);
	mqi_coef_linexpr1(VECT(p0,1),2, -1,MAT(C2,0,0),VECT(detq,0),		-1,MAT(C1,0,0),VECT(detq,1));

	mqi_coef_linexpr3(VECT(p1,0),2, -1,MAT(C1,1,1),VECT(detq,0),NULL,	-1,MAT(C2,1,1),VECT(detq,1),delta);
	mqi_coef_linexpr1(VECT(p1,1),2, -1,MAT(C2,1,1),VECT(detq,0),		-1,MAT(C1,1,1),VECT(detq,1));

	mqi_coef_linexpr3(VECT(p2,0),2, -1,MAT(C1,2,2),VECT(detq,0),NULL,	-1,MAT(C2,2,2),VECT(detq,1),delta);
	mqi_coef_linexpr1(VECT(p2,1),2, -1,MAT(C2,2,2),VECT(detq,0),		-1,MAT(C1,2,2),VECT(detq,1));

	mqi_vect_list_initpool_dynamic(mqi_mat_typeof(C1), 3, v1, v2, v3, v4, v5, v6, NULL);

	mqi_coef_cpy (VECT(v1,0), VECT(dxy,0));
	mqi_coef_cpy (VECT(v2,0), VECT(dxz,0));
	mqi_coef_cpy (VECT(v3,0), VECT(dyz,0));
	mqi_coef_cpy (VECT(v4,0), VECT(p0,0));
	mqi_coef_cpy (VECT(v5,0), VECT(p1,0));
	mqi_coef_cpy (VECT(v6,0), VECT(p2,0));
	
	mqi_coef_cpy (VECT(v1,1), VECT(dxy,1));
	mqi_coef_cpy (VECT(v2,1), VECT(dxz,1));
	mqi_coef_cpy (VECT(v3,1), VECT(dyz,1));
	mqi_coef_cpy (VECT(v4,1), VECT(p0,1));
	mqi_coef_cpy (VECT(v5,1), VECT(p1,1));
	mqi_coef_cpy (VECT(v6,1), VECT(p2,1));

	mqi_coef_set_schar (VECT(v1,2), 1);
	mqi_coef_set_schar (VECT(v2,2), 1);
	mqi_coef_set_schar (VECT(v3,2), 1);
	mqi_coef_set_schar (VECT(v4,2), 1);
	mqi_coef_set_schar (VECT(v5,2), 1);
	mqi_coef_set_schar (VECT(v6,2), 1);

	mqi_coef_list_initpool_dynamic (mqi_mat_typeof(C1), acc, tmp, sq, cofactor, abs_vect, abs_vect2, NULL);

#define optimize_vect(v)								\
		if ( qi_coef_sign (VECT(v,0), VECT(v,1), delta) > 0 ) {			\
											\
			mqi_coef_gcd (tmp, VECT(v,0), VECT(v,1));			\
			qi_coef_extract_square_factors (sq, cofactor, tmp);		\
											\
			if ( mqi_coef_cmp_schar(sq, 1) != 0 ) {				\
				mqi_coef_mul (tmp, sq, sq);				\
				mqi_vect_divexact_coef (v, v, tmp);			\
				mqi_coef_cpy (VECT(v,2), sq);				\
			}								\
		}


	if ( qi_settings.optimize == 1 ) {
	
		optimize_vect(v1)
		optimize_vect(v2)
		optimize_vect(v3)
		optimize_vect(v4)
		optimize_vect(v5)
		optimize_vect(v6)	

	}	
	
	
	/** Find the "smallest square root" among dxy, dxz, dyz, p1, p2, p3 */

	/** vi >0 iff vi!=v_neg
    	  * If dxy < 0, then p1 > 0 and conversely (initialization) */

	if ( qi_coef_sign (VECT(v1,0),VECT(v1,1),delta) < 0 ) {
		id = 4;
		v  = v4;
	}else{
		id = 1;
		v  = v1;		
	}

	/** Find the "smallest square root" among dxy, dxz, dyz, p1, p2, p3
	 * We take vj=vj[2]^2*(vj[0] + sqrt(delta)*vj[1]) instead of v
	 * iff v >0 and [ (v[1]!=0 and (vj[1]=0 or ||vj||<||v||)) or (v[1]=0 and
	 * vj[1]=0 and ||vj||<||v||)], we take ||v|| = abs(v[0]) + abs(v[1]) */
	
	/** A bit of code simplification here. NORM1 stores the manhattan norm of the given vector "op", and stores
	 *  it into "rop".
	 *  TEST performs a rather complex boolean test (see original QI code). */
#define NORM1(rop,op)			\
	mqi_coef_abs (rop,VECT(op,0));	\
	mqi_coef_abs (acc,VECT(op,1));	\
	mqi_coef_add (rop,rop,acc);

#define TEST(vect)														   \
	(qi_coef_sign(VECT(v2,0), VECT(v2,1), delta) > 0) &&									   \
	(( (mqi_coef_sign(VECT(v,1)) != 0) && ( (mqi_coef_sign(VECT(vect,1))== 0) || (mqi_coef_cmp (abs_vect2, abs_vect) < 0  ) )) \
		||														   \
	     ( (mqi_coef_sign(VECT(v,1)) == 0) && (mqi_coef_sign(VECT(vect,1)) == 0) && (mqi_coef_cmp (abs_vect2, abs_vect) < 0) ))

	NORM1(abs_vect, v)
	NORM1(abs_vect2, v2)
	if (  TEST(v2) ) {
	
		v = v2;
		id = 2;
	}

	NORM1(abs_vect2, v3)
	if ( TEST(v3) ) {
	
		v = v3;
		id = 3;
	}

	NORM1(abs_vect2, v4)
	if ( (id != 4) && TEST(v4) ) {
		v = v4;
		id = 4;
	}	
	
	NORM1(abs_vect2, v5)
	if ( TEST(v5) ) {
		v = v5;
		id = 5;
	}

	NORM1(abs_vect2, v6)
	if ( TEST(v6) )
		id = 6;

	
	switch ( id ) {

		case 1:
		/** Switch y and z */
		mqi_mat_initpool_dynamic (tr, 3, 3, mqi_mat_typeof(C1));
		mqi_mat_set_at_int (tr, 0, 0, 1);
		mqi_mat_set_at_int (tr, 1, 2, 1);
		mqi_mat_set_at_int (tr, 2, 1, 1);

		mqi_mat_initpool_cpy (C1p, C1);
		mqi_mat_mul 	 (C1p, C1p, tr);
		mqi_mat_mul	 (C1p, tr, C1p);
		
		mqi_mat_initpool_cpy (C2p, C2);
		mqi_mat_mul	 (C2p, C2p, tr);
		mqi_mat_mul	 (C2p, tr, C2p);

		_qi_param_conic_nr1 (C1p, C2p, delta, D, v1, p_trans0, p_trans1, p_trans2, p_trans3, par);

		mqi_mat_mul (p_trans0, tr, p_trans0);
		mqi_mat_mul (p_trans1, tr, p_trans1);
		mqi_mat_mul (p_trans2, tr, p_trans2);
		mqi_mat_mul (p_trans3, tr, p_trans3);

		break;

		case 2:
			_qi_param_conic_nr1(C1, C2, delta, D, v2, p_trans0, p_trans1, p_trans2, p_trans3, par);
			break;

		case 3:
		/** Switch x and y */
		mqi_mat_initpool_dynamic (tr, 3, 3, mqi_mat_typeof(C1));
		mqi_mat_set_at_int (tr, 0, 1, 1);
		mqi_mat_set_at_int (tr, 1, 0, 1);
		mqi_mat_set_at_int (tr, 2, 2, 1);

		mqi_mat_initpool_cpy (C1p, C1);
		mqi_mat_mul 	 (C1p, C1p, tr);
		mqi_mat_mul	 (C1p, tr, C1p);
		
		mqi_mat_initpool_cpy (C2p, C2);
		mqi_mat_mul	 (C2p, C2p, tr);
		mqi_mat_mul	 (C2p, tr, C2p);

		_qi_param_conic_nr1 (C1p, C2p, delta, D, v3, p_trans0, p_trans1, p_trans2, p_trans3, par);

		mqi_mat_mul (p_trans0, tr, p_trans0);
		mqi_mat_mul (p_trans1, tr, p_trans1);
		mqi_mat_mul (p_trans2, tr, p_trans2);
		mqi_mat_mul (p_trans3, tr, p_trans3);

		break;

		case 4:
		/** Switch x and z */
		mqi_mat_initpool_dynamic (tr, 3, 3, mqi_mat_typeof(C1));
		mqi_mat_set_at_int (tr, 0, 2, 1);
		mqi_mat_set_at_int (tr, 1, 1, 1);
		mqi_mat_set_at_int (tr, 2, 0, 1);

		mqi_mat_initpool_cpy (C1p, C1);
		mqi_mat_mul 	 (C1p, C1p, tr);
		mqi_mat_mul	 (C1p, tr, C1p);
		
		mqi_mat_initpool_cpy (C2p, C2);
		mqi_mat_mul	 (C2p, C2p, tr);
		mqi_mat_mul	 (C2p, tr, C2p);

		_qi_param_conic_nr2 (C1p, C2p, delta, D, dxz, p0, v2, v4, p_trans0, p_trans1, p_trans2, p_trans3, par);

		mqi_mat_mul (p_trans0, tr, p_trans0);
		mqi_mat_mul (p_trans1, tr, p_trans1);
		mqi_mat_mul (p_trans2, tr, p_trans2);
		mqi_mat_mul (p_trans3, tr, p_trans3);

		break;
	
		case 5:
		/** Switch y and z */
		mqi_mat_initpool_dynamic (tr, 3, 3, mqi_mat_typeof(C1));
		mqi_mat_set_at_int (tr, 0, 0, 1);
		mqi_mat_set_at_int (tr, 1, 2, 1);
		mqi_mat_set_at_int (tr, 2, 1, 1);

		mqi_mat_initpool_cpy (C1p, C1);
		mqi_mat_mul 	 (C1p, C1p, tr);
		mqi_mat_mul	 (C1p, tr, C1p);

		mqi_mat_initpool_cpy (C2p, C2);
		mqi_mat_mul	 (C2p, C2p, tr);
		mqi_mat_mul	 (C2p, tr, C2p);

		_qi_param_conic_nr2 (C1p, C2p, delta, D, dxy, p1, v1, v5, p_trans0, p_trans1, p_trans2, p_trans3, par);

		mqi_mat_mul (p_trans0, tr, p_trans0);
		mqi_mat_mul (p_trans1, tr, p_trans1);
		mqi_mat_mul (p_trans2, tr, p_trans2);
		mqi_mat_mul (p_trans3, tr, p_trans3);

		break;

		case 6:
			_qi_param_conic_nr2(C1, C2, delta, D, dxz, p2, v2, v6, p_trans0, p_trans1, p_trans2, p_trans3, par);
			break;

		default:
			mqi_log_error ("In %s: case not recognized: %d\n", __func__, id);
			break;

	}		


	mempool_clear();

}

