#include "qi_param.h"

/** Computes the parameterization of a double plane
  * Output is m1*[s*u t*u v] */
void qi_param_plane_double              (mqi_mat_t __IN q, mqi_mat_t __IN sing, mqi_mat_t m1)
{

	mqi_mat_t p_trans, q_r, tmp;

	/** Rotates the plane by sending one of its singular line to infinity */
	qi_mat_send_to_zw (p_trans, (mqi_vect_ptr)COL(sing,0), (mqi_vect_ptr)COL(sing,1));

	mqi_mat_transformation (tmp, q, p_trans, 0);

	/** Resizes  to a 2D matrix */
	mqi_mat_resize(q_r, tmp, 2, 2);

	/** Parametrizes the reduced 2D matrix */
	_qi_param_2x2sing (q_r, m1);

	/** $$ Bug here: destination matrix not sized properly ! */
	/** Augments the result and stacks the transformation matrices
	 * Adds a "1" at the right places in m1 for the two new variables of "par" */
	mqi_mat_set_at_int (m1, 2, 1, 1);
	mqi_mat_set_at_int (m1, 3, 2, 1);

	mqi_mat_mul (m1, p_trans, m1);

	mqi_mat_list_clear(p_trans, tmp, q_r, NULL);

}

/** Computes the parameterization of a pair of planes
  * Output is m1*[s*u t*u v] +/- sqrt(D)*m2*[s*u] if D is non zero
  * Output is m1*[s*u t*u v] +/- m2*[s*u] otherwise */
void qi_param_plane_pair                (mqi_mat_t __IN q, mqi_mat_t __IN sing, mqi_coef_t __INIT D,
					 mqi_mat_t m1, mqi_mat_t m2)
{

	mqi_mat_t p_trans, q_r, tmp;

	/** Rotates the plane by sending one of its singular line to infinity */
	qi_mat_send_to_zw (p_trans, (mqi_vect_ptr)COL(sing,0), (mqi_vect_ptr)COL(sing,1));

	mqi_mat_transformation (tmp, q, p_trans, 0);

	/** Resizes  to a 2D matrix */
	mqi_mat_resize (q_r, tmp, 2, 2);

	/** Parametrizes the reduced 2D matrix */
	qi_param_2x2 (q_r, D, m1, m2);

	/** $$ Bug here: the m1 matrix is not resized properly ! */
	/** Augments the result and stacks the transformation matrices
	 * Adds a "1" at the right places in m1 for the two new variables of "par" */
	mqi_mat_set_at_int (m1, 2, 1, 1);
	mqi_mat_set_at_int (m1, 3, 2, 1);

	mqi_mat_mul (m1, p_trans, m1);
	mqi_mat_mul (m2, p_trans, m2);

	mqi_mat_list_clear(p_trans, q_r, tmp, NULL);
}

