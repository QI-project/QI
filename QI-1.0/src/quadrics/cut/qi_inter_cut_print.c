#include "qi_inter_t.h"
#include <mqi/mqi_log.h>

/* Internal subroutines for the printing of terms of the form:
 *
 * 	a
 * 	a*sqrt(b)		or	a\sqrt{b}		(LaTeX form)
 * 	a*sqrt(b*c)		or	a\sqrt{bc}		(LaTeX form)
 *
 */

#define	IS_FIRST_TERM	0x00000001
#define IS_LAST_TERM	0x00000002

/* Print of an isolated coefficient in a sum.
 *
 * @return	0	if nothing was displayed because a is null
 * 		1	if something was displayed
 */
static int __qi_inter_cut_print_a (mqi_coef_t a, int flags)
{

	mqi_coef_t abscoef;

	if ( 
		(	(mqi_coef_sign(a) == 0) && (flags & IS_LAST_TERM) )
		||
		(	(mqi_coef_sign(a) != 0) && (flags & IS_FIRST_TERM) )
	   )
	{
		mqi_coef_print(a);
		return 1;
	}

	/* Print nothing */
	if ( mqi_coef_sign(a) == 0 ) return 0;

	mqi_coef_init_dynamic(abscoef,mqi_coef_typeof(a));

	/* We already know that it's not the first term 
	 * and it's not null */
	if (mqi_coef_sign(a) > 0)
	{
		mqi_log_printf ("+");
	}
	else
	{
		mqi_log_printf ("-");
	}

	mqi_coef_abs(abscoef,a);
	mqi_coef_print(abscoef);
	mqi_coef_clear(abscoef);

	return 1;

}

/* Print of an expression of the form "a*sqrt(b)" in a sum.
 *
 * @see __qi_inter_cut_print_a for the return values and their
 * 	meaning.
 */
static int	__qi_inter_cut_print_a_sqrt_b (mqi_coef_t a, mqi_coef_t b, int flags)
{
	
	mqi_coef_t abscoef;

	if ( mqi_coef_sign(a)*mqi_coef_sign(b) == 0 )
	{
		/* Nothing to display */
		return 0;
	}

	/* Here we know that the expression is not null */
	mqi_coef_init_dynamic(abscoef,mqi_coef_typeof(a));

	mqi_coef_abs(abscoef,a);

	if ((mqi_coef_sign(a) > 0) && !(flags & IS_FIRST_TERM))
	{
		mqi_log_printf ("+");
	}
	else if (mqi_coef_sign(a) < 0)
	{
		mqi_log_printf ("-");
	}

	if ( mqi_coef_cmp_schar(abscoef,1) != 0 )
	{
		mqi_coef_print(abscoef);

		/* Print the square root */
		if ( mqi_coef_cmp_schar(b,1) != 0 )
		{
			if ( !(USE_LATEX) )
			{
				mqi_log_printf ("*sqrt(");
				mqi_coef_print (b);
				mqi_log_printf (")");
			}
			else
			{
				mqi_log_printf ("\\sqrt{");
				mqi_coef_print (b);
				mqi_log_printf ("}");
			}
		}

	}
	else
	{
		/* Print the square root */
		if ( mqi_coef_cmp_schar(b,1) != 0 )
		{
			if ( !(USE_LATEX) )
			{
				mqi_log_printf ("sqrt(");
				mqi_coef_print (b);
				mqi_log_printf (")");
			}
			else
			{
				mqi_log_printf ("\\sqrt{");
				mqi_coef_print (b);
				mqi_log_printf ("}");
			}
		}
		else
		{
			/* Simply print a one (1) */
			mqi_coef_print(abscoef);
		}

	}

	mqi_coef_clear(abscoef);	
	return 1;

}

/* Print of an expression of the form "a*sqrt(b*c)" in a sum.
 *
 * @see __qi_inter_cut_print_a for the return values and their
 * 	meaning.
 *
 * @note $$ What about simplifying the output by printing out
 * 		directly the result of "b*c" instead of "b" "*" "c" ?
 */
static int 	__qi_inter_cut_print_a_sqrt_bc (mqi_coef_t a, mqi_coef_t b, mqi_coef_t c, int flags)
{

	mqi_coef_t abscoef;
	mqi_coef_t prod;

	if ( mqi_coef_sign(a)*mqi_coef_sign(b)*mqi_coef_sign(c) == 0 )
	{
		/* Nothing to display */
		return 0;
	}

	/* Here we know that the expression is not null */
	mqi_coef_list_init_dynamic(mqi_coef_typeof(a), abscoef, prod, NULL);

	mqi_coef_abs(abscoef,a);
	mqi_coef_mul(prod, b, c);

	if ((mqi_coef_sign(a) > 0) && !(flags & IS_FIRST_TERM))
	{
		mqi_log_printf ("+");
	}
	else if (mqi_coef_sign(a) < 0)
	{
		mqi_log_printf ("-");
	}

	if ( mqi_coef_cmp_schar(abscoef,1) != 0 )
	{
		mqi_coef_print(abscoef);

		/* Print the square root */
		if ( mqi_coef_cmp_schar(prod,1) != 0 )
		{
			if ( !(USE_LATEX) )
			{
				mqi_log_printf ("*sqrt(");
				mqi_coef_print (b);
				mqi_log_printf ("*");
				mqi_coef_print (c);
				mqi_log_printf (")");
			}
			else
			{
				mqi_log_printf ("\\sqrt{");
				mqi_coef_print (b);
				mqi_log_printf ("*");
				mqi_coef_print (c);
				mqi_log_printf ("}");
			}
		}

	}
	else
	{
		/* Print the square root */
		if ( mqi_coef_cmp_schar(prod,1) != 0 )
		{
			if ( !(USE_LATEX) )
			{
				mqi_log_printf ("sqrt(");
				mqi_coef_print (b);
				mqi_log_printf ("*");
				mqi_coef_print (c);
				mqi_log_printf (")");
			}
			else
			{
				mqi_log_printf ("\\sqrt{");
				mqi_coef_print (b);
				mqi_log_printf ("*");
				mqi_coef_print (c);
				mqi_log_printf ("}");
			}
		}
		else
		{
			/* Simply print a one (1) */
			mqi_coef_print(abscoef);
		}

	}

	mqi_coef_list_clear(abscoef, prod, NULL);	
	return 1;

}

void	qi_inter_cut_print (qi_inter_cut_t cut)
{
	
	int flags;
	int retval;
	mqi_log_printf ("(");

	flags 		&= 0x00000000;

	switch (cut->form)
	{

		case QI_INTER_CUT_0:

			/* u0 */
			__qi_inter_cut_print_a(cut->u[0], flags|IS_FIRST_TERM|IS_LAST_TERM);
			break;

		case QI_INTER_CUT_1:

			/* u0 */
			retval = __qi_inter_cut_print_a(cut->u[0], flags|IS_FIRST_TERM);

			/* u1*sqrt(d0) */
			if ( retval == 0 ) flags |= IS_FIRST_TERM;
			__qi_inter_cut_print_a_sqrt_b  (cut->u[1], cut->D[0], flags|IS_LAST_TERM);

			break;

		case QI_INTER_CUT_2:

			/* u0 */
			retval = __qi_inter_cut_print_a(cut->u[0], flags|IS_FIRST_TERM);

			/* u1*sqrt(d0) */
			if ( retval == 0 ) flags |= IS_FIRST_TERM;
			retval = __qi_inter_cut_print_a_sqrt_b  (cut->u[1], cut->D[0], flags);

			/* u2*sqrt(d1) */
			if ( (retval != 0) && (flags & IS_FIRST_TERM) ) flags &= ~IS_FIRST_TERM;
			retval = __qi_inter_cut_print_a_sqrt_b  (cut->u[2], cut->D[1], flags);

			/* u3*sqrt(d0*d1) */
			if ( (retval != 0) && (flags & IS_FIRST_TERM) ) flags &= ~IS_FIRST_TERM;
			retval = __qi_inter_cut_print_a_sqrt_bc  (cut->u[3], cut->D[0], cut->D[1], flags|IS_LAST_TERM);

			break;

		default:
			mqi_log_warning(
			"cut parameter with expression of type %d not supported yet.",
			cut->form);
			mqi_log_printf (")");
			return;

	}

	mqi_log_printf (", ");
	mqi_coef_print (cut->v);
	mqi_log_printf (")");


}

