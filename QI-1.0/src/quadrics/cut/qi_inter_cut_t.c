#include "qi_inter_t.h"
#include <mqi/mqi_log.h>

void __qi_inter_cut_init_0 (qi_inter_cut_t cut, va_list args)
{
	/* u0 */
	mqi_coef_init_cpy (cut->u[0], va_arg(args, mqi_coef_ptr));

	/* v */
	mqi_coef_init_cpy (cut->v, va_arg(args, mqi_coef_ptr));
}

void __qi_inter_cut_init_1 (qi_inter_cut_t cut, va_list args)
{
	/* u0 */
	mqi_coef_init_cpy (cut->u[0], va_arg(args, mqi_coef_ptr));

	/* u1 */
	mqi_coef_init_cpy (cut->u[1], va_arg(args, mqi_coef_ptr));

	/* d0 */
	mqi_coef_init_cpy (cut->D[0], va_arg(args, mqi_coef_ptr));

	/* v */
	mqi_coef_init_cpy (cut->v, va_arg(args, mqi_coef_ptr));
}

void __qi_inter_cut_init_2 (qi_inter_cut_t cut, va_list args)
{
	/* u0 */
	mqi_coef_init_cpy (cut->u[0], va_arg (args, mqi_coef_ptr));

	/* u1 */
	mqi_coef_init_cpy (cut->u[1], va_arg (args, mqi_coef_ptr));

	/* d0 */
	mqi_coef_init_cpy (cut->D[0], va_arg (args, mqi_coef_ptr));

	/* u2 */
	mqi_coef_init_cpy (cut->u[2], va_arg (args, mqi_coef_ptr));

	/* d1 */
	mqi_coef_init_cpy (cut->D[1], va_arg (args, mqi_coef_ptr));

	/* u3 */
	mqi_coef_init_cpy (cut->u[3], va_arg (args, mqi_coef_ptr));

	/* v */
	mqi_coef_init_cpy (cut->v, va_arg (args, mqi_coef_ptr));
}



void	qi_inter_cut_init (qi_inter_cut_t cut,
			   qi_inter_cut_form_t form,
			   ...)
{

	va_list	args;

	va_start(args, form);

	cut->form 	= form;
	cut->owner	= NULL;

	switch (form)
	{
		case QI_INTER_CUT_0:
			__qi_inter_cut_init_0 (cut, args);
			break;

		case QI_INTER_CUT_1:
			__qi_inter_cut_init_1 (cut, args);
			break;
	
		case QI_INTER_CUT_2:
			__qi_inter_cut_init_2 (cut, args);
			break;

		default:
			mqi_log_error (
			"incorrect inter cut form identifier: %d\n",
			form);
			break;
		
	}

	va_end(args);

}

void 	qi_inter_cut_init_cpy (qi_inter_cut_t	rop,
			       qi_inter_cut_t	op)
{

	rop->form  = op->form;

	/* Warning: the owner is not copied for the new cut parameter
	 * may not belong to the same component group.
	 */
	rop->owner = NULL;

	mqi_coef_init_cpy(rop->u[0], op->u[0]);
	mqi_coef_init_cpy(rop->v, op->v);

	if ( op->form != QI_INTER_CUT_0 )
	{
		mqi_coef_init_cpy(rop->u[1], op->u[1]);
		mqi_coef_init_cpy(rop->D[0], op->D[0]);
		if ( op->form != QI_INTER_CUT_1 )
		{
			mqi_coef_init_cpy(rop->u[2], op->u[2]);
			mqi_coef_init_cpy(rop->D[1], op->D[1]);
			mqi_coef_init_cpy(rop->u[3], op->u[3]);
		}
	}

}

void 	qi_inter_cut_cpy (qi_inter_cut_t	rop,
		          qi_inter_cut_t	op)
{

	rop->form = op->form;

	mqi_coef_cpy(rop->u[0], op->u[0]);
	mqi_coef_cpy(rop->v, op->v);

	if ( op->form != QI_INTER_CUT_0 )
	{
		mqi_coef_cpy(rop->u[1], op->u[1]);
		mqi_coef_cpy(rop->D[0], op->D[0]);
		if ( op->form != QI_INTER_CUT_1 )
		{
			mqi_coef_cpy(rop->u[2], op->u[2]);
			mqi_coef_cpy(rop->D[1], op->D[1]);
			mqi_coef_cpy(rop->u[3], op->u[3]);
		}
	}

}

void 	qi_inter_cut_clear (qi_inter_cut_t cut)
{

	mqi_coef_clear(cut->u[0]);
	mqi_coef_clear(cut->v);

	if ( cut->form != QI_INTER_CUT_0 )
	{
		mqi_coef_clear(cut->u[1]);
		mqi_coef_clear(cut->D[0]);
		if ( cut->form != QI_INTER_CUT_1 )
		{
			mqi_coef_clear(cut->u[2]);
			mqi_coef_clear(cut->D[1]);
			mqi_coef_clear(cut->u[3]);
		}
	}

}

void qi_inter_cut_set_owner (qi_inter_cut_t cut, qi_inter_component_t owner)
{
	if ( cut->owner != NULL )
	{
		mqi_log_error(
		"Cut parameter: %p is already owned by component: %p.\n"
		"Cannot add another owner (requested: %p)\n",
		cut, cut->owner, owner);
		return;
	}

	cut->owner = owner;
}

