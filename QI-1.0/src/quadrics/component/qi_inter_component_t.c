#include "qi_inter_t.h"
#include <mqi/mqi_log.h>

void __qi_inter_component_init_0 (qi_inter_component_t comp,
				  va_list args)
{

	/* Universe */
	comp->type	=	QI_INTER_COMPONENT_UNIVERSE;
	return;
	
}

void __qi_inter_component_init_1 (qi_inter_component_t comp,
				  va_list args)
{

	/* Quadratic surface */
	mqi_mat_init_cpy (comp->surface, va_arg(args, mqi_mat_ptr));

}

void __qi_inter_component_init_2 (qi_inter_component_t comp,
				  va_list args)
{
	mqi_curve_param_init_cpy(comp->c[0], va_arg(args, mqi_curve_param_ptr));
}

void __qi_inter_component_init_3 (qi_inter_component_t comp,
				  va_list args)
{

	mqi_coef_ptr coef;
	mqi_curve_param_init_cpy(comp->c[0], va_arg(args, mqi_curve_param_ptr));
	mqi_curve_param_init_cpy(comp->c[1], va_arg(args, mqi_curve_param_ptr));
	
	coef = va_arg(args, mqi_coef_ptr);
	mqi_poly_init_dynamic (comp->D[0], mqi_coef_typeof(coef));
	mqi_poly_set_at (comp->D[0], 0, coef);

}

void __qi_inter_component_init_4 (qi_inter_component_t comp,
				  va_list args)
{

	mqi_curve_param_init_cpy(comp->c[0], va_arg(args, mqi_curve_param_ptr));
	mqi_curve_param_init_cpy(comp->c[1], va_arg(args, mqi_curve_param_ptr));
	mqi_poly_init_cpy(comp->D[0], va_arg(args, mqi_poly_ptr));

}

void __qi_inter_component_init_5 (qi_inter_component_t comp,
				  va_list args)
{
	
	mqi_coef_ptr coef;

	mqi_curve_param_init_cpy(comp->c[0], va_arg(args, mqi_curve_param_ptr));
	mqi_curve_param_init_cpy(comp->c[1], va_arg(args, mqi_curve_param_ptr));

	coef = va_arg(args, mqi_coef_ptr);
	mqi_poly_init_dynamic (comp->D[0], mqi_coef_typeof(coef));
	mqi_poly_set_at (comp->D[0], 0, coef);

	mqi_curve_param_init_cpy(comp->c[2], va_arg(args, mqi_curve_param_ptr));

	coef = va_arg(args, mqi_coef_ptr);
	mqi_poly_init_dynamic (comp->D[1], mqi_coef_typeof(coef));
	mqi_poly_set_at (comp->D[1], 0, coef);

}

void __qi_inter_component_init_6 (qi_inter_component_t comp,
				  va_list args)
{

	mqi_coef_ptr coef;
	
	mqi_curve_param_init_cpy(comp->c[0], va_arg(args, mqi_curve_param_ptr));
	mqi_curve_param_init_cpy(comp->c[1], va_arg(args, mqi_curve_param_ptr));

	coef = va_arg(args, mqi_coef_ptr);
	mqi_poly_init_dynamic (comp->D[0], mqi_coef_typeof(coef));
	mqi_poly_set_at (comp->D[0], 0, coef);

	mqi_curve_param_init_cpy(comp->c[2], va_arg(args, mqi_curve_param_ptr));
	mqi_curve_param_init_cpy(comp->c[3], va_arg(args, mqi_curve_param_ptr));

	coef = va_arg(args, mqi_coef_ptr);
	mqi_poly_init_dynamic (comp->D[1], mqi_coef_typeof(coef));
	mqi_poly_set_at (comp->D[1], 0, coef);

	coef = va_arg(args, mqi_coef_ptr);
	mqi_poly_init_dynamic (comp->D[2], mqi_coef_typeof(coef));
	mqi_poly_set_at (comp->D[2], 0, coef);

}

void __qi_inter_component_init_7 (qi_inter_component_t comp,
				  va_list args)
{

	mqi_coef_ptr coef;
	
	mqi_curve_param_init_cpy(comp->c[0], va_arg(args, mqi_curve_param_ptr));
	mqi_curve_param_init_cpy(comp->c[1], va_arg(args, mqi_curve_param_ptr));

	coef = va_arg(args, mqi_coef_ptr);
	mqi_poly_init_dynamic (comp->D[0], mqi_coef_typeof(coef));
	mqi_poly_set_at (comp->D[0], 0, coef);

	mqi_curve_param_init_cpy(comp->c[2], va_arg(args, mqi_curve_param_ptr));
	mqi_curve_param_init_cpy(comp->c[3], va_arg(args, mqi_curve_param_ptr));

	mqi_poly_init_cpy(comp->D[1], va_arg(args, mqi_poly_ptr));
	mqi_poly_init_cpy(comp->D[2], va_arg(args, mqi_poly_ptr));

}

void qi_inter_component_init (qi_inter_component_t comp,
			      qi_inter_component_form_t form,
			      ...)
{

	va_list args;

	va_start(args, form);

	comp->form 		= form;
	comp->optimal		= 0;
	comp->multiplicity	= 0;
	comp->n_cut_parameters	= 0;

	switch (form)
	{

		case QI_INTER_COMPONENT_0:
			__qi_inter_component_init_0 (comp, args);
			break;

		case QI_INTER_COMPONENT_1:
			__qi_inter_component_init_1 (comp, args);
			break;

		case QI_INTER_COMPONENT_2:
			__qi_inter_component_init_2 (comp, args);
			break;

		case QI_INTER_COMPONENT_3:
			__qi_inter_component_init_3 (comp, args);
			break;

		case QI_INTER_COMPONENT_4:
			__qi_inter_component_init_4 (comp, args);
			break;

		case QI_INTER_COMPONENT_5:
			__qi_inter_component_init_5 (comp, args);
			break;

		case QI_INTER_COMPONENT_6:
			__qi_inter_component_init_6 (comp, args);
			break;

		case QI_INTER_COMPONENT_7:
			__qi_inter_component_init_7 (comp, args);
			break;
		
		default:
			mqi_log_error (
			"incorrect inter component form identifier: %d\n",
			form);
			break;

	}

	va_end(args);

}

void qi_inter_component_init_cpy (qi_inter_component_t rop,
			     qi_inter_component_t op)
{
	
	rop->form 		= op->form;
	rop->type 		= op->type;
	rop->optimal		= op->optimal;
	rop->multiplicity 	= op->multiplicity;

	if ( rop->form == QI_INTER_COMPONENT_0 )
	{
		/* Nothing to do and no cut parameters are possible */
		return;
	}

	if ( rop->form == QI_INTER_COMPONENT_1 )
	{
		/* We have a quadratic surface */
		mqi_mat_init_cpy (rop->surface, op->surface);
	}
	else
	{
	
		/* c0 */
		mqi_curve_param_init_cpy (
			rop->c[0], op->c[0]);

		if ( rop->form != QI_INTER_COMPONENT_2 )
		{

			/* c1, d0 */
			mqi_curve_param_init_cpy (
				rop->c[1], op->c[1]);
			mqi_poly_init_cpy (
				rop->D[0], op->D[0]);

			if ( (rop->form != QI_INTER_COMPONENT_3) &&
			     (rop->form != QI_INTER_COMPONENT_4) )
			{

				/* c2, d1 */
				mqi_curve_param_init_cpy (
					rop->c[2], op->c[2]);
				mqi_poly_init_cpy (
					rop->D[1], op->D[1]);
				
				if ( (rop->form == QI_INTER_COMPONENT_6)
					||
				     (rop->form == QI_INTER_COMPONENT_7)
				   )
				{
					/* c3, d2 */
					mqi_curve_param_init_cpy (
						rop->c[3], op->c[3]);
					mqi_poly_init_cpy (
						rop->D[2], op->D[2]);
				}

			}

		}

	}

	/* Copy the cut parameters, if any. */
	for ( rop->n_cut_parameters = 0;
	      rop->n_cut_parameters < op->n_cut_parameters;
	      (rop->n_cut_parameters)++ )
	{
		qi_inter_cut_init_cpy (
			rop->cut_parameters[rop->n_cut_parameters],
			op->cut_parameters[rop->n_cut_parameters]
		);
	}

}

void qi_inter_component_clear (qi_inter_component_t comp)
{

	size_t	k;

	comp->optimal		= 0;
	comp->multiplicity 	= 0;

	if ( comp->form == QI_INTER_COMPONENT_0 )
	{
		/* Nothing to do and no cut parameters are possible */
		return;
	}

	if ( comp->form == QI_INTER_COMPONENT_1 )
	{
		/* We have a quadratic surface */
		mqi_mat_clear(comp->surface);
	}
	else
	{
	
		/* c0 */
		mqi_curve_param_clear(comp->c[0]);

		if ( comp->form != QI_INTER_COMPONENT_2 )
		{

			/* c1, d0 */
			mqi_curve_param_clear(comp->c[1]);
			mqi_poly_clear	     (comp->D[0]);

			if ( (comp->form != QI_INTER_COMPONENT_3) &&
			     (comp->form != QI_INTER_COMPONENT_4) )
			{

				/* c2, d1 */
				mqi_curve_param_clear(comp->c[2]);
				mqi_poly_clear	     (comp->D[1]);

				if ( (comp->form == QI_INTER_COMPONENT_6)
					||
				     (comp->form == QI_INTER_COMPONENT_7)
				   )
				{
					/* c3, d2 */
					mqi_curve_param_clear(comp->c[3]);
					mqi_poly_clear	     (comp->D[2]);
				}

			}

		}

	}

	/* Copy the cut parameters, if any. */
	for ( k = 0; k < comp->n_cut_parameters; k++ )
	{

		/* If this component is responsible of the destruction
		 * of the cut parameter, clear it.
		 * Else, it should be done by another component sharing
		 * the same cut parameter.
		 */
		if ( comp->cut_parameters[k]->owner == comp )
		{
			qi_inter_cut_clear (
				comp->cut_parameters[k]
			);
		}

	}

	comp->n_cut_parameters = 0;

}

int qi_inter_component_is_in_real_affine_space (qi_inter_component_t comp)
{

	/* Universe or quadratic surface: not in real affine space */
	if ( (comp->form == QI_INTER_COMPONENT_0)
			||
	     (comp->form == QI_INTER_COMPONENT_1)
	   )
		return 0;

	/* Check c0(3) */
	if ( ! mqi_poly_is_zero(CP(comp->c[0],3)) )				return 1;
	
	if ( comp->form != QI_INTER_COMPONENT_2 )
	{

		/* Check c1(3) */
		if ( ! mqi_poly_is_zero(CP(comp->c[1],3)) )			return 1;

		if ( (comp->form != QI_INTER_COMPONENT_3) &&
		     (comp->form != QI_INTER_COMPONENT_4)
		   )
		{

			/* Check c2(3) */
			if ( ! mqi_poly_is_zero(CP(comp->c[2],3)) )		return 1;

			if ( comp->form != QI_INTER_COMPONENT_5 ) {
				/* Check c3(3) */
				if ( ! mqi_poly_is_zero(CP(comp->c[3],3)) )	return 1;
			}

		}

	}

	return 0;

}

void	qi_inter_component_set_type (qi_inter_component_t comp,
				     qi_inter_component_type_t type)
{
	comp->type = type;
}


void	qi_inter_component_set_optimal (qi_inter_component_t comp)
{
	comp->optimal = 1;
}

void	qi_inter_component_set_multiplicity (qi_inter_component_t comp,
					     int multiplicity)
{
	comp->multiplicity = multiplicity;
}

void	qi_inter_component_add_cut_parameter (qi_inter_component_t comp,
					      qi_inter_cut_t cut)
{
	comp->cut_parameters[comp->n_cut_parameters] = cut;
	comp->n_cut_parameters ++;
}

