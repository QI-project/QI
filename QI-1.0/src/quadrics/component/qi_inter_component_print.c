#include "qi_inter_t.h"
#include <mqi/mqi_log.h>
#include "qi_backends.h"
#include "qi_settings.h"

const char * qi_inter_component_types [] =
 {
	 "smooth quartic branch 1",
	 "smooth quartic branch 2",
	 "nodal quartic",
	 "cuspidal quartic",
	 "cubic",
	 "conic",
	 "line",
	 "lines with constraint",
	 "point",
	 "smooth quadric",
	 "cone",
	 "pair of planes (planes are not rational)",
	 "plane",
	 "universe"
 };

/* Print an elementary term of the equation of the system, with the following form:
 *
 *  	p * sqrt(a)
 */
static void __qi_inter_component_print_expr (mqi_poly_ptr p, mqi_coef_ptr D, int *flag)
{
	if ( mqi_poly_is_zero(p) || !mqi_coef_sign(D) ) return;
	
	/* Skip the square root when it equals one */
	if ( mqi_coef_cmp_schar(D,1) == 0 )
	{

		if ( *flag )
		{
			mqi_log_printf (" ");
			if ( mqi_coef_sign(POLY(p,mqi_poly_highest_x_degree(p))) > 0 )
				mqi_log_printf ("+ ");
		}
		if ( USE_LATEX )
		{
			if ( qi_settings.output_projective )
				mqi_poly_print_xw_LaTeX (p, 'u', 'v');
			else
				mqi_poly_print_xw_LaTeX (p, 'u', '1');
		}
		else
		{
			if ( qi_settings.output_projective )
				mqi_poly_print_xw (p, 'u', 'v');
			else
				mqi_poly_print_xw (p, 'u', '1');
		}
	}
	else
	{
		/* If p is a monomial */
		if (mqi_poly_highest_x_degree(p) + mqi_poly_highest_w_degree(p) == mqi_poly_degree(p))
		{
			if ( *flag )
			{
				mqi_log_printf (" ");
				if ( mqi_coef_sign(POLY(p,mqi_poly_highest_x_degree(p))) > 0 )
				mqi_log_printf ("+ ");
			}

			if ( (mqi_poly_degree(p) == 0) && (mqi_coef_cmp_schar(POLY(p,0),-1) == 0) )
				mqi_log_printf ("- ");
			else
			{
				if ( (mqi_poly_degree(p) != 0) || (mqi_coef_cmp_schar(POLY(p,0),1) != 0) )
				{
					if ( USE_LATEX )
					{
						if ( qi_settings.output_projective )
							mqi_poly_print_xw_LaTeX (p, 'u', 'v');
						else
							mqi_poly_print_xw_LaTeX (p, 'u', '1');
					}
					else
					{
						if ( qi_settings.output_projective )
							mqi_poly_print_xw (p, 'u', 'v');
						else
							mqi_poly_print_xw (p, 'u', '1');
					}
				}
			}
		}
		else
		{
			if ( *flag )
				mqi_log_printf (" + ");

			mqi_log_printf ("(");
			if ( USE_LATEX )
			{
				if ( qi_settings.output_projective )
					mqi_poly_print_xw_LaTeX (p, 'u', 'v');
				else
					mqi_poly_print_xw_LaTeX (p, 'u', '1');
			}
			else
			{
				if ( qi_settings.output_projective )
					mqi_poly_print_xw (p, 'u', 'v');
				else
					mqi_poly_print_xw (p, 'u', '1');
			}
			mqi_log_printf (")");
		}
		
		/* p = +/- 1 */
		if ( (mqi_poly_degree(p) == 0) && ( (mqi_coef_cmp_schar(POLY(p,0),1) == 0) ||
						    (mqi_coef_cmp_schar(POLY(p,0),-1) == 0) )
		   )
		{
			mqi_log_printf ("*");
		}

		if ( USE_LATEX )
		{
			mqi_log_printf ("\\sqrt{");
			mqi_coef_print(D);
			mqi_log_printf ("}");
		}
		else
		{
			mqi_log_printf ("sqrt(");
			mqi_coef_print(D);
			mqi_log_printf (")");
		}
	}

	*flag = 1;

	/* Finished */
	return;
}

/* Print a particular equation of the system */
static void __qi_inter_component_print_equation
		(mqi_poly_ptr p1, mqi_poly_ptr p2, mqi_poly_ptr p3, mqi_poly_ptr p4,
		 mqi_coef_ptr D, mqi_poly_ptr D1, mqi_poly_ptr D2)
{
	/* Flags used for printing a plus sign (+) when it is necessary */
	int flag, flag2, flag3;
	mqi_coef_t coef;
	mqi_coef_type_t type;

	flag = flag2 = flag3 = 0;

	if ( p1 ) type = mqi_poly_typeof(p1);
	else
	if ( p2 ) type = mqi_poly_typeof(p2);
	else
	if ( p3 ) type = mqi_poly_typeof(p3);
	else
	if  ( p4 ) type = mqi_poly_typeof(p4);
	else
	if ( D ) type = mqi_coef_typeof(D);
	else
	if ( D1 ) type = mqi_poly_typeof(D1);
	else
	if ( D2 ) type = mqi_poly_typeof(D2);
							
	mqi_coef_init_dynamic(coef, type);
	
	mqi_coef_set_schar(coef, 1);
	__qi_inter_component_print_expr(p1, coef, &flag);
	__qi_inter_component_print_expr(p2, D, &flag);

	/* D1+sqrt(D)*D2 != 0 */
	if ( ( (!mqi_poly_is_zero(D1)) || ((!mqi_poly_is_zero(D2)) && (mqi_coef_sign(D))) )
	/* p3 + p4*sqrt(D) != 0 */
			&&
	     ( (mqi_poly_is_zero(p3)) || ((mqi_poly_is_zero(p4)) && (mqi_coef_sign(D))) )
	   )
	{
		if ( ( mqi_poly_highest_x_degree(p3) + mqi_poly_highest_w_degree(p3) == mqi_poly_degree(p3) )
				&&
		     ( mqi_poly_is_zero(p4) || !mqi_coef_sign(D) )
		   )
		{
			if ( (mqi_poly_degree(p3) == 0) &&
				( (mqi_coef_cmp_schar(POLY(p3,0),1) == 0) ||
				  (mqi_coef_cmp_schar(POLY(p3,0),-1) == 0) )
			   )
			{
				if ( mqi_coef_cmp_schar(POLY(p3,0), -1) == 0 )
				{
					mqi_log_printf (" - ");	
					flag = 1;
				}
				else if ( flag )
					mqi_log_printf (" + ");
				
				flag3 = 1;
			}
			else
				__qi_inter_component_print_expr(p3, coef, &flag);
		}
		else if ( (mqi_poly_highest_x_degree(p4) + mqi_poly_highest_w_degree(p4) == mqi_poly_degree(p4))
				&&
			  (mqi_poly_is_zero(p3)) )
			__qi_inter_component_print_expr(p4, D, &flag);
		else
		{
			if ( flag ) mqi_log_printf (" + ");
			mqi_log_printf ("(");
			__qi_inter_component_print_expr(p3, coef, &flag2);
			__qi_inter_component_print_expr(p4, D, &flag2);
			mqi_log_printf (")");
		}

		if ( !flag3 )
			mqi_log_printf ("*");

		if ( (mqi_poly_degree(D1) > 0) || (mqi_poly_degree(D2) > 0) )
			if ( USE_LATEX )
				mqi_log_printf ("\\sqrt{\\delta}");
			else
				mqi_log_printf ("sqrt(Delta)");
		else
		{
			if ( USE_LATEX )
				mqi_log_printf ("\\sqrt{");
			else
				mqi_log_printf ("sqrt(");
				
			flag2 = 0;
			__qi_inter_component_print_expr(D1, coef, &flag2);
			__qi_inter_component_print_expr(D2, D, &flag2);

			if ( USE_LATEX )
				mqi_log_printf ("}");
			else
				mqi_log_printf (")");
		}

		flag = 1;
	}

	if ( !flag ) mqi_log_printf ("0");

	/* Finished */
	return;
}

static void __qi_inter_component_print_delta (mqi_coef_t D, mqi_poly_t D1, mqi_poly_t D2)
{
	int flag2 = 0;
	mqi_coef_t coef;

	/* Left member of the equation */
	if ( USE_LATEX )
		mqi_log_printf ("\\delta = ");
	else
		mqi_log_printf ("Delta = ");

	mqi_coef_init_dynamic(coef, mqi_coef_typeof(D));
	mqi_coef_set_schar(coef, 1);
	__qi_inter_component_print_expr(D1, coef, &flag2);
	__qi_inter_component_print_expr(D2, D, &flag2);

	mqi_coef_clear(coef);
}

static void __qi_inter_component_print_cut_parameters (qi_inter_component_t comp)
{
/*	mqi_curve_param_t cut1, cut2, cut3, cut4;
	mqi_coef_t u0, u1, u2, u3, v, D;
	mqi_hpoly_t D1, D2;
*/	
	/* Iterate through each cut parameter */
	int k;

	/* Number of cut parameters to display */
	int size;

	/* Skip if no cut parameters */
	size = comp->n_cut_parameters;
	if ( size == 0 ) return;

	/* Write a little header note only if the verbosity level is greater than
	 * VERBOSITY_BRUTE */
	if ( qi_settings.output_verbosity >= VERBOSITY_LABELS )
		mqi_log_printf ("Cut parameters:\n");

	for ( k = 0; k < size; k++ )
	{
		/* The print routine in "qi_cut_param_print.c" should be adequate */
		qi_inter_cut_print(comp->cut_parameters[k]);
		mqi_log_printf ("\n");
	}

	/* Finished */
	return;
}

static void __qi_inter_component_print_parametrization
		(mqi_curve_param_ptr c1, mqi_curve_param_ptr c2, mqi_curve_param_ptr c3, mqi_curve_param_ptr c4,
		 mqi_coef_ptr D, mqi_poly_ptr D1, mqi_poly_ptr D2)
{
	/* Parameters of the equations, depending on the coordinate system. */
	const char * const parameters =
		(qi_settings.output_projective) ? ( "u, v" ) : ( "u" );

	/* Variables of the equations */
	const char variables[4] = { 'x', 'y', 'z', 'w' };

	/* Used to iterate through each equation of the system. */
	int k;

	/* Size of the curve params to print */
	int size;

	/* Only the system format is allowed for the moment */
	/* Known bug on LaTeX output:
	 *
	 * There's a trailing "(1)" at the right of the equation system
	 */

	/* Prepare the LaTeX equation system */
	if ( USE_LATEX )
	{
		mqi_log_printf (
		"\\begin{equation}"
		"\\left\\lbrace"
		"\t\\begin{aligned}"
		);
	}
	
	/* Output the equations */

	size = mqi_curve_param_n_equations(c1);
	for ( k = 0; k < size; k++ )
	{

		/* Left member of the equation */
		mqi_log_printf ("%c(%s) = ", variables[k], parameters);

		/* Right member of the equation */
		__qi_inter_component_print_equation(
			CP(c1,k),
			CP(c2,k),
			CP(c3,k),
			CP(c4,k),
			D, D1, D2
		);

		/* If in latex mode, terminate the line with a double
		 * anti-slash. */
		if ( USE_LATEX )
			mqi_log_printf ("\\\\");

		/* Begin a new line */
		mqi_log_printf ("\n");
	}

	/* Print the expression of "delta", if any */
	if ( (mqi_poly_degree(D1) > 0) || (mqi_poly_degree(D2) > 0) )
	{
		__qi_inter_component_print_delta(D, D1, D2);

		/* Terminate the delta line */
		if ( USE_LATEX )
			mqi_log_printf ("\\\\");
	}

	/* Finish the LaTeX equation system.
	 * Note that the expression of "delta" is included
	 * in the system. */
	if ( USE_LATEX )
	{
		mqi_log_printf (
		"\\end{aligned}"
		"\\right."
		"\\end{equation}"
		);
	}


	/* Finished */
	return;
}


/** Specific printers */
static void __qi_inter_component_print_plane (qi_inter_component_t plane)
{

	const char variables[4] = { 'x', 'y', 'z', 'w' };
	size_t	   k, nrows;
	int	   sign;
	mqi_coef_t abscoef;
	mqi_mat_t  trans, matrix;

	mqi_mat_transpose_init(trans, plane->surface);
	mqi_mat_kernel	      (matrix, trans);

	mqi_coef_init_dynamic(abscoef, mqi_mat_typeof(matrix));

	nrows = mqi_mat_nrows(matrix);
	
	for ( k = 0; k < nrows; k++ )
	{

		sign = mqi_coef_sign(MAT(matrix,k,0));

		if ( sign == 0 ) continue;

		if ( k > 0 )
			mqi_log_printf (" ");

		if ( sign < 0 )
			mqi_log_printf ("- ");
		else if ( k > 0 )
			mqi_log_printf ("+ ");

		mqi_coef_abs (abscoef, MAT(matrix,k,0));

		if ( (mqi_coef_cmp_schar(abscoef,1) != 0) ||
		     ((k == nrows) && (qi_settings.output_projective == 0))
		   )
		{
			mqi_coef_print(abscoef);

			if ( (USE_LATEX == 0)
				&&
			     (mqi_coef_cmp_schar(abscoef,1) != 0)
				&&
			     ((k != nrows) || (qi_settings.output_projective == 1))
			   )
				mqi_log_printf ("*");
		}

		mqi_log_printf ("%c", variables[k]);

	}

	mqi_coef_clear (abscoef);
	mqi_mat_list_clear (trans, matrix, NULL);
}

static void __qi_inter_component_print_quadric (qi_inter_component_t quadric)
{

	
	const char * hom_labels_no_LaTeX[10] = {"x^2","x*y","x*z","x*w","y^2","y*z","y*w","z^2","z*w","w^2"};
	const char * aff_labels_no_LaTeX[9]  = {"x^2","x*y","x*z","x","y^2","y*z","y","z^2","z"};
	const char * hom_labels_LaTeX[10]    = {"x^2","xy","xz","xw","y^2","yz","yw","z^2","zw","w^2"};
	const char * aff_labels_LaTeX[9]     = {"x^2","xy","xz","x","y^2","yz","y","z^2","z"};
	const char  **labels;
	size_t k;
	int firstCoef = 1;
	mqi_coef_t	coef;
	mqi_vect_t  	vect;

	if ( USE_LATEX )
	{
		if ( qi_settings.output_projective )
			labels = hom_labels_LaTeX;
		else
			labels = aff_labels_LaTeX;
	}
	else
	{
		if ( qi_settings.output_projective )
			labels = hom_labels_no_LaTeX;
		else
			labels = aff_labels_no_LaTeX;
	}

	qi_quad_to_vect (__INIT vect, quadric->surface);

	mqi_coef_init_dynamic(coef, mqi_vect_typeof(vect));

	for ( k = 0; k < 10; k ++) {
	
		if ( mqi_coef_sign(VECT(vect,k)) == 0 ) continue;
			
		if ( (firstCoef == 0) && (mqi_coef_sign(VECT(vect,k)) > 0) )
			mqi_log_printf ("+");
		else if (mqi_coef_sign(VECT(vect,k)) < 0)
			mqi_log_printf ("-");

		mqi_coef_abs(coef, VECT(vect,k));

		if ( (mqi_coef_cmp_schar(coef,1) != 0) ||
		     ( (k > 8) && (qi_settings.output_projective == 0) )
		   )
			mqi_coef_print(coef);
		
		if ((qi_settings.output_projective == 1) || (k <= 8)) {
			if ( (USE_LATEX == 0) && (mqi_coef_cmp_schar(coef,1) != 0) )
				mqi_log_printf ("*");
			mqi_log_printf ("%s", labels[k]);
		}
		
		firstCoef = 0;

	}
	
	if ( firstCoef == 1 )
		mqi_log_printf ("0");
	
	mqi_coef_clear(coef);
	mqi_vect_clear(vect);

}

static void __qi_inter_component_print_surface (qi_inter_component_t surf)
{

	switch ( surf->type )
	{
		
		case QI_INTER_COMPONENT_UNIVERSE:
			mqi_log_printf ("[u, v, s, t]");
			return;

		case QI_INTER_COMPONENT_PLANE:
			__qi_inter_component_print_plane(surf);
			return;

		default:
			__qi_inter_component_print_quadric(surf);
			return;
	}

}

static void __qi_inter_component_print_line_with_constraints (qi_inter_component_t cline)
{

	mqi_curve_param_ptr	c1, c2;
	mqi_poly_ptr		p;
	mqi_hpoly_t		upol, vpol;
	mqi_surface_param_t	lines, tmplines;

	c1 = cline->c[0];
	c2 = cline->c[1];
	p  = cline->D[0];

	mqi_surface_param_list_init_dynamic (4, mqi_curve_param_typeof(c1), lines, tmplines, NULL);
	mqi_hpoly_list_init_dynamic	    (mqi_curve_param_typeof(c1), upol, vpol, NULL);

	mqi_poly_set_x(upol);
	mqi_poly_set_wi(vpol, 0);

	mqi_surface_param_inject_poly_in_cp(lines, 	c1, upol);
	mqi_surface_param_inject_poly_in_cp(tmplines, 	c2, vpol);

	mqi_surface_param_add (lines, lines, tmplines);

	if ( USE_LATEX )
	{
		if ( qi_settings.output_projective )
			mqi_surface_param_print_xwst_LaTeX (lines, 'x', 'w', 's', 't');
		else
			mqi_surface_param_print_LaTeX (lines);
	}
	else
	{
		if ( qi_settings.output_projective )
			mqi_surface_param_print_xwst (lines, 'x', 'w', 's', 't');
		else
			mqi_surface_param_print (lines);
	}

	if ( qi_settings.output_verbosity >= VERBOSITY_BRUTE )
	{
		mqi_log_printf ("\n");
		mqi_log_printf ("Constraint polynomial: ");
		if ( USE_LATEX )
		{
			if ( qi_settings.output_projective )
				mqi_poly_print_xw_LaTeX (p, 'x', 'w');
			else
				mqi_poly_print_xw_LaTeX (p, 'x', '1');
		}
		else
		{
			if ( qi_settings.output_projective )
				mqi_poly_print_xw (p, 'x', 'w');
			else
				mqi_poly_print_xw (p, 'x', '1');
		}
	}

	mqi_poly_list_clear(upol, vpol, NULL);
	mqi_surface_param_list_clear(lines, tmplines, NULL);

}

static void __qi_inter_component_print_smooth_quartic (qi_inter_component_t squartic)
{
	mqi_curve_param_t c1, c2, c3, c4;
	mqi_coef_t	  D;
	mqi_poly_t	  D1, D2;

	/* Initialize everything to zero */
	mqi_curve_param_list_init_dynamic (
			mqi_curve_param_n_equations(squartic->c[0]),
			mqi_curve_param_typeof(squartic->c[0]),
			c1, c2, c3, c4, NULL);
	mqi_coef_init_dynamic (D, mqi_curve_param_typeof(c1));
	mqi_poly_list_init_dynamic (mqi_curve_param_typeof(c1), D1, D2, NULL);

	mqi_curve_param_cpy(c1, squartic->c[0]);
	mqi_curve_param_cpy(c2, squartic->c[1]);
	mqi_poly_cpy(D1, squartic->D[0]);
	mqi_coef_set_schar(D, 1);
	mqi_poly_set_zero(D2);

	/* Curve param c3 and c4 are zero (NULL).
	 * Print the parametrization
	 */
	__qi_inter_component_print_parametrization (c1, c3, c2, c4, D, D1, D2);

	mqi_coef_clear(D);
	mqi_curve_param_list_clear(c1, c2, c3, c4, NULL);
	mqi_poly_list_clear(D1, D2, NULL);
}

static void __qi_inter_component_print_other_cases (qi_inter_component_t comp)
{
	mqi_curve_param_t c1, c2, c3, c4;
	mqi_coef_t	  D;
	mqi_poly_t	  D1, D2;

	/* Initialize everything to zero */
	mqi_curve_param_list_init_dynamic (
			mqi_curve_param_n_equations(comp->c[0]),
			mqi_curve_param_typeof(comp->c[0]),
			c1, c2, c3, c4, NULL);
	mqi_coef_init_dynamic (D, mqi_curve_param_typeof(c1));
	mqi_poly_list_init_dynamic (mqi_curve_param_typeof(c1), D1, D2, NULL);

	switch (comp->form)
	{
		/* WARNING: the absence of "breaks" is completly intended for
		 * we have an inclusion of the cases.
		 */
		case QI_INTER_COMPONENT_7:
		case QI_INTER_COMPONENT_6:
			mqi_curve_param_cpy(c4, comp->c[3]);
			mqi_poly_cpy(D2, comp->D[2]);

		case QI_INTER_COMPONENT_5:
			mqi_curve_param_cpy(c3, comp->c[2]);
			mqi_poly_cpy(D1, comp->D[1]);

		case QI_INTER_COMPONENT_4:
		case QI_INTER_COMPONENT_3:
			mqi_curve_param_cpy(c2, comp->c[1]);
			mqi_coef_cpy(D, POLY(comp->D[0],0));

		case QI_INTER_COMPONENT_2:
			mqi_curve_param_cpy(c1, comp->c[1]);
			break;

		default:
			mqi_log_error("Unexpected component expression form: %d\n", comp->form);
			break;
	}

	/* Print the parametrization */
	__qi_inter_component_print_parametrization (c1, c2, c3, c4, D, D1, D2);

	/* If requested by the user, print the cut parameters */	
	if ( qi_settings.output_cut_parameters )
		__qi_inter_component_print_cut_parameters (comp);

	mqi_coef_clear(D);
	mqi_curve_param_list_clear(c1, c2, c3, c4, NULL);
	mqi_poly_list_clear(D1, D2, NULL);
}

void qi_inter_component_print (qi_inter_component_t comp)
{

	/* Selected backend */
	qi_backend_t * bk = NULL;

/* Lighten the syntax */
#define _P	mqi_log_printf
#define _D	bk->style_default();
#define _M	bk->style_math();
#define _I	bk->style_info();
#define _S(x)	bk->symbol(x);
#define _e(x)	bk->exponent(x);
#define _N	bk->newline();
#define section bk->section
#define subsection bk->subsection

	/* Select the appropriate backend */
	bk = qi_backends[qi_settings.backend];

	/* Print the type of the component */
	if ( qi_settings.output_verbosity >= VERBOSITY_BRUTE )
	{
		/* The final type of the component is a combination
		 * of its topology and its multiplicity. Example:
		 * "double line" */
		char type_str[255];
		char mult[16];
		char subsection_str[255];

		switch (comp->multiplicity)
		{
			/* Universe has multiplicity 0 */
			case 0:
			case 1:
				mult[0] = '\0';
				break;

			case 2:
				strcpy(mult, "double ");
				break;
			
			case 3:
				strcpy(mult, "triple ");
				break;

			case 4:
				strcpy(mult, "quadruple ");
				break;

			default:
				mqi_log_error (
				"bad component multiplicity: %d\n",
				comp->multiplicity);
				break;
		}		

		sprintf(type_str, "%s%s", mult, qi_inter_component_types[comp->type]);
		sprintf(subsection_str, "[%s]", type_str);
	
		subsection(subsection_str);
	}
	else
	{
		_N
		_N
	}
	
	/* Print optimality */
	if ( qi_settings.output_verbosity >= VERBOSITY_BRUTE )
	{
		_D _P("Parametrization is ");  _I _P("%s",
						(comp->optimal) ? ("OPTIMAL") : ("NEAR-OPTIMAL"));
		
		/* Explain the optimality */
		if ( qi_settings.output_verbosity >= VERBOSITY_EXHAUSTIVE )
		{
			if ( comp->optimal )
			{
				_D _P(": the number of square roots in the coefficients of the ");
			}
			else
			{
				_D _P(": there might be one extra square root in the coefficients of the ");
			}
			if ( qi_settings.output_projective )
			{
				_M _P("u"); _e("p")
				if ( !USE_LATEX ) _P("*");
				_P("v"); _e("q")
			}
			else
			{
				_M _P("u"); _e("k")
			}
		}

		_N
	}

	_N
	
	/* Print the component equations */

	/** **************************************** */
	/** Redirect to more specific print routines */
	/** **************************************** */
	if ( comp->type >= QI_INTER_COMPONENT_SMOOTH_QUADRIC )
	{
		__qi_inter_component_print_surface(comp);
	}
	else if ( comp->type == QI_INTER_COMPONENT_LINES_WITH_CONSTRAINT )
	{
		__qi_inter_component_print_line_with_constraints(comp);
	}
	else if ( (comp->type == QI_INTER_COMPONENT_SMOOTH_QUARTIC_BRANCH_1)
			||
		  (comp->type == QI_INTER_COMPONENT_SMOOTH_QUARTIC_BRANCH_2)
		)
	{
		__qi_inter_component_print_smooth_quartic(comp);
	}
	else
	{
		__qi_inter_component_print_other_cases(comp);
	}

#undef _P
#undef _D
#undef _M
#undef _I
#undef _S
#undef _e
#undef _N
#undef section
#undef subsection
}

