#include "qi_inter_t.h"
#include <stdarg.h>
#include <string.h>
#include <mqi/mqi_log.h>

/** List of all the possible topologies of the intersection curve
 *  in the real field, stored as human readable
 *  character strings.
 */
const char 		*qi_inter_real_types[58] = {
	
	"empty set",
	"smooth quartic two finite components",
	"smooth quartic one finite component",
	"smooth quartic two infinite components",
	"point",
	"nodal quartic with isolated node",
	"nodal quartic affinely finite",
	"nodal quartic affinely infinite",
	"empty set",
	"two points",
	"two non-secant conics",
	"two secant conics affinely finite",
	"one conic",
	"two secant conics affinely infinite",
	"cuspidal quartic",
	"double point",
	"two tangent conics",
	"empty set",
	"double conic",
	"cubic and tangent line",
	"conic ",
	"conic and two lines crossing",
	"double line",
	"two simple lines and a double line",
	"point",
	"two secant double lines",
	"empty set",
	"quadric of inertia ",
	"quadric of inertia ",
	"cubic and secant line",
	"cubic and non-secant line",
	"point",
	"conic and point",
	"conic and two lines not crossing",
	"empty set",
	"skew quadrilateral",
	"two points",
	"two lines",
	"conic and double line",
	"point",
	"two concurrent lines",
	"four concurrent lines",
	"double line",
	"two concurrent lines and a double line",
	"line and triple line",
	"point",
	"two concurrent double lines",
	"quadruple line",
	"quadric of inertia ",
	"quadric of inertia ",
	"line and plane",
	"quadruple line",
	"quadruple line",
	"plane",
	"quadric of inertia ",
	"quadric of inertia ",
	"double plane",
	"universe"
	
};

/** List of all the possible topologies of the intersection curve
 *  in the complex field, stored as human readable
 *  character strings.
 */
const char 		*qi_inter_complex_types[27] = {
	
	"smooth quartic",
	"nodal quartic",
	"two secant conics",
	"cuspidal quartic",
	"two tangent conics",
	"double conic",
	"cubic and tangent line",
	"conic and two lines crossing",
	"two simple lines and a double line",
	"two secant double lines",
	"smooth quadric",
	"cubic and secant line",
	"conic and two lines not crossing",
	"skew quadrilateral",
	"conic and double line",
	"four concurrent lines",
	"two concurrent lines and a double line",
	"line and triple line",
	"two concurrent double lines",
	"quadruple line",
	"projective cone",
	"line and plane",
	"quadruple line",
	"plane",
	"pair of planes",
	"double plane",
	"universe"
	
};

void qi_inter_init (qi_inter_t inter)
{

	inter->n_components 		= 0;

	/* (0,0) forms an invalid topology */
	inter->complex_type_code	= 0;
	inter->real_type_offset		= 0;

	inter->complex_type[0]		= '\0';
	inter->real_type[0]		= '\0';

	inter->is_smooth_quartic	= 0;

}

void qi_inter_clear (qi_inter_t inter)
{

	size_t k;

	/* As each component has been set with a deep copy,
	 * we need to clear them.
	 */
	for ( k = 0; k < inter->n_components; k++ )
		qi_inter_component_clear(inter->components[k]);

	inter->n_components = 0;

	inter->complex_type_code	= 0;
	inter->real_type_offset		= 0;

	inter->complex_type[0]		= '\0';
	inter->real_type[0]		= '\0';

	if ( inter->is_smooth_quartic )
	{
		mqi_surface_param_list_clear(inter->s1, inter->s2, NULL);		
		inter->is_smooth_quartic	= 0;
	}

}

void qi_inter_add_component (qi_inter_t inter, qi_inter_component_t comp)
{

	if ( inter->n_components == 3 )
	{
	mqi_log_error (
	"Cannot add one more component to the intersection object %p. The maximum of\n"
	"4 components is already reached.",
	inter);
	}

	qi_inter_component_init_cpy (inter->components[inter->n_components], comp);
	inter->n_components ++;	

}

void qi_inter_set_optimal (qi_inter_t inter)
{

	size_t k;

	for ( k = 0; k < inter->n_components; k++ )
		qi_inter_component_set_optimal (inter->components[k]);

}


/** To convert those (x,y) codes into an array indice, we do a
 *  pseudo hashing using the following offset table: */
const int 	__qi_inter_type_offsets[27] = {
	
	4, 4, 6, 1, 2, 2, 1, 2, 2, 2, 3, 2, 3, 4, 1, 3, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 1
	
};

/** *********************** */
/** Pseudo hashing function */
/** *********************** */
int __calculate_inter_real_type (int complex_type_code, int real_type_offset) {

	int k;
	int hash = 0;
	
	for ( k = 0; k < complex_type_code; k++)
		hash += __qi_inter_type_offsets[k];

	hash += (real_type_offset-1);

	return (hash);

}

void qi_inter_set_22 (qi_inter_t inter,
		      mqi_surface_param_t s1, mqi_surface_param_t s2)
{

	mqi_surface_param_init_cpy (inter->s1, s1);
	mqi_surface_param_init_cpy (inter->s2, s2);
	inter->is_smooth_quartic = 1;

}

void	qi_inter_set_type (qi_inter_t inter, int complex_type_code, int real_type_offset)
{
	
	char	* real_type_str;

	/* We count from zero in C, and the codes start at one, so
	 * we need to translate.
	 */
	complex_type_code --;

	inter->complex_type_code = complex_type_code;
	inter->real_type_offset	 = real_type_offset;

	/* In the following calls to "strncpy", we need to copy
	 * the length of the original string plus 1 to include the
	 * string termination symbol '\0'
	 */
	strncpy (inter->complex_type,
		 qi_inter_complex_types[complex_type_code],
		 strlen(qi_inter_complex_types[complex_type_code])+1);

	real_type_str =
		(char *)qi_inter_real_types[__calculate_inter_real_type(complex_type_code, real_type_offset)];

	strncpy (inter->real_type, real_type_str, strlen(real_type_str)+1);

}

static void __qi_inter_create_components_case_1 (qi_inter_t inter, qi_inter_component_type_t type,
						 qi_inter_component_form_t form,
						 va_list args)
{

	qi_inter_component_t	comp;
	mqi_curve_param_ptr	c1p;
	mqi_curve_param_t	c2p;
	mqi_coef_ptr		delta;

	/* Pop arguments */
	c1p = va_arg(args, mqi_curve_param_ptr);
	mqi_curve_param_init_cpy(c2p, va_arg(args, mqi_curve_param_ptr));
	delta = va_arg(args, mqi_coef_ptr);

	qi_inter_component_init(comp, form, c1p, c2p, delta);
	qi_inter_component_set_type(comp, type);
	qi_inter_add_component(inter, comp);
	qi_inter_component_clear(comp);

	mqi_curve_param_neg(c2p, c2p);
	qi_inter_component_init(comp, form, c1p, c2p, delta);
	qi_inter_component_set_type(comp, type);
	qi_inter_add_component(inter, comp);
	qi_inter_component_clear(comp);

	mqi_curve_param_clear(c2p);

}

static void __qi_inter_create_components_case_2 (qi_inter_t inter, qi_inter_component_type_t type,
						 qi_inter_component_form_t form,
						 va_list args)
{

	qi_inter_component_t	comp;
	mqi_curve_param_ptr	c1p;
	mqi_curve_param_t	c2p, c3p, c3n;
	mqi_coef_ptr		delta1, delta2;

	/* Pop arguments */
	c1p = va_arg(args, mqi_curve_param_ptr);
	mqi_curve_param_init_cpy(c2p, va_arg(args, mqi_curve_param_ptr));
	delta1 = va_arg(args, mqi_coef_ptr);
	mqi_curve_param_init_cpy(c3p, va_arg(args, mqi_curve_param_ptr));
	delta2 = va_arg(args, mqi_coef_ptr);

	mqi_curve_param_init_cpy(c3n, c3p);
	mqi_curve_param_neg(c3n, c3n);

	qi_inter_component_init(comp, form, c1p, c2p, delta1, c3p, delta2);
	qi_inter_component_set_type(comp, type);
	qi_inter_add_component(inter, comp);
	qi_inter_component_clear(comp);

	qi_inter_component_init(comp, form, c1p, c2p, delta1, c3n, delta2);
	qi_inter_component_set_type(comp, type);
	qi_inter_add_component(inter, comp);
	qi_inter_component_clear(comp);

	mqi_curve_param_neg(c2p, c2p);
	qi_inter_component_init(comp, form, c1p, c2p, delta1, c3p, delta2);
	qi_inter_component_set_type(comp, type);
	qi_inter_add_component(inter, comp);
	qi_inter_component_clear(comp);

	qi_inter_component_init(comp, form, c1p, c2p, delta1, c3n, delta2);
	qi_inter_component_set_type(comp, type);
	qi_inter_add_component(inter, comp);
	qi_inter_component_clear(comp);

	mqi_curve_param_list_clear(c2p, c3p, c3n, NULL);

}

static void __qi_inter_create_components_case_3 (qi_inter_t inter, qi_inter_component_type_t type,
						 qi_inter_component_form_t form,
						 va_list args)
{

	qi_inter_component_t	comp;
	mqi_curve_param_ptr	c1p, c2p;
	mqi_curve_param_t	c3p, c4p;
	mqi_coef_ptr		delta1, delta2, delta3;

	/* Pop arguments */
	c1p = va_arg(args, mqi_curve_param_ptr);
	c2p = va_arg(args, mqi_curve_param_ptr);
	delta1 = va_arg(args, mqi_coef_ptr);
	mqi_curve_param_init_cpy(c3p, va_arg(args, mqi_curve_param_ptr));
	mqi_curve_param_init_cpy(c4p, va_arg(args, mqi_curve_param_ptr));
	delta2 = va_arg(args, mqi_coef_ptr);
	delta3 = va_arg(args, mqi_coef_ptr);

	qi_inter_component_init(comp, form, c1p, c2p, delta1, c3p, c4p, delta2, delta3);
	qi_inter_component_set_type(comp, type);
	qi_inter_add_component(inter, comp);
	qi_inter_component_clear(comp);

	mqi_curve_param_neg(c3p, c3p);
	mqi_curve_param_neg(c4p, c4p);

	qi_inter_component_init(comp, form, c1p, c2p, delta1, c3p, c4p, delta2, delta3);
	qi_inter_component_set_type(comp, type);
	qi_inter_add_component(inter, comp);
	qi_inter_component_clear(comp);

	mqi_curve_param_list_clear(c3p, c4p, NULL);

}

static void __qi_inter_create_components_case_4 (qi_inter_t inter, qi_inter_component_type_t type,
						 qi_inter_component_form_t form,
						 va_list args)
{

	qi_inter_component_t	comp;
	mqi_curve_param_ptr	c1p;
	mqi_curve_param_t	c2p, c3p, c4p, c2n, c3n, c4n;
	mqi_coef_ptr		delta1, delta2;
	mqi_coef_t		delta3; /* set to zero */

	/* Pop arguments */
	c1p = va_arg(args, mqi_curve_param_ptr);
	mqi_curve_param_init_cpy(c2p, va_arg(args, mqi_curve_param_ptr));
	delta1 = va_arg(args, mqi_coef_ptr);
	mqi_curve_param_init_cpy(c3p, va_arg(args, mqi_curve_param_ptr));
	mqi_curve_param_init_cpy(c4p, va_arg(args, mqi_curve_param_ptr));
	delta2 = va_arg(args, mqi_coef_ptr);

	mqi_curve_param_init_cpy(c2n, c2p);
	mqi_curve_param_init_cpy(c3n, c3p);
	mqi_curve_param_init_cpy(c4n, c4p);
	mqi_curve_param_neg(c2n, c2n);
	mqi_curve_param_neg(c3n, c3n);
	mqi_curve_param_neg(c4n, c4n);

	mqi_coef_init_dynamic(delta3, mqi_coef_typeof(delta1));
	mqi_coef_set_zero(delta3);

	qi_inter_component_init(comp, form, c1p, c2p, delta1, c3p, c4p, delta2, delta3);
	qi_inter_component_set_type(comp, type);
	qi_inter_add_component(inter, comp);
	qi_inter_component_clear(comp);

	qi_inter_component_init(comp, form, c1p, c2p, delta1, c3n, c4n, delta2, delta3);
	qi_inter_component_set_type(comp, type);
	qi_inter_add_component(inter, comp);
	qi_inter_component_clear(comp);

	qi_inter_component_init(comp, form, c1p, c2n, delta1, c3p, c4n, delta2, delta3);
	qi_inter_component_set_type(comp, type);
	qi_inter_add_component(inter, comp);
	qi_inter_component_clear(comp);

	qi_inter_component_init(comp, form, c1p, c2n, delta1, c3n, c4p, delta2, delta3);
	qi_inter_component_set_type(comp, type);
	qi_inter_add_component(inter, comp);
	qi_inter_component_clear(comp);

	mqi_curve_param_list_clear(c2p, c3p, c4p, c2n, c3n, c4n, NULL);
	mqi_coef_clear(delta3);

}

void	qi_inter_create_components (qi_inter_t inter, size_t n_components,
				    qi_inter_component_type_t type,
				    qi_inter_component_form_t form,
				       ...)
{
	
	va_list	args;

	va_start(args, form);

	switch ( form )
	{
		
		case QI_INTER_COMPONENT_3:
			/* Create a group of 2 components of the form
			 * c0 + c1*sqrt(d0)
			 */
			__qi_inter_create_components_case_1 (inter, type, form, args);
			break;		

		case QI_INTER_COMPONENT_5:
			/* Create a group of 4 components of the form
			 * c0 + c1*sqrt(d0) + c2*sqrt(d1)
			 */
			__qi_inter_create_components_case_2 (inter, type, form, args);

		case QI_INTER_COMPONENT_6:
			/* Create a group of either 2 or 4 components of the
			 * form
			 * c0 + c1*sqrt(d0) + (c2 + c3*sqrt(d0))*sqrt(d1 + d2*sqrt(d0))
			 */
			switch ( n_components )
			{
				case 2:
					__qi_inter_create_components_case_3 (inter, type, form, args);
					break;

				case 4:
					__qi_inter_create_components_case_4 (inter, type, form, args);
					break;

				default:
					mqi_log_error (
					"Invalid number of components: %zu to create an inter component group\n"
					"with form QI_INTER_COMPONENT_6. Valid values are 2 and 4.\n", n_components);
					break;
			}
			break;
	
		default:
			mqi_log_error (
			"Unsupported inter component group form: %d.",
			form);
			break;
	}	
	
	va_end(args);

}

