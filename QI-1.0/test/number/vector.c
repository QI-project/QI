#include "qi_number.h"
#include "qi_assert.h"

static mqi_vect_t u, v, w, z, _u, _v, _w, _z;
	
static slong udata[4] = { 7, -28, 12,  30 };
static slong vdata[4] = { 5, 9, 7, 13	  };
static slong wdata[4] = { -4, 15, 23, 24  };
static slong zdata[4] = { 8, 11, 31, 67   };

static mqi_coef_t coef;

void free_memory (void) {

	mqi_vect_clear (u);
	mqi_vect_clear (v);
	mqi_vect_clear (w);
	mqi_vect_clear (z);
	mqi_vect_clear (_u);
	mqi_vect_clear (_v);
	mqi_vect_clear (_w);
	mqi_vect_clear (_z);
	mqi_coef_clear (coef);

	mqi_log_printf ("\n");
	
}

int main (void) {


	atexit (free_memory);

	mqi_vect_init_slong (u, 4);
	mqi_vect_init_slong (v, 4);
	mqi_vect_init_slong (w, 4);
	mqi_vect_init_slong (z, 4);
	
	mqi_vect_set_all_slong (u, udata, 4);
	mqi_vect_set_all_slong (v, vdata, 4);
	mqi_vect_set_all_slong (w, wdata, 4);
	mqi_vect_set_all_slong (z, zdata, 4);
	
	mqi_vect_init_cpy (_u, u);
	mqi_vect_init_cpy (_v, v);
	mqi_vect_init_cpy (_w, w);
	mqi_vect_init_cpy (_z, z);

	mqi_coef_init_slong (coef);
	mqi_coef_set_slong  (coef, 3);
	mqi_vect_mul_coef (u, u, coef);
	mqi_log_printf ("Optimizing: "); mqi_vect_print(u); mqi_log_printf (" gives:\n");
	qi_vect_optimize(u);
	mqi_vect_print (u); mqi_log_printf ("\n");

	qi_assert ( mqi_vect_equals (u, _u) == 1 );

	mqi_vect_mul_coef (u, u, coef);
	mqi_vect_mul_coef (v, v, coef);
	mqi_log_printf ("Optimizing gcd:\n"); mqi_vect_print(u); mqi_log_printf ("\nAnd\n");
	mqi_vect_print(v);
	mqi_log_printf ("\nGives:\n");
	
	qi_vect_optimize_gcd (u, v);
	mqi_vect_print (u);
	mqi_log_printf ("\n");
	mqi_vect_print (v);
	mqi_log_printf ("\n");

	qi_assert ( mqi_vect_equals (u, _u) == 1 );
	qi_assert ( mqi_vect_equals (v, _v) == 1 );
	
	mqi_coef_set_slong (coef, 13);
	mqi_vect_mul_coef  (u, u, coef);
	mqi_vect_mul_coef  (v, v, coef);
	mqi_vect_mul_coef  (w, w, coef);
	
	mqi_log_printf ("Optimizing gcd:\n"); mqi_vect_print(u); mqi_log_printf ("\nAnd\n");
	mqi_vect_print(v); mqi_log_printf ("\nAnd\n");
	mqi_vect_print(w);
	mqi_log_printf ("\nGives:\n");
	
	qi_vect_optimize_gcd_triplet (u, v, w);
	mqi_vect_print (u);
	mqi_log_printf ("\n");
	mqi_vect_print (v);
	mqi_log_printf ("\n");
	mqi_vect_print (w);
	mqi_log_printf ("\n");

	mqi_vect_neg (u, u);
	mqi_vect_neg (v, v);
	mqi_vect_neg (w, w);

	qi_assert ( mqi_vect_equals (u, _u) == 1 );
	qi_assert ( mqi_vect_equals (v, _v) == 1 );
	qi_assert ( mqi_vect_equals (w, _w) == 1 );

	mqi_coef_set_slong (coef, 6);
	mqi_vect_mul_coef  (u, u, coef);
	mqi_vect_mul_coef  (v, v, coef);
	mqi_vect_mul_coef  (w, w, coef);
	mqi_vect_mul_coef  (z, z, coef);
	
	mqi_log_printf ("Optimizing gcd:\n"); mqi_vect_print(u); mqi_log_printf ("\nAnd\n");
	mqi_vect_print(v); mqi_log_printf ("\nAnd\n");
	mqi_vect_print(w); mqi_log_printf ("\nAnd\n");
	mqi_vect_print(z);
	mqi_log_printf ("\nGives:\n");
	
	qi_vect_optimize_gcd_quadruplet (u, v, w, z);
	mqi_vect_print (u);
	mqi_log_printf ("\n");
	mqi_vect_print (v);
	mqi_log_printf ("\n");
	mqi_vect_print (w);
	mqi_log_printf ("\n");
	mqi_vect_print (z);
	mqi_log_printf ("\n");
	
	qi_assert ( mqi_vect_equals (u, _u) == 1 );
	qi_assert ( mqi_vect_equals (v, _v) == 1 );
	qi_assert ( mqi_vect_equals (w, _w) == 1 );
	qi_assert ( mqi_vect_equals (z, _z) == 1 );

	mqi_log_printf ("Optimize by half: "); mqi_vect_print (u); mqi_log_printf ("\nGives: ");
	
	qi_vect_optimize_by_half (u);

	mqi_vect_print (u);
	mqi_log_printf ("\n");
	
	mqi_vect_cpy (u, _u);

	mqi_coef_set_slong (coef, 64);
	mqi_vect_mul_coef  (u, u, coef);	
	mqi_log_printf ("Load balancing between: "); mqi_vect_print (u); mqi_log_printf (" and "); mqi_vect_print (v);
	mqi_log_printf ("\nGives:\n");
	qi_vect_load_balancing (u, v);
		
	mqi_vect_print (u);
	mqi_log_printf ("\nAnd\n");
	mqi_vect_print (v);
	
	return (EXIT_SUCCESS);

}

