#include "qi_param.h"

static mqi_surface_param_t	s;
static mqi_coef_t		d;

void free_memory (void) {

	mqi_surface_param_clear (s);
	mqi_coef_clear		(d);
	mqi_log_printf ("\n");

}

int main (void) {

	atexit (free_memory);
	
	mqi_coef_init_sshort(d);
	mqi_coef_set_sshort(d, 144);

	mqi_log_printf ("Parametrizing (2,2) surface [ut,vs,us,vt]:\n");

	mqi_surface_param_init_dynamic (s, 4, QI_POLY_MAXDEG, 0, MQI_TYPE_sshort);
	qi_param_surface_22 (s);

	mqi_surface_param_print_xyzw (s, 's', 't', 'u', 'v');
	
	
	return (EXIT_SUCCESS);

}

