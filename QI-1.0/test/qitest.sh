#!/bin/bash

# libqi test script.
# Each test unit responds with either an EXIT_SUCCESS or
# EXIT_FAILURE error code.
# This script launches each unit and checks whether it goes
# wrong or not.

logfile="$HOME/.libqi/qitest.log"
if [ ! -d "$HOME/.libqi" ]; then mkdir "$HOME/.libqi"; fi 
bash=$(which bash)

echo "--- libqi test script ---"
echo

for test_unit in $(find . -name "test_*")
do
	test -f ${logfile} && rm -f ${logfile}
	printf "Launching test unit: ${test_unit} ... (please wait)"
	${test_unit} 2>&1>>${logfile} && printf "\033[13D\033[KSuccess\n" || printf "\033[13D\033[KFailure, check ${logfile} for more details.\n"
done

echo
echo "--- End ---"

# eof

