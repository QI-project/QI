#include "qi_elem.h"
#include "qi_assert.h"

#ifdef __USE_GMP

static sshort  C_data[4][4] = {

        { 1, 0 ,0 ,0 },
        { 0, 1, 0, 0 },
        { 0, 0, -1, 0},
        { 0, 0, 0, 0 }

};

/** Singular point on conic */
static schar sing_data[4]  = { 0, 0, 0, 1 };

static mqi_mat_t	mat, conic, C, null_mat, check;
static mqi_vect_t	sing, dir, v,inertia, tmp;
static mqi_coef_t	max;

void free_memory (void) {

	mqi_mat_clear 	(mat);
	mqi_mat_clear 	(C);
	mqi_mat_clear	(null_mat);
	mqi_mat_clear	(check);
	mqi_vect_clear	(conic);
	mqi_vect_clear  (sing);
	mqi_vect_clear  (dir);
	mqi_vect_clear	(v);
	mqi_vect_clear	(tmp);
	mqi_vect_clear	(inertia);
	mqi_coef_clear	(max);
	
	mqi_log_printf ("\n");
	
}

int main (void) {

	atexit (free_memory);
	
	mqi_mat_init_mpz_t	(C, 4, 4);
	mqi_mat_init_mpz_t	(null_mat, 4, 4);
	mqi_vect_init_sshort	(inertia, 2);
	mqi_vect_init_mpz_t	(tmp, 4);
	mqi_coef_init_mpz_t	(max);
	mqi_vect_init_mpz_t 	(sing, 4);
	mqi_vect_init_mpz_t	(dir, 4);

	mqi_coef_set_schar	(max, 100);	
	mqi_mat_set_all_sshort	(C, &C_data[0][0]);
	
	mqi_vect_set_all_schar	(sing, sing_data, 4);

	/** Randomly transforms matrix C, using the transposed of the comatrix of "mat" */
	mqi_mat_random_transformation (conic, C, mat, max, 1);

	mqi_mat_mul		(sing, mat, sing);
	
	/** Check that "sing" belongs to "conic" */
	mqi_mat_transformation (check, conic, sing, 0);

	qi_assert ( mqi_coef_sign (MAT(check,0,0)) == 0 );

	do {
		mqi_mat_randomize (dir, max);
		mqi_mat_mul	 (tmp, mat, dir);
	} while ( qi_point_equals (tmp, sing) );

	mqi_mat_mul (dir, conic, dir);

	mqi_log_printf ("Solving equation: \n");
	mqi_mat_print  (conic);
	mqi_log_printf ("\n * v = l * "); mqi_vect_print(dir); mqi_log_printf ("\n");
	mqi_log_printf ("\nKnowing rational point: "); mqi_vect_print(sing); mqi_log_printf ("\n");
	qi_mat_inertia (inertia, conic);
	mqi_log_printf ("Inertia of matrix: "); mqi_vect_print(inertia);mqi_log_printf ("\n");
	
	qi_solve_proj (v, conic, dir, sing);
	
	mqi_log_printf ("\nCalculated result: "); mqi_vect_print(v); mqi_log_printf ("\n");

	/** Check: does q.v = l.dir ? */
	mqi_mat_mul		(v, conic, v);

	qi_assert ( qi_point_equals (v, dir) == 1 );
	
	
	return (EXIT_SUCCESS);

}
#else
int main (void) { mqi_log_printf ("Solve proj test needs GMP support.\n"); return (EXIT_SUCCESS); }
#endif

