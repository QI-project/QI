#ifndef _QI_KERNEL_H_
#define _QI_KERNEL_H_

/** Include this header if you intend to use
    the QI's calculus kernel. */

#include <lidia/bigint_matrix.h>
/** $$ Toujours pertinent ? */
#include <qi/qi_elem.h>
/** ******************** */
#include <qi/qi_inter.h>

#endif
