#ifndef _qi_io_h_
#define _qi_io_h_

/** Include this file if you want to use the
    QI's Input/Output layer, to parse input
    from the user, and pretty display outputs.
    The IO package acts as an interface between
    the QI's calculus kernel and the end user.
*/

#include <qi/qi_ansicolors.h>
#include <qi/QIParser.h>
#include <qi/QIWriter.h>
#include <qi/QIOutputter.h>
#include <qi/QIConsoleWriter.h>

#ifdef WEB
#include <qi/QIHTMLWriter.h>
#endif

#endif
