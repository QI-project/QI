#ifndef _QI_H_
#define _QI_H_

/** Top level header to be included in the case
    you intend to use both the calculus kernel
    and the IO kit for parsing user input and
    pretty display results. */

#include <qi/qi_kernel.h>
#include <qi/qi_io.h>

#endif
