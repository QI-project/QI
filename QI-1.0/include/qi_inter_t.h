#ifndef _qi_inter_t_h_
#define _qi_inter_t_h_

#include <mqi/mqi_surface_param.h>
#include <mqi/mqi_poly.h>
#include <stddef.h>
#include <stdarg.h>

#include "qi_elem.h"
#include "qi_settings.h"

/** @file	qi_inter_t.h
 *  @brief	This file groups the 3 data structures
 *  		used to store the result of the
 *  		intersection calculation.
 *  		qi_inter_cut_t stores the auto intersection points
 *  		qi_inter_component_t stores a component of the intersection
 *  		qi_inter_t stores the intersection object
 */

/** @enum 	qi_inter_cut_form_t
 *  @brief	Used to discriminate between the 3 possible cases
 *  		of cut parameter expression.
 */
typedef enum {

	QI_INTER_CUT_0, /**<	No delta  : (u0, v) 				*/
	QI_INTER_CUT_1, /**<	One delta : (u0 + u1*sqrt(d0), v)		*/
	QI_INTER_CUT_2	/**<	Two deltas: (u0 + u1*sqrt(d0) + u2*sqrt(d1) +
			  	u3 * sqrt(d0*d1), v)				*/
}qi_inter_cut_form_t;

/** @enum	qi_inter_component_form_t
 *  @brief	Used to discriminate between the 8 possible cases
 *  		of intersection component expression.
 */
typedef enum {

	QI_INTER_COMPONENT_0, /**<	[u,v,s,t] (universe)			*/
	QI_INTER_COMPONENT_1, /**<	quadratic surface			*/
	QI_INTER_COMPONENT_2, /**<	c0					*/
	QI_INTER_COMPONENT_3, /**<	c0 + c1*sqrt(d0)			*/
	QI_INTER_COMPONENT_4, /**<	c0 + c1*sqrt( d0(x,w) )			*/
	QI_INTER_COMPONENT_5, /**<	c0 + c1*sqrt(d0) + c2*sqrt(d1)		*/
	QI_INTER_COMPONENT_6, /**<	c0 + c1*sqrt(d0) + (c2 + c3*sqrt(d0))
					 * sqrt(d1 + d2*sqrt(d0))		*/
	QI_INTER_COMPONENT_7  /**<	c0 + c1*sqrt(d0) + (c2 + c3*sqrt(d0))
					 * sqrt( d1(x,w) + d2(x,w) * sqrt(d0))	*/
}qi_inter_component_form_t;


/** @enum	qi_inter_component_type_t
 *  @brief	List of all the possible types for a given
 *  		intersection component.
 */
typedef enum {

        QI_INTER_COMPONENT_UNDEFINED                    =       9999, /** No value */
        QI_INTER_COMPONENT_SMOOTH_QUARTIC_BRANCH_1      =       0,
        QI_INTER_COMPONENT_SMOOTH_QUARTIC_BRANCH_2      =       1,
        QI_INTER_COMPONENT_NODAL_QUARTIC                =       2,
        QI_INTER_COMPONENT_CUSPIDAL_QUARTIC             =       3,
        QI_INTER_COMPONENT_CUBIC                        =       4,
        QI_INTER_COMPONENT_CONIC                        =       5,
        QI_INTER_COMPONENT_LINE                         =       6,
        QI_INTER_COMPONENT_LINES_WITH_CONSTRAINT        =       7,
        QI_INTER_COMPONENT_POINT                        =       8,
        QI_INTER_COMPONENT_SMOOTH_QUADRIC               =       9,
        QI_INTER_COMPONENT_CONE                         =       10,
        QI_INTER_COMPONENT_PAIR_OF_PLANES               =       11,
        QI_INTER_COMPONENT_PLANE                        =       12,
        QI_INTER_COMPONENT_UNIVERSE                     =       13

}qi_inter_component_type_t;

/** List of all the possible types for a given intersection component,
 *  as a list of character strings.
 *  Note that depending on the multiplicity, those textual information
 *  may be completed by other character strings such as "double", "triple", "quadruple",
 *  etc. in the final result.
 */
extern const char		*qi_inter_component_types[];

/** List of all the possible topologies of the intersection curve, both
 *  in the complex and in the real fields, stored as human readable
 *  character strings.
 *  You must look at the qi_inter_t.c source code where resides the static
 *  initialization of those two arrays.
 *
 *  @see	qi_inter_t.c
 */
extern const char		*qi_inter_complex_types[];
extern const char		*qi_inter_real_types[];


/** @struct	qi_inter_cut_t
 *  @brief	Stores the relevant information of a cut parameter.
 */
struct _qi_inter_cut_t_ {

	/** Form of the expression */
	qi_inter_cut_form_t	form;

	mqi_coef_t	u[4];
	mqi_coef_t	D[2];
	mqi_coef_t	v;

	/* A cut parameter can be shared by multiple
	 * components. This stores the reference of
	 * the component in charge of clearing the cut
	 * parameter.
	 */
	void	*	owner;

};

typedef struct _qi_inter_cut_t_		qi_inter_cut_t[1];
typedef struct _qi_inter_cut_t_		*qi_inter_cut_ptr;


/** @struct	qi_inter_component_t
 *  @brief	Component of the intersection object
 */
struct _qi_inter_component_t_ {

	/** Form of the expression */
	qi_inter_component_form_t	form;

	/** Type of the component */
	qi_inter_component_type_t	type;

	/** Is this component optimal (1) or near-optimal (0) */
	int				optimal;

	/** Multiplicity */
	int				multiplicity;

	/** Cut parameters */
	qi_inter_cut_ptr		cut_parameters[10];
	size_t				n_cut_parameters;

	/** Parametrized curves involved in the expression of
	 * this component, if any.
	 */
	mqi_curve_param_t		c[4];

	/** Deltas involved in the expression of this
	 *  component, if any.
	 */
	mqi_hpoly_t			D[3];

	/** Matrix representing a quadratic surface when this component
	 * is a surface.
	 */
	mqi_mat_t			surface;

};

typedef struct _qi_inter_component_t_	qi_inter_component_t[1];

/** @struct	qi_inter_t
 *  @brief	The intersection object
 *  @note	Here is a list of all the possible types of the intersection
 *  		object in the real space:<br>
 *  (1,1): empty set<br>
 *  (1,2): smooth quartic, two finite components<br>
 *  (1,3): smooth quartic, one finite component<br>
 *  (1,4): smooth quartic, two infinite components<br>
 *  (2,1): point<br>
 *  (2,2): nodal quartic with isolated node<br>
 *  (2,3): nodal quartic, affinely finite<br>
 *  (2,4): nodal quartic, affinely infinite<br>
 *  (3,1): empty set<br>
 *  (3,2): two points<br>
 *  (3,3): two non-secant conics<br>
 *  (3,4): two secant conics, affinely finite<br>
 *  (3,5): one conic<br>
 *  (3,6): two secant conics, affinely infinite<br>
 *  (4,1): cuspidal quartic<br>
 *  (5,1): double point<br>
 *  (5,2): two tangent conics<br>
 *  (6,1): empty set<br>
 *  (6,2): double conic<br>
 *  (7,1): cubic and tangent line<br>
 *  (8,1): conic <br>
 *  (8,2): conic and two lines crossing<br>
 *  (9,1): double line<br>
 *  (9,2): two simple lines and a double line<br>
 * (10,1): point<br>
 * (10,2): two secant double lines<br>
 * (11,1): empty set<br>
 * (11,2): quadric of inertia (3,1)<br>
 * (11,3): quadric of inertia (2,2)<br>
 * (12,1): cubic and secant line<br>
 * (12,2): cubic and non-secant line<br>
 * (13,1): point<br>
 * (13,2): conic and point<br>
 * (13,3): conic and two lines not crossing<br>
 * (14,1): empty set<br>
 * (14,2): skew quadrilateral<br>
 * (14,3): two points<br>
 * (14,4): two lines<br>
 * (15,1): conic and double line<br>
 * (16,1): point<br>
 * (16,2): two concurrent lines<br>
 * (16,3): four concurrent lines<br>
 * (17,1): double line<br>
 * (17,2): two concurrent lines and a double line<br>
 * (18,1): line and triple line<br>
 * (19,1): point<br>
 * (19,2): two concurrent double lines<br>
 * (20,1): quadruple line<br>
 * (21,1): quadric of inertia (3,0)<br>
 * (21,2): quadric of inertia (2,1)<br>
 * (22,1): line and plane<br>
 * (23,1): quadruple line<br>
 * (23,2): quadruple line<br>
 * (24,1): plane<br>
 * (25,1): quadric of inertia (2,0)<br>
 * (25,2): quadric of inertia (1,1)<br>
 * (26,1): double plane<br>
 * (27,1): universe<br>
 */
struct _qi_inter_t_ {

	/** Components of the intersection.
	 *  The maximum number of components is 4.
	 *  It acts as a stack to which we add
	 *  the components, starting from indice 0.
	 */
	qi_inter_component_t		components[4];

	/* Count the number of components and also serves
	 * as an indice to store the components.
	 */
	size_t				n_components;

	/** The topology of the intersection object can be
	 *  expressed in the complex field or in the real field.
	 *  In the complex field, the <i>complex_type_code</i> value
	 *  is enough to characterize the topology.
	 *  In the real field, you need the pair:			<br>
	 *  	(<i>complex_type_code</i>, <i>real_type_offset</i>)	<br>
	 *  to characterize the topology in a unique manner. See the
	 *  list of possible values above,example: (20,1) for quadruple line.
	 */
	int				complex_type_code;
	int				real_type_offset;

	/** Topology of the intersection object both in the complex field
	 *  and in the real field, stored in a human readable character string.
	 */
	char				complex_type[64];
	char				real_type   [64];

	/** Store the intermediate quadrics of the pencil with inertia (2,2)
	 *  used to find a parametrization of the intersection in the
	 *  case of a <b>smooth quartic</b> only.
	 */
	mqi_surface_param_t		s1, s2;
	int				is_smooth_quartic;

};

typedef struct _qi_inter_t_		qi_inter_t[1];


/*@{*/
/** @name	Cut parameters
 */

/** Initialize a new cut parameter.
 *  
 *  @param	cut	The cut paramters to initialize
 *  @param	form	Form of the expression
 *
 *  @see	qi_inter_cut_form_t
 *
 */
void	qi_inter_cut_init (qi_inter_cut_t cut,
			   qi_inter_cut_form_t form,
			   ...);

/** Initialize a new cut parameter by copying the content
 *  of another.
 */
void 	qi_inter_cut_init_cpy (qi_inter_cut_t	rop,
			       qi_inter_cut_t	op);

/** Copy the content of a cut parameter to another.
 */
void 	qi_inter_cut_cpy (qi_inter_cut_t	rop,
		          qi_inter_cut_t	op);

/** Clear the memory allocated for a cut parameter.
 */
void 	qi_inter_cut_clear 	(qi_inter_cut_t cut);

/** A cut parameter may be shared by various components.
 *  The problem is to decide which of those components will clear the
 *  memory allocated by this cut parameter.
 *  One per group should do that, and it is called the "owner".
 */
void	qi_inter_cut_set_owner 	(qi_inter_cut_t cut, qi_inter_component_t comp);

/** Print a cut parameter
 */
void 	qi_inter_cut_print		(qi_inter_cut_t	cut);

/** Print a cut parameter with LaTeX symbols and syntax.
 */
void	qi_inter_cut_print_LaTeX	(qi_inter_cut_t cut);


/*@}*/

/*@{*/
/** @name	Intersection components
 */

/** Initialize a new intersection component.
 *
 *  @param	comp	The intersection component to initialize
 *  @param	form	Form of the expression
 *
 *  @see	qi_inter_component_form_t
 *
 */
void	qi_inter_component_init (qi_inter_component_t comp,
				 qi_inter_component_form_t form,
				 ...);

/** Initialize a new intersection component by copying the
 *  content of another.
 */
void	qi_inter_component_init_cpy (qi_inter_component_t rop,
				     qi_inter_component_t op);

/** Clear the memory allocated for an intersection component.
 *
 *  @note	Each cut parameter whose owner is this component
 *  		will get cleared, if any.
 *  		
 */
void	qi_inter_component_clear (qi_inter_component_t comp);

/** Check whether a given intersection parameter belongs to the
 *  real affine space (R^3) or not.
 *  This routine is used to simplify the output of the
 *  intersection.
 */
int	qi_inter_component_is_in_real_affine_space (qi_inter_component_t comp);

/** Set the type of an intersection component.
 *
 *  @param	comp	The intersection component to modify
 *  @param	type	Type of the component
 *
 *  @see	qi_inter_component_type_t
 *
 */
void	qi_inter_component_set_type (qi_inter_component_t comp,
				     qi_inter_component_type_t type);


/** Set the optimality flag of an intersection component.
 *  @note By default, an intersection component is considered as near-optimal.
 *  You need to explicitely call this function to declare a component
 *  to be optimal, or use the "grouped" function <i>qi_inter_set_optimal</i>.
 *
 *  @see	qi_inter_set_optimal
 *
 */
void	qi_inter_component_set_optimal (qi_inter_component_t comp);


/** Set the multiplicity of an intersection component.
 */
void	qi_inter_component_set_multiplicity (qi_inter_component_t comp,
					     int multiplicity);


/** Assign a new cut parameter to an intersection component.
 *  
 *  @note	The cut parameter is <b>not</b> copied but only referenced
 *  		by the intersection component.
 *  		It will be cleared when the component <i>owning</i> the cut
 *  		parameter will be cleared itself.
 *
 *  @see	qi_inter_cut_t
 *  @see	qi_inter_cut_set_owner
 *
 */
void	qi_inter_component_add_cut_parameter (qi_inter_component_t comp,
					      qi_inter_cut_t cut);

/* Print an intersection component according to the selected
 * backend.
 * @see qi_backends.h
 */
void	qi_inter_component_print (qi_inter_component_t comp);

/*@}*/


/**@{*/
/** @name	Intersection object
 */

/** Initialize a new intersection object
 */
void	qi_inter_init (qi_inter_t inter);

/** Create a group of components which differ only in their
 *  sign.
 *
 *  @param	inter		The intersection object for which we create components
 *  @param	n_components	Number of components to create.
 *  @param	form		Form of the expressions of the group of components.
 *
 *  @note	The current implementation of the QI algorithm reduces the possibilities
 *  		of use of the following function. Here is the list of supported forms
 *  		and the possible values of the <i>n_components</i> parameter:
 *
 *  			-	QI_INTER_COMPONENT_3	(2)
 *  			-	QI_INTER_COMPONENT_5	(4)
 *  			-	QI_INTER_COMPONENT_6	(2, 4)
 *
 *		For the last form, the third delta is automatically set to zero.
 *		It's not dangerous to pass a value for this delta but it will be simply
 *		ignored.
 *  @note	Beware of the usage made of those two functions.
 */
void	qi_inter_create_components    (qi_inter_t inter, size_t n_components,
				       qi_inter_component_type_t type,
				       qi_inter_component_form_t form,
				       ...);

/** Clear an intersection object
 */
void	qi_inter_clear (qi_inter_t inter);

/** Add a new component to an intersection object.
 *  
 *  @note	A <b>deep copy</b> is performed using
 *  		<i>qi_inter_component_init_cpy</i> so you
 *  		have to discard the original component
 *  		passed in parameter to avoid a memory leak.
 */
void	qi_inter_add_component (qi_inter_t inter, qi_inter_component_t comp);

/** Set the type of the intersection object as
 *  a pair (complex type code, real_type_offset).
 *  It also set the human readable topology.
 */
void	qi_inter_set_type (qi_inter_t inter, int complex_type_code, int real_type_offset);


/** Set the intermediate quadrics of the pencil with inertia (2,2) */
void	qi_inter_set_22 (qi_inter_t inter, mqi_surface_param_t s1, mqi_surface_param_t s2);

/** Set the "optimal" flag of each component of the intersection object as true.
 */
void	qi_inter_set_optimal (qi_inter_t inter);


/*@}*/

#endif

