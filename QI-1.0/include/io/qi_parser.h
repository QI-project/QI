#ifndef _qi_parser_h_
#define _qi_parser_h_

/* Maximum number of digits for a coefficient */
#define COEF_MAX_SIZE	8192


#include "parser.h"
#include <mqi/mqi_vect.h>
#include <stdio.h>
#include <stdlib.h>

/** ******************** */
/** Parsing declarations */
/** ******************** */
DECL_NON_TERMINAL(expression);
DECL_NON_TERMINAL(termlist);
DECL_NON_TERMINAL(term);
DECL_NON_TERMINAL(monomial);
DECL_NON_TERMINAL(var);
DECL_NON_TERMINAL(sign);
DECL_NON_TERMINAL(coef);
DECL_NON_TERMINAL(exponent);
DECL_NON_TERMINAL(eol);
	

/** ************************************************* */
/** Monomial identification and checking declarations */
/** See: monomial_mapping.txt			      */
/** ************************************************* */


/** INTERNAL MACROS */

/** Error string */
extern char error[512];

extern unsigned char powers[4];   /** Powers of X,Y,Z,W 	  */
extern unsigned char checksum;	  /** Sum of powers of X,Y,Z,W 	  */
extern signed char   sign;	  /** Current monomial sign	  */
extern char	     *coef;   	  /** Current monomial coef	  */
extern unsigned char current_var; /** Current variable {0,1,2,3}  */

/** Handy constants to identify each variable (X,Y,Z,W)
 *  inside the "powers" array */
#define VAR_X	3
#define VAR_Y	2
#define VAR_Z	1
#define VAR_W	0

/** Initializes "powers" and "checksum" to zero */
/** Used when we want to parse a new monomial.	*/
#define reset_powers		\
	checksum 	= 0;	\
	powers[0] 	= 0;	\
	powers[1]	= 0;	\
	powers[2]	= 0;	\
	powers[3]	= 0;

/** We use two embedded macros here, because if we call
 *  raise(VAR_X), VAR_X will not be substituted in the
 *  first round of preprocessing, and we would have
 *  a "VAR_X" appearing in the final source code, instead
 *  of its value, 3 */
#define _raise(varname)	++powers[varname]; current_var = varname;
#define raise(varname)	_raise(varname);

/** Raising the current variable power */
#define raise_X		raise(VAR_X)
#define raise_Y		raise(VAR_Y)
#define raise_Z		raise(VAR_Z)
#define raise_W		raise(VAR_W)

/** Calculating the current monomial checksum. This is
 *  important in order to verify that we effectively
 *  have an homogeneous monomial of degree 2. */
#define calc_checksum			   \
		checksum = powers[VAR_X] + \
			   powers[VAR_Y] + \
			   powers[VAR_Z] + \
			   powers[VAR_W]

/** Given a non-homogeneous monomial, homogenize it using
 *  "w" as the homogeneization variable */
#define homogenize	powers[VAR_W] += ( 2 - checksum )

/** Checking the current monomial and homogenize it if
 *  necessary.
 *  If checksum is not equal to 2, then we have two cases:
 *   > 2	: Invalid monomial for we're expecting degree 2
 *   		  monomials
 *   < 2	: Not homogeneous monomial, so try to
 *   		  homogenize it (see the homogenize macro)
 *		  Note that if the monomial is equal to "w"
 *		  then it's not valid as well.
 */
#define check_monomial					\
	calc_checksum;					\
	if ( checksum > 2 ) {				\
		fprintf (stderr,"Invalid monomial: ");	\
		decode_monomial(stderr);		\
		fprintf (stderr, "\n");			\
		exit(-1);				\
	} else if ( checksum < 2 ) {			\
		if ( powers[VAR_W] ) {			\
			fprintf (stderr,		\
			       "Invalid monomial: ");	\
			decode_monomial(stderr);	\
			fprintf (stderr,		\
				"\n");			\
			exit(-1);			\
		} else homogenize;			\
	}


/** Hashing: from the current checksum, it is possible to
 *  have a direct correspondence (i.e. a O(1) operation)
 *  with the position in the coefficient vector.
 *  See monomial_mapping.txt for more information */

extern signed char hashtable[17];

/** Calculate the hash key */
#define hash_monomial		\
	powers[VAR_W] 	  + 	\
	2 * powers[VAR_Z] +	\
	4 * powers[VAR_Y] +	\
	8 * powers[VAR_X]

/** Use a correspondence table to finally obtain the
 *  indice inside the coefficient vector. */
#define	coef_indice	hashtable[hash_monomial]

/** Decodes the actual monomial to report errors to the
 *  user */
#define decode_monomial(stream)			\
	if ( powers[VAR_X] > 0 ) {		\
		fprintf (stream,"x");		\
		if ( powers[VAR_X] > 1 )	\
			fprintf (stream,"^%d", powers[VAR_X]);	\
	}					\
	if ( powers[VAR_Y] > 0 ) {		\
		fprintf (stream,"y");		\
		if ( powers[VAR_Y] > 1 )	\
			fprintf (stream,"^%d", powers[VAR_Y]);	\
	}					\
	if ( powers[VAR_Z] > 0 ) {		\
		fprintf (stream,"z");		\
		if ( powers[VAR_Z] > 1 )	\
			fprintf (stream,"^%d", powers[VAR_Z]);	\
	}					\
	if ( powers[VAR_W] > 0 ) {		\
		fprintf (stream,"w");		\
		if ( powers[VAR_W] > 1 )	\
			fprintf (stream,"^%d", powers[VAR_W]);	\
	}

/** PUBLIC MACROS USED INSIDE THE PARSER CODE */

/** Coefficient vector (final result):
 *  10 elements with maximum size COEF_MAX_SIZE
 */
extern char **coefficients;

/** Set each coefficient to "0" */
void init_coefficients (void);

/** Initializes the output result and prepare for the first monomial */
#define init_all		\
	init_qi_parser();	\
	init_monomial();	\
	init_coefficients();

/** Initialization function used when parsing a new monomial */
void init_monomial (void);
	/** Attempts to store the current monomial in the final
 *  coefficient vector, and prepare for the next monomial. */
#define	next_coef					\
	check_monomial;					\
	if ( sign < 0 ) {				\
		coefficients[coef_indice][0] = '-';	\
		memcpy(&coefficients[coef_indice][1], \
				&coef[0],		\
			strlen(coef) * sizeof(char));	\
		coefficients[coef_indice]		\
			    [1+strlen(coef)] = '\0';	\
	} else {					\
		memcpy(&coefficients[coef_indice][0], \
				&coef[0],		\
			strlen(coef) * sizeof(char));	\
		coefficients[coef_indice]		\
			    [strlen(coef)] = '\0';	\
	}						\
	init_monomial();	
	

/** Public parsing function.
 *  Input:  a character string containing the quadric description
 *  Output: an mqi_vect_t with mpz_t coefficients
 *
 *  The quadric description can be provided in two different formats:
 *
 *  1) Affine/Homogeneous format, like: "x^2+x*w-w^2"
 *  2) Vectorial format, like:		"[1 0 0 1 0 0 0 0 0 -1]"
 *
 */
void qi_parse (mqi_vect_ptr rop, const char *quadric_desc);

/** Internal functions used by "qi_parser_parse" */
void _qi_parse_equation (mqi_vect_ptr rop, const char *equation_desc);
void _qi_parse_vector   (mqi_vect_ptr rop, const char *vectorial_desc);

/** Remove spaces and tabulations in the input string */
char *trim (const char *str, size_t n);

#endif

