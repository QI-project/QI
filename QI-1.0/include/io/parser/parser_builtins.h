#ifndef _parser_builtins_h_
#define _parser_builtins_h_

#include <ctype.h>

#define parser_parse_alnum	parser_parse_condition(isalnum(scanner_get_current_symbol()));
#define parser_parse_alpha	parser_parse_condition(isalpha(scanner_get_current_symbol()));
#define parser_parse_digit	parser_parse_condition(isdigit(scanner_get_current_symbol()));
#define parser_parse_ascii	parser_parse_condition(isascii(scanner_get_current_symbol()));
#define parser_parse_blank	parser_parse_condition(isblank(scanner_get_current_symbol()));
#define parser_parse_space	parser_parse_condition(isspace(scanner_get_current_symbol()));
#define parser_parse_lower	parser_parse_condition(islower(scanner_get_current_symbol()));
#define parser_parse_upper	parser_parse_condition(isupper(scanner_get_current_symbol()));
#define parser_parse_punct	parser_parse_condition(ispunct(scanner_get_current_symbol()));
#define parser_parse_xdigit	parser_parse_condition(isxdigit(scanner_get_current_symbol()));

#define parser_parse_number			\
	do {					\
		parser_parse_digit;		\
		ON_ERROR (break;);		\
		\
		for ( ; ; ) {			\
			parser_parse_digit;	\
			IGNORE_ERROR (break;);	\
		}				\
	}while(0)

#define parser_parse_string			\
	do {					\
		parser_parse_ascii;		\
		ON_ERROR (break;);		\
		\
		for ( ; ; ) {			\
			TOKEN_NEW;		\
			parser_parse_ascii;	\
			if ( token[0] == '\0' ) \
				errno = EIO;	\
		IGNORE_ERROR (break;);		\
		}				\
	}while(0)


/** ******* */
/** Aliases */
/** ******* */
#define PARSE_BUILTIN(name)	parser_parse_##name


#endif

