#ifndef _parser_token_h_
#define _parser_token_h_

/** **************** **/
/** TOKEN management **/
/** **************** **/

/** Number of characters held in a memory block */
#define TOKEN_BLOCK_SIZE	50
								  
extern char * token;

/** Current indice in the token string */
extern unsigned short token_indice;

/** Number of memory blocks occupied by "token" */
extern unsigned short token_n_blocks;

/** Are we interested in storing the current 
 *  read characters inside "token" ? */
extern unsigned char tokenize;

/** Allocates enough memory so that token can hold
 *  "size" more characters. */
#define parser_token_memchunk(size)							\
	do {										\
		while ( token_indice + size >						\
			token_n_blocks * TOKEN_BLOCK_SIZE )				\
			token = (char *)realloc (token,					\
				(token_n_blocks++)*TOKEN_BLOCK_SIZE*sizeof(char));	\
	}while(0)

/** Copy "data" into "token", reallocating new memory
 *  blocks if needed. */
/*#define parser_token_strncpy(data, size)		\
	do {						\
		parser_token_memchunk( size ); 		\
		strncpy ( &token[token_indice], 	\
      			  data,				\
			  size );			\
		token_indice += size;			\
	} while(0)
*/

/** Copy one single character into "token", reallocating
 *  new memory blocks if needed. */
#define parser_token_set(character)			\
	do {						\
		parser_token_memchunk( 2 );		\
		token[token_indice] = character;	\
		token[token_indice+1] = '\0';		\
	}while(0)
		
#define parser_token_new				\
	do {						\
		token_indice 	= 0;			\
		token_n_blocks 	= 0;			\
		if ( token != NULL ) {			\
			free ( token );			\
			token = NULL;			\
		}					\
		tokenize = 1;				\
	}while(0)

/** Alias to the previous macro */
#define TOKEN_NEW parser_token_new

#endif

