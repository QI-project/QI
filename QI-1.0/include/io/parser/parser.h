#ifndef _parser_h_
#define _parser_h_ 


/** Needed for errno */
#include <errno.h>

/** Needed for sprintf */
#include <stdio.h>

/** Needed for exit */
#include <stdlib.h>

/** Needed for strlen */
#include <string.h>

/** Needed for open, fstat */
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

/** Needed for mmap */
#include <sys/mman.h>

#include "parser_io.h"
#include "parser_token.h"
#include "parser_scanner.h"
#include "parser_trigger.h"

void parser_parse (void);
void parser_reset (void);

/** *********************** **/
/** Parse terminal function **/
/** *********************** **/

#define parser_parse_condition(condition)	 \
	   do {\
		  errno = ENOMSG; \
		  if ( ! (condition) ) { \
	            errno = EIO; \
		    sprintf (error_str, "Parse error: '%c' at position %d.\n", \
				    scanner_get_current_symbol(), CURRENT_SYMBOL_INDEX + 1);    \
		    break; \
                  } \
	    if ( tokenize ) parser_token_set ( scanner_get_current_symbol() ); \
            NEXT_SYMBOL_INDEX; \
          }while(0)

#define parser_parse_terminal(expected_character) parser_parse_condition( scanner_get_current_symbol() == expected_character )
void	parser_parse_terminal_string ( char * stringptr );

#include "parser_builtins.h"
								  
/** *************************************************** **/
/** Handy macros for prototyping non-terminal functions **/
/** *************************************************** **/
								  
#define PARSE_DECL(non_terminal_identifier)  \
           void parser_parse_##non_terminal_identifier (void)

#define PARSE(non_terminal_identifier) \
	   parser_parse_##non_terminal_identifier ()

#define PARSE_TERMINAL 			parser_parse_terminal
#define PARSE_TERMINAL_STRING		parser_parse_terminal_string

/** ******************** **/
/** Parse error handlers **/
/** ******************** **/					  

/* What to do when the parsing is ok */
#define ON_SUCCESS(actions) \
           if ( errno == ENOMSG )  { \
                 actions \
           }


/* What to do when the parsing fails */
#define ON_ERROR(actions) \
              if ( errno == EIO )  { \
                 actions \
              }


/* After an error, ignore the EIO restoring *errno to
 *
 *     ENOMSG so that eventual upper level functions
 *
 *         are not aware of an error, and do some action */
#define IGNORE_ERROR(actions) \
              ON_ERROR(errno=ENOMSG; actions)


								  
#endif

