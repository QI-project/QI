#ifndef _parser_trigger_h_
#define _parser_trigger_h_

/** *************************************** */
/** Triggering non terminal symbols parsing */
/** *************************************** */

/** Declare the trigger function */
#define TRIGGER_DECL(non_terminal_identifier)	void parser_trigger_##non_terminal_identifier (void)

/** Call it */
#define TRIGGER(non_terminal_identifier)			\
	do {							\
		tokenize = 0;					\
		parser_trigger_##non_terminal_identifier ();	\
	}while(0)


/** Automatic declaration of prototypes and triggers
 *  for the specified non-terminal element. */

#define DECL_NON_TERMINAL(name)					\
	PARSE_DECL(name);					\
	TRIGGER_DECL(name);

#endif

