#ifndef _qi_backends_h_
#define _qi_backends_h_

/* The structure of a backend is defined in the
 * following header. */
#include "qi_backend_t.h"

/** Definition of the supported backends */
enum _qi_backend_type_t_
 {
	QI_BACKEND_PLAIN,
	QI_BACKEND_HTML,
	QI_BACKEND_LATEX
 };

/** Alias */
typedef enum 	_qi_backend_type_t_		qi_backend_type_t;

/** [HELP] Declaration of the supported backends.
 *  You must provide a C implementation file for
 *  each of the following backends with the structure fields
 *  statically initialized and pointing to valid functions.
 *  Do not write a new backend from scratch, use the
 *  "qi_backend_skeleton.txt" as a starter and rename
 *  it "qi_backend_<name>.c" with "<name>" beeing replaced
 *  by the name of the new backend, example: "latex".
 *  Adjust qi_backends.c to reference a new backend.
 */
extern qi_backend_t	* qi_backends[];

#endif

