#ifndef _qi_utils_h_
#define _qi_utils_h_

#define tracefunc(trace_infostr)		\
	do{					\
	printf ("__trace@(%s) : \"%s\"\n",	\
		__func__, trace_infostr);	\
	}while(0)

#endif

