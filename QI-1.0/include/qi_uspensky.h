#ifndef _qi_uspensky_h_
#define _qi_uspensky_h_

/** @file	qi_uspensky.h
 *  @author	
 * Copyright 1999, 2002, 2004 
 *    Guillaume Hanrot (Guillaume.Hanrot@inria.fr)
 *    Fabrice Rouillier (Fabrice.Rouillier@inria.fr)
 *    Paul Zimmermann (Paul.Zimmermann@inria.fr)
 *    Sylvain Petitjean (Sylvain.Petitjean@inria.fr)
 *
 * This software is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * If interested by a copy of the GNU Lesser General Public License, write to
 * the Free Software Foundation, Inc., 51 Franklin Place, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * @brief	Polynomial system solving
 * @note	Uspensky's algorithm : isolation of real roots of a polynomial
 * based on Descartes' rule, following the procedure described by
 * Rouillier & Zimmermann.
 *
 *  Requires GMP.
 *
*/

#include "qi_uspensky_t.h"
#include <mqi/mqi_poly.h>

#define vali(x) mpz_scan1((x), 0)
#define ilog2(a) mpz_sizeinbase(a,2)
#define TOT_POS -1 

/* Probably out of memory long since on nontrivial examples, but anyway. */
#define DEPTH_MAX 1024

extern unsigned long usign;
extern int b1, b2;

/** @name Public routines
 */
/*@{*/
void qi_uspensky_pick_point_outside_roots(mqi_vect_t __INIT rop, interval *roots, int __IN *infinity_flag,
                                          mqi_hpoly_t __IN det_p,
                                          mqi_hpoly_t __IN deriv,
                                          unsigned int __IN nbroot, unsigned int __IN index);

void qi_uspensky (interval **roots, mqi_hpoly_t detp, unsigned long __IN deg, unsigned int *nbroot);
/*@}*/

/** @name Local routines
 */
/*@{*/
int _qi_uspensky_remove_content(mpz_t *P, unsigned long __IN deg);

/* Johnson's bound : 2*max(abs(-ai/an)^(1/(n-i)),
   the maximum being taken over the i with sgn(a_i) != sgn(an) */
long _qi_uspensky_bound_roots(mpz_t *t, unsigned long __IN deg);

/* Computes P(X*2^k) */
int _qi_uspensky_homoth(mpz_t *P, long __IN k, unsigned long __IN deg);

/* Replaces P by the polynomial P(X+1) */
void _qi_uspensky_x2xp1(mpz_t *P, unsigned long __IN deg);

/* Number of sign changes in the coefficients of P(1/(X+1)) (Descartes' rule) */
unsigned long _qi_uspensky_descartes(mpz_t *P, unsigned long __IN deg, long __IN sigh, long *flag);

/* Returns the sign of P(1/2) */
long _qi_uspensky_evalhalf(mpz_t *P, unsigned long __IN deg);

void _qi_uspensky_add_root(interval *roots, mpz_ptr __IN c, int __IN k, unsigned int __IN flag,
              		   unsigned int __IN nbroot);

/** Check interval [c/2^k, (c+1)/2^k]. The value of k is returned, this
    is necessary to know from where we come [i.e., what exactly is the 
    current polynomial P] when several recursive calls return in a row. 
    In practice, this is used to update the polynomial at HERE */
long _qi_uspensky_rec(mpz_t *P, mpz_ptr __IN c, unsigned long __IN k, unsigned long *Deg,
                  	 interval *roots, unsigned int *nbroot);

void _qi_uspensky_gmp(interval *roots, mpz_t *Q, unsigned long __IN deg, unsigned int *nbroot);

/** Output things in the right order */
void _qi_uspensky_clean_output(interval *roots, unsigned long __IN deg, unsigned int __IN nbroot);

/*@}*/

/** Deprecated as we use libmqi now */
/* Convert between LiDIA and gmp format, then launch Uspensky*/
/*void qi_uspensky_Uspensky(interval **roots, const hom_polynomial <bigint> &det_p, const unsigned long deg,
unsigned int &nbroot);*/

void qi_uspensky_print_root (interval z);
void qi_uspensky_print_roots (interval *roots, int nbroot);

#endif

