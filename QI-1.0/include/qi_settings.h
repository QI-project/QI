#ifndef _qi_settings_h_
#define _qi_settings_h_

#include "qi_backends.h"

/** @file	qi_settings.h
 *  @author	Julien CLEMENT
 *  @brief	Structure grouping parameters affecting
 *  		both the kernel and the output of QI.
 */

/** @struct qi_settings
 *  @brief  Put every option affecting the kernel behavior and/or
 *  	    the input/output layer in that structure.
 *  	    
 *  @note   See the source file qi_settings.c to change the
 *  	    default settings.
 *  @see    qi_settings.c
 */
struct _qi_settings_t_ {


	/** *************** */
	/** KERNEL settings */
	/** *************** */

	/** Upper bound for the search of prime factors */
	long maxfactor;

	/** This is a replacement of the old "opt_level"
	 *  option. If set to one, this flag causes the
	 *  kernel to simplify the output parametrizations
	 *  as much as it can. Notably, it tries to find a rational point
	 *  on a conic.
	 *  Default: no (0)
	 */
	int optimize; 

	/** Compute the cut parameters
	 *  Default: yes (1)
	 */
	/* For the moment the code simply ignores
	 * this option and always compute
	 * the cut parameters.
	 */
	/*int compute_cut_parameters;*/

	/** ********************** */
	/** OUTPUT format settings */
	/** ********************** */

	/** Toggle for switching between an affine and a projective
	 *  representation of the equations in the output. 
	 *  Default: no (0)
	 */
	int output_projective;

	/** Use a LaTeX style for the output expressions.
	 *  Notably, "*" symbols are omitted, square roots
	 *  use the appropriate LaTeX symbol, and more.
	 */
	/* obsolete */
	/*int output_LaTeX;*/

	/** Backend to use for the printing of the results of the
	 *  intersection.
	 *  Currently supported backends are:
	 *
	 *  - QI_BACKEND_PLAIN	: suitable for console exploitation
	 *  - QI_BACKEND_HTML	: suitable for web exploitation
	 *  - QI_BACKEND_LATEX	: suitable for edition / inclusion in
	 *  		  	  a scientific document
	 *
	 * @see 	qi_backend.h
	 * @note	qi_backend_type_t is an enumeration field
	 */
	qi_backend_type_t	backend;


	/** Omit components of the intersection not belonging in
	 *  the real affine space R^3
	 *  Default: yes (1)
	 */
	int output_omit_imaginary_components;

	/** Compute and display the euclidean type of the input
	 *  quadrics.
	 *  Default: yes (1)
	 */
	int output_show_input_euclidean_type;
	
	/** Display the cut parameters.
	 *  Note that it cannot be activated if "compute_cut_parameters"
	 *  wasn't.
	 *  Default: yes (1)
	 */
	int output_cut_parameters;

	/** Verbosity level of the output information.
	 *	
	 *  Default: VERBOSITY_EXHAUSTIVE
	 *  Possible values:				*/

#define VERBOSITY_PARAM_ONLY		( 0 )	/* Parametrizations only 			 */
#define VERBOSITY_BRUTE			( 1 )	/* Parametrizations and type of the intersection */
#define VERBOSITY_LABELS		( 2 ) 	/* Adds some decription labels			 */
#define VERBOSITY_EXHAUSTIVE		( 3 )	/* Full information				 */

	int output_verbosity;

};

typedef struct _qi_settings_t_ qi_settings_t;

extern qi_settings_t qi_settings;

/* Handy macro used in the printing functions
 * of qi_inter_cut and qi_inter_component.
 */
#define USE_LATEX	((qi_settings.backend == QI_BACKEND_LATEX) ? (1) : (0))

#endif

