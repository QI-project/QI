#ifndef _qi_number_h_
#define _qi_number_h_

/** @file	qi_number.h
 *  @author	Julien CLEMENT, Sylvain PETITJEAN, Sylvain LAZARD
 *  @brief	Number theory functions for Quadric Intersection.
 *  		It includes optimization functions to simplify the coefficients
 *  		of mathematical expressions like polynomial coefficients,
 *		vectors and matrices coefficients,
 *		parametrized curves equations, and more.
 *  @note	"__INIT" means that the corresponding object is initialized
 *  		inside the function. Only for documentation purpose.
 *  @note	"__IN" is also for documentation purpose.
 *  		It facilitates the idenfication of the inputs when it is not
 *  		straightforward.
 *  @note	If some arguments where specified to be "__IN" or "__INIT",
 *  		then the other arguments with no specifier are necesarily
 *  		<b>output</b> arguments.
 *  		
 */

#include <mqi/mqi_curve_param.h>
#include <mqi/mqi_hhpoly.h>
#include <mqi/mqi_vect.h>
#include <mqi/mqi_prime_factorization.h>
#include <cmm/mempool/mempool.h>

#include "qi_settings.h"

/** @name Root extraction
 */
/*@{*/

/** Compute the square factors of an integer.
 */
void qi_coef_extract_square_factors (mqi_coef_t __INIT square,
				     mqi_coef_t __INIT cofactor,
				     __IN mqi_coef_t number);
/*@}*/

/** @name Homogeneous polynomials optimization
 */
/*@{*/

/** Optimize coefficients of polynomial */
void qi_hpoly_optimize 			(mqi_hpoly_t op);

/** Optimize coefficients of a pair of homogeneous polynomials by taking
 *  the gcd of their coeff */
void qi_hpoly_optimize_gcd		(mqi_hpoly_t op1, mqi_hpoly_t op2);

/** Computes the pseudo-discriminant of a quadratic homogeneous polynomial */
void qi_hpoly_pseudo_discriminant	(mqi_coef_t rop, mqi_hpoly_t poly);

/** Compute the primitive part of a hhpoly "op", i.e. "op" divided by its content */
void qi_hhpoly_pp 			(mqi_hhpoly_t __INIT rop,
					 mqi_hhpoly_t op, mqi_coef_t content);

/** Compute the primitive part of a hom_hom_polynomial, but using the polynomial content */
void qi_hhpoly_pp_hpoly			(mqi_hhpoly_t __INIT rop,
					 mqi_hhpoly_t op, mqi_hpoly_t content);

/** Optimize coefficients of a pair of hom_hom_polynomials by taking the gcd of the
    contents of the coefficients (we only eliminate constants, not polynomial factors) */
void qi_hhpoly_optimize_gcd		(mqi_hhpoly_t op1, mqi_hhpoly_t op2);


/*@}*/

/** @name Curve params optimization
*/

/*@{*/

/** Optimize a curve_param */
void qi_curve_param_optimize 		(mqi_curve_param_t curve_param);

/** Optimize a pair of curve_params by taking the gcd of their content */
void qi_curve_param_optimize_gcd 	(mqi_curve_param_t curve_param1,
					 mqi_curve_param_t curve_param2);

/** Optimize the parameterization of a conic 
 *  c1 + sqrt(xi). c2 + eps. sqrt(D). (c3 + sqrt(xi). c4)
 *  where D and xi are already optimized. 
 *  Simplification by the common factor of the parameterization
 */
void qi_curve_param_optimize_conic (mqi_curve_param_t curve_param1,
				    mqi_curve_param_t curve_param2,
                                    mqi_curve_param_t curve_param3,
				    mqi_curve_param_t curve_param4);

/** Optimize the parameterization of a smooth quartic 
 *  c1 + sqrt(xi). c2 + eps. sqrt(Delta). (c3 + sqrt(xi). c4)
 *  Delta = Delta1 + sqrt(xi). Delta2
 *  If xi = 1 then c2, c4 and Delta2 are 0
 *  Simplification by the common factor of the parameterization
 * */
void qi_curve_param_optimize_smooth_quartic (mqi_curve_param_t curve_param1,
					     mqi_curve_param_t curve_param2,
					     mqi_curve_param_t curve_param3,
					     mqi_curve_param_t curve_param4,
					     mqi_hpoly_t delta1, mqi_hpoly_t delta2);

/*@}*/

/** @name Vectors optimization
*/

/*@{*/

/** Optimize a vector by dividing by its content */
void qi_vect_optimize (mqi_vect_t vector);

/** Optimize a pair of vectors by dividing by the gcd of the contents */
void qi_vect_optimize_gcd (mqi_vect_t vector1, mqi_vect_t vector2);

/** Optimize a triplet of vectors by dividing by the gcd of the contents */
void qi_vect_optimize_gcd_triplet (mqi_vect_t vector1,
				   mqi_vect_t vector2,
				   mqi_vect_t vector3);

/** Optimize a quadruplet of vectors by dividing by the gcd of the contents */
void qi_vect_optimize_gcd_quadruplet (mqi_vect_t vector1,
				      mqi_vect_t vector2,
                                      mqi_vect_t vector3,
				      mqi_vect_t vector4);

/** Optimize first half, then last half */
void qi_vect_optimize_by_half (mqi_vect_t vector);

/** Load balancing of two vectors */
void qi_vect_load_balancing (mqi_vect_t op1, mqi_vect_t op2);

/*@}*/

/** @name Matrices
 */
/*@{*/

/** Optimizes by dividing each column by its content */
void qi_mat_optimize (mqi_mat_t matrix);

/** optimizes by dividing each matrix column by the gcd of the contents
 *  of the corresponding matrix columns. */
void qi_mat_optimize_2 (mqi_mat_t matrix1, mqi_mat_t matrix2);

/** Optimize by dividing columns 1, 2, 3 by a, b, c such that a b = c^2
 *  Divide remaining columns by their content.
 *  Update additional line values in "line" */
void qi_mat_optimize_3_update (mqi_mat_t matrix, mqi_vect_t line);

/** Optimize by dividing columns 1, 2, 3 by a, b, c such that a b = c^2
 *  Divide remaining columns by their content */
void qi_mat_optimize_3 (mqi_mat_t matrix);

/** Load balancing of columns of a matrix such that a*b = c*d
 *  Configuration:
 *
 *  	0: Balance only the two first columns
 *  	1: Balance the four columns
 *
 *  	(matrices are supposed to be 4x4)
 */
void qi_mat_load_balancing (mqi_mat_t matrix, const int configuration);

/** Optimize by dividing columns 1, 2, 3 by a, b, c such that a b = c^2
 *  Divide remaining columns by their content.
 *  Here the two matrices should be divided simultaneously */
void qi_mat_optimize_4 (mqi_mat_t matrix1, mqi_mat_t matrix2);

/* Function used in __qi_inter_two_mult_four_skew_lines_non_rational.
 * Calculates:
 *
 * 	alpha * m' * p * q
 *
 */
void qi_mat_transformation (mqi_mat_t __INIT rop, mqi_coef_t alpha, mqi_mat_t __IN m, mqi_mat_t __IN p, mqi_mat_t __IN q);
void qi_mat_transformation_int (mqi_mat_t __INIT rop, int alpha, mqi_mat_t __IN m, mqi_mat_t __IN p, mqi_mat_t __IN q);


/*@}*/

#endif

