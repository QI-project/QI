#ifndef _qi_uspensky_t_h_
#define _qi_uspensky_t_h_

/** @file	qi_uspensky_t.h
 *  @see	qi_uspensky.h
 *  @brief	Structure used to store intervals by Uspensky
 */

#ifndef __USE_GMP
#error "The polynomial system solving package (uspensky) needs GMP support."
#endif

/** GMP */
#include "gmp.h"

/** @struct interval
 *  @brief  Structure used to store intervals by Uspensky.
 */
typedef struct
{
        mpz_t c;
        long k;
        unsigned int isexact;
} interval;

#endif

