This the QI (Quadrics Intersection) project. It has been rewritten to be
independent from the LiDIA library. 
Currently, for compiling QI you need : 

    - Cmake 2.8 or later
    - gcc-4.0.1 or later
    - boost 1.42 or later
    - gmp 5.0.1 or later

To compile QI : 
    - execute ./configure.sh
    - go into build/$os-Release ($os may be Linux or Darwin)
    - run 'make'
    - optional but recommended: run the tests ('make test')

To install it:
    - copy the executable qi from build/$os-Release/src/qi 
      where you want and use-it


It has been successfully tested on Linux Gentoo 64 and 
MacOS X.6 with MacPort. 

For any question, please contact : 
sylvain.petitjean@inria.fr
sylvain.lazard@inria.fr
