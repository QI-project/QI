// The things for explicit instantiation of LiDIA templates

/** LiDIA */
#include <lidia/bigint.h>
#include <lidia/bigint_matrix.h>

/** QI */
#include "QIHompoly.h"

using namespace LiDIA;
using namespace QI;

// Vectors of hom_polynomials
#define TYPE hom_polynomial <bigint>
#define BASE_VECTOR
#define MATH_VECTOR

#include <lidia/instantiate/vector.cc>

// Matrices of hom_polynomials
#define BASE_MATRIX
#define NORMAL

#include <lidia/instantiate/matrix.cc>

// Vectors of hom_hom_polynomials
#undef TYPE
#undef MATH_VECTOR
#define TYPE hom_hom_polynomial <bigint>

#include <lidia/instantiate/vector.cc>
