dnl	
dnl	check for GNU MP : From the LiDIA acinclude.m4 file
dnl
AC_DEFUN([CHECK_LIB_GMP],
[
	AC_MSG_CHECKING(for GMP (version >= 4.2))
	AC_CACHE_VAL(found_gmp,
	[
		if AC_TRY_COMMAND(${CC} test_gmp.c -o test_gmp -I"${GMP_PATH}/include" -L"${GMP_PATH}/lib" -lgmp) > /dev/null 2>&1; then
			found_gmp="yes"
		else
			found_gmp="no"
		fi
		test -f test_gmp && rm test_gmp
	])
	AC_MSG_RESULT($found_gmp)
])

dnl	
dnl	check for LiDIA
dnl
AC_DEFUN([CHECK_LIB_LIDIA],
[
	AC_MSG_CHECKING(for LiDIA (version >= 2.2))
	AC_CACHE_VAL(found_lidia,
	[
		if AC_TRY_COMMAND(${CXX} test_lidia.cc -o test_lidia -I"${LiDIA_PATH}/include" -L"${LiDIA_PATH}/lib" -I"${GMP_PATH}/include" -L"${GMP_PATH}/lib" -lLiDIA -lgmp) > /dev/null 2>&1; then
			found_lidia="yes"
		else
			found_lidia="no"
		fi
		test -f test_lidia && rm test_lidia
	])
	AC_MSG_RESULT($found_lidia)
])

dnl
dnl	check if libqi has been compiled with
dnl	options compliant with testing.
dnl
AC_DEFUN([CHECK_TESTING_COMPLIANT],
[
	AC_MSG_CHECKING(if compilation options are testing compliant)
	AC_CACHE_VAL(testing_compliant,
	[
		test $debug_mode = "no" -a $color_option = "no" && testing_compliant="yes" || testing_compliant="no"
	])
	AC_MSG_RESULT($testing_compliant)
])

