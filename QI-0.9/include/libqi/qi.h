#ifndef _qi_h_
#define _qi_h_

/** Top level header to be included in the case
    you intend to use both the calculus kernel
    and the IO kit for parsing user input and
    pretty display results. */

#include "qi_kernel.h"
#include "qi_io.h"

#endif
