// Intersection when the determinantal equation vanishes identically

#ifndef _qi_vanish_det_h_
#define _qi_vanish_det_h_

/** LiDIA */
#include <lidia/rational_factorization.h>

/** QI */
#include "QIHompoly.h"
#include "QIQsicStruct.h"

using namespace std;
using namespace LiDIA;

// Enter namespace QI
namespace QI {

// The main intersection procedure when the determinantal equation vanishes
quad_inter <bigint> inter_vanish_det(const bigint_matrix &q1, const bigint_matrix &q2, 
				     const hom_polynomial <bigint> &det_p, 
				     const int opt_level, 
				     ostream &s);

} // end of namespace QI

#endif
