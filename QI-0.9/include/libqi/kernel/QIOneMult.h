// Intersection when the determinantal equation has one multiple root

#ifndef _qi_one_mult_h_
#define _qi_one_mult_h_

/** QI */
#include "QIHompoly.h"
#include "QIQsicStruct.h"

using namespace std;
using namespace LiDIA;

// Enter namespace QI
namespace QI {

// The main procedure in the one multiple root case
quad_inter <bigint> inter_one_mult(const bigint_matrix &q1, const bigint_matrix &q2, 
				   const hom_polynomial <bigint> &det_p, 
				   const hom_polynomial <bigint> &det_p_orig,
				   const hom_polynomial <bigint> &gcd_p, 
				   const int opt_level, std::ostream &s);

} // end of namespace QI

#endif
